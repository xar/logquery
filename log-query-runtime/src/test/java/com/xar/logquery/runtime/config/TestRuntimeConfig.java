/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.config;

import com.xar.logquery.runtime.config.entities.DateFormat;
import org.junit.Assert;
import org.junit.Test;

import java.util.UUID;

/**
 * Created by akoekemo on 3/11/15.
 */
public class TestRuntimeConfig {

    @Test
    public void shouldReplaceExistingDateFormat() {

        String pattern = UUID.randomUUID().toString();
        String format1 = UUID.randomUUID().toString();
        String format2 = UUID.randomUUID().toString();
        try {
            RuntimeConfiguration.registerDateFormat(pattern, format1);
            boolean found = false;
            for (DateFormat fmt : RuntimeConfiguration.getDateFormats()){
                if (fmt.getPattern().equals(pattern)){
                    Assert.assertEquals(format1, fmt.getFormatString());
                    found = true;
                }
            }
            Assert.assertTrue("Format not found", found);

            RuntimeConfiguration.registerDateFormat(pattern, format2);
            found = false;
            for (DateFormat fmt : RuntimeConfiguration.getDateFormats()){
                if (fmt.getPattern().equals(pattern)){
                    Assert.assertEquals(format2, fmt.getFormatString());
                    found = true;
                }
            }
            Assert.assertTrue("Format not found", found);

        }
        finally {
            RuntimeConfiguration.unregisterDateFormat(pattern);
        }
    }

    @Test
    public void shouldSetNullText() {

        String nullText = RuntimeConfiguration.getNullText();
        try {
            String text = UUID.randomUUID().toString();
            RuntimeConfiguration.setNullText(text);
            Assert.assertEquals(text, RuntimeConfiguration.getNullText());
        }
        finally {
            RuntimeConfiguration.setNullText(nullText);
        }
    }


}
