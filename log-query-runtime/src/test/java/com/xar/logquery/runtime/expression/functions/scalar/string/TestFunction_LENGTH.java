/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.functions.scalar.string;

import com.xar.logquery.runtime.HelperFunctions;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.constant.StringConstant;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.expression.functions.exceptions.InvalidParametersException;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Anton Koekemoer on 2014/11/23.
 */
public class TestFunction_LENGTH {

    @Test
    public void shouldReturnTheLenghtOfAString() throws IllegalAccessException, InvalidExpressionException, InstantiationException {
        Assert.assertEquals(10, HelperFunctions.executeFunction(LengthFunction.class, DataType.INTEGER, "1234567890"));
        Assert.assertEquals(null, HelperFunctions.executeFunction(LengthFunction.class, DataType.INTEGER, new StringConstant(null)));
    }

}
