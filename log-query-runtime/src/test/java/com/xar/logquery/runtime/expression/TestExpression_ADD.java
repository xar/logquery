/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression;

import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.ValidationContext;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.arithmitic.AddExpression;
import com.xar.logquery.runtime.expression.constant.ConstantExpression;
import com.xar.logquery.runtime.expression.constant.RealConstant;
import com.xar.logquery.runtime.expression.constant.IntegerConstant;
import com.xar.logquery.runtime.expression.constant.StringConstant;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.expression.exceptions.InvalidOperandsException;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import org.joda.time.LocalDate;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

/**
 * Created by koekemoera on 2014/11/13.
 */
public class TestExpression_ADD {

    @Test
    public void shouldAddValues() throws InvalidExpressionException {

        testExpression(new IntegerConstant(10), new IntegerConstant(2), BigInteger.class, new BigInteger("12"));
        testExpression(new IntegerConstant(10), new RealConstant(2), BigDecimal.class, new BigDecimal("12.0"));


        testExpression(new RealConstant(10), new RealConstant(2), BigDecimal.class, new BigDecimal("12.0"));
        testExpression(new RealConstant(10), new IntegerConstant(2), BigDecimal.class, new BigDecimal("12.0"));

        testExpression(new AddExpression(new StringConstant("10"), new StringConstant("2")), String.class, "102");


        Date start =new Date();

        testExpression(new ConstantExpression<>(DataType.DATE, start), new IntegerConstant(1), Date.class, LocalDate.fromDateFields(start).plusDays(1).toDate());
        testExpression(new ConstantExpression<>(DataType.DATE, start), new RealConstant(1.5f), Date.class, LocalDate.fromDateFields(start).plusDays(1).toDate());


        testExpression(new ConstantExpression<>(DataType.TIMESTAMP, start), new IntegerConstant(1000), Date.class, new Date(start.getTime() + 1000));
        testExpression(new ConstantExpression<>(DataType.TIMESTAMP, start), new RealConstant(1.5d), Date.class, new Date((long) (start.getTime() + (86400000 * 1.5d))));

    }

    @Test
    public void NullPlusAnythingIsNull() throws InvalidExpressionException {

        final AddExpression addExpression = new AddExpression(new RealConstant(10), new ConstantExpression<Long>(DataType.INTEGER, null));
        addExpression.validate(new ValidationContext());
        Assert.assertNull(addExpression.getValue(new ExecutionContext()));
    }

    @Test
    public void addShouldThrowExceptionForInvalidOperands(){

        try {
            testExpression(new AddExpression(new StringConstant("10"), new IntegerConstant(2)), String.class, "102");
            Assert.fail("Expect error");
        }
        catch (InvalidExpressionException e) {
            Assert.assertEquals("Incompatible operands STRING and INTEGER", e.getMessage());
        }

    }

    private void testExpression(final IExpression p_lhs,final IExpression p_rhs, final Class<?> p_resultType, final Object p_expectedValue) throws InvalidExpressionException {

        final AddExpression expression = new AddExpression(p_lhs, p_rhs);
        expression.validate(new ValidationContext());
        testExpression(expression, p_resultType, p_expectedValue);

        final AddExpression expression1 = new AddExpression(p_rhs, p_lhs);
        expression1.validate(new ValidationContext());
        testExpression(expression1, p_resultType, p_expectedValue);
    }

    private void testExpression(final IExpression p_expression, final Class<?> p_resultType, final Object p_expectedValue) throws InvalidExpressionException {
        p_expression.validate(new ValidationContext());
        Object value = p_expression.getValue(new ExecutionContext());

        Assert.assertEquals(p_resultType, value.getClass());
        Assert.assertEquals(p_expectedValue, value);


    }


}
