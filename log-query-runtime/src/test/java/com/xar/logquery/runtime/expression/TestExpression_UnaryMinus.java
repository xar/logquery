/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression;

import com.xar.logquery.parser.exceptions.ParseException;
import com.xar.logquery.runtime.DataSource;
import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.HelperFunctions;
import com.xar.logquery.runtime.ValidationContext;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.exceptions.AmbiguousColumnNameException;
import com.xar.logquery.runtime.exceptions.ColumnNotFoundException;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.expression.arithmitic.UnaryMINUS;
import com.xar.logquery.runtime.expression.constant.ConstantExpression;
import com.xar.logquery.runtime.expression.exceptions.InvalidOperandsException;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Enumeration;

/**
 * Created by akoekemo on 3/13/15.
 */
public class TestExpression_UnaryMinus {

    @Test
    public void failsIfUnaryAppliedToNonNumber() throws IOException, DataSourceException, ColumnNotFoundException, IOException, DataSourceException {

        try {
            DataSource.parse("SELECT -Text from 'test'");
            Assert.fail("Expected exception");
        }
        catch (InvalidOperandsException ex) {
            Assert.assertEquals("[1, 7] Unable to apply '-' to an expression of type STRING", ex.getMessage());
        }
        // TODO: Handle non number +
//        try {
//            DataSource.parse("SELECT +Text from 'test'");
//            Assert.fail("Expected exception");
//        }
//        catch (InvalidOperandsException ex) {
//            Assert.assertEquals("[1, 7] Unable to apply '+' to an expression of type STRING", ex.getMessage());
//        }
    }

    @Test
    public void shouldNegateReal() throws IOException, DataSourceException, AmbiguousColumnNameException, ColumnNotFoundException {
        try {
            IDataSource dataSource = DataSource.parse("SELECT -20.0 AS v from DUAL");
            int rowCount = 0;
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            for (Enumeration<IDataRow> rows = resultSet.getRows(); rows.hasMoreElements(); ) {

                IDataRow dataRow = rows.nextElement();
                Assert.assertEquals(-20.0, ((Number) dataRow.get("v")).doubleValue(), 0.0001);
                rowCount++;
            }

            Assert.assertEquals(1, rowCount);
        }
        finally {
        }

    }

    @Test
    public void minuNullIsNull() throws IOException, DataSourceException, AmbiguousColumnNameException, ColumnNotFoundException {

        UnaryMINUS minus = new UnaryMINUS(new ConstantExpression<BigInteger>(DataType.INTEGER, null));
        minus.validate(new ValidationContext());
        try {
                Assert.assertNull(minus.getValue(new ExecutionContext()));
        }
        finally {
        }

    }



    @Test
    public void shouldApplyUnaryExpressions() throws IOException, DataSourceException, ColumnNotFoundException, AmbiguousColumnNameException {

        File file = new File("test.txt");
        HelperFunctions.createTestFile(file);
        try {
        IDataSource dataSource = DataSource.parse("SELECT groupNumber, +groupRowNumber AS groupRowNumber, intCol, floatCol, dateCol, timestampCol from '" + file.getName() + "' TRANSFORM(TO_INT(STRTOK(Text, ',', 0)) AS groupNumber, STRTOK(Text, ',', 1) AS line, -TO_INT(STRTOK(Text, ',', 2)) AS groupRowNumber, STRTOK(Text, ',', 3) AS intCol, STRTOK(Text, ',', 4) AS floatCol, STRTOK(Text, ',', 5) AS dateCol, STRTOK(Text, ',', 6) AS timestampCol) WHERE groupNumber=0 AND groupRowNumber > -10");
        int rowCount = 0;
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            for (Enumeration<IDataRow> rows = resultSet.getRows(); rows.hasMoreElements(); ) {

                IDataRow dataRow = rows.nextElement();
                System.out.println(dataRow);
            Assert.assertEquals(-rowCount, ((Number) dataRow.get("groupRowNumber")).intValue());
            rowCount++;
        }

        Assert.assertEquals(10, rowCount);
        }
        finally {
            file.delete();
        }

    }


}
