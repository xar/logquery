/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource.join;

import com.xar.logquery.runtime.DataSource;
import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.exceptions.AmbiguousColumnNameException;
import com.xar.logquery.runtime.exceptions.ColumnNotFoundException;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

/**
 * Created by akoekemo on 3/16/15.
 */
public class TestJoin_INNER {

    @Test
    public void shouldJoinValues() throws IOException, DataSourceException, AmbiguousColumnNameException, ColumnNotFoundException {

        try (PrintWriter out = new PrintWriter("left.csv")) {
            out.println("Table,Row");
            out.println("left,1");
            out.println("left,2");
            out.println("left,4");
            out.println("left,5");
            out.println("left,6");
            out.println("left,7");
            out.println("left,8");
            out.println("left,9");
            out.println("left,10");
        }
        try (PrintWriter out = new PrintWriter("right.csv")) {
            out.println("Table,Row");
            out.println("right,1");
            out.println("right,2");
            out.println("right,3");
            out.println("right,5");
            out.println("right,6");
            out.println("right,7");
       }
        try {
            IDataSource dataSource = DataSource.parse("SELECT * FROM 'left.csv' USING CSV AS A INNER JOIN 'right.csv' USING CSV AS B ON A.Row = B.Row");
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            Enumeration<IDataRow> rows = resultSet.getRows();

            int rowCount = 0;
            while (rows.hasMoreElements()) {
                IDataRow dataRow = rows.nextElement();
                Assert.assertEquals(dataRow.get("A.Row"), dataRow.get("B.Row"));
                System.out.println(dataRow);
                rowCount++;
            }
            Assert.assertEquals(5, rowCount);
        }
        finally {
            new File("left.csv").delete();
            new File("right.csv").delete();
        }

    }

    @Test
    public void shouldInnerJoinByDefault() throws IOException, DataSourceException, AmbiguousColumnNameException, ColumnNotFoundException {

        try (PrintWriter out = new PrintWriter("left.csv")) {
            out.println("Table,Row");
            out.println("left,1");
            out.println("left,2");
            out.println("left,3");
            out.println("left,4");
            out.println("left,5");
            out.println("left,6");
            out.println("left,7");
            out.println("left,8");
            out.println("left,9");
            out.println("left,10");
        }
        try (PrintWriter out = new PrintWriter("right.csv")) {
            out.println("Table,Row");
            out.println("right,1");
            out.println("right,2");
            out.println("right,3");
            out.println("right,4");
            out.println("right,5");
        }
        try {
            IDataSource dataSource = DataSource.parse("SELECT * FROM 'left.csv' USING CSV AS A JOIN 'right.csv' USING CSV AS B ON A.Row = B.Row");
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            Enumeration<IDataRow> rows = resultSet.getRows();

            int rowCount = 0;
            while (rows.hasMoreElements()) {
                IDataRow dataRow = rows.nextElement();
                Assert.assertEquals(dataRow.get("A.Row"), dataRow.get("B.Row"));
                System.out.println(dataRow);
                rowCount++;
            }
            Assert.assertEquals(5, rowCount);
        }
        finally {
            new File("left.csv").delete();
            new File("right.csv").delete();
        }

    }

    @Test
    public void returnsNoValuesIfNothingMatches() throws IOException, DataSourceException, AmbiguousColumnNameException, ColumnNotFoundException {
        try (PrintWriter out = new PrintWriter("left.csv")) {
            out.println("Table,Row");
            out.println("left,1");
            out.println("left,2");
            out.println("left,3");
            out.println("left,4");
            out.println("left,5");
            out.println("left,6");
            out.println("left,7");
            out.println("left,8");
            out.println("left,9");
            out.println("left,10");
        }
        try (PrintWriter out = new PrintWriter("right.csv")) {
            out.println("Table,Row");
            out.println("right,11");
            out.println("right,12");
            out.println("right,13");
            out.println("right,14");
            out.println("right,15");
        }
        try {
            IDataSource dataSource = DataSource.parse("SELECT * FROM 'left.csv' USING CSV AS A JOIN 'right.csv' USING CSV AS B ON A.Row = B.Row");
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            Enumeration<IDataRow> rows = resultSet.getRows();

            Assert.assertFalse(rows.hasMoreElements());

        }
        finally {
            new File("left.csv").delete();
            new File("right.csv").delete();
        }

    }



}
