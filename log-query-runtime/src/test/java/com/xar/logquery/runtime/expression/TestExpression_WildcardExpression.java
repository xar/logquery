/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression;

import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.enums.DataType;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by akoekemo on 3/22/15.
 */
public class TestExpression_WildcardExpression {

    @Test
    public void wildcardExpressionHasDataTypeOfUNDEFINED() {
        WildcardExpression expression = new WildcardExpression("A");
        Assert.assertEquals(DataType.UNDEFINED, expression.getDataType());
    }

    @Test
    public void returnNull() {
        WildcardExpression expression = new WildcardExpression("A");
        Assert.assertEquals(DataType.UNDEFINED, expression.getDataType());
        Assert.assertNull(expression.getValue(new ExecutionContext()));
    }


}
