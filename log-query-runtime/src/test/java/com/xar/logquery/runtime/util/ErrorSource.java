/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.util;

import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;

import java.util.Map;

/**
 * Created by akoekemo on 2/25/15.
 */
public class ErrorSource implements IDataSource {

    private static Throwable constructError;

    public ErrorSource() throws Throwable {

        if (constructError != null) { throw constructError; }
    }

    /**
     * Open the data source and return a resultset. This method can be called multiple times
     *
     * @param p_context the execution context
     *
     * @return a new resultset
     */
    @Override
    public IResultSet getResultSet(final IExecutionContext p_context) throws DataSourceException {

        return null;
    }

    /**
     * Set the attributes for the datasource. These are assigned to the datasource during execution
     *
     * @param p_attributes the attributes
     */
    @Override
    public void setAttributes(final Map<String, IExpression> p_attributes) {

    }

    /**
     * Bind the datasource to the execution context
     *
     * @param p_context the binding context
     *
     * @throws com.xar.logquery.runtime.exceptions.DataSourceException if the datasource is invalid
     */
    @Override
    public void validate(final IValidationContext p_context) throws DataSourceException {

    }

    public static void setConstructError(final Throwable p_constructError) {

        constructError = p_constructError;
    }
}
