/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.functions;

import com.xar.logquery.parser.exceptions.ParseException;
import com.xar.logquery.runtime.DataSource;
import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.ValidationContext;
import com.xar.logquery.runtime.config.RuntimeConfiguration;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.expression.constant.BooleanConstant;
import com.xar.logquery.runtime.expression.exceptions.EvaluationException;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.expression.functions.exceptions.InvalidParametersException;
import com.xar.logquery.runtime.expression.interfaces.IFunctionParameter;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by akoekemo on 3/9/15.
 */
public class TestFunctions {

    class FailFunction extends AbstractFunction{

        /**
         * Instantiates a new Abstract function.
         *
         */
        protected FailFunction() {

            super(DataType.INTEGER, new FunctionParameter(BooleanConstant.TRUE));
            super.addCallback(new DataType[]{DataType.BOOLEAN}, String.class.getMethods()[0]);
        }
    }
    @Test
    public void throwEvaluationExceptionIfMethodInvokeFails() throws InvalidExpressionException {
        FailFunction function = new FailFunction();
        function.validate(new ValidationContext());
        try {
            function.getValue(new ExecutionContext());
            Assert.fail("Expected exception");
        }
        catch (EvaluationException ex){
            Assert.assertEquals("object is not an instance of declaring class", ex.getMessage());
        }
    }

    @Test
    public void throwsInvalidParametersException() throws IOException, DataSourceException {

        try {
            IDataSource dataSource = DataSource.parse("SELECT EXTRACT_PREFIX('10', '10', 1, 20) FROM 'text'");
            Assert.fail("throwsInvalidParametersException not implemented!");
        }
        catch (InvalidParametersException ex){
            Assert.assertTrue(ex.getMessage().startsWith("[1, 7] Invalid parameters in function call."));
        }
    }

    @Test
    public void shouldFailForUnknownFunctions() throws IOException, DataSourceException {

        try {
            IDataSource dataSource = DataSource.parse("SELECT __UNKNOWN__() FROM 'test.txt'");
            Assert.fail("Expected exception");
        }
        catch (ParseException e) {
            Assert.assertEquals("[1, 7] Function __UNKNOWN__ does not exist", e.getMessage());
        }
    }

    @Test
    public void shouldFailIfFunctionDoesNotImplementIFinction() throws IOException, DataSourceException {
        try {
            RuntimeConfiguration.addFunction("__NOTIFUNCTION__", Object.class);
            DataSource.parse("SELECT __NOTIFUNCTION__() FROM 'test.txt'");
            Assert.fail("Expected exception");
        }
        catch (ParseException e) {
            Assert.assertEquals("[1, 7] java.lang.Object cannot be cast to com.xar.logquery.runtime.expression.interfaces.IFunction", e.getMessage());
        }
    }

    @Test
    public void faildForParameterErrors() throws IOException, DataSourceException {

        try {
            DataSource.parse("SELECT * FROM 'file' WHERE TO_INT(1,2,3,4)");
            Assert.fail("Expected error");
        }
        catch (InvalidExpressionException ex) {
            Assert.assertEquals("[1, 27] TO_INTEGER requires one parameter", ex.getMessage());
        }
    }



}
