/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.functions.scalar.date;

import com.xar.logquery.runtime.HelperFunctions;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.constant.ConstantExpression;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.expression.functions.exceptions.InvalidParametersException;
import org.joda.time.LocalDate;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

/**
 * Created by Anton Koekemoer on 2014/12/07.
 */
public class TestFunction_MONTH {

    @Test
    public void shouldGetTheMonthFromADate() throws IllegalAccessException, InvalidParametersException, InstantiationException, InvalidExpressionException {

        Date date = new Date();
        LocalDate now = LocalDate.fromDateFields(date);
        Assert.assertEquals(now.monthOfYear().get(), HelperFunctions.executeFunction(MonthFunction.class, DataType.INTEGER, new ConstantExpression<>(DataType.TIMESTAMP, date)));
        Assert.assertEquals(now.monthOfYear().get(), HelperFunctions.executeFunction(MonthFunction.class, DataType.INTEGER, new ConstantExpression<>(DataType.DATE, date)));
        Assert.assertEquals(2, HelperFunctions.executeFunction(MonthFunction.class, DataType.INTEGER, "2014-02-01"));
        Assert.assertNull(HelperFunctions.executeFunction(MonthFunction.class, DataType.INTEGER, "aaaa-aa-aa"));
    }

}
