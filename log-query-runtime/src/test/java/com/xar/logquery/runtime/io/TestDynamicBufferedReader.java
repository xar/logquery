/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.io;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.Field;

/**
 * Created by Anton Koekemoer on 2014/11/27.
 */
public class TestDynamicBufferedReader {

    @Test
    public void shouldReadLinesLargerThanBuffer() throws IOException, NoSuchFieldException, IllegalAccessException {
        DynamicBufferedReader reader = new DynamicBufferedReader(new StringReader("11234567890\n21234567890\n31234567890"), 5);
        Field field = DynamicBufferedReader.class.getDeclaredField("m_buffer");
        field.setAccessible(true);

        Assert.assertEquals("11234567890", reader.readLine());
        Assert.assertEquals(5, ((char[]) field.get(reader)).length);

        Assert.assertEquals("21234567890", reader.readLine());
        Assert.assertEquals(5, ((char[]) field.get(reader)).length);

        Assert.assertEquals("31234567890", reader.readLine());
        Assert.assertEquals(5, ((char[]) field.get(reader)).length);

        Assert.assertNull(reader.readLine());
        Assert.assertEquals(5, ((char[]) field.get(reader)).length);
    }

    @Test
    public void shouldGroBufferByRequiredAmountIfMoreSpaceNeeded() throws NoSuchFieldException, IOException, IllegalAccessException {
        DynamicBufferedReader reader = new DynamicBufferedReader(new StringReader("11234567890\n21234567890\n31234567890"), 5);
        Assert.assertTrue(reader.markSupported());
        Field field = DynamicBufferedReader.class.getDeclaredField("m_buffer");
        field.setAccessible(true);

        Assert.assertEquals("11234567890", reader.readLine());
        Assert.assertEquals(5, ((char[]) field.get(reader)).length);
        reader.mark(2);

        Assert.assertEquals("21234567890", reader.readLine());
        Assert.assertEquals(15, ((char[]) field.get(reader)).length);

        reader.reset();
        reader.mark();
        Assert.assertEquals("21234567890", reader.readLine());
        Assert.assertEquals(1039, ((char[]) field.get(reader)).length);

        Assert.assertEquals("31234567890", reader.readLine());
        Assert.assertEquals(1039, ((char[]) field.get(reader)).length);

        reader.reset();

        Assert.assertEquals("21234567890", reader.readLine());
        Assert.assertEquals(1039, ((char[]) field.get(reader)).length);

        Assert.assertEquals("31234567890", reader.readLine());
        Assert.assertEquals(1039, ((char[]) field.get(reader)).length);


        Assert.assertNull(reader.readLine());
        Assert.assertEquals(1039, ((char[]) field.get(reader)).length);

        reader.close();
    }

    @Test
    public void shouldRecognise_CR_LF_and_CRLF_AsLineEnds() throws NoSuchFieldException, IOException, IllegalAccessException {
        DynamicBufferedReader reader = new DynamicBufferedReader(new StringReader("11234567890\n21234567890\r31234567890\r\n41234567890\r\n"), 5);
        Field field = DynamicBufferedReader.class.getDeclaredField("m_buffer");
        field.setAccessible(true);

        Assert.assertEquals("11234567890", reader.readLine());
        Assert.assertEquals(5, ((char[]) field.get(reader)).length);

        Assert.assertEquals("21234567890", reader.readLine());
        Assert.assertEquals("31234567890", reader.readLine());
        Assert.assertEquals("41234567890", reader.readLine());

        Assert.assertNull(reader.readLine());
        Assert.assertEquals(5, ((char[]) field.get(reader)).length);
    }

    @Test
    public void shouldReadCharactersFromStream() throws NoSuchFieldException, IOException {
        DynamicBufferedReader reader = new DynamicBufferedReader(new StringReader("11234567890\n21234567890\r31234567890\r\n41234567890\r\n"), 5);
        Field field = DynamicBufferedReader.class.getDeclaredField("m_buffer");
        field.setAccessible(true);

        char[] chars = new char[20];
        StringBuilder builder = new StringBuilder();
        int read = reader.read(chars, 0, 2);
        builder.append(chars, 0, read);
        reader.mark();
        read = reader.read(chars, 0, 2);
        builder.append(chars, 0, read);
        reader.mark();
        while (true){
            read = reader.read(chars, 0, chars.length);
            if (read == -1) break;
            builder.append(chars, 0, read);
        }
        Assert.assertEquals(-1, reader.peek());
        Assert.assertEquals("11234567890\n21234567890\r31234567890\r\n41234567890\r\n", builder.toString());
        reader.reset();
        Assert.assertEquals("4567890", reader.readLine());
    }


    @Test
    public void shouldReadWithDefaultBuffer() throws NoSuchFieldException, IOException, IllegalAccessException {
        DynamicBufferedReader reader = new DynamicBufferedReader(new StringReader("11234567890\n21234567890\r31234567890\r\n41234567890\r\n"));
        Field field = DynamicBufferedReader.class.getDeclaredField("m_buffer");
        field.setAccessible(true);

        Assert.assertEquals("11234567890", reader.readLine());
        Assert.assertEquals(1024, ((char[]) field.get(reader)).length);

        Assert.assertEquals("21234567890", reader.readLine());
        Assert.assertEquals("31234567890", reader.readLine());
        Assert.assertEquals("41234567890", reader.readLine());

        Assert.assertNull(reader.readLine());
        Assert.assertEquals(1024, ((char[]) field.get(reader)).length);
    }


}
