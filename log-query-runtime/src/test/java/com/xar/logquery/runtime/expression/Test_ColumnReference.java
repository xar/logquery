/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression;

import com.xar.logquery.runtime.ValidationContext;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import org.junit.Assert;
import org.junit.Test;

import java.util.UUID;

/**
 * Created by akoekemo on 3/15/15.
 */
public class Test_ColumnReference {

    @Test
    public void constructOfColumnReferenceShouldInitialiseProperties() {

        String context = UUID.randomUUID().toString();
        String name = UUID.randomUUID().toString();
        ColumnReference columnReference = new ColumnReference(context, name);
        Assert.assertEquals(context, columnReference.getContext());
        Assert.assertEquals(name, columnReference.getName());

        Assert.assertEquals("ColumnReference{context='" + context + "', columnName='" + name + "', dataType=UNDEFINED}", columnReference.toString());
    }

    @Test
    public void validateExpressionThrowExceptionIfColumnNotFound() {

        String context = UUID.randomUUID().toString();
        String name = UUID.randomUUID().toString();
        ColumnReference columnReference = new ColumnReference(context, name);

        try {
            columnReference.validate(new ValidationContext());
            Assert.fail("Expected exception");
        }
        catch (InvalidExpressionException e) {
            Assert.assertEquals("Column not found - " + context + "." + name, e.getMessage());
        }
    }


}
