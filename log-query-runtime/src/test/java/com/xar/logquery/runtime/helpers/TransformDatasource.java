/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.helpers;

import com.xar.logquery.runtime.ResultSet;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.datasource.interfaces.ITransformer;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;

import java.util.Enumeration;
import java.util.Map;

/**
 * Created by akoekemo on 2/26/15.
 */
public class TransformDatasource implements ITransformer {

    private IDataSource m_dataSource;

    /**
     * Get the datasource being transformed
     *
     * @return the datasource being transformed
     */
    @Override
    public IDataSource getDataSource() {

        return m_dataSource;
    }

    /**
     * Set the datasource to be transformed
     *
     * @param p_dataSource the datasource to be transformed
     */
    @Override
    public void setDataSource(final IDataSource p_dataSource) {

        m_dataSource = p_dataSource;
    }

    /**
     * Open the data source and return a resultset.
     *
     * @param p_context the execution context
     *
     * @return a new resultset
     */
    @Override
    public IResultSet getResultSet(final IExecutionContext p_context) throws DataSourceException {

        IResultSet resultSet = m_dataSource.getResultSet(p_context);
        final Enumeration<IDataRow> enumeration = resultSet.getRows();

        return new ResultSet(resultSet.getColumnInfo(), new Enumeration<IDataRow>() {

            @Override
            public boolean hasMoreElements() {

                return enumeration.hasMoreElements();

            }

            @Override
            public IDataRow nextElement() {

                IDataRow row = enumeration.nextElement();
                row.getData()[0] = ((Number) row.getData()[0]).intValue() + 1;
                return row;
            }
        });
    }

    /**
     * Set the attributes for the datasource. These are assigned to the datasource during execution
     *
     * @param p_attributes the attributes
     */
    @Override
    public void setAttributes(final Map<String, IExpression> p_attributes) {

    }

    /**
     * Bind the datasource to the execution context
     *
     * @param p_context the binding context
     *
     * @throws com.xar.logquery.runtime.exceptions.DataSourceException if the datasource is invalid
     */
    @Override
    public void validate(final IValidationContext p_context) throws DataSourceException {

        m_dataSource.validate(p_context);
    }
}
