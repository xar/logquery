/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.output;

import com.xar.logquery.runtime.ColumnInfo;
import com.xar.logquery.runtime.datasource.StaticDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.output.csv.CSVOutputFormatter;
import com.xar.logquery.runtime.output.interfaces.IOutputFormatter;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.io.StringWriter;

/**
 * Created by akoekemo on 4/15/15.
 */
public class Test_CSVOutputFormatter {

    @Test
    public void shouldDelimitStringsWithSpecialCharacters() throws DataSourceException {

        StaticDataSource ds = new StaticDataSource();
        ds.setColumnInfo(new IColumnInfo[]{
                new ColumnInfo("", "col1", DataType.STRING), new ColumnInfo("", "col2", DataType.STRING)});
        IOutputFormatter csv = new CSVOutputFormatter();

        PrintStream out = System.out;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(baos);
        System.setOut(printStream);
        try {
            ds.addRow("r1\"c1", "r1c2");

            csv.format(ds);
            
            
        }
        finally {
            System.setOut(out);
        }
        Assert.assertEquals("col1,col2\n\"r1\"\"c1\",r1c2\n", new String(baos.toByteArray()));
    }

}
