/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime;

import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.expression.constant.BooleanConstant;
import com.xar.logquery.runtime.expression.constant.ConstantExpression;
import com.xar.logquery.runtime.expression.constant.IntegerConstant;
import com.xar.logquery.runtime.expression.constant.RealConstant;
import com.xar.logquery.runtime.expression.constant.StringConstant;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.expression.functions.FunctionParameter;
import com.xar.logquery.runtime.expression.functions.aggregate.interfaces.IAggregateExpression;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.expression.interfaces.IFunction;
import com.xar.logquery.runtime.expression.interfaces.IFunctionParameter;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.util.RuntimeFunctions;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.junit.Assert;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;

/**
 * Created by Anton Koekemoer on 2014/11/21.
 */
public class HelperFunctions {


    /**
     * Create a test input file, that can be used to test all the features of the application
     *
     * @param p_fileName name of the file to create
     */
    public static void createCSVTestFile(String p_fileName, boolean heading) throws FileNotFoundException {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        // Group, seq_int, seq_float, Date, Timestamp,
        int groupCount = 100;
        int groupMod = 10;
        int lineNumber = 0;

        try (PrintWriter out = new PrintWriter(p_fileName)) {
            if (heading) {
                out.println("groupNumber,line,groupRowNumber,intCol,floatCol,dateCol,timestampCol,textCol");
            }
            for (int group = 0; group < groupCount; group++) {
                // Calculate the number of rows for the group. A group is not allowed to have
                // less that 10 rows, with an additional number of rows of the mod 10 of the group
                int rowCount = groupMod + (group % groupMod);
                int startInt = rowCount;
                LocalDate date = new LocalDate(2014, 1, 1);
                DateTime dateTime = new DateTime(date.toDate());
                for (int row = 0; row < rowCount; row++) {
                    dateTime = dateTime.minusHours(1);
                    StringBuilder builder = new StringBuilder();
                    builder.append(String.valueOf(group)).append(',')
                            .append("\"Line ").append(lineNumber).append("\",")
                            .append(String.valueOf(row)).append(',')
                            .append(String.valueOf(startInt--)).append(",")
                            .append(String.valueOf(((float) group) + (0.1 * row + 1))).append(",")
                            .append(dateFormat.format(date.toDate())).append(',')
                            .append(timeStampFormat.format(dateTime.toDate())).append(',')
                            .append('"').append("Line1\nLine2\n").append('"').append('"').append("Line3").append('"');

                    out.println(builder);
                    lineNumber++;
                    date = date.plusDays(1);
                }

            }
        }
    }

    public static IFunction createFunction(final Class<?> functionClass, final Object... p_parameters) throws InstantiationException, IllegalAccessException, InvalidExpressionException {

        IFunction function = (IFunction) functionClass.newInstance();
        FunctionParameter[] parameters = new FunctionParameter[p_parameters.length];
        for (int i = 0; i < parameters.length; i++) {
            if (p_parameters[i] instanceof String) {
                parameters[i] = new FunctionParameter(new StringConstant((String) p_parameters[i]), false);
            }
            else if (p_parameters[i] instanceof Integer) {
                parameters[i] = new FunctionParameter(new IntegerConstant((Integer) p_parameters[i]), false);
            }
            else if (p_parameters[i] instanceof Long) {
                parameters[i] = new FunctionParameter(new IntegerConstant((Long) p_parameters[i]), false);
            }
            else if (p_parameters[i] instanceof Double) {
                parameters[i] = new FunctionParameter(new RealConstant((Double) p_parameters[i]), false);
            }
            else if (p_parameters[i] instanceof Float) {
                parameters[i] = new FunctionParameter(new RealConstant((Float) p_parameters[i]), false);
            }
            else if (p_parameters[i] instanceof BigDecimal) {
                parameters[i] = new FunctionParameter(new RealConstant(((BigDecimal) p_parameters[0]).doubleValue()), false);
            }
            else if (p_parameters[i] instanceof Boolean) {
                parameters[i] = new FunctionParameter(
                        (Boolean) p_parameters[i] ? BooleanConstant.TRUE : BooleanConstant.FALSE);
            }
            else if (p_parameters[i] instanceof Date) {
                parameters[i] = new FunctionParameter(
                        new ConstantExpression<>(DataType.DATE, (Date) p_parameters[0]));
            }
            else if (p_parameters[i] instanceof IExpression) {
                parameters[i] = new FunctionParameter((IExpression) p_parameters[i]);

            }
        }
        function.setParameters(parameters);
        function.validate(new ValidationContext());
        return function;
    }

    /**
     * Create a test input file, that can be used to test all the features of the application
     *
     * @param p_fileName name of the file to create
     */
    public static void createTestFile(File p_fileName) throws FileNotFoundException {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        // Group, seq_int, seq_float, Date, Timestamp,
        int groupCount = 100;
        int groupMod = 10;
        int lineNumber = 0;

        try (PrintWriter out = new PrintWriter(p_fileName)) {
            for (int group = 0; group < groupCount; group++) {
                // Calculate the number of rows for the group. A group is not allowed to have
                // less that 10 rows, with an additional number of rows of the mod 10 of the group
                int rowCount = groupMod + (group % groupMod);
                int startInt = rowCount;
                LocalDate date = new LocalDate(2014, 1, 1);
                DateTime dateTime = new DateTime(date.toDate());
                for (int row = 0; row < rowCount; row++) {
                    dateTime = dateTime.minusHours(1);
                    StringBuilder builder = new StringBuilder();
                    builder.append(String.valueOf(group)).append(',')
                            .append("Line ").append(lineNumber).append(',')
                            .append(String.valueOf(row)).append(',')
                            .append(String.valueOf(startInt--)).append(",")
                            .append(String.valueOf(((float) group) + (0.1 * row + 1))).append(",")
                            .append(dateFormat.format(date.toDate())).append(',')
                            .append(timeStampFormat.format(dateTime.toDate()));

                    out.println(builder);
                    lineNumber++;
                    date = date.plusDays(1);
                }

            }
        }
    }

    public static Object executeAggregate(final Class<?> p_functionClass, final DataType p_dataType, final IDataSource p_dataSource, final IFunctionParameter... p_parameter) throws IllegalAccessException, InstantiationException, DataSourceException {

        IFunction function = (IFunction) p_functionClass.newInstance();
        function.setParameters(p_parameter);
        function.validate(new ValidationContext());
        IAggregateExpression aggregate = (IAggregateExpression) function;
        IExecutionContext context = new ExecutionContext();

        Object state = aggregate.newState();
        IResultSet resultSet = p_dataSource.getResultSet(context);

        for (Enumeration<IDataRow> rows = resultSet.getRows(); rows.hasMoreElements(); ) {
            IDataRow row = rows.nextElement();
            context = RuntimeFunctions.applyRowToContext(context, row);
            aggregate.aggregate(state, context);
        }
        context.setValue(p_functionClass.getName(), aggregate.getKey(), state);
        Object result = function.getValue(context);
        return result;
    }

    public static Object executeFunction(Class<?> functionClass, DataType p_returnType, Object... p_expression) throws IllegalAccessException, InstantiationException, InvalidExpressionException {

        IFunction function = createFunction(functionClass, p_expression);
        Assert.assertEquals(p_returnType, function.getDataType());

        StringBuilder builder = new StringBuilder(RuntimeFunctions.getFunctionName(functionClass));
        builder.append("(");
        for (int i = 0; i < p_expression.length; i++) {
            if (i > 0) { builder.append(", "); }
            builder.append(HelperFunctions.toCode(p_expression[i]));
        }
        builder.append(") --> Returns ");

        Object result = function.getValue(new ExecutionContext());
        builder.append(HelperFunctions.toCode(result));
        System.out.println(builder);
        return result;
    }

    public static String findFile(final String p_fileName) {

        try {
            File file = new File(p_fileName);

            if (file.exists()) { return file.getCanonicalPath(); }

            File path = file.getCanonicalFile().getParentFile().getParentFile();

            file = new File(path, file.getName());

            return file.getCanonicalPath();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return p_fileName;
    }

    private static String toCode(final Object p_expression) {

        if (p_expression instanceof String) {
            return "\"" + p_expression + "\"";
        }
        return String.valueOf(p_expression);
    }

}
