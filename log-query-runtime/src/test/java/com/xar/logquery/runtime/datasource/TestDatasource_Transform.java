/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource;

import com.xar.logquery.parser.exceptions.ParseException;
import com.xar.logquery.runtime.ColumnInfo;
import com.xar.logquery.runtime.DataSource;
import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.ValidationContext;
import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.exceptions.AmbiguousColumnNameException;
import com.xar.logquery.runtime.exceptions.ColumnNotFoundException;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.exceptions.DuplicateColumnNameException;
import com.xar.logquery.runtime.expression.NamedExpression;
import com.xar.logquery.runtime.expression.constant.IntegerConstant;
import com.xar.logquery.runtime.expression.constant.RealConstant;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;
import com.xar.logquery.runtime.parse.ParseData;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.Enumeration;

/**
 * Created by akoekemo on 3/3/15.
 */
public class TestDatasource_Transform {

    @Test
    public void failsForDuplicateColumnNamesInTransform() throws IOException, DataSourceException {

        try {
            IDataSource dataSource = DataSource.parse("SELECT * FROM 'file' TRANSFORM ( 1 AS col1, 2 as col1)");
            Assert.fail("Expected error");
        }
        catch (DuplicateColumnNameException ex) {
            Assert.assertEquals("[1, 44] Column name 'col1' already exists", ex.getMessage());
        }
    }


    @Test
    public void shouldAssignChildDatasourceAlias() {

        final AliasableDataSource dataSource = new AliasableDataSource() {


            @Override
            protected IResultSet executeQuery(final IExecutionContext p_context) throws DataSourceException {

                return null;
            }

            @Override
            protected void validateQuery(final IValidationContext p_context) throws DataSourceException {

            }
        };

        TransformDataSource transform = new TransformDataSource(dataSource, new NamedExpression[0]);
        transform.setAlias("aliasable");

        Assert.assertEquals("aliasable", transform.getAlias());
        Assert.assertEquals("aliasable", dataSource.getAlias());
    }


    @Test
    public void shouldUseChildDatasourceAliasIfOwnAliasIsEmpty() {

        final AliasableDataSource dataSource = new AliasableDataSource() {


            @Override
            protected IResultSet executeQuery(final IExecutionContext p_context) throws DataSourceException {

                return null;
            }

            @Override
            protected void validateQuery(final IValidationContext p_context) throws DataSourceException {

            }
        };

        dataSource.setAlias("aliasable");
        TransformDataSource transform = new TransformDataSource(dataSource, new NamedExpression[0]);

        Assert.assertEquals("aliasable", transform.getAlias());
    }

    @Test
    public void shouldUseOwnAliasIfChildDatasourceIsNotAliasable() {
        TransformDataSource transform = new TransformDataSource(new AbstractDataSource() {

            @Override
            protected IResultSet executeQuery(final IExecutionContext p_context) throws DataSourceException {

                return null;
            }

            @Override
            protected void validateQuery(final IValidationContext p_context) throws DataSourceException {

            }
        }, new NamedExpression[0]);

        transform.setAlias("transform");
        Assert.assertEquals("transform", transform.getAlias());
    }

    @Test
    public void shouldThrowExceptionForDuplicateNames() throws IOException, DataSourceException {

        try {
            IDataSource dataSource = DataSource.parse("SELECT ds.RowNumber AS RowNumber, col2 FROM DUAL(rows=10) TRANSFORM(RowNumber+1 AS \"RowNumber\", RowNumber%2 AS col2) AS ds");
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            Assert.fail("Expected exception");
        }
        catch (DuplicateColumnNameException ex) {
            Assert.assertEquals("[1, 68] Column name 'ds.RowNumber' already exists", ex.getMessage());
        }
    }

    @Test
    public void shouldTransformDatasource() throws IOException, DataSourceException, AmbiguousColumnNameException, ColumnNotFoundException {

        IDataSource dataSource = DataSource.parse("SELECT ds.RowNumber AS RowNumber, col1, col2 FROM DUAL(rows=10) TRANSFORM(RowNumber+1 AS \"col1\", RowNumber%2 AS col2) AS ds");
        IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());

        Enumeration<IDataRow> rows = resultSet.getRows();
        int count = 1;
        while (rows.hasMoreElements()) {
            IDataRow row = rows.nextElement();
            System.out.println(row);
            long rowNumber = (int) row.get("RowNumber");
            long col1 = ((Number) row.get("col1")).intValue();
            long col2 = ((Number) row.get("col2")).intValue();

            Assert.assertEquals(String.valueOf(count % 2), count % 2, col2);
            Assert.assertEquals(String.valueOf(count + 1), count + 1, col1);
            Assert.assertEquals(String.valueOf(count), count++, rowNumber);

        }
    }

    @Test
    public void transformShouldAddColumnsToThecolumnsOftheInnerDatasource() throws DataSourceException {

        TransformDataSource dataSource = new TransformDataSource(new transformShouldAddColumnsToThecolumnsOftheInnerDatasource(), new NamedExpression[]{
                new NamedExpression(new IntegerConstant(100), "int", new ParseData(0, 0, "100")),
                new NamedExpression(new RealConstant(100), "real", new ParseData(0, 0, "100"))});
        dataSource.setAlias("C");

        dataSource.validate(new ValidationContext());

        IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());

        IColumnInfo[] columns = resultSet.getColumnInfo();


        Assert.assertEquals("col1", columns[0].getName());
        Assert.assertEquals("col2", columns[1].getName());
        Assert.assertEquals("col3", columns[2].getName());
        Assert.assertEquals("col4", columns[3].getName());
        Assert.assertEquals("col1", columns[4].getName());
        Assert.assertEquals("col2", columns[5].getName());
        Assert.assertEquals("col3", columns[6].getName());
        Assert.assertEquals("col4", columns[7].getName());

        Assert.assertEquals("A", columns[0].getContext());
        Assert.assertEquals("A", columns[1].getContext());
        Assert.assertEquals("A", columns[2].getContext());
        Assert.assertEquals("A", columns[3].getContext());
        Assert.assertEquals("B", columns[4].getContext());
        Assert.assertEquals("B", columns[5].getContext());
        Assert.assertEquals("B", columns[6].getContext());
        Assert.assertEquals("B", columns[7].getContext());


        Assert.assertEquals("int", columns[8].getName());
        Assert.assertEquals("real", columns[9].getName());
        Assert.assertEquals("C", columns[8].getContext());
        Assert.assertEquals("C", columns[9].getContext());

    }

    public static class transformShouldAddColumnsToThecolumnsOftheInnerDatasource extends StaticDataSource {

        /**
         * Initialise a new Static datasource
         */
        public transformShouldAddColumnsToThecolumnsOftheInnerDatasource() {

            setColumnInfo(new IColumnInfo[]{
                    new ColumnInfo("A", "col1", DataType.INTEGER),
                    new ColumnInfo("A", "col2", DataType.INTEGER),
                    new ColumnInfo("A", "col3", DataType.INTEGER),
                    new ColumnInfo("A", "col4", DataType.INTEGER),
                    new ColumnInfo("B", "col1", DataType.INTEGER),
                    new ColumnInfo("B", "col2", DataType.INTEGER),
                    new ColumnInfo("B", "col3", DataType.INTEGER),
                    new ColumnInfo("B", "col4", DataType.INTEGER)
            });
            addRow(new Object[]{1, 1, 2, 3, 4, 5, 6, 7});
            addRow(new Object[]{2, 1, 2, 3, 4, 5, 6, 7});
            addRow(new Object[]{3, 1, 2, 3, 4, 5, 6, 7});
            addRow(new Object[]{4, 1, 2, 3, 4, 5, 6, 7});
        }


    }


}
