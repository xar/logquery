/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource.text;

import com.xar.logquery.runtime.AliasableColumnInfo;
import com.xar.logquery.runtime.DataRow;
import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.ValidationContext;
import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.exceptions.AmbiguousColumnNameException;
import com.xar.logquery.runtime.exceptions.ColumnNotFoundException;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.interfaces.IValidationContext;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Enumeration;

/**
 * Created by Anton Koekemoer on 2014/11/27.
 */
public class TestDataSource_TextFileDataSource {

    @Test
    public void shouldFailForInvalidFilePatter() {

        TextLineDataSource source = new TextLineDataSource("a\u0000bc", "");
        try {
            source.getResultSet(new ExecutionContext());
            Assert.fail("Expected error!");
        }
        catch (DataSourceException e) {
            Assert.assertEquals("Error opening files", e.getMessage());
        }
    }

    @Test
    public void shouldRecover() throws IOException, DataSourceException, ColumnNotFoundException, AmbiguousColumnNameException {

        try {
            createFile("test1.txt");
            createFile("test2.txt");
            createFile("test3.txt");
            TestSource source = new TestSource("test*.txt", "alias");

            source.validate(new ValidationContext());
            IResultSet resultSet = source.getResultSet(new ExecutionContext());
            Assert.assertEquals(1, resultSet.getColumnInfo().length);
            Assert.assertEquals("alias", resultSet.getColumnInfo()[0].getContext());
            Enumeration<IDataRow> rows = resultSet.getRows();
            while (rows.hasMoreElements()) {
                final IDataRow dataRow = rows.nextElement();
                System.out.println(dataRow);
                Assert.assertEquals(dataRow.get("FileName"), new File("test3.txt").getAbsolutePath());
            }
        }
        finally {
            for (int i = 0; i < 4; i++) { new File("test" + i + ".txt").delete(); }
        }

    }

    private File createFile(final String p_fileName) throws IOException {

        File f = new File(p_fileName);
        try (FileWriter writer = new FileWriter(f)) {

            for (int i = 0; i < 1000; i++) {
                writer.write(f.getAbsolutePath() + " Line " + i + "\n");
            }
        }
        return f;
    }


    class TestSource extends TextFileDataSource<File> {

        int rows = 0;

        /**
         * Instantiates a new Text file datasource.
         *
         * @param p_filePattern the file pattern
         * @param p_alias       the alias
         */
        public TestSource(final String p_filePattern, final String p_alias) {

            super(p_filePattern, p_alias);
            setColumnInfo(new IColumnInfo[]{new AliasableColumnInfo(this, "FileName", DataType.STRING)});
        }

        /**
         * Closes the file reader
         *
         * @param p_file the file reader
         */
        @Override
        protected void closeFileReader(final File p_file) {

        }

        /**
         * Bind the datasource to the execution context
         *
         * @param p_context the binding context
         *
         * @throws com.xar.logquery.runtime.exceptions.DataSourceException if the datasource is invalid
         */
        @Override
        public void validateQuery(final IValidationContext p_context) throws DataSourceException {

        }

        /**
         * Create the reader appropriate for this datasource
         *
         * @param p_fileName the file to be read
         *
         * @return the reader
         *
         * @throws java.io.FileNotFoundException the file not found exception
         */
        @Override
        protected File createFileReader(final File p_fileName) throws FileNotFoundException {

            if (p_fileName.getName().equals("test1.txt")) {
                throw new FileNotFoundException();
            }
            return p_fileName;
        }

        /**
         * Construct a row from the data
         *
         * @param p_text the text
         *
         * @return a new data row
         */
        @Override
        protected IDataRow readLine(final File p_text) throws IOException {

            if (p_text.getName().equals("test2.txt")) {
                throw new IOException();
            }
            if (rows++ < 10) { return new DataRow(getColumnInfo(), new Object[]{p_text.getAbsolutePath()}); }
            return null;
        }
    }


}
