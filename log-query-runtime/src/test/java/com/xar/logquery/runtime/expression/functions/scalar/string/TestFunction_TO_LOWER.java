/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.functions.scalar.string;

import com.xar.logquery.runtime.HelperFunctions;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.expression.functions.exceptions.InvalidParametersException;
import org.junit.Assert;
import org.junit.Test;

import static com.xar.logquery.runtime.enums.DataType.STRING;

/**
 * Created by Anton Koekemoer on 2014/11/22.
 */
public class TestFunction_TO_LOWER {

    @Test
    public void tolower_shouldConvertToLowerCase() throws IllegalAccessException, InvalidParametersException, InstantiationException, InvalidExpressionException {

        Assert.assertEquals("abcdefghijk", HelperFunctions.executeFunction(ToLowerFunction.class, STRING, "aBcdEfGHIjk"));
    }

}
