/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.functions.scalar.convert;

import com.xar.logquery.runtime.HelperFunctions;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.constant.ConstantExpression;
import com.xar.logquery.runtime.expression.constant.StringConstant;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.util.DateTimeFunctions;
import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.util.Date;

/**
 * Created by Anton Koekemoer on 2014/11/23.
 */
public class TestFunction_TO_TIMESTAMP {
    @Test
    public void shouldConvertToTimestamp() throws ParseException, InstantiationException, IllegalAccessException, InvalidExpressionException {


        Assert.assertEquals(DateTimeFunctions.parseDate("2000-01-31 13:24:33", "yyyy-MM-dd HH:mm:ss"), HelperFunctions.executeFunction(ToTimestampFunction.class, DataType.TIMESTAMP, "2000-01-31 13:24:33"));
        Assert.assertEquals(DateTimeFunctions.parseDate("2000-01-31 13:33:24", "yyyy-MM-dd HH:mm:ss"), HelperFunctions.executeFunction(ToTimestampFunction.class, DataType.TIMESTAMP, "31-01-2000 13:24:33", "dd-MM-yyyy HH:ss:mm"));

        Assert.assertEquals(null, HelperFunctions.executeFunction(ToTimestampFunction.class, DataType.TIMESTAMP, "q31-01-2000", "dd-MM-yyyy"));

        long now = System.currentTimeMillis();

        Assert.assertEquals(new Date(now), HelperFunctions.executeFunction(ToTimestampFunction.class, DataType.TIMESTAMP, now));
        Assert.assertEquals(new Date((int) now), HelperFunctions.executeFunction(ToTimestampFunction.class, DataType.TIMESTAMP, (int) now));

        double dNow = now / (double) (86400000);
        float fNow = (float)dNow;

        Assert.assertEquals(new Date((long) (dNow * 86400000)), HelperFunctions.executeFunction(ToTimestampFunction.class, DataType.TIMESTAMP, dNow));
        Assert.assertEquals(new Date((long) ((double)fNow * 86400000)), HelperFunctions.executeFunction(ToTimestampFunction.class, DataType.TIMESTAMP, fNow));
        Assert.assertEquals(new Date(now), HelperFunctions.executeFunction(ToTimestampFunction.class, DataType.TIMESTAMP, new ConstantExpression<>(DataType.TIMESTAMP, new Date(now))));

        Assert.assertEquals(null, HelperFunctions.executeFunction(ToTimestampFunction.class, DataType.TIMESTAMP, new StringConstant(null)));


//        testConvert(new ToTimestampFunction(), sdf.parse("2000-01-31 13:24:33"), new StringConstant("2000-01-31 13:24:33"));
//        testConvert(new ToTimestampFunction(), sdf.parse("2000-01-31 13:33:24"), new StringConstant("31-01-2000 13:24:33"), new StringConstant("dd-MM-yyyy HH:ss:mm"));
//        try {
//            testConvert(new ToTimestampFunction(), sdf.parse("2000-01-31 13:24:33"), new StringConstant("q31-01-2000"), new StringConstant("dd-MM-yyyy"));
//            Assert.fail("Expected exception");
//        }
//        catch (Exception ex){
//            Assert.assertEquals("Unable to convert value to a timestamp: \"q31-01-2000\"", ex.getMessage());
//        }
//
//        long now = System.currentTimeMillis();
//        testConvert(new ToTimestampFunction(), new Date(now), new LongConstant(now));
//        testConvert(new ToTimestampFunction(), new Date((int)now), new IntegerConstant((int)now));
//
//        double dNow = now / (double)(86400000);
//        float fNow = (float)dNow;
//        testConvert(new ToTimestampFunction(), new Date((long) (dNow*86400000)), new DoubleConstant(dNow));
//        testConvert(new ToTimestampFunction(), new Date((long) ((double)fNow*86400000)), new FloatConstant(fNow));
//
//        testConvert(new ToTimestampFunction(), LocalDate.fromDateFields(new Date(now)).toDate(), new ConstantExpression<>(DataType.TIMESTAMP, new Date(now)));
    }

}
