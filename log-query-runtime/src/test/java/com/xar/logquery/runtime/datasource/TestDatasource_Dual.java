/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource;

import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.ValidationContext;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.exceptions.AmbiguousColumnNameException;
import com.xar.logquery.runtime.exceptions.ColumnNotFoundException;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import org.junit.Assert;
import org.junit.Test;

import java.util.Enumeration;
import java.util.NoSuchElementException;

/**
 * Created by Anton Koekemoer on 2014/11/24.
 */
public class TestDatasource_Dual {

    @Test
    public void hasOneColumn() throws DataSourceException {

        DualDataSource dataSource = new DualDataSource();

        dataSource.validate(new ValidationContext());
        IResultSet result = dataSource.getResultSet(new ExecutionContext());
        Assert.assertEquals(1, result.getColumnInfo().length);
        Assert.assertEquals("RowNumber", result.getColumnInfo()[0].getName());
        Assert.assertEquals(DataType.INTEGER, result.getColumnInfo()[0].getDataType());

    }

    @Test
    public void returnsTheNumberOfRows() throws DataSourceException, AmbiguousColumnNameException, ColumnNotFoundException {

        DualDataSource dual = new DualDataSource(10);

        dual.validate(new ValidationContext());
        IResultSet result = dual.getResultSet(new ExecutionContext());
        Enumeration<IDataRow> rows = result.getRows();
        int count = 1;
        while (rows.hasMoreElements()) {
            IDataRow row = rows.nextElement();
            long rowNumber = (int) row.get("RowNumber");

            Assert.assertEquals(String.valueOf(count), count++, rowNumber);
        }
        try {
            rows.nextElement();
            Assert.fail("Expect no such element!");
        }
        catch (NoSuchElementException ex) {

        }
    }


}
