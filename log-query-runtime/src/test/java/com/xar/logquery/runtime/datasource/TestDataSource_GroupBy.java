/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource;

import com.xar.logquery.parser.exceptions.ParseException;
import com.xar.logquery.runtime.DataSource;
import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.HelperFunctions;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.exceptions.AmbiguousColumnNameException;
import com.xar.logquery.runtime.exceptions.ColumnNotFoundException;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Enumeration;

/**
 * Created by akoekemo on 3/10/15.
 */
public class TestDataSource_GroupBy {

    @Test
    public void shouldNotAllowAggregatesInGroupByClause() throws IOException, DataSourceException {
        try {
            IDataSource dataSource = DataSource.parse("SELECT groupNumber, FIRST(RowNumber), LAST(RowNumber), COUNT(*) from 'file' TRANSFORM (TO_INT(STRTOK(Text, ',', 0)) AS groupNumber, STRTOK(Text, ',', 1) AS line, STRTOK(Text, ',', 2) AS groupRowNumber, STRTOK(Text, ',', 3) AS intCol, STRTOK(Text, ',', 4) AS floatCol, STRTOK(Text, ',', 5) AS dateCol, STRTOK(Text, ',', 6) AS timestampCol) GROUP BY SUM(groupNumber)");
            Assert.fail("Expected exception");
        }
        catch (ParseException ex){
            Assert.assertEquals("[1, 339] No aggregate functions allowed in GROUP BY", ex.getMessage());
        }
    }

    @Test
    public void shouldFilterGroupData() throws IOException, ColumnNotFoundException, DataSourceException, AmbiguousColumnNameException {

        File file = new File("test.txt");
        HelperFunctions.createTestFile(file);
        try {
            int groupMod = 10;
            int lineNumber = 0;
            int group = 70;

            IDataSource dataSource = DataSource.parse("SELECT groupNumber, FIRST(RowNumber), LAST(RowNumber), COUNT(*) from '" + file.getName() + "' TRANSFORM (TO_INT(STRTOK(Text, ',', 0)) AS groupNumber, STRTOK(Text, ',', 1) AS line, STRTOK(Text, ',', 2) AS groupRowNumber, STRTOK(Text, ',', 3) AS intCol, STRTOK(Text, ',', 4) AS floatCol, STRTOK(Text, ',', 5) AS dateCol, STRTOK(Text, ',', 6) AS timestampCol) GROUP BY groupNumber HAVING groupNumber BETWEEN 70 AND 80 ORDER BY 1");
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            Enumeration<IDataRow> rows = resultSet.getRows();
            while (rows.hasMoreElements()) {

                IDataRow dataRow = rows.nextElement();
                System.out.println(dataRow);
                int rowCount = groupMod + (group % groupMod);
                Assert.assertEquals(group, ((Number) dataRow.get("groupNumber")).intValue());
                Assert.assertEquals((long) rowCount, ((Number) dataRow.get("COUNT(*)")).longValue());
                Assert.assertEquals(dataRow.get("COUNT(*)"), ((long) dataRow.get("LAST(RowNumber)") - (long) dataRow.get("FIRST(RowNumber)")) + 1);
                lineNumber++;

                group++;
            }

            Assert.assertEquals(11, lineNumber);
        }
        finally {
            file.delete();
        }
    }

    @Test
    public void shouldGroupData() throws IOException, ColumnNotFoundException, DataSourceException, AmbiguousColumnNameException {

        File file = new File("test.txt");
        HelperFunctions.createTestFile(file);
        try {
            int groupMod = 10;
            int lineNumber = 0;
            int group = 0;

            IDataSource dataSource = DataSource.parse("SELECT groupNumber, COUNT(*) from '" + file.getName() + "' TRANSFORM (TO_INT(STRTOK(Text, ',', 0)) AS groupNumber, STRTOK(Text, ',', 1) AS line, STRTOK(Text, ',', 2) AS groupRowNumber, STRTOK(Text, ',', 3) AS intCol, STRTOK(Text, ',', 4) AS floatCol, STRTOK(Text, ',', 5) AS dateCol, STRTOK(Text, ',', 6) AS timestampCol) GROUP BY groupNumber ORDER BY 1");
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            Enumeration<IDataRow> rows = resultSet.getRows();
            while (rows.hasMoreElements()) {

                IDataRow dataRow = rows.nextElement();
                System.out.println(dataRow);
                int rowCount = groupMod + (group % groupMod);
                Assert.assertEquals(group, ((Number) dataRow.get("groupNumber")).intValue());
                Assert.assertEquals((long) rowCount, ((Number) dataRow.get("COUNT(*)")).longValue());
                lineNumber++;

                group++;
            }

            Assert.assertEquals(100, lineNumber);
        }
        finally {
            file.delete();
        }
    }

    @Test
    public void shouldGroupWithoutGroupByIfOnlyAggregatesInColumns() throws IOException, DataSourceException {

        File file = new File("test.txt");
        HelperFunctions.createTestFile(file);
        try {
            IDataSource dataSource = DataSource.parse("SELECT COUNT(*), SUM(intCol) FROM '" + file.getName() + "' TRANSFORM(TO_INT(STRTOK(Text, ',', 0)) AS groupNumber, STRTOK(Text, ',', 1) AS line, STRTOK(Text, ',', 2) AS groupRowNumber, TO_INT(STRTOK(Text, ',', 3)) AS intCol, STRTOK(Text, ',', 4) AS floatCol, STRTOK(Text, ',', 5) AS dateCol, STRTOK(Text, ',', 6) AS timestampCol) AS a");
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            Enumeration<IDataRow> rows = resultSet.getRows();
            while (rows.hasMoreElements()) {
                IDataRow dataRow = rows.nextElement();
                Assert.assertEquals(1450L, dataRow.get(0));
                Assert.assertEquals(BigInteger.valueOf(11650), dataRow.get(1));
            }
        }
        finally {
            file.delete();
        }
    }

    @Test
    public void shouldNotAllowNonAggregateReferencedColumnsOutsideOfGroupBy() throws IOException, DataSourceException {

        File file = new File("test.txt");
        HelperFunctions.createTestFile(file);
        try {
            DataSource.parse("SELECT intCol, Text, COUNT(*), SUM(intCol) FROM '" + file.getName() + "' TRANSFORM(TO_INT(STRTOK(Text, ',', 0)) AS groupNumber, STRTOK(Text, ',', 1) AS line, STRTOK(Text, ',', 2) AS groupRowNumber, TO_INT(STRTOK(Text, ',', 3)) AS intCol, STRTOK(Text, ',', 4) AS floatCol, STRTOK(Text, ',', 5) AS dateCol, STRTOK(Text, ',', 6) AS timestampCol) AS a");
            Assert.fail("Expected error");
        }
        catch (ParseException ex) {
            Assert.assertEquals("[0, 0] Group by invalid. Referenced column [intCol] does not appear in the group by", ex.getMessage());
        }
        finally {
            file.delete();
        }
    }

}
