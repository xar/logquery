/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression;

import com.xar.logquery.runtime.DataSource;
import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.HelperFunctions;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.exceptions.AmbiguousColumnNameException;
import com.xar.logquery.runtime.exceptions.ColumnNotFoundException;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

/**
 * Created by akoekemo on 3/11/15.
 */
public class TestExpression_CASE {

    @Test
    public void caseFunctionThrowsExceptionForIncompatibleTypes() throws IOException, DataSourceException {

        try {
            IDataSource dataSource = DataSource.parse("SELECT Text, Line, CASE ' ' WHEN 10 THEN 'Ten' WHEN 20 THEN 'Twenty' WHEN 30 THEN 'Thirty'  WHEN 40 THEN 'Forty' ELSE 'Fifty' END AS Name from 'file' TRANSFORM (TO_INT(STRTOK(STRTOK(Text, ',', 1), ' ', 1)) AS Line) WHERE Line IN (SELECT TO_INT(Text) FROM 'in.txt' WHERE TO_INT(Text) = Line) ORDER BY Line");
        }
        catch (DataSourceException e) {
            Assert.assertEquals("[1, 19] DECODE - Invalid function parameters, unable to cast INTEGER to STRING", e.getMessage());
        }
    }


    @Test
    public void shouldSupportCaseWithComparisons() throws IOException, DataSourceException, ColumnNotFoundException, AmbiguousColumnNameException {

        try (PrintWriter out = new PrintWriter("in.txt")) {
            out.println("10");
            out.println("20");
            out.println("30");
            out.println("40");
            out.println("50");
        }
        File file = new File("test.txt");
        HelperFunctions.createTestFile(file);
        try {
            IDataSource dataSource = DataSource.parse("SELECT Text, Line, CASE WHEN Line=10 THEN 'Ten' WHEN Line=20 THEN 'Twenty' WHEN Line=30 THEN 'Thirty'  WHEN Line=40 THEN 'Forty' ELSE 'Fifty' END AS Name from '" + file.getName() + "' TRANSFORM (TO_INT(STRTOK(STRTOK(Text, ',', 1), ' ', 1)) AS Line) WHERE Line IN (SELECT TO_INT(Text) FROM 'in.txt' WHERE TO_INT(Text) = Line) ORDER BY Line");
            int line = 10;
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            for (Enumeration<IDataRow> rows = resultSet.getRows(); rows.hasMoreElements(); ) {
                IDataRow dataRow = rows.nextElement();
                Assert.assertEquals(line, ((Number) dataRow.get("Line")).intValue());
                switch (((Number) dataRow.get("Line")).intValue()) {
                    case 10:
                        Assert.assertEquals("Ten", dataRow.get("Name"));
                        break;
                    case 20:
                        Assert.assertEquals("Twenty", dataRow.get("Name"));
                        break;
                    case 30:
                        Assert.assertEquals("Thirty", dataRow.get("Name"));
                        break;
                    case 40:
                        Assert.assertEquals("Forty", dataRow.get("Name"));
                        break;
                    default:
                        Assert.assertEquals("Fifty", dataRow.get("Name"));
                        break;
                }
                line += 10;
                System.out.println(dataRow);
            }
            Assert.assertEquals(60, line);
        }
        finally {
            file.delete();
            new File("in.txt").delete();
        }
    }

    @Test
    public void shouldSupportCaseWithValue() throws IOException, DataSourceException, ColumnNotFoundException, AmbiguousColumnNameException {

        try (PrintWriter out = new PrintWriter("in.txt")) {
            out.println("10");
            out.println("20");
            out.println("30");
            out.println("40");
            out.println("50");
        }
        File file = new File("test.txt");
        HelperFunctions.createTestFile(file);
        try {
            IDataSource dataSource = DataSource.parse("SELECT Text, Line, CASE Line WHEN 10 THEN 'Ten' WHEN 20 THEN 'Twenty' WHEN 30 THEN 'Thirty'  WHEN 40 THEN 'Forty' ELSE 'Fifty' END AS Name from '" + file.getName() + "' TRANSFORM (TO_INT(STRTOK(STRTOK(Text, ',', 1), ' ', 1)) AS Line) WHERE Line IN (SELECT TO_INT(Text) FROM 'in.txt' WHERE TO_INT(Text) = Line) ORDER BY Line");
            int line = 10;
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            for (Enumeration<IDataRow> rows = resultSet.getRows(); rows.hasMoreElements(); ) {
                IDataRow dataRow = rows.nextElement();
                Assert.assertEquals(line, ((Number) dataRow.get("Line")).intValue());
                switch (((Number) dataRow.get("Line")).intValue()) {
                    case 10:
                        Assert.assertEquals("Ten", dataRow.get("Name"));
                        break;
                    case 20:
                        Assert.assertEquals("Twenty", dataRow.get("Name"));
                        break;
                    case 30:
                        Assert.assertEquals("Thirty", dataRow.get("Name"));
                        break;
                    case 40:
                        Assert.assertEquals("Forty", dataRow.get("Name"));
                        break;
                    default:
                        Assert.assertEquals("Fifty", dataRow.get("Name"));
                        break;
                }
                line += 10;
                System.out.println(dataRow);
            }
            Assert.assertEquals(60, line);
        }
        finally {
            file.delete();
            new File("in.txt").delete();
        }
    }



}
