/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource;

import com.xar.logquery.parser.exceptions.ParseException;
import com.xar.logquery.runtime.DataSource;
import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.exceptions.DuplicateColumnNameException;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.Enumeration;
import java.util.concurrent.ExecutionException;

/**
 * Created by akoekemo on 3/12/15.
 */
public class TestDataSource_PIVOT {

    @Test
    public void shouldOrderPivotData() throws IOException, DataSourceException {

        try (PrintWriter out = new PrintWriter("pivot.csv")) {
            out.println("col1,col2,col3,col4");
            out.println("r1c1,r1c2,h1,1");
            out.println("r1c1,r1c2,h2,2");
            out.println("r1c1,r1c2,h1,3");
            out.println("r2c1,r2c2,h2,4");
            out.println("r2c1,r2c2,h1,5");
            out.println("r3c1,r3c2,h1,6");
            out.println("r3c1,r3c2,h1,7");
        }
        String[] headings = new String[]{"col1", "col2", "h1", "h2"};
        Object[][] data = new Object[][]{
                new Object[]{"r3c1", "r3c2", BigInteger.valueOf(13), BigInteger.valueOf(0)},
                new Object[]{"r2c1", "r2c2", BigInteger.valueOf(5), BigInteger.valueOf(4)},
                new Object[]{"r1c1", "r1c2", BigInteger.valueOf(4), BigInteger.valueOf(2)},
        };
        try {
            IDataSource dataSource = DataSource.parse("SELECT col1, col2 FROM 'pivot.csv' PIVOT ISNULL(SUM(TO_INT(col4)), 0) ON col3 ORDER BY col1 DESC, col2");
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            IColumnInfo[] columnInfo = resultSet.getColumnInfo();
            for (int i = 0; i < columnInfo.length; i++) {
                Assert.assertEquals(headings[i], columnInfo[i].getName());
            }

            int row = 0;
            Enumeration<IDataRow> rows = resultSet.getRows();
            while (rows.hasMoreElements()) {
                IDataRow dataRow = rows.nextElement();
                System.out.println(dataRow);
                for (int i = 0; i < columnInfo.length; i++) {
                    Assert.assertEquals(data[row][i], dataRow.get(i));
                }
                row++;
            }
            Assert.assertEquals(3, row);
        }
        finally {
            new File("pivot.csv").delete();
        }
    }

    @Test
    public void shouldPivotData() throws IOException, DataSourceException {

        try (PrintWriter out = new PrintWriter("pivot.csv")) {
            out.println("col1,col2,col3,col4");
            out.println("r1c1,r1c2,h1,1");
            out.println("r1c1,r1c2,h2,2");
            out.println("r1c1,r1c2,h1,3");
            out.println("r2c1,r2c2,h2,4");
            out.println("r2c1,r2c2,h1,5");
            out.println("r3c1,r3c2,h1,6");
            out.println("r3c1,r3c2,h1,7");
        }
        String[] headings = new String[]{"Header", "col1", "col2", "Total", "h1", "h2"};
        Object[][] data = new Object[][]{
                new Object[]{"r1c1|r1c2", "r1c1", "r1c2", "r1c26", BigInteger.valueOf(4), BigInteger.valueOf(2)},
                new Object[]{"r2c1|r2c2", "r2c1", "r2c2", "r2c29", BigInteger.valueOf(5), BigInteger.valueOf(4)},
                new Object[]{"r3c1|r3c2", "r3c1", "r3c2", "r3c213", BigInteger.valueOf(13), BigInteger.valueOf(0)},
        };
        try {
            IDataSource dataSource = DataSource.parse("SELECT col1 + '|' + col2 AS Header, col1, col2, col2 + TO_STRING(SUM(TO_INT(col4))) AS Total FROM 'pivot.csv' PIVOT ISNULL(SUM(TO_INT(col4)), 0) ON col3");
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            IColumnInfo[] columnInfo = resultSet.getColumnInfo();
            for (int i = 0; i < columnInfo.length; i++) {
                Assert.assertEquals(headings[i], columnInfo[i].getName());
            }

            int row = 0;
            Enumeration<IDataRow> rows = resultSet.getRows();
            while (rows.hasMoreElements()) {
                IDataRow dataRow = rows.nextElement();
                for (int i = 0; i < columnInfo.length; i++) {
                    Assert.assertEquals(data[row][i], dataRow.get(i));
                }
                row++;
            }
            Assert.assertEquals(3, row);
        }
        finally {
            new File("pivot.csv").delete();
        }
    }

    @Test
    public void shouldOrderPivotDataAscending() throws IOException, DataSourceException {

        try (PrintWriter out = new PrintWriter("pivot.csv")) {
            out.println("col1,col2,col3,col4");
            out.println("r1c1,r1c2,h2,2");
            out.println("r1c1,r1c2,h1,1");
            out.println("r1c1,r1c2,h1,3");
            out.println("r2c1,r2c2,h2,4");
            out.println("r2c1,r2c2,h1,5");
            out.println("r3c1,r3c2,h1,6");
            out.println("r3c1,r3c2,h1,7");
        }
        String[] headings = new String[]{"Header", "col1", "col2", "Total", "h1", "h2"};
        Object[][] data = new Object[][]{
                new Object[]{"r1c1|r1c2", "r1c1", "r1c2", "r1c26", BigInteger.valueOf(4), BigInteger.valueOf(2)},
                new Object[]{"r2c1|r2c2", "r2c1", "r2c2", "r2c29", BigInteger.valueOf(5), BigInteger.valueOf(4)},
                new Object[]{"r3c1|r3c2", "r3c1", "r3c2", "r3c213", BigInteger.valueOf(13), BigInteger.valueOf(0)},
        };
        try {
            IDataSource dataSource = DataSource.parse("SELECT col1 + '|' + col2 AS Header, col1, col2, col2 + TO_STRING(SUM(TO_INT(col4))) AS Total FROM 'pivot.csv' PIVOT ISNULL(SUM(TO_INT(col4)), 0) ON col3 ASC");
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            IColumnInfo[] columnInfo = resultSet.getColumnInfo();
            for (int i = 0; i < columnInfo.length; i++) {
                Assert.assertEquals(headings[i], columnInfo[i].getName());
            }

            int row = 0;
            Enumeration<IDataRow> rows = resultSet.getRows();
            while (rows.hasMoreElements()) {
                IDataRow dataRow = rows.nextElement();
                for (int i = 0; i < columnInfo.length; i++) {
                    Assert.assertEquals(data[row][i], dataRow.get(i));
                }
                row++;
            }
            Assert.assertEquals(3, row);
        }
        finally {
            new File("pivot.csv").delete();
        }
    }

    @Test
    public void shouldSynthesizeColumnNames() throws DataSourceException, IOException {

        try (PrintWriter out = new PrintWriter("pivot.csv")) {
            out.println("col1,col2,col3,col4");
            out.println("r1c1,r1c2,h1,1");
            out.println("r1c1,r1c2,h2,2");
            out.println("r1c1,r1c2,h1,3");
            out.println("r2c1,r2c2,h2,4");
            out.println("r2c1,r2c2,h1,5");
            out.println("r3c1,r3c2,h1,6");
            out.println("r3c1,r3c2,h1,7");
            out.println("r3c1,r3c2,col1,6");
            out.println("r3c1,r3c2,col2,8");
        }
        String[] headings = new String[]{"Header", "col1_1", "col2_1", "Total", "h2", "h1", "col2", "col1"};
        Object[][] data = new Object[][]{
                new Object[]{"r1c1|r1c2", "r1c1", "r1c2", "r1c26", BigInteger.valueOf(2), BigInteger.valueOf(4), BigInteger.valueOf(0), BigInteger.valueOf(0)},
                new Object[]{"r2c1|r2c2", "r2c1", "r2c2", "r2c29", BigInteger.valueOf(4), BigInteger.valueOf(5), BigInteger.valueOf(0), BigInteger.valueOf(0)},
                new Object[]{"r3c1|r3c2", "r3c1", "r3c2", "r3c227", BigInteger.valueOf(0), BigInteger.valueOf(13), BigInteger.valueOf(8), BigInteger.valueOf(6)},
        };
        try {
            IDataSource dataSource = DataSource.parse("SELECT col1 + '|' + col2 AS Header, col1, col2, col2 + TO_STRING(SUM(TO_INT(col4))) AS Total FROM 'pivot.csv' PIVOT ISNULL(SUM(TO_INT(col4)), 0) ON col3 DESC");
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            IColumnInfo[] columnInfo = resultSet.getColumnInfo();
            for (int i = 0; i < columnInfo.length; i++) {
                Assert.assertEquals(headings[i], columnInfo[i].getName());
            }

            int row = 0;
            Enumeration<IDataRow> rows = resultSet.getRows();
            while (rows.hasMoreElements()) {
                IDataRow dataRow = rows.nextElement();
                System.out.println(dataRow);
                for (int i = 0; i < columnInfo.length; i++) {
                    Assert.assertEquals(data[row][i], dataRow.get(i));
                }
                row++;
            }
            Assert.assertEquals(3, row);
        }
        finally {
            new File("pivot.csv").delete();
        }
    }

    @Test
    public void throwsExceptionIfPivotColumnNotString() throws DataSourceException, IOException {
        try (PrintWriter out = new PrintWriter("pivot.csv")) {
            out.println("col1,col2,col3,col4");
            out.println("r1c1,r1c2,1,1");
            out.println("r1c1,r1c2,2,2");
            out.println("r1c1,r1c2,1,3");
            out.println("r2c1,r2c2,2,4");
            out.println("r2c1,r2c2,1,5");
            out.println("r3c1,r3c2,1,6");
            out.println("r3c1,r3c2,1,7");
        }
        try {
            IDataSource dataSource = DataSource.parse("SELECT col1 + '|' + col2 AS Header, col1, col2, col2 + TO_STRING(SUM(TO_INT(col4))) AS Total FROM 'pivot.csv' PIVOT ISNULL(SUM(TO_INT(col4)), 0) ON TO_INT(col3) DESC");
            Assert.fail("Expected exception");
        }
        catch (InvalidExpressionException ex){
            Assert.assertEquals("[1, 148] Column headers in PIVOT must be string", ex.getMessage());
        }
        finally {
            new File("pivot.csv").delete();
        }
    }

    @Test
    public void throwsExceptionIfPivotpointOutOfBounds() throws DataSourceException, IOException {
        try {
            DataSource.parse("SELECT col1 + '|' + col2 AS Header, col1, col2, col2 + TO_STRING(SUM(TO_INT(col4))) AS Total FROM 'pivot.csv' PIVOT ISNULL(SUM(TO_INT(col4)), 0) ON TO_INT(col3) DESC AFTER 0");
            Assert.fail("Expected exception");
        }
        catch (ParseException ex){
            Assert.assertEquals("[1, 172] Pivot point must be an INTEGER value between 1 and 4", ex.getMessage());
        }
        finally {
        }
        try {
            DataSource.parse("SELECT col1 + '|' + col2 AS Header, col1, col2, col2 + TO_STRING(SUM(TO_INT(col4))) AS Total FROM 'pivot.csv' PIVOT ISNULL(SUM(TO_INT(col4)), 0) ON TO_INT(col3) DESC AFTER 5");
            Assert.fail("Expected exception");
        }
        catch (ParseException ex){
            Assert.assertEquals("[1, 172] Pivot point must be an INTEGER value between 1 and 4", ex.getMessage());
        }
        finally {
        }
    }

    @Test
    public void shouldNotAllowWildcardsInSelectList() throws DataSourceException, IOException {
        try (PrintWriter out = new PrintWriter("pivot.csv")) {
            out.println("col1,col2,col3,col4");
            out.println("r1c1,r1c2,1,1");
            out.println("r1c1,r1c2,2,2");
            out.println("r1c1,r1c2,1,3");
            out.println("r2c1,r2c2,2,4");
            out.println("r2c1,r2c2,1,5");
            out.println("r3c1,r3c2,1,6");
            out.println("r3c1,r3c2,1,7");
        }
        try {
            IDataSource dataSource = DataSource.parse("SELECT *, col1 + '|' + col2 AS Header, col1, col2, col2 + TO_STRING(SUM(TO_INT(col4))) AS Total FROM 'pivot.csv' PIVOT ISNULL(SUM(TO_INT(col4)), 0) ON col3 DESC");
//            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            Assert.fail("Expected exception");
        }
        catch (DataSourceException ex){
            Assert.assertEquals("[1, 7] Wildcards are not allowed in pivot", ex.getMessage());
        }
        finally {
            new File("pivot.csv").delete();
        }
    }


    @Test
    public void shouldTHrowExceptionIfRowHeadersDuplicateColumnNames() throws DataSourceException, IOException {
        try (PrintWriter out = new PrintWriter("pivot.csv")) {
            out.println("col1,col2,col3,col4");
            out.println("r1c1,r1c2,h1,1");
            out.println("r1c1,r1c2,h2,2");
            out.println("r1c1,r1c2,h1,3");
            out.println("r2c1,r2c2,h2,4");
            out.println("r2c1,r2c2,h1,5");
            out.println("r3c1,r3c2,h1,6");
            out.println("r3c1,r3c2,h1,7");
            out.println("r3c1,r3c2,col1,6");
            out.println("r3c1,r3c2,col2,8");
        }
        String[] headings = new String[]{"Header", "col1_1", "col2_1", "Total", "h2", "h1", "col2", "col1"};
        Object[][] data = new Object[][]{
                new Object[]{"r1c1|r1c2", "r1c1", "r1c2", "r1c26", BigInteger.valueOf(2), BigInteger.valueOf(4), BigInteger.valueOf(0), BigInteger.valueOf(0)},
                new Object[]{"r2c1|r2c2", "r2c1", "r2c2", "r2c29", BigInteger.valueOf(4), BigInteger.valueOf(5), BigInteger.valueOf(0), BigInteger.valueOf(0)},
                new Object[]{"r3c1|r3c2", "r3c1", "r3c2", "r3c227", BigInteger.valueOf(0), BigInteger.valueOf(13), BigInteger.valueOf(8), BigInteger.valueOf(6)},
        };
        try {
            IDataSource dataSource = DataSource.parse("SELECT col1 + '|' + col2 AS Header, col1 as col1, col2, col2 + TO_STRING(SUM(TO_INT(col4))) AS Total FROM 'pivot.csv' PIVOT ISNULL(SUM(TO_INT(col4)), 0) ON col3 DESC");
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            Assert.fail("Expected exception");
        }
        catch (DuplicateColumnNameException ex){
            Assert.assertEquals("[1, 36] Duplicate column name - col1", ex.getMessage());
        }
        finally {
            new File("pivot.csv").delete();
        }
    }

    @Test
    public void shouldOrderPivotDataDescending() throws IOException, DataSourceException {

        try (PrintWriter out = new PrintWriter("pivot.csv")) {
            out.println("col1,col2,col3,col4");
            out.println("r1c1,r1c2,h1,1");
            out.println("r1c1,r1c2,h2,2");
            out.println("r1c1,r1c2,h1,3");
            out.println("r2c1,r2c2,h2,4");
            out.println("r2c1,r2c2,h1,5");
            out.println("r3c1,r3c2,h1,6");
            out.println("r3c1,r3c2,h1,7");
        }
        String[] headings = new String[]{"Header", "col1", "col2", "Total", "h2", "h1"};
        Object[][] data = new Object[][]{
                new Object[]{"r1c1|r1c2", "r1c1", "r1c2", "r1c26", BigInteger.valueOf(2), BigInteger.valueOf(4)},
                new Object[]{"r2c1|r2c2", "r2c1", "r2c2", "r2c29", BigInteger.valueOf(4), BigInteger.valueOf(5)},
                new Object[]{"r3c1|r3c2", "r3c1", "r3c2", "r3c213", BigInteger.valueOf(0), BigInteger.valueOf(13)},
        };
        try {
            IDataSource dataSource = DataSource.parse("SELECT col1 + '|' + col2 AS Header, col1, col2, col2 + TO_STRING(SUM(TO_INT(col4))) AS Total FROM 'pivot.csv' PIVOT ISNULL(SUM(TO_INT(col4)), 0) ON col3 DESC");
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            IColumnInfo[] columnInfo = resultSet.getColumnInfo();
            for (int i = 0; i < columnInfo.length; i++) {
                Assert.assertEquals(headings[i], columnInfo[i].getName());
            }

            int row = 0;
            Enumeration<IDataRow> rows = resultSet.getRows();
            while (rows.hasMoreElements()) {
                IDataRow dataRow = rows.nextElement();
                for (int i = 0; i < columnInfo.length; i++) {
                    Assert.assertEquals(data[row][i], dataRow.get(i));
                }
                row++;
            }
            Assert.assertEquals(3, row);
        }
        finally {
            new File("pivot.csv").delete();
        }
    }


    @Test
    public void shouldSupportAddingPivotsAfterIndex() throws IOException, DataSourceException {

        try (PrintWriter out = new PrintWriter("pivot.csv")) {
            out.println("col1,col2,col3,col4");
            out.println("r1c1,r1c2,h1,1");
            out.println("r1c1,r1c2,h2,2");
            out.println("r1c1,r1c2,h1,3");
            out.println("r2c1,r2c2,h2,4");
            out.println("r2c1,r2c2,h1,5");
            out.println("r3c1,r3c2,h1,6");
            out.println("r3c1,r3c2,h1,7");
        }
        String[] headings = new String[]{"Header", "col1", "col2", "h1", "h2", "Total"};
        Object[][] data = new Object[][]{
                new Object[]{"r1c1|r1c2", "r1c1", "r1c2", BigInteger.valueOf(4), BigInteger.valueOf(2),
                             BigInteger.valueOf(6)},
                new Object[]{"r2c1|r2c2", "r2c1", "r2c2", BigInteger.valueOf(5), BigInteger.valueOf(4),
                             BigInteger.valueOf(9)},
                new Object[]{"r3c1|r3c2", "r3c1", "r3c2", BigInteger.valueOf(13), BigInteger.valueOf(0),
                             BigInteger.valueOf(13)},
        };
        try {
            IDataSource dataSource = DataSource.parse("SELECT col1 + '|' + col2 AS Header, col1, col2, SUM(TO_INT(col4)) AS Total FROM 'pivot.csv' PIVOT ISNULL(SUM(TO_INT(col4)), 0) ON col3 AFTER 3");
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            IColumnInfo[] columnInfo = resultSet.getColumnInfo();
            for (int i = 0; i < columnInfo.length; i++) {
                Assert.assertEquals(headings[i], columnInfo[i].getName());
            }

            int row = 0;
            Enumeration<IDataRow> rows = resultSet.getRows();
            while (rows.hasMoreElements()) {
                IDataRow dataRow = rows.nextElement();
                for (int i = 0; i < columnInfo.length; i++) {
                    Assert.assertEquals(data[row][i], dataRow.get(i));
                }
                row++;
            }
            Assert.assertEquals(3, row);
        }
        finally {
            new File("pivot.csv").delete();
        }
    }

    @Test
    public void shouldSupportAddingPivotsAfterName() throws IOException, DataSourceException {

        try (PrintWriter out = new PrintWriter("pivot.csv")) {
            out.println("col1,col2,col3,col4");
            out.println("r1c1,r1c2,h1,1");
            out.println("r1c1,r1c2,h2,2");
            out.println("r1c1,r1c2,h1,3");
            out.println("r2c1,r2c2,h2,4");
            out.println("r2c1,r2c2,h1,5");
            out.println("r3c1,r3c2,h1,6");
            out.println("r3c1,r3c2,h1,7");
        }
        String[] headings = new String[]{"Header", "col1", "col2", "h1", "h2", "Total"};
        Object[][] data = new Object[][]{
                new Object[]{"r1c1|r1c2", "r1c1", "r1c2", BigInteger.valueOf(4), BigInteger.valueOf(2),
                             BigInteger.valueOf(6)},
                new Object[]{"r2c1|r2c2", "r2c1", "r2c2", BigInteger.valueOf(5), BigInteger.valueOf(4),
                             BigInteger.valueOf(9)},
                new Object[]{"r3c1|r3c2", "r3c1", "r3c2", BigInteger.valueOf(13), BigInteger.valueOf(0),
                             BigInteger.valueOf(13)},
        };
        try {
            IDataSource dataSource = DataSource.parse("SELECT col1 + '|' + col2 AS Header, col1, col2, SUM(TO_INT(col4)) AS Total FROM 'pivot.csv' PIVOT ISNULL(SUM(TO_INT(col4)), 0) ON col3 AFTER col2");
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            IColumnInfo[] columnInfo = resultSet.getColumnInfo();
            for (int i = 0; i < columnInfo.length; i++) {
                Assert.assertEquals(headings[i], columnInfo[i].getName());
            }

            int row = 0;
            Enumeration<IDataRow> rows = resultSet.getRows();
            while (rows.hasMoreElements()) {
                IDataRow dataRow = rows.nextElement();
                for (int i = 0; i < columnInfo.length; i++) {
                    Assert.assertEquals(data[row][i], dataRow.get(i));
                }
                row++;
            }
            Assert.assertEquals(3, row);
        }
        finally {
            new File("pivot.csv").delete();
        }
    }

    @Test
    public void shouldSupportAddingPivotsBeforeIndex() throws IOException, DataSourceException {

        try (PrintWriter out = new PrintWriter("pivot.csv")) {
            out.println("col1,col2,col3,col4");
            out.println("r1c1,r1c2,h1,1");
            out.println("r1c1,r1c2,h2,2");
            out.println("r1c1,r1c2,h1,3");
            out.println("r2c1,r2c2,h2,4");
            out.println("r2c1,r2c2,h1,5");
            out.println("r3c1,r3c2,h1,6");
            out.println("r3c1,r3c2,h1,7");
        }
        String[] headings = new String[]{"Header", "col1", "col2", "h1", "h2", "Total"};
        Object[][] data = new Object[][]{
                new Object[]{"r1c1|r1c2", "r1c1", "r1c2", BigInteger.valueOf(4), BigInteger.valueOf(2),
                             BigInteger.valueOf(6)},
                new Object[]{"r2c1|r2c2", "r2c1", "r2c2", BigInteger.valueOf(5), BigInteger.valueOf(4),
                             BigInteger.valueOf(9)},
                new Object[]{"r3c1|r3c2", "r3c1", "r3c2", BigInteger.valueOf(13), BigInteger.valueOf(0),
                             BigInteger.valueOf(13)},
        };
        try {
            IDataSource dataSource = DataSource.parse("SELECT col1 + '|' + col2 AS Header, col1, col2, SUM(TO_INT(col4)) AS Total FROM 'pivot.csv' PIVOT ISNULL(SUM(TO_INT(col4)), 0) ON col3 BEFORE 4");
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            IColumnInfo[] columnInfo = resultSet.getColumnInfo();
            for (int i = 0; i < columnInfo.length; i++) {
                Assert.assertEquals(headings[i], columnInfo[i].getName());
            }

            int row = 0;
            Enumeration<IDataRow> rows = resultSet.getRows();
            while (rows.hasMoreElements()) {
                IDataRow dataRow = rows.nextElement();
                for (int i = 0; i < columnInfo.length; i++) {
                    Assert.assertEquals(data[row][i], dataRow.get(i));
                }
                row++;
            }
            Assert.assertEquals(3, row);
        }
        finally {
            new File("pivot.csv").delete();
        }
    }

    @Test
    public void shouldSupportAddingPivotsBeforeName() throws IOException, DataSourceException {

        try (PrintWriter out = new PrintWriter("pivot.csv")) {
            out.println("col1,col2,col3,col4");
            out.println("r1c1,r1c2,h1,1");
            out.println("r1c1,r1c2,h2,2");
            out.println("r1c1,r1c2,h1,3");
            out.println("r2c1,r2c2,h2,4");
            out.println("r2c1,r2c2,h1,5");
            out.println("r3c1,r3c2,h1,6");
            out.println("r3c1,r3c2,h1,7");
        }
        String[] headings = new String[]{"Header", "col1", "col2", "h1", "h2", "Total"};
        Object[][] data = new Object[][]{
                new Object[]{"r1c1|r1c2", "r1c1", "r1c2", BigInteger.valueOf(4), BigInteger.valueOf(2),
                             BigInteger.valueOf(6)},
                new Object[]{"r2c1|r2c2", "r2c1", "r2c2", BigInteger.valueOf(5), BigInteger.valueOf(4),
                             BigInteger.valueOf(9)},
                new Object[]{"r3c1|r3c2", "r3c1", "r3c2", BigInteger.valueOf(13), BigInteger.valueOf(0),
                             BigInteger.valueOf(13)},
        };
        try {
            IDataSource dataSource = DataSource.parse("SELECT col1 + '|' + col2 AS Header, col1, col2, SUM(TO_INT(col4)) AS Total FROM 'pivot.csv' PIVOT ISNULL(SUM(TO_INT(col4)), 0) ON col3 BEFORE Total");
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            IColumnInfo[] columnInfo = resultSet.getColumnInfo();
            for (int i = 0; i < columnInfo.length; i++) {
                Assert.assertEquals(headings[i], columnInfo[i].getName());
            }

            int row = 0;
            Enumeration<IDataRow> rows = resultSet.getRows();
            while (rows.hasMoreElements()) {
                IDataRow dataRow = rows.nextElement();
                for (int i = 0; i < columnInfo.length; i++) {
                    Assert.assertEquals(data[row][i], dataRow.get(i));
                }
                row++;
            }
            Assert.assertEquals(3, row);
        }
        finally {
            new File("pivot.csv").delete();
        }
    }

    @Test
    public void shouldThrowExceptionIfAddingWithUnrecognisedColumnName() throws IOException, DataSourceException {

        try {
            IDataSource dataSource = DataSource.parse("SELECT col1 + '|' + col2 AS Header, col1, col2, SUM(TO_INT(col4)) AS Total FROM 'pivot.csv' PIVOT ISNULL(SUM(TO_INT(col4)), 0) ON col3 BEFORE sTotal");
            Assert.fail("Expected exception");
        }
        catch (ParseException ex) {
            Assert.assertEquals("[1, 142] Unable to find column with name - sTotal", ex.getMessage());
        }
    }

}
