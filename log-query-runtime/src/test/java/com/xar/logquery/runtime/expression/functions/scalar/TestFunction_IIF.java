/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.functions.scalar;

import com.xar.logquery.runtime.HelperFunctions;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.math.BigInteger;

/**
 * Created by Anton Koekemoer on 2014/11/21.
 */
public class TestFunction_IIF {

    @Test
    public void IIFReturnsTheFalseValueIfEvalueationIsFalse() throws IOException, DataSourceException, IllegalAccessException, InstantiationException {

        Assert.assertEquals(BigInteger.valueOf(1), HelperFunctions.executeFunction(IIFFunction.class, DataType.INTEGER, true, 1, 2));
    }

    @Test
    public void IIFReturnsTheTrueValueIfEvalueationIsTrue() throws IOException, DataSourceException, IllegalAccessException, InstantiationException {

        Assert.assertEquals(BigInteger.valueOf(1), HelperFunctions.executeFunction(IIFFunction.class, DataType.INTEGER, true, 1, 2));
    }

    @Test
    public void IIFThrowsExceptionIfFirstParameterNotBoolean() throws InstantiationException, IllegalAccessException {

        try {
            Assert.assertEquals(1, HelperFunctions.executeFunction(IIFFunction.class, DataType.INTEGER, 1, 1, 2));
            Assert.fail("Expected error");
        }
        catch (InvalidExpressionException ex) {
            Assert.assertEquals("IIF requires the first parameter to be a BOOLEAN expression", ex.getMessage());
        }
    }

    @Test
    public void IIFThrowsExceptionIfNo3Parameters() throws InstantiationException, IllegalAccessException {

        try {
            Assert.assertEquals(1, HelperFunctions.executeFunction(IIFFunction.class, DataType.INTEGER, 1, 2));
            Assert.fail("Expected error");
        }
        catch (InvalidExpressionException ex) {
            Assert.assertEquals("IIF requires three parameters", ex.getMessage());

        }
    }

    @Test
    public void IIFThrowsExceptionIfResultsNotCompatable() throws InstantiationException, IllegalAccessException {

        try {
            Assert.assertEquals(1, HelperFunctions.executeFunction(IIFFunction.class, DataType.INTEGER, true, 1, "2"));
            Assert.fail("Expected error");
        }
        catch (InvalidExpressionException ex) {
            Assert.assertEquals("IIF data types INTEGER and STRING is not compatible", ex.getMessage());

        }
    }

}
