/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.logic;

import com.xar.logquery.runtime.DataSource;
import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.HelperFunctions;
import com.xar.logquery.runtime.ValidationContext;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.exceptions.AmbiguousColumnNameException;
import com.xar.logquery.runtime.exceptions.ColumnNotFoundException;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.expression.constant.IntegerConstant;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.expression.exceptions.InvalidOperandsException;
import com.xar.logquery.runtime.parse.ParseData;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;

/**
 * Created by akoekemo on 3/5/15.
 */
public class TestCompare {

    @Test
    public void notExpressionThrowsExceptionIfDataTypeNotBoolean() {

        final IntegerConstant expression = new IntegerConstant(1);
        expression.setParseData(ParseData.EMPTY);
        NotExpression notExpression = new NotExpression(expression);
        try {
            notExpression.validate(new ValidationContext());
            Assert.fail("Expected exception");
        }
        catch (InvalidExpressionException e) {
            Assert.assertEquals("[0, 0] Unable to apply NOT to INTEGER", e.getMessage());
        }
    }

    @Test
    public void shouldCompareAND() throws IOException, DataSourceException {

        testCompare("true AND false", false);
        testCompare("false AND true", false);
        testCompare("false AND false", false);
        testCompare("true AND true", true);

        try {
            testCompare("' ' AND true", true);
            Assert.fail("Expected exception");
        }
        catch (InvalidOperandsException ex) {
            Assert.assertEquals("[1, 7] Invalid data type, found STRING, expected BOOLEAN", ex.getMessage());
        }

        try {
            testCompare("true AND ' '", true);
            Assert.fail("Expected exception");
        }
        catch (InvalidOperandsException ex) {
            Assert.assertEquals("[1, 7] Invalid data type, found STRING, expected BOOLEAN", ex.getMessage());
        }

    }

    @Test
    public void shouldCompareGT() throws IOException, DataSourceException {

        testCompare("1>1", false);
        testCompare("1>2", false);
        testCompare("2>1", true);
        testCompare("1.0>2.0", false);
        testCompare("2.0>1.0", true);
        testCompare("1>2.0", false);
        testCompare("2.0>1", true);
        testCompare("1.0>2", false);
        testCompare("2>1.0", true);
        testCompare("1.0>1", false);
        testCompare("1>1.0", false);
        testCompare("'1'>'1'", false);
        testCompare("'1'>'2'", false);
        testCompare("'2'>'1'", true);
    }

    @Test
    public void shouldCompareGTE() throws IOException, DataSourceException {

        testCompare("1>=1", true);
        testCompare("1>=2", false);
        testCompare("2>=1", true);
        testCompare("1.0>=2.0", false);
        testCompare("2.0>=1.0", true);
        testCompare("1>=2.0", false);
        testCompare("2.0>=1", true);
        testCompare("1.0>=2", false);
        testCompare("2>=1.0", true);
        testCompare("1.0>=1", true);
        testCompare("1>=1.0", true);
        testCompare("'1'>='1'", true);
        testCompare("'1'>='2'", false);
        testCompare("'2'>='1'", true);
    }

    @Test
    public void shouldCompareNull() throws DataSourceException, IOException, ColumnNotFoundException, AmbiguousColumnNameException {

        File file = new File("test.txt");
        HelperFunctions.createTestFile(file);
        try {

            IDataSource dataSource = DataSource.parse("SELECT * from '" + file.getName() + "' TRANSFORM (TO_INT(IIF(RowNumber%2=0, NULL, TO_STRING(RowNumber))) AS NullColumn)  WHERE NullColumn IS NULL");
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            for (Enumeration<IDataRow> rows = resultSet.getRows(); rows.hasMoreElements(); ) {
                IDataRow dataRow = rows.nextElement();
                Assert.assertEquals(dataRow.get("NullColumn") + " IS NULL", null, dataRow.get("NullColumn"));
            }

            dataSource = DataSource.parse("SELECT * from '" + file.getName() + "' TRANSFORM (TO_INT(IIF(RowNumber%2=0, NULL, TO_STRING(RowNumber))) AS NullColumn)  WHERE NullColumn IS NOT NULL");
            resultSet = dataSource.getResultSet(new ExecutionContext());
            for (Enumeration<IDataRow> rows = resultSet.getRows(); rows.hasMoreElements(); ) {
                IDataRow dataRow = rows.nextElement();
                Assert.assertNotNull(dataRow.get("NullColumn") + " IS NOT NULL", dataRow.get("NullColumn"));
            }
        }
        finally {
            file.delete();
        }

    }

    @Test
    public void shouldCompareOR() throws IOException, DataSourceException {

        testCompare("true OR false", true);
        testCompare("false OR true", true);
        testCompare("false OR false", false);


        try {
            testCompare("' ' OR true", true);
            Assert.fail("Expected exception");
        }
        catch (InvalidOperandsException ex) {
            Assert.assertEquals("[1, 7] Invalid data type, found STRING, expected BOOLEAN", ex.getMessage());
        }

        try {
            testCompare("true OR ' '", true);
            Assert.fail("Expected exception");
        }
        catch (InvalidOperandsException ex) {
            Assert.assertEquals("[1, 7] Invalid data type, found STRING, expected BOOLEAN", ex.getMessage());
        }

    }

    @Test
    public void shouldCompare_EQ() throws IOException, DataSourceException {

        testCompare("1=1", true);
        testCompare("1=2", false);
        testCompare("1.0=2.0", false);
        testCompare("1=2.0", false);
        testCompare("1.0=2", false);
        testCompare("1.0=1", true);
        testCompare("1=1.0", true);
        testCompare("'1'='1'", true);
        testCompare("'1'='2'", false);

    }

    @Test
    public void shouldCompare_LT() throws IOException, DataSourceException {

        testCompare("1<1", false);
        testCompare("1<2", true);
        testCompare("2<1", false);
        testCompare("1.0<2.0", true);
        testCompare("2.0<1.0", false);
        testCompare("1<2.0", true);
        testCompare("2.0<1", false);
        testCompare("1.0<2", true);
        testCompare("2<1.0", false);
        testCompare("1.0<1", false);
        testCompare("1<1.0", false);
        testCompare("'1'<'1'", false);
        testCompare("'1'<'2'", true);
        testCompare("'2'<'1'", false);

    }

    @Test
    public void shouldCompare_LTE() throws IOException, DataSourceException {

        testCompare("1<=1", true);
        testCompare("1<=2", true);
        testCompare("2<=1", false);
        testCompare("1.0<=2.0", true);
        testCompare("2.0<=1.0", false);
        testCompare("1<=2.0", true);
        testCompare("2.0<=1", false);
        testCompare("1.0<=2", true);
        testCompare("2<=1.0", false);
        testCompare("1.0<=1", true);
        testCompare("1<=1.0", true);
        testCompare("'1'<='1'", true);
        testCompare("'1'<='2'", true);
        testCompare("'2'<='1'", false);

    }

    @Test
    public void shouldCompare_NEQ() throws IOException, DataSourceException {

        testCompare("1!=1", false);
        testCompare("1!=2", true);
        testCompare("1.0!=2.0", true);
        testCompare("1!=2.0", true);
        testCompare("1.0!=2", true);
        testCompare("1.0!=1", false);
        testCompare("1!=1.0", false);
        testCompare("'1'!='1'", false);
        testCompare("'1'!='2'", true);

    }

    public static void testCompare(String p_expression, boolean p_expected) throws IOException, DataSourceException {

        IDataSource dataSource = DataSource.parse("SELECT " + p_expression + " AS Result FROM DUAL");
        IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());

        Enumeration<IDataRow> rows = resultSet.getRows();

        Assert.assertTrue(rows.hasMoreElements());
        IDataRow row = rows.nextElement();

        final Object actual = row.get(0);
        Assert.assertEquals(p_expression, p_expected, actual);

        Assert.assertFalse(rows.hasMoreElements());
    }


}
