/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime;

import com.xar.logquery.parser.exceptions.ParseException;
import com.xar.logquery.runtime.config.RuntimeConfiguration;
import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.exceptions.AmbiguousColumnNameException;
import com.xar.logquery.runtime.exceptions.ColumnNotFoundException;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.helpers.TransformDatasource;
import org.apache.log4j.PropertyConfigurator;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Enumeration;
import java.util.NoSuchElementException;

/**
 * Tests the datasource
 */
@SuppressWarnings("ALL")
public class TestDataSource {

    @Test
    public void throwsExceptionForInvalidPropertyAssign() throws IOException, DataSourceException {
        try {
            DataSource.parse("SELECT 10, 20, 30 FROM DUAL(rows=columnName)");
            Assert.fail("Expected error");
        }
        catch (ParseException ex) {
            Assert.assertEquals("[1, 33] Column not found - columnName", ex.getMessage());
        }
    }

    @Test
    public void throwsExceptionIfUsingIsNotATransformDataSource() throws IOException, DataSourceException {
        try {
            DataSource.parse("SELECT 10, 20, 30 FROM DUAL USING DUAL");
            Assert.fail("Expected error");
        }
        catch (ParseException ex) {
            Assert.assertEquals("[1, 34] DUAL - Is not a transformation provider", ex.getMessage());
        }
    }

    @Test
    public void throwsExceptionForDuplicateAliases() throws IOException, DataSourceException {
        try {
            IDataSource dataSource = DataSource.parse("SELECT * FROM 'left.csv' USING CSV AS A CROSS JOIN 'right.csv' USING CSV AS A");
            Assert.fail("Expected error");
        }
        catch (ParseException ex){
            Assert.assertEquals("[1, 76] Alias A already in use!", ex.getMessage());
        }
    }


    @Test
    public void shouldInitializeCustomDataSource() throws IOException, DataSourceException, AmbiguousColumnNameException, ColumnNotFoundException {

        IDataSource dataSource = DataSource.parse("SELECT 10, 20, 30, RowNumber FROM DUAL(rows=10)");

        IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());

        Enumeration<IDataRow> rows = resultSet.getRows();
        int count = 1;
        while (rows.hasMoreElements()) {
            IDataRow row = rows.nextElement();
            long rowNumber = (int) row.get("RowNumber");

            Assert.assertEquals(String.valueOf(count), count++, rowNumber);
        }
        try {
            rows.nextElement();
            Assert.fail("Expect no such element!");
        }
        catch (NoSuchElementException ignored) {

        }
    }

    @Test
    public void shouldLoadDataSourceBasedOnExtension() throws IOException, DataSourceException {

        HelperFunctions.createCSVTestFile("test.csv", true);
        try {
            IDataSource dataSource = DataSource.parse("SELECT * FROM 'test.csv'");

            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());

            IColumnInfo[] columnInfo = resultSet.getColumnInfo();
            String[] columns = {"FileName", "RowNumber", "groupNumber", "line", "groupRowNumber", "intCol", "floatCol",
                                "dateCol", "timestampCol", "textCol"};
            Assert.assertEquals(columns.length, columnInfo.length);
            for (int i = 0; i < columns.length; i++) {
                Assert.assertEquals(columns[i], columnInfo[i].getName());

            }
        }
        finally {
            new File("test.csv").delete();
        }
    }

    @Test
    public void shouldLoadTransformer() throws IOException, DataSourceException, AmbiguousColumnNameException, ColumnNotFoundException {

        try {
            RuntimeConfiguration.addDatasource("__TRANSFORMER", TransformDatasource.class);
            IDataSource dataSource = DataSource.parse("SELECT 10, 20, 30, RowNumber FROM DUAL(rows=10) USING __TRANSFORMER(item='value')");
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());

            Enumeration<IDataRow> rows = resultSet.getRows();
            int count = 1;
            while (rows.hasMoreElements()) {
                IDataRow row = rows.nextElement();
                System.out.println(row);
                int rowNumber = ((Number) row.get("RowNumber")).intValue();
                Assert.assertEquals(String.valueOf(count + 1), (++count), rowNumber);
            }

        }
        catch (ParseException ex) {
            Assert.assertEquals("[1, 43] Unbable to load formatter - __UNKNOWN", ex.getMessage());
        }
    }

    @Test
    public void shouldParseConstantExpressions() throws IOException, DataSourceException {

        IDataSource dataSource = DataSource.parse("SELECT 10 AS intValue, 20.5 AS realValue, TRUE as trueValue, FALSE as falseValue, NULL as nullValue, 0xa1 as hexValue FROM DUAL");

        IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());

        IColumnInfo[] columnInfo = resultSet.getColumnInfo();

        Assert.assertEquals("intValue", columnInfo[0].getName());
        Assert.assertEquals("realValue", columnInfo[1].getName());
        Assert.assertEquals("trueValue", columnInfo[2].getName());
        Assert.assertEquals("falseValue", columnInfo[3].getName());
        Assert.assertEquals("nullValue", columnInfo[4].getName());
        Assert.assertEquals("hexValue", columnInfo[5].getName());

        Assert.assertEquals(DataType.INTEGER, columnInfo[0].getDataType());
        Assert.assertEquals(DataType.REAL, columnInfo[1].getDataType());
        Assert.assertEquals(DataType.BOOLEAN, columnInfo[2].getDataType());
        Assert.assertEquals(DataType.BOOLEAN, columnInfo[3].getDataType());
        Assert.assertEquals(DataType.NULL, columnInfo[4].getDataType());
        Assert.assertEquals(DataType.INTEGER, columnInfo[5].getDataType());


        for (Enumeration<IDataRow> rows = resultSet.getRows(); rows.hasMoreElements(); ) {

            IDataRow dataRow = rows.nextElement();

            Assert.assertEquals(BigInteger.valueOf(10L), dataRow.get(0));
            Assert.assertEquals(new BigDecimal(20.5), dataRow.get(1));
            Assert.assertEquals(true, dataRow.get(2));
            Assert.assertEquals(false, dataRow.get(3));
            Assert.assertEquals(null, dataRow.get(4));
            Assert.assertEquals(BigInteger.valueOf(0xa1L), dataRow.get(5));

        }

    }

    @Test
    public void shouldParseConstants() throws DataSourceException, IOException {

        IDataSource dataSource = DataSource.parse("SELECT 10 as int, 20.1 as real, '30' as string, true as boolean FROM DUAL");
        IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());

        IColumnInfo[] info = resultSet.getColumnInfo();
        Assert.assertEquals("int", info[0].getName());
        Assert.assertEquals("real", info[1].getName());
        Assert.assertEquals("string", info[2].getName());
        Assert.assertEquals("boolean", info[3].getName());

        Assert.assertEquals(DataType.INTEGER, info[0].getDataType());
        Assert.assertEquals(DataType.REAL, info[1].getDataType());
        Assert.assertEquals(DataType.STRING, info[2].getDataType());
        Assert.assertEquals(DataType.BOOLEAN, info[3].getDataType());

        Enumeration<IDataRow> rows = resultSet.getRows();

        if (rows.hasMoreElements()) {
            IDataRow row = rows.nextElement();
            Assert.assertTrue(row.get(0) instanceof BigInteger);
            Assert.assertTrue(row.get(1) instanceof BigDecimal);
            Assert.assertTrue(row.get(2) instanceof String);
            Assert.assertTrue(row.get(3) instanceof Boolean);
        }
    }

    @Test
    public void shouldSelectNewNameForColumnNameClashes() throws IOException, DataSourceException {

        IDataSource dataSource = DataSource.parse("SELECT 10, 10, 20 as int, '30' as string, true as boolean FROM DUAL");
        IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());

        IColumnInfo[] columnInfo = resultSet.getColumnInfo();

        Assert.assertEquals("10", columnInfo[0].getName());
        Assert.assertEquals("10_1", columnInfo[1].getName());

    }

//    @Test
//    public void shouldParseSql() throws IOException, DataSourceException, AmbiguousColumnNameException, ColumnNotFoundException {
//
//        IDataSource dataSource = DataSource.parse("SELECT 10, 20, 30, * FROM DUAL");
//
//        IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
//
//        Enumeration<IDataRow> rows = resultSet.getRows();
//        int count = 1;
//        while (rows.hasMoreElements()) {
//            IDataRow row = rows.nextElement();
//            System.out.println(row);
//            long rowNumber = (int) row.get("RowNumber");
//
//            Assert.assertEquals(String.valueOf(count), count++, rowNumber);
//        }
//        try {
//            rows.nextElement();
//            Assert.fail("Expect no such element!");
//        }
//        catch (NoSuchElementException ex) {
//
//        }
//
//        Assert.fail("shouldParseSql not implemented!");
//    }

    @Test
    public void shouldThrowExceptionForUnknownDataSource() throws IOException, DataSourceException {

        try {
            DataSource.parse("SELECT 10, 20, 30 FROM __DUAL(rows=10)");
            Assert.fail("Expected error");
        }
        catch (ParseException ex) {
            Assert.assertEquals("[1, 23] Unbable to load datasource - __DUAL", ex.getMessage());
        }
    }

    @Test
    public void shouldThrowExceptionForUnknownTransformer() throws IOException, DataSourceException {

        try {
            DataSource.parse("SELECT 10, 20, 30 FROM DUAL(rows=10) USING __UNKNOWN(item='value')");
            Assert.fail("Expected error");
        }
        catch (ParseException ex) {
            Assert.assertEquals("[1, 43] Unable to load formatter - __UNKNOWN", ex.getMessage());
        }
    }

    @Test
    public void shouldThrowExceptionIfUnknownColumnReferenced() throws IOException, DataSourceException {

        try {
            DataSource.parse("SELECT 10, 20, 30, unknown FROM DUAL(rows=10)");
            Assert.fail("Expected error");
        }
        catch (DataSourceException ex) {
            Assert.assertEquals("[1, 19] Column not found - unknown", ex.getMessage());
        }
    }

    @Test
    public void shouldThrowExceptonIfUsingNonFileDataSource() throws IOException, DataSourceException {

        try {
            IDataSource dataSource = DataSource.parse("SELECT * from 'file' USING DUAL");
            Assert.fail("Expected exception");
        }
        catch (ParseException ex) {
            Assert.assertEquals("[1, 27] DUAL - Is not a file based format provider", ex.getMessage());
        }
    }

    @Test
    public void shouldThrowParseExceptionIfInitializeCustomDataSourceFindsUnknownAttribute() throws IOException, DataSourceException, AmbiguousColumnNameException, ColumnNotFoundException {

        try {
            IDataSource dataSource = DataSource.parse("SELECT 10, 20, 30 FROM DUAL(rows=10, unknown=200)");
            dataSource.getResultSet(new ExecutionContext());
            Assert.fail("Expected error");
        }
        catch (DataSourceException e) {
            Assert.assertEquals("[1, 45] Unknown attribute: unknown", e.getMessage());
        }
    }

    @Test
    public void shouldUseTextLineIfDataSourceNotFileBased() throws IOException, DataSourceException {

        RuntimeConfiguration.registerExtension("tsv", "DUAL");
        HelperFunctions.createCSVTestFile("test.tsv", true);
        try {
            IDataSource dataSource = DataSource.parse("SELECT * FROM 'test.tsv'");

            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());

            IColumnInfo[] columnInfo = resultSet.getColumnInfo();
            String[] columns = {"FileName", "RowNumber", "Text"};
            Assert.assertEquals(columns.length, columnInfo.length);
            for (int i = 0; i < columns.length; i++) {
                Assert.assertEquals(columns[i], columnInfo[i].getName());

            }
        }
        finally {
            boolean ignored = new File("test.tsv").delete();
        }
    }

    @BeforeClass
    public static void testFixtureSetup() {

        PropertyConfigurator.configure(System.getProperty("log4j.properties", HelperFunctions.findFile("log4j.properties")));

    }

    @AfterClass
    public static void testFixtureTeardown() {

    }


}
