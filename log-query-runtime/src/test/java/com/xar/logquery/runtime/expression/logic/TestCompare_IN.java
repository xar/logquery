/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.logic;

import com.xar.logquery.runtime.ColumnInfo;
import com.xar.logquery.runtime.DataSource;
import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.HelperFunctions;
import com.xar.logquery.runtime.ValidationContext;
import com.xar.logquery.runtime.datasource.StaticDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.exceptions.AmbiguousColumnNameException;
import com.xar.logquery.runtime.exceptions.ColumnNotFoundException;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.expression.constant.ConstantExpression;
import com.xar.logquery.runtime.expression.constant.IntegerConstant;
import com.xar.logquery.runtime.expression.exceptions.EvaluationException;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * Created by akoekemo on 3/9/15.
 */
public class TestCompare_IN {

    @Test
    public void shouldFilterByINWilthSubQuery() throws IOException, DataSourceException, ColumnNotFoundException {

        File file = new File("test.txt");
        HelperFunctions.createTestFile(file);
        try (PrintWriter out = new PrintWriter("in.txt")) {
            out.println("10");
            out.println("20");
            out.println("30");
            out.println("40");
            out.println("50");
        }
        try {
            IDataSource dataSource = DataSource.parse("SELECT Text, Line from '" + file.getName() + "' TRANSFORM (TO_INT(STRTOK(STRTOK(Text, ',', 1), ' ', 1)) AS Line) WHERE Line IN (SELECT TO_INT(Text) FROM 'in.txt') ORDER BY Line");
            int line = 10;
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            Enumeration<IDataRow> rows = resultSet.getRows();
            while (rows.hasMoreElements()) {
                IDataRow dataRow = rows.nextElement();
                Assert.assertEquals(line, ((Number) dataRow.get("Line")).intValue());
                line += 10;
                System.out.println(dataRow);
            }
            Assert.assertEquals(60, line);
        }
        catch (AmbiguousColumnNameException e) {
            e.printStackTrace();
        }
        finally {
            new File("in.txt").delete();
            file.delete();
        }
    }

    @Test
    public void shouldFilterByINWilthSubQueryReferencingValuesFromOuter() throws IOException, DataSourceException, ColumnNotFoundException, AmbiguousColumnNameException {

        File file = new File("test.txt");
        HelperFunctions.createTestFile(file);
        try (PrintWriter out = new PrintWriter("in.txt")) {
            out.println("10");
            out.println("20");
            out.println("30");
            out.println("40");
            out.println("50");
        }
        try {
            IDataSource dataSource = DataSource.parse("SELECT Text, Line from '" + file.getName() + "' TRANSFORM (TO_INT(STRTOK(STRTOK(Text, ',', 1), ' ', 1)) AS Line) WHERE Line IN (SELECT TO_INT(Text) FROM 'in.txt' WHERE TO_INT(Text) = Line) ORDER BY Line");
            int line = 10;
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            Enumeration<IDataRow> rows = resultSet.getRows();
            while (rows.hasMoreElements()) {
                IDataRow dataRow = rows.nextElement();
                System.out.println(dataRow);
                Assert.assertEquals(line, ((Number) dataRow.get("Line")).intValue());
                line += 10;
            }
            Assert.assertEquals(60, line);
        }
        finally {
            new File("in.txt").delete();
            file.delete();
        }
    }

    @Test
    public void shouldFilterRowsUsingINQueryResults() throws IOException, DataSourceException, AmbiguousColumnNameException, ColumnNotFoundException {

        File file = new File("test.txt");
        HelperFunctions.createTestFile(file);
        try {
            int lineNumber = 10;

            IDataSource dataSource = DataSource.parse("SELECT * from '" + file.getName() + "'  WHERE RowNumber IN (SELECT 10 FROM DUMMY UNION SELECT 20 FROM DUMMY UNION SELECT 30 FROM DUMMY UNION SELECT 40 FROM DUMMY)");

            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            Enumeration<IDataRow> rows = resultSet.getRows();
            while (rows.hasMoreElements()) {
                IDataRow dataRow = rows.nextElement();
                System.out.println(dataRow);
                Assert.assertEquals(lineNumber, ((Number) dataRow.get("RowNumber")).intValue());
                lineNumber += 10;
            }
            Assert.assertFalse(rows.hasMoreElements());
            try {
                rows.nextElement();
                Assert.fail("Expected exception");
            }
            catch (NoSuchElementException ex) {}
        }
        finally {
            file.delete();
        }
    }

    @Test
    public void shouldFilterRowsUsingINStaticList() throws IOException, DataSourceException, AmbiguousColumnNameException, ColumnNotFoundException {

        File file = new File("test.txt");
        HelperFunctions.createTestFile(file);
        try {
            int lineNumber = 10;

            IDataSource dataSource = DataSource.parse("SELECT * from '" + file.getName() + "'  WHERE RowNumber IN (10, 20, 30, 40)");

            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            Enumeration<IDataRow> rows = resultSet.getRows();
            while (rows.hasMoreElements()) {
                IDataRow dataRow = rows.nextElement();
                System.out.println(dataRow);
                Assert.assertEquals(lineNumber, ((Number) dataRow.get("RowNumber")).intValue());
                lineNumber += 10;
            }
            Assert.assertFalse(rows.hasMoreElements());
            try {
                rows.nextElement();
                Assert.fail("Expected exception");
            }
            catch (NoSuchElementException ex) {}
        }
        finally {
            file.delete();
        }
    }

    @Test
    public void shouldThrowExceptionIfDataSourceFailsValidation() throws IOException, DataSourceException {

        File file = new File("test.txt");
        HelperFunctions.createTestFile(file);
        try (PrintWriter out = new PrintWriter("in.txt")) {
            out.println("10");
            out.println("20");
            out.println("30");
            out.println("40");
            out.println("50");
        }
        try {
            DataSource.parse("SELECT * FROM 'test.txt' WHERE RowNumber IN (SELECT Text FROM 'in.txt' WHERE a=b)");
            Assert.fail("Expected exception");

        }
        catch (InvalidExpressionException ex){
            Assert.assertEquals("[1, 77] Column not found - a", ex.getMessage());
        }
        finally {
            new File("in.txt").delete();
            file.delete();
        }
    }

    @Test
    public void shouldThrowExceptionIfCompareFails() throws InvalidExpressionException {

        StaticDataSource dataSource = new StaticDataSource(new IColumnInfo[]{new ColumnInfo("A","A", DataType.INTEGER)}, new Object[][]{new Object[]{"AA"}});
        final Map<String, IExpression> attributes = new HashMap<>();
        attributes.put("unknown", new IntegerConstant(1));
        dataSource.setAttributes(attributes);
        CompareIN compare = new CompareIN(new IntegerConstant(10), dataSource);
        compare.validate(new ValidationContext());
        try {
            compare.getValue(new ExecutionContext());
            Assert.fail("Expected exception");
        }
        catch (EvaluationException ex){
            Assert.assertEquals("Unknown attribute: unknown", ex.getMessage());
        }
    }



}
