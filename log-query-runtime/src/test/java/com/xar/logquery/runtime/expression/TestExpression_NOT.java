/*
* Copyright 2015 Anton Koekemoer
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.xar.logquery.runtime.expression;

import com.xar.logquery.runtime.DataSource;
import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.Enumeration;

/**
 * Created by koekemoera on 2014/11/19.
 */
public class TestExpression_NOT {

    @Test
    public void notShouldNegateExpression() throws IOException, DataSourceException {

        IDataSource dataSource = DataSource.parse("SELECT NOT true FROM DUAL");
        IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
        Assert.assertEquals(DataType.BOOLEAN, resultSet.getColumnInfo()[0].getDataType());

        for (Enumeration<IDataRow> rows = resultSet.getRows(); rows.hasMoreElements(); ) {

            IDataRow dataRow = rows.nextElement();
            Assert.assertEquals(false, dataRow.get(0));
        }

    }

    @Test
    public void notShouldThrowExceptionForNonBooleanExpressions() throws DataSourceException, IOException {

        try {
            DataSource.parse("SELECT NOT 1 FROM DUAL");
            Assert.fail("Expected exception");
        }
        catch (InvalidExpressionException ex) {
            Assert.assertEquals("[1, 11] Unable to apply NOT to INTEGER", ex.getMessage());
        }
    }


}
