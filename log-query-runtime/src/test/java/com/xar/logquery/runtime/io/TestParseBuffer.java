/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.io;

import com.xar.logquery.runtime.io.ParseBuffer;
import org.junit.Assert;
import org.junit.Test;

/**
 */
public class TestParseBuffer {

    @Test
    public void shouldExtractStrings() {

        ParseBuffer buffer = new ParseBuffer("xxxx1234567890yyy".toCharArray());
        Assert.assertEquals("xxxx1234", buffer.extractString('5'));
        Assert.assertEquals("567890yyy", buffer.extractString('z'));
    }

    @Test
    public void shouldMatchLookahead() {

        ParseBuffer buffer = new ParseBuffer("xxxx1234567890yyy".toCharArray(), 4, 10);

        Assert.assertEquals(5, buffer.skip(5));
        Assert.assertTrue(buffer.la('6'));
        Assert.assertTrue(buffer.la("678"));
        Assert.assertEquals(5, buffer.skip(100));
        Assert.assertFalse(buffer.la('y'));
    }

    @Test
    public void shouldNotSkipPastTheEnd() {

        ParseBuffer buffer = new ParseBuffer("xxxx1234567890yyy".toCharArray(), 4, 10);

        Assert.assertEquals(5, buffer.skip(5));
        Assert.assertEquals(5, buffer.skip(100));
        Assert.assertEquals(10, buffer.getPosition());

        Assert.assertEquals(-5, buffer.skip(-5));
        Assert.assertEquals(-5, buffer.skip(-100));
        Assert.assertEquals(0, buffer.getPosition());
    }

    @Test
    public void shouldReturnEachCharacterTillTheEndOfFile() {

        ParseBuffer buffer = new ParseBuffer("1234567890");

        Assert.assertEquals('1', buffer.next());
        Assert.assertEquals('2', buffer.next());
        Assert.assertEquals('3', buffer.next());
        Assert.assertEquals('4', buffer.next());
        Assert.assertEquals('5', buffer.next());
        Assert.assertEquals('6', buffer.next());
        Assert.assertEquals('7', buffer.next());
        Assert.assertEquals('8', buffer.next());
        Assert.assertEquals('9', buffer.next());
        Assert.assertEquals('0', buffer.next());
        Assert.assertEquals(-1, buffer.next());
    }
}
