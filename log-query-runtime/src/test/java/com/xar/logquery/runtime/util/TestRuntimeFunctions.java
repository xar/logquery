/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.util;

import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.config.RuntimeConfiguration;
import com.xar.logquery.runtime.datasource.DualDataSource;
import com.xar.logquery.runtime.datasource.annotations.DataSourceAttribute;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.datasource.text.TextLineDataSource;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.exceptions.RequiredAttributeMissing;
import com.xar.logquery.runtime.exceptions.UnknownAttributeException;
import com.xar.logquery.runtime.expression.constant.IntegerConstant;
import com.xar.logquery.runtime.expression.constant.RealConstant;
import com.xar.logquery.runtime.expression.constant.StringConstant;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by akoekemo on 2/24/15.
 */
public class TestRuntimeFunctions {

    @Test
    public void initialiseDatasource_shouldConvertStringToNumbers() throws RequiredAttributeMissing, UnknownAttributeException {

        DSChild ds = new DSChild();
        Map<String, IExpression> map = new HashMap<>();

        map.put("field1", new StringConstant("v1"));
        map.put("field2", new StringConstant("v2"));
        map.put("field3", new StringConstant("v3"));
        map.put("field4", new StringConstant("v4"));
        map.put("field5", new StringConstant("v5"));
        map.put("byteField", new StringConstant("v5"));
        map.put("charField", new StringConstant("v5"));
        map.put("shortField", new StringConstant("100"));
        map.put("intField", new StringConstant("200"));
        map.put("longField", new StringConstant("300"));
        map.put("floatField", new StringConstant("400.3"));
        map.put("doubleField", new StringConstant("500.4"));

        RuntimeFunctions.initialiseDataSource(ds, map, new ExecutionContext());

        Assert.assertEquals("v1", ds.field1);
        Assert.assertEquals("v2", ds.field2);
        Assert.assertEquals("v3", ds.field3);
        Assert.assertEquals("v4", ds.field4);
        Assert.assertEquals("v5", ds.field5);
        Assert.assertEquals('v', ds.byteField);
        Assert.assertEquals('v', ds.charField);
        Assert.assertEquals((short) 100, ds.shortField);
        Assert.assertEquals(200, ds.intField);
        Assert.assertEquals(300l, ds.longField);
        Assert.assertEquals(Float.parseFloat("400.3"), ds.floatField, 0.01);
        Assert.assertEquals(Double.parseDouble("500.4"), ds.doubleField, 0.01);

    }


    @Test
    public void initialiseDatasource_shouldThrowExceptionForMissingAttributes() throws UnknownAttributeException {

        DSChild ds = new DSChild();
        Map<String, IExpression> map = new HashMap<>();

        map.put("field2", new StringConstant("v2"));
        map.put("field3", new StringConstant("v3"));
        map.put("field4", new StringConstant("v4"));
        map.put("field5", new StringConstant("v5"));

        try {
            RuntimeFunctions.initialiseDataSource(ds, map, new ExecutionContext());
            Assert.fail("Expected error");
        }
        catch (RequiredAttributeMissing e) {
            Assert.assertEquals("field1 - Is a required attribute", e.getMessage());
        }
    }

    @Test
    public void initialiseDatasource_shouldThrowExceptionForUnknownAttributes() throws RequiredAttributeMissing {

        DSChild ds = new DSChild();
        Map<String, IExpression> map = new HashMap<>();

        map.put("field1", new StringConstant("v1"));
        map.put("field5", new StringConstant("v5"));
        map.put("unknown", new StringConstant("v5"));

        try {
            RuntimeFunctions.initialiseDataSource(ds, map, new ExecutionContext());
            Assert.fail("Expected error");
        }
        catch (UnknownAttributeException e) {
            Assert.assertEquals("Unknown attribute: unknown", e.getMessage());
        }
    }

    @Test
    public void shouldConstructNamedDataSource() {

        IDataSource source = RuntimeFunctions.createDataSource("DUAL");
        Assert.assertTrue(source instanceof DualDataSource);
    }

    @Test
    public void shouldConvertLikeToRegex() {

        testLike("start%end", "^start.*end$", "start some text end", true);
        testLike("sstart%end", "^sstart.*end$", "start some text end", false);
        testLike("start%", "^start.*$", "start some text end", true);
        testLike("%end", "^.*end$", "start some text end", true);

        testLike("s_a_t%", "^s.a.t.*$", "start some text end", true);
        testLike("s\\_a_t%", "^s_a.t.*$", "s_art some text end", true);

        testLike("[start]%", "^\\[start\\].*$", "[start] some text end", true);
        testLike("(start)%", "^\\(start\\).*$", "(start) some text end", true);
        testLike("{start}%", "^\\{start\\}.*$", "{start} some text end", true);
        testLike(".start.%", "^\\.start\\..*$", ".start. some text end", true);
        testLike(".start.%", "^\\.start\\..*$", "xstartx some text end", false);
        testLike("+start+%", "^\\+start\\+.*$", "+start+ some text end", true);
        testLike("*start*%", "^\\*start\\*.*$", "*start* some text end", true);

    }

    @Test
    public void shouldDefaultToTextLineIfUnableToConstructDataSource() {

        RuntimeConfiguration.addDatasource("__ERROR__", ErrorSource.class);
        ErrorSource.setConstructError(new InstantiationException());
        IDataSource source = RuntimeFunctions.createDataSource("__ERROR__");
        Assert.assertTrue(source instanceof TextLineDataSource);
    }

    @Test
    public void shouldDefaultToTextLineIfUnableToFindDataSource() {

        IDataSource source = RuntimeFunctions.createDataSource("__DUAL__");
        Assert.assertTrue(source instanceof TextLineDataSource);
    }

    @Test
    public void shouldInitialiseDatasource() throws RequiredAttributeMissing, UnknownAttributeException {

        DSChild ds = new DSChild();
        Map<String, IExpression> map = new HashMap<>();

        map.put("field1", new StringConstant("v1"));
        map.put("field2", new StringConstant("v2"));
        map.put("field3", new StringConstant("v3"));
        map.put("field4", new StringConstant("v4"));
        map.put("field5", new StringConstant("v5"));
        map.put("byteField", new StringConstant("v5"));
        map.put("charField", new StringConstant("v5"));
        map.put("shortField", new StringConstant("100"));
        map.put("intField", new StringConstant("200"));
        map.put("longField", new StringConstant("300"));
        map.put("floatField", new StringConstant("400.3"));
        map.put("doubleField", new StringConstant("500.4"));

        RuntimeFunctions.initialiseDataSource(ds, map, new ExecutionContext());

        Assert.assertEquals("v1", ds.field1);
        Assert.assertEquals("v2", ds.field2);
        Assert.assertEquals("v3", ds.field3);
        Assert.assertEquals("v4", ds.field4);
        Assert.assertEquals("v5", ds.field5);
        Assert.assertEquals('v', ds.byteField);
        Assert.assertEquals('v', ds.charField);
        Assert.assertEquals((short) 100, ds.shortField);
        Assert.assertEquals(200, ds.intField);
        Assert.assertEquals(300l, ds.longField);
        Assert.assertEquals(Float.parseFloat("400.3"), ds.floatField, 0.01);
        Assert.assertEquals(Double.parseDouble("500.4"), ds.doubleField, 0.01);

        map.put("byteField", new IntegerConstant(1));
        map.put("charField", new IntegerConstant(2));
        map.put("shortField", new IntegerConstant(3));
        map.put("intField", new IntegerConstant(4));
        map.put("longField", new IntegerConstant(5));
        map.put("floatField", new RealConstant(6.7));
        map.put("doubleField", new RealConstant(7.8));

        RuntimeFunctions.initialiseDataSource(ds, map, new ExecutionContext());

        Assert.assertEquals(1, ds.byteField);
        Assert.assertEquals(2, ds.charField);
        Assert.assertEquals(3, ds.shortField);
        Assert.assertEquals(4, ds.intField);
        Assert.assertEquals(5, ds.longField);
        Assert.assertEquals(6.7, ds.floatField, 0.01);
        Assert.assertEquals(7.8, ds.doubleField, 0.01);

    }

    private void testLike(final String pattern, String regexPattern, final String value, final boolean result) {

        String regex = RuntimeFunctions.like2Regex(pattern, "\\");
        Assert.assertEquals(regex, regexPattern);
        System.out.println(regex);
        Assert.assertEquals("'" + value + " LIKE '" + regex + "'", result, value.matches(regex));

    }


    class DSChild extends DSRoot {

        String field4;

        @DataSourceAttribute(name = "field5", description = "field5", required = true)
        String field5;
    }

    class DSRoot implements IDataSource {

        @DataSourceAttribute(name = "field1", description = "field1", required = true)
        String field1;

        @DataSourceAttribute(name = "field2", description = "field2", required = false)
        String field2;

        String field3;

        byte byteField;

        char charField;

        short shortField;

        int intField;

        long longField;

        float floatField;

        double doubleField;

        /**
         * Open the data source and return a resultset. This method can be called multiple times
         *
         * @param p_context the execution context
         *
         * @return a new resultset
         */
        @Override
        public IResultSet getResultSet(final IExecutionContext p_context) throws DataSourceException {

            return null;
        }

        /**
         * Set the attributes for the datasource. These are assigned to the datasource during execution
         *
         * @param p_attributes the attributes
         */
        @Override
        public void setAttributes(final Map<String, IExpression> p_attributes) {

        }

        /**
         * Bind the datasource to the execution context
         *
         * @param p_context the binding context
         *
         * @throws com.xar.logquery.runtime.exceptions.DataSourceException if the datasource is invalid
         */
        @Override
        public void validate(final IValidationContext p_context) throws DataSourceException {

        }
    }

}
