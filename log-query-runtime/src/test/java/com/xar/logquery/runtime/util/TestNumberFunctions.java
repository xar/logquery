/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.util;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Anton Koekemoer on 2014/12/12.
 */
public class TestNumberFunctions {

    @Test
    public void shouldTestFloats() {

        Assert.assertFalse(NumberFunctions.isFloatingPoint(null));
        Assert.assertFalse(NumberFunctions.isFloatingPoint("a1234.11"));
        Assert.assertFalse(NumberFunctions.isFloatingPoint("1234a.2"));
        Assert.assertFalse(NumberFunctions.isFloatingPoint("a1234a.2"));
        Assert.assertFalse(NumberFunctions.isFloatingPoint("1234."));
        Assert.assertTrue(NumberFunctions.isFloatingPoint("1234.1"));
        Assert.assertTrue(NumberFunctions.isFloatingPoint("1234.124"));
        Assert.assertTrue(NumberFunctions.isFloatingPoint("-1234.123"));
    }

    @Test
    public void shouldTestIntegers() {

        Assert.assertFalse(NumberFunctions.isIntegral(null));
        Assert.assertFalse(NumberFunctions.isIntegral("a1234"));
        Assert.assertFalse(NumberFunctions.isIntegral("1234a"));
        Assert.assertFalse(NumberFunctions.isIntegral("a1234a"));
        Assert.assertFalse(NumberFunctions.isIntegral("1234.00"));
        Assert.assertTrue(NumberFunctions.isIntegral("1234"));
        Assert.assertTrue(NumberFunctions.isIntegral("-1234"));
    }
}
