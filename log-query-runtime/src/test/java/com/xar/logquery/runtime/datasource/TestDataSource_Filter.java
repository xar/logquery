/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource;

import com.xar.logquery.runtime.DataSource;
import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.HelperFunctions;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.exceptions.AmbiguousColumnNameException;
import com.xar.logquery.runtime.exceptions.ColumnNotFoundException;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Enumeration;

/**
 * Created by akoekemo on 3/5/15.
 */
public class TestDataSource_Filter {

    @Test
    public void shouldFilterData() throws IOException, DataSourceException, AmbiguousColumnNameException, ColumnNotFoundException {

        try {
            createFile("test1.txt");
            createFile("test2.txt");
            createFile("test3.txt");

            IDataSource dataSource = DataSource.parse("SELECT * FROM 'test*.txt' WHERE RowNumber=1");

            IResultSet result = dataSource.getResultSet(new ExecutionContext());
            Enumeration<IDataRow> rows = result.getRows();

            int rowCount = 0;

            while (rows.hasMoreElements()) {
                IDataRow dataRow = rows.nextElement();
                System.out.println(dataRow);
                rowCount++;
                Assert.assertEquals(1, ((Number) dataRow.get("RowNumber")).intValue());
                Assert.assertEquals(new File("test" + rowCount + ".txt").getAbsolutePath(), dataRow.get("FileName"));
            }


            Assert.assertEquals(3, rowCount);
        }
        finally {
            for (int i = 1; i < 5; i++) { new File("test" + i + ".txt").delete(); }
        }
    }

    @Test
    public void shouldFilterDataWithWhere() throws IOException, DataSourceException, ColumnNotFoundException, AmbiguousColumnNameException {

        File file = new File("test.txt");
        HelperFunctions.createTestFile(file);
        try {
            IDataSource dataSource = DataSource.parse("SELECT groupNumber, groupRowNumber, intCol, floatCol, dateCol, timestampCol from '" + file.getName() + "' TRANSFORM(TO_INT(STRTOK(Text, ',', 0)) AS groupNumber, STRTOK(Text, ',', 1) AS line, TO_INT(STRTOK(Text, ',', 2)) AS groupRowNumber, STRTOK(Text, ',', 3) AS intCol, STRTOK(Text, ',', 4) AS floatCol, STRTOK(Text, ',', 5) AS dateCol, STRTOK(Text, ',', 6) AS timestampCol) WHERE groupNumber=0 AND (groupRowNumber = 0 OR groupRowNumber = 1)");
            int rowCount = 0;
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            Enumeration<IDataRow> rows = resultSet.getRows();
            while (rows.hasMoreElements()) {

                IDataRow dataRow = rows.nextElement();
                Assert.assertEquals(rowCount, ((Number) dataRow.get("groupRowNumber")).intValue());
                rowCount++;
            }

            Assert.assertEquals(2, rowCount);
        }
        finally {
            file.delete();
        }
    }

    @Test
    public void throwExecptionIfConditionNotBoolean() throws IOException, DataSourceException {

        try {
            IDataSource dataSource = DataSource.parse("SELECT * FROM 'test*.txt' WHERE 1");
            Assert.fail("Expect exception");
        }
        catch (InvalidExpressionException ex) {
            Assert.assertEquals("[1, 32] Expression does not evaluate to a BOOLEAN result", ex.getMessage());
        }
    }

    private File createFile(final String p_fileName) throws IOException {

        File f = new File(p_fileName);
        try (FileWriter writer = new FileWriter(f)) {

            for (int i = 0; i < 1000; i++) {
                writer.write(f.getAbsolutePath() + " Line " + i + "\n");
            }
        }
        return f;
    }

}
