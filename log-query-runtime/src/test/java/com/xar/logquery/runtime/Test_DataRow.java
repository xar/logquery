/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime;

import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.exceptions.AmbiguousColumnNameException;
import com.xar.logquery.runtime.exceptions.ColumnNotFoundException;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by akoekemo on 2/24/15.
 */
public class Test_DataRow {

    @Test
    public void shouldFailForAmbiguousColumns() throws ColumnNotFoundException {

        IColumnInfo[] columns = new IColumnInfo[]{
                new ColumnInfo("alias1", "column1", DataType.STRING),
                new ColumnInfo("alias1", "column2", DataType.STRING),
                new ColumnInfo("alias1", "column3", DataType.STRING),
                new ColumnInfo("alias2", "column1", DataType.STRING),
                new ColumnInfo("alias2", "column2", DataType.STRING)
        };

        DataRow row = new DataRow(columns, new Object[]{"1", "2", "3", "4", "5"});

        try {
            row.get("column1");
            Assert.fail("Expected error");
        }
        catch (AmbiguousColumnNameException e) {
            Assert.assertEquals("Ambiguous column name - column1", e.getMessage());
        }
    }

    @Test
    public void shouldFailIfColumnCouldNotBeFound() throws AmbiguousColumnNameException {

        IColumnInfo[] columns = new IColumnInfo[]{
                new ColumnInfo("alias1", "column1", DataType.STRING),
                new ColumnInfo("alias1", "column2", DataType.STRING),
                new ColumnInfo("alias1", "column3", DataType.STRING),
                new ColumnInfo("alias2", "column1", DataType.STRING),
                new ColumnInfo("alias2", "column2", DataType.STRING)
        };

        DataRow row = new DataRow(columns, new Object[]{"1", "2", "3", "4", "5"});

        try {
            row.get("column7");
            Assert.fail("Expected error");
        }
        catch (ColumnNotFoundException e) {
            Assert.assertEquals("Column not found - ColumnName: column7", e.getMessage());
        }
    }

    @Test
    public void shouldGetColumnsByNameOnly() throws AmbiguousColumnNameException, ColumnNotFoundException {

        IColumnInfo[] columns = new IColumnInfo[]{
                new ColumnInfo("alias1", "column1", DataType.STRING),
                new ColumnInfo("alias1", "column2", DataType.STRING),
                new ColumnInfo("alias1", "column3", DataType.STRING),
                new ColumnInfo("alias2", "column1", DataType.STRING),
                new ColumnInfo("alias2", "column2", DataType.STRING),
                new ColumnInfo("alias2", "column4", DataType.STRING),
                new ColumnInfo("alias2", "column5", DataType.STRING),
                new ColumnInfo("alias2", "column6", DataType.STRING)
        };

        DataRow row = new DataRow(columns, new Object[]{"1", "2", "3", "4", "5", "6", "7", "8"});

        Assert.assertEquals("3", row.get("column3"));
        Assert.assertEquals("6", row.get("column4"));
        Assert.assertEquals("7", row.get("column5"));
        Assert.assertEquals("8", row.get("column6"));
    }

    @Test
    public void throwsExceptionIfColumnInfoAndDataDoesNotMatch() {

        try {
            IColumnInfo[] columns = new IColumnInfo[]{
                    new ColumnInfo("alias1", "column1", DataType.STRING),
                    new ColumnInfo("alias1", "column2", DataType.STRING),
                    new ColumnInfo("alias1", "column3", DataType.STRING),
                    new ColumnInfo("alias2", "column1", DataType.STRING),
                    new ColumnInfo("alias2", "column2", DataType.STRING)
            };

            DataRow row = new DataRow(columns, new Object[]{"1", "2", "3", "4"});

            Assert.fail("Expected error!");
        }
        catch (RuntimeException ex) {
            Assert.assertEquals("Invalid number of columns. Expected 5, but found 4", ex.getMessage());
        }
    }


}
