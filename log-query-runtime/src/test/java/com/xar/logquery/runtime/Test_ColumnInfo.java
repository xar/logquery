/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime;

import com.xar.logquery.runtime.ColumnInfo;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.interfaces.IAliasable;
import com.xar.logquery.runtime.parse.ParseData;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by akoekemo on 2/24/15.
 */
public class Test_ColumnInfo {

    @Test
    public void shouldImplementEquals() {

        ColumnInfo info1 = new ColumnInfo("alais", "name1", DataType.STRING);
        ColumnInfo info2 = new ColumnInfo("alais", "name1", DataType.STRING);
        ColumnInfo info3 = new ColumnInfo("alais", "name2", DataType.STRING);

        Assert.assertTrue(info1.equals(info2));
        Assert.assertFalse(info1.equals(info3));
    }

    @Test
    public void shouldCompare() {
        AliasableColumnInfo info1 = new AliasableColumnInfo(new MyIAliasable("alias"), "name", DataType.REAL);
        AliasableColumnInfo info2 = new AliasableColumnInfo(new MyIAliasable("alias"), "name", DataType.REAL);
        AliasableColumnInfo info3 = new AliasableColumnInfo(new MyIAliasable("a"), "name", DataType.REAL);

        Assert.assertTrue(info1.equals(info2));
        Assert.assertFalse(info1.equals(info3));

        ColumnInfo info4 = new ColumnInfo("alias", "name", DataType.REAL);
        ColumnInfo info5 = new ColumnInfo("alias", "name", DataType.REAL);
        ColumnInfo info6 = new ColumnInfo("a", "name", DataType.REAL);

        Assert.assertTrue(info4.equals(info5));
        Assert.assertFalse(info4.equals(info6));


    }


    private static class MyIAliasable implements IAliasable {

        private String m_alias;

        public MyIAliasable(final String p_alias) {

            m_alias = p_alias;
        }

        @Override
        public String getAlias() {

            return m_alias;
        }

        @Override
        public void setAlias(final String p_alias) {
            m_alias = p_alias;
        }
    }
}
