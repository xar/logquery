/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.functions.scalar.convert;

import com.xar.logquery.runtime.HelperFunctions;
import com.xar.logquery.runtime.config.RuntimeConfiguration;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.constant.ConstantExpression;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.expression.functions.exceptions.InvalidParametersException;
import com.xar.logquery.runtime.util.DateTimeFunctions;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Anton Koekemoer on 2014/11/23.
 */
public class TestFunction_TO_STRING {
    @Test
    public void shouldConvertNumberToString() throws InvalidExpressionException, InstantiationException, IllegalAccessException {

        Assert.assertEquals("10", HelperFunctions.executeFunction(ToStringFunction.class, DataType.STRING, 10));
        Assert.assertEquals("10", HelperFunctions.executeFunction(ToStringFunction.class, DataType.STRING, 10));
//        Assert.assertEquals(String.valueOf((new Float(10.12f)).doubleValue()), HelperFunctions.executeFunction(ToStringFunction.class, DataType.STRING, 10.12f));
        Assert.assertEquals("10.12", HelperFunctions.executeFunction(ToStringFunction.class, DataType.STRING, BigDecimal.valueOf(10.12d)));
        Assert.assertEquals("10.12", HelperFunctions.executeFunction(ToStringFunction.class, DataType.STRING, "10.12"));

        Assert.assertEquals("1,000.00", HelperFunctions.executeFunction(ToStringFunction.class, DataType.STRING, 1000, "###,###.00"));
        Assert.assertEquals("1,000", HelperFunctions.executeFunction(ToStringFunction.class, DataType.STRING, 1000, "###,###"));
        Assert.assertEquals("1,000.12", HelperFunctions.executeFunction(ToStringFunction.class, DataType.STRING, 1000.12f, "###,###.00"));
        Assert.assertEquals("1,000.12", HelperFunctions.executeFunction(ToStringFunction.class, DataType.STRING, 1000.12d, "###,###.00"));
        Assert.assertEquals("0.12", HelperFunctions.executeFunction(ToStringFunction.class, DataType.STRING, 0.12, "###,##0.00"));
        Assert.assertEquals(null, HelperFunctions.executeFunction(ToStringFunction.class, DataType.STRING, new ConstantExpression<Integer>(DataType.INTEGER, null)));


        Assert.assertEquals(DateTimeFunctions.toString(new Date(), "yyyy/MM/dd") + " 00:00", HelperFunctions.executeFunction(ToStringFunction.class, DataType.STRING, new ConstantExpression<>(DataType.DATE, new Date()), "yyyy/MM/dd HH:mm"));
        Assert.assertEquals(DateTimeFunctions.toString(new Date(), RuntimeConfiguration.getDateFormat()), HelperFunctions.executeFunction(ToStringFunction.class, DataType.STRING, new ConstantExpression<>(DataType.DATE, new Date())));

        Date date = new Date();
        Assert.assertEquals(DateTimeFunctions.toString(date, "yyyy/MM/dd HH:mm"), HelperFunctions.executeFunction(ToStringFunction.class, DataType.STRING, new ConstantExpression<>(DataType.TIMESTAMP, date), "yyyy/MM/dd HH:mm"));
        Assert.assertEquals(DateTimeFunctions.toString(date, RuntimeConfiguration.getTimestampFormat()), HelperFunctions.executeFunction(ToStringFunction.class, DataType.STRING, new ConstantExpression<>(DataType.TIMESTAMP, date)));

    }

//    public static void main(String[] args) throws UnconvertableValueException {
//        test(10.12d);
//    }
//
//    private static void test(final Object v) throws UnconvertableValueException {
//        System.out.println(ConvertFunctions.toReal(v));
//
//        System.out.println(new BigDecimal((Double)v));
//
//    }
}
