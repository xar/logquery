/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.functions.aggregate;

import com.xar.logquery.runtime.ColumnInfo;
import com.xar.logquery.runtime.HelperFunctions;
import com.xar.logquery.runtime.datasource.DualDataSource;
import com.xar.logquery.runtime.datasource.StaticDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.expression.ColumnReference;
import com.xar.logquery.runtime.expression.functions.FunctionParameter;
import com.xar.logquery.runtime.expression.functions.exceptions.InvalidParametersException;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Anton Koekemoer on 2014/11/23.
 */
public class TestAggregate_AVG {

    @Test
    public void shouldReturnTheAverageOfTheIntegerColumnValue() throws DataSourceException, InstantiationException, IllegalAccessException {

        IDataSource dataSource = new StaticDataSource(new IColumnInfo[]{
                new ColumnInfo("", "column", DataType.INTEGER)}, new Object[][]{
                new Object[]{1},
                new Object[]{1},
                new Object[]{2},
                new Object[]{2},
                new Object[]{3},
                new Object[]{3},
                new Object[]{4},
                new Object[]{4},
                new Object[]{5},
                new Object[]{5},
                new Object[]{null},
                new Object[]{null},
        });
        Assert.assertEquals(3d, HelperFunctions.executeAggregate(AverageFunction.class, DataType.INTEGER, dataSource, new FunctionParameter(new ColumnReference(new ColumnInfo("", "column", DataType.INTEGER)))));
    }

    @Test
    public void shouldThrowExceptionForParameterError() throws DataSourceException, InstantiationException, IllegalAccessException {

        try {
            HelperFunctions.executeAggregate(AverageFunction.class, DataType.INTEGER, new DualDataSource());
            Assert.fail("Expected parameter error");
        }
        catch (InvalidParametersException e) {
            Assert.assertTrue(e.getMessage(), e.getMessage().startsWith("Invalid parameters in function call"));
        }
    }
}
