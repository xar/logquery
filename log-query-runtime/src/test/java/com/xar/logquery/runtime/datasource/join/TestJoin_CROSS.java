/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource.join;

import com.xar.logquery.parser.exceptions.ParseException;
import com.xar.logquery.runtime.DataSource;
import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.exceptions.AmbiguousColumnNameException;
import com.xar.logquery.runtime.exceptions.ColumnNotFoundException;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.NoSuchElementException;

/**
 * Created by akoekemo on 3/16/15.
 */
public class TestJoin_CROSS {

    @Test
    public void returnsNoRowsIfLHSEmpty() throws IOException, DataSourceException {
        try (PrintWriter out = new PrintWriter("left.csv")) {
            out.println("Table,Row");
        }
        try (PrintWriter out = new PrintWriter("right.csv")) {
            out.println("Table,Row");
            out.println("right,1");
            out.println("right,2");
            out.println("right,3");
            out.println("right,4");
            out.println("right,5");
        }
        try {
            IDataSource dataSource = DataSource.parse("SELECT * FROM 'left.csv' USING CSV AS A CROSS JOIN 'right.csv' USING CSV AS B");
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            Enumeration<IDataRow> rows = resultSet.getRows();

            Assert.assertFalse(rows.hasMoreElements());

            try{
                rows.nextElement();
                Assert.fail("Expected exception");
            }
            catch (NoSuchElementException ex){

            }
        }
        finally {
            new File("left.csv").delete();
            new File("right.csv").delete();
        }
    }

    @Test
    public void returnsNoRowsIfRHSEmpty() throws IOException, DataSourceException {
        try (PrintWriter out = new PrintWriter("left.csv")) {
            out.println("Table,Row");
            out.println("left,1");
            out.println("left,2");
            out.println("left,3");
            out.println("left,4");
            out.println("left,5");
            out.println("left,6");
            out.println("left,7");
            out.println("left,8");
            out.println("left,9");
            out.println("left,10");
        }
        try (PrintWriter out = new PrintWriter("right.csv")) {
            out.println("Table,Row");
        }
        try {
            IDataSource dataSource = DataSource.parse("SELECT * FROM 'left.csv' USING CSV AS A CROSS JOIN 'right.csv' USING CSV AS B");
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            Enumeration<IDataRow> rows = resultSet.getRows();

            Assert.assertFalse(rows.hasMoreElements());
        }
        finally {
            new File("left.csv").delete();
            new File("right.csv").delete();
        }
    }

    @Test
    public void shouldCrossJoin() throws IOException, DataSourceException, AmbiguousColumnNameException, ColumnNotFoundException {

        try (PrintWriter out = new PrintWriter("left.csv")) {
            out.println("Table,Row");
            out.println("left,1");
            out.println("left,2");
            out.println("left,3");
            out.println("left,4");
            out.println("left,5");
            out.println("left,6");
            out.println("left,7");
            out.println("left,8");
            out.println("left,9");
            out.println("left,10");
        }
        try (PrintWriter out = new PrintWriter("right.csv")) {
            out.println("Table,Row");
            out.println("right,1");
            out.println("right,2");
            out.println("right,3");
            out.println("right,4");
            out.println("right,5");
        }
        try {
            IDataSource dataSource = DataSource.parse("SELECT * FROM 'left.csv' USING CSV AS A CROSS JOIN 'right.csv' USING CSV AS B");
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            Enumeration<IDataRow> rows = resultSet.getRows();
            int left = 1;
            int right = 1;
            int count = 0;

            while (rows.hasMoreElements()){
                IDataRow dataRow = rows.nextElement();

                if (right > 5) {left++; right = 1;}

                Assert.assertEquals("left", dataRow.get("A.Table"));
                Assert.assertEquals(""+left, dataRow.get("A.Row"));
                Assert.assertEquals("right", dataRow.get("B.Table"));
                Assert.assertEquals(""+right, dataRow.get("B.Row"));

                right++;
                System.out.println(dataRow);
                count++;
            }
            Assert.assertEquals(10, left);
            Assert.assertEquals(6, right);

            Assert.assertEquals(50, count);
        }
        finally {
            new File("left.csv").delete();
            new File("right.csv").delete();
        }
    }

    @Test
    public void throwsExceptionIfDatasourcesAreNotAliased() throws IOException, DataSourceException {

        try {
            IDataSource dataSource = DataSource.parse("SELECT * FROM 'left.csv' USING CSV CROSS JOIN 'right.csv' USING CSV AS B");
            Assert.fail("Expected error");
        }
        catch (ParseException ex){
            Assert.assertEquals("[1, 35] Datasource requires an alias", ex.getMessage());
        }
        try {
            IDataSource dataSource = DataSource.parse("SELECT * FROM 'left.csv' USING CSV AS A CROSS JOIN 'right.csv' USING CSV");
            Assert.fail("Expected error");
        }
        catch (ParseException ex){
            Assert.assertEquals("[1, 40] Datasource requires an alias", ex.getMessage());
        }
    }





}
