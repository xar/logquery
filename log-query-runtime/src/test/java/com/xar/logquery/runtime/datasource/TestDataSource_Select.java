/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource;

import com.xar.logquery.runtime.ColumnInfo;
import com.xar.logquery.runtime.DataSource;
import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.HelperFunctions;
import com.xar.logquery.runtime.ValidationContext;
import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IOrderable;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.exceptions.AmbiguousColumnNameException;
import com.xar.logquery.runtime.exceptions.ColumnNotFoundException;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.exceptions.DuplicateColumnNameException;
import com.xar.logquery.runtime.expression.OrderExpression;
import com.xar.logquery.runtime.expression.WildcardExpression;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;
import com.xar.logquery.runtime.parse.ParseData;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Enumeration;
import java.util.Map;

/**
 * Created by akoekemo on 3/1/15.
 */
public class TestDataSource_Select {

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    private static SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Test
    public void shouldPassOrderDataToInnerDatasource() {

        shouldPassOrderDataToInnerDatasource_DataSource inner = new shouldPassOrderDataToInnerDatasource_DataSource();
        SelectDataSource dataSource = new SelectDataSource(inner, new IExpression[0]);
        dataSource.setOrderBy(new OrderExpression[0]);
        Assert.assertNotNull(inner.orderBy);
    }

    @Test
    public void shouldSelectColumnsByAlias() throws IOException, DataSourceException, ColumnNotFoundException {

        File file = new File("test.txt");
        HelperFunctions.createTestFile(file);
        try {
            IDataSource dataSource = DataSource.parse("SELECT intCol, A.*, A.* from '" + file.getName() + "' TRANSFORM (TO_INT(STRTOK(Text, ',', 0)) AS groupNumber, STRTOK(Text, ',', 1) AS line, STRTOK(Text, ',', 2) AS groupRowNumber, STRTOK(Text, ',', 3) AS intCol, STRTOK(Text, ',', 4) AS floatCol, STRTOK(Text, ',', 5) AS dateCol, STRTOK(Text, ',', 6) AS timestampCol) as A");

            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            IColumnInfo[] columnInfo = resultSet.getColumnInfo();

            String names[] = new String[]{
                    "intCol",
                    "A.FileName",
                    "A.RowNumber",
                    "A.Text",
                    "A.groupNumber",
                    "A.line",
                    "A.groupRowNumber",
                    "A.intCol",
                    "A.floatCol",
                    "A.dateCol",
                    "A.timestampCol",
                    "A.FileName_1",
                    "A.RowNumber_1",
                    "A.Text_1",
                    "A.groupNumber_1",
                    "A.line_1",
                    "A.groupRowNumber_1",
                    "A.intCol_1",
                    "A.floatCol_1",
                    "A.dateCol_1",
                    "A.timestampCol_1"

            };

            for (int i = 0; i < names.length; i++) { Assert.assertEquals(names[i], columnInfo[i].getName()); }

        }
        finally {
            file.delete();
        }

    }

    /**
     * Test that the datasource returns all the rows from the input file, and that the data is returned correctly
     *
     * @throws IOException
     * @throws DataSourceException
     * @throws ColumnNotFoundException
     */
    @Test
    public void shouldSelectRowsFromDatasource() throws IOException, DataSourceException, ColumnNotFoundException, AmbiguousColumnNameException {

        int groupMod = 10;
        int lineNumber = 0;
        int group = 0;

        File file = new File("test.txt");
        HelperFunctions.createTestFile(file);
        try {
            IDataSource dataSource = DataSource.parse("SELECT * from '" + file.getName() + "'");
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());

            for (Enumeration<IDataRow> rows = resultSet.getRows(); rows.hasMoreElements(); ) {

                // Calculate the number of rows for the group. A group is not allowed to have
                // less that 10 rows, with an additional number of rows of the mod 10 of the group
                int rowCount = groupMod + (group % groupMod);
                int startInt = rowCount;

                LocalDate date = new LocalDate(2014, 1, 1);

                DateTime dateTime = new DateTime(date.toDate());

                for (int row = 0; row < rowCount; row++) {

                    IDataRow dataRow = rows.nextElement();

                    dateTime = dateTime.minusHours(1);
                    StringBuilder builder = new StringBuilder();
                    builder.append(String.valueOf(group)).append(',')
                            .append("Line ").append(lineNumber).append(',')
                            .append(String.valueOf(row)).append(',')
                            .append(String.valueOf(startInt--)).append(",")
                            .append(String.valueOf(((float) group) + (0.1 * row + 1))).append(",")
                            .append(dateFormat.format(date.toDate())).append(',')
                            .append(timeStampFormat.format(dateTime.toDate()));

                    Assert.assertEquals(builder.toString(), dataRow.get("Text"));
                    lineNumber++;
                    date = date.plusDays(1);
                }

                group++;
            }

            Assert.assertEquals(1450, lineNumber);
        }
        finally {
            file.delete();
        }
    }

    @Test
    public void shouldThrowExceptionForDuplicateColumnNames() throws IOException, DataSourceException {

        try {
            IDataSource dataSource = DataSource.parse("SELECT 10 as int, 20 as int, '30' as string, true as boolean FROM DUAL");
            Assert.fail("Expected error");
        }
        catch (DuplicateColumnNameException ex) {
            Assert.assertEquals("[1, 18] Duplicate column name - int", ex.getMessage());
        }
    }

    @Test
    public void wildCardShouldAddColumnsFromValidationContext() throws DataSourceException {

        final WildcardExpression wildcardExpression = new WildcardExpression("");
        wildcardExpression.setParseData(new ParseData(0, 0, ""));
        SelectDataSource dataSource = new SelectDataSource(new wildCardShouldAddColumnsFromValidationContextDataSource(), new IExpression[]{
                wildcardExpression});

        ValidationContext context = new ValidationContext();

        dataSource.validate(context);

        IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());

        IColumnInfo[] columns = resultSet.getColumnInfo();

        Assert.assertEquals("A.col1", columns[0].getName());
        Assert.assertEquals("A.col2", columns[1].getName());
        Assert.assertEquals("A.col3", columns[2].getName());
        Assert.assertEquals("A.col4", columns[3].getName());
        Assert.assertEquals("B.col1", columns[4].getName());
        Assert.assertEquals("B.col2", columns[5].getName());
        Assert.assertEquals("B.col3", columns[6].getName());
        Assert.assertEquals("B.col4", columns[7].getName());

        Assert.assertEquals(8, columns.length);

        Enumeration<IDataRow> rows = resultSet.getRows();

        Assert.assertTrue(rows.hasMoreElements());
        System.out.println(rows.nextElement());
    }

    @Test
    public void wildCardWithContextShouldAddColumnsFromValidationContext() throws DataSourceException {

        SelectDataSource dataSource = new SelectDataSource(new wildCardShouldAddColumnsFromValidationContextDataSource(), new IExpression[]{
                new WildcardExpression("B", new ParseData(0, 0, "")),
                new WildcardExpression("A", new ParseData(0, 0, "")),
                new WildcardExpression("A", new ParseData(0, 0, ""))});

        ValidationContext context = new ValidationContext();

        dataSource.validate(context);

        IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());

        IColumnInfo[] columns = resultSet.getColumnInfo();

        Assert.assertEquals("B.col1", columns[0].getName());
        Assert.assertEquals("B.col2", columns[1].getName());
        Assert.assertEquals("B.col3", columns[2].getName());
        Assert.assertEquals("B.col4", columns[3].getName());
        Assert.assertEquals("A.col1", columns[4].getName());
        Assert.assertEquals("A.col2", columns[5].getName());
        Assert.assertEquals("A.col3", columns[6].getName());
        Assert.assertEquals("A.col4", columns[7].getName());
        Assert.assertEquals("A.col1_1", columns[8].getName());
        Assert.assertEquals("A.col2_1", columns[9].getName());
        Assert.assertEquals("A.col3_1", columns[10].getName());
        Assert.assertEquals("A.col4_1", columns[11].getName());

        Assert.assertEquals(12, columns.length);

        Enumeration<IDataRow> rows = resultSet.getRows();

        Assert.assertTrue(rows.hasMoreElements());
        System.out.println(rows.nextElement());
    }

    public static class wildCardShouldAddColumnsFromValidationContextDataSource extends StaticDataSource {

        /**
         * Initialise a new Static datasource
         */
        public wildCardShouldAddColumnsFromValidationContextDataSource() {

            setColumnInfo(new IColumnInfo[]{
                    new ColumnInfo("A", "col1", DataType.INTEGER),
                    new ColumnInfo("A", "col2", DataType.INTEGER),
                    new ColumnInfo("A", "col3", DataType.INTEGER),
                    new ColumnInfo("A", "col4", DataType.INTEGER),
                    new ColumnInfo("B", "col1", DataType.INTEGER),
                    new ColumnInfo("B", "col2", DataType.INTEGER),
                    new ColumnInfo("B", "col3", DataType.INTEGER),
                    new ColumnInfo("B", "col4", DataType.INTEGER)
            });
            addRow(new Object[]{1, 1, 2, 3, 4, 5, 6, 7});
            addRow(new Object[]{2, 1, 2, 3, 4, 5, 6, 7});
            addRow(new Object[]{3, 1, 2, 3, 4, 5, 6, 7});
            addRow(new Object[]{4, 1, 2, 3, 4, 5, 6, 7});
        }


    }


    private class shouldPassOrderDataToInnerDatasource_DataSource implements IDataSource, IOrderable {

        private OrderExpression[] orderBy;

        /**
         * Open the data source and return a resultset.
         *
         * @param p_context the execution context
         *
         * @return a new resultset
         */
        @Override
        public IResultSet getResultSet(final IExecutionContext p_context) throws DataSourceException {

            return null;
        }

        /**
         * Set the attributes for the datasource. These are assigned to the datasource during execution
         *
         * @param p_attributes the attributes
         */
        @Override
        public void setAttributes(final Map<String, IExpression> p_attributes) {

        }

        @Override
        public void setOrderBy(final OrderExpression[] p_orderBy) {

            orderBy = p_orderBy;
        }

        /**
         * Bind the datasource to the execution context
         *
         * @param p_context the binding context
         *
         * @throws com.xar.logquery.runtime.exceptions.DataSourceException if the datasource is invalid
         */
        @Override
        public void validate(final IValidationContext p_context) throws DataSourceException {

        }
    }
}
