/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource;

import com.xar.logquery.runtime.ColumnInfo;
import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.expression.constant.IntegerConstant;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import org.junit.Assert;
import org.junit.Test;

import java.util.Enumeration;
import java.util.NoSuchElementException;

/**
 * Created by akoekemo on 3/9/15.
 */
public class TestDataSource_Expression {

    @Test
    public void shouldReturnValuesOfExpressions() throws DataSourceException {

        ExpressionDataSource dataSource = new ExpressionDataSource(new IColumnInfo[]{
                new ColumnInfo("A", "col1", DataType.INTEGER), new ColumnInfo("A", "col2", DataType.INTEGER)},
                new IExpression[][]{
                        new IExpression[]{new IntegerConstant(11), new IntegerConstant(12)},
                        new IExpression[]{new IntegerConstant(21), new IntegerConstant(22)},
                        new IExpression[]{new IntegerConstant(31), new IntegerConstant(32)},
                        new IExpression[]{new IntegerConstant(41), new IntegerConstant(42)},
                });

        IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
        Enumeration<IDataRow> rows = resultSet.getRows();
        int row = 1;
        while (rows.hasMoreElements()) {
            IDataRow dataRow = rows.nextElement();
            Assert.assertEquals((row * 10) + 1, ((Number) dataRow.get(0)).intValue());
            Assert.assertEquals((row * 10) + 2, ((Number) dataRow.get(1)).intValue());
            row++;
        }
        Assert.assertEquals(5, row);
        try{
            rows.nextElement();
            Assert.fail("Expected exception");
        }
        catch (NoSuchElementException e){}
    }

}
