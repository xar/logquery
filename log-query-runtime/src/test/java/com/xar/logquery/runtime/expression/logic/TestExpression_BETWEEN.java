/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.logic;

import com.xar.logquery.runtime.DataSource;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.exceptions.ColumnNotFoundException;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by akoekemo on 3/5/15.
 */
public class TestExpression_BETWEEN {

    @Test
    public void shouldCompare_BETWEEN() throws IOException, DataSourceException {
        TestCompare.testCompare("1 BETWEEN 0 AND 2", true);
        TestCompare.testCompare("1 BETWEEN 1 AND 2", true);
        TestCompare.testCompare("1 BETWEEN 0 AND 1", true);
        TestCompare.testCompare("1 BETWEEN 2 AND 3", false);
        TestCompare.testCompare("'1' BETWEEN '0' AND '2'", true);
        TestCompare.testCompare("'1' BETWEEN '1' AND '2'", true);

    }

    @Test
    public void betweenFilterThrowsExceptionFOrIncompatibleTypes() throws IOException, DataSourceException, ColumnNotFoundException {

        try {
            IDataSource dataSource = DataSource.parse("SELECT * FROM 'file' WHERE RowNumber BETWEEN '10' AND 100");
            Assert.fail("Expected error");
        }
        catch (Exception ex) {
            Assert.assertEquals("[1, 45] Incompatible operands INTEGER and STRING", ex.getMessage());
        }
        try {
            IDataSource dataSource = DataSource.parse("SELECT * FROM 'file' WHERE RowNumber BETWEEN 10 AND '100'");
            Assert.fail("Expected error");
        }
        catch (Exception ex) {
            Assert.assertEquals("[1, 52] Incompatible operands INTEGER and STRING", ex.getMessage());
        }
    }

}
