/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource;

import com.xar.logquery.runtime.ColumnInfo;
import com.xar.logquery.runtime.ValidationContext;
import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.parse.ParseData;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by akoekemo on 3/2/15.
 */
public class TestStaticDataSource {

    @Test
    public void shouldConstructWithColumnIOnfoAndData() throws DataSourceException {

        final IColumnInfo[] columnInfo = {
                new ColumnInfo("A", "col1", DataType.REAL),
                new ColumnInfo("A", "col2", DataType.REAL),
                new ColumnInfo("A", "col3", DataType.REAL)
        };
        final List<Object[]> data = new ArrayList<>();
        data.add(new Object[]{1, 2, 3});
        data.add(new Object[]{1, 2, 3});
        data.add(new Object[]{1, 2, 3});


        StaticDataSource dataSource = new StaticDataSource(columnInfo, data);
        dataSource.setColumnInfo(columnInfo);
        dataSource.addRow(new Object[]{1, 2, 3});
        dataSource.addRow(new Object[]{1, 2, 3});
        dataSource.addRow(new Object[]{1, 2, 3});

        dataSource.validate(new ValidationContext());

        IColumnInfo[] info = dataSource.getColumnInfo();

        Assert.assertEquals(columnInfo.length, info.length);
        for (int i = 0; i < info.length; i++) {
            Assert.assertEquals(columnInfo[i].getName(), info[i].getName());

        }
    }

    @Test
    public void shouldReturnColumnInfoAssigned() throws DataSourceException {

        StaticDataSource dataSource = new StaticDataSource();
        final IColumnInfo[] columnInfo = {
                new ColumnInfo("A", "col1", DataType.REAL),
                new ColumnInfo("A", "col2", DataType.REAL),
                new ColumnInfo("A", "col3", DataType.REAL)
        };
        dataSource.setColumnInfo(columnInfo);
        dataSource.addRow(new Object[]{1, 2, 3});
        dataSource.addRow(new Object[]{1, 2, 3});
        dataSource.addRow(new Object[]{1, 2, 3});

        dataSource.validate(new ValidationContext());

        IColumnInfo[] info = dataSource.getColumnInfo();

        Assert.assertEquals(columnInfo.length, info.length);
        for (int i = 0; i < info.length; i++) {
            Assert.assertEquals(columnInfo[i].getName(), info[i].getName());

        }

    }


}
