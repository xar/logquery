/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.logic;

import com.xar.logquery.runtime.ColumnInfo;
import com.xar.logquery.runtime.DataSource;
import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.HelperFunctions;
import com.xar.logquery.runtime.ValidationContext;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.exceptions.AmbiguousColumnNameException;
import com.xar.logquery.runtime.exceptions.ColumnNotFoundException;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.expression.ColumnReference;
import com.xar.logquery.runtime.expression.constant.IntegerConstant;
import com.xar.logquery.runtime.expression.constant.StringConstant;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;

/**
 * Created by akoekemo on 2/17/15.
 */
public class TestCompareLIKE {

    @Test
    public void compareIsOfTypeBOOLEAN() {

        IExpression expression = new CompareLIKE(new StringConstant(""), new StringConstant(""), new StringConstant(""));
        Assert.assertEquals(DataType.BOOLEAN, expression.getDataType());
    }

    @Test
    public void likeAcceptsOnlyStrings() {

        try {
            new CompareLIKE(new IntegerConstant(100), new StringConstant(""), new StringConstant("")).validate(new ValidationContext());
            Assert.fail("Expected error");
        }
        catch (InvalidExpressionException e) {
            Assert.assertEquals(e.getMessage(), "Expected value expression of type STRING", e.getMessage());
        }
        try {
            new CompareLIKE(new StringConstant(""), new IntegerConstant(100), new StringConstant("")).validate(new ValidationContext());
            Assert.fail("Expected error");
        }
        catch (InvalidExpressionException ex) {
            Assert.assertEquals(ex.getMessage(), "Expected pattern expression of type STRING", ex.getMessage());
        }
        try {
            new CompareLIKE(new StringConstant(""), new StringConstant(""), new IntegerConstant(100)).validate(new ValidationContext());
            Assert.fail("Expected error");
        }
        catch (InvalidExpressionException ex) {
            Assert.assertEquals(ex.getMessage(), "Expected escape expression of type STRING", ex.getMessage());
        }


    }

    @Test
    public void shouldCompareLike() throws DataSourceException, IOException, ColumnNotFoundException, AmbiguousColumnNameException {


        testLike("SELECT * from 'test.txt' TRANSFORM( IIF(RowNumber%2=0, '%', '')+Text AS Escaped) WHERE Escaped LIKE '\\%%5,Line _1%'", "^.*5,Line .1.*$");
        testLike("SELECT * from 'test.txt' TRANSFORM( IIF(RowNumber%2=0, '%', '')+Text AS Escaped) WHERE Escaped LIKE '+%%5,Line _1%' ESCAPE '+'", "^.*5,Line .1.*$");


        File file = new File("test.txt");
        HelperFunctions.createTestFile(file);
        try {
            int i = 0;
            IDataSource dataSource = DataSource.parse("SELECT * from 'test.txt' TRANSFORM( IIF(RowNumber%2=0, '%', '')+Text AS Escaped) WHERE Escaped LIKE '+%%5,Line ' + TO_STRING(RowNumber-1) + '%' ESCAPE '+'");
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            for (Enumeration<IDataRow> rows = resultSet.getRows(); rows.hasMoreElements(); ) {
                IDataRow dataRow = rows.nextElement();
                Assert.assertTrue(((String) dataRow.get("Escaped")).matches("^%.*5,Line " + (((Number) dataRow.get("RowNumber")).intValue() - 1) + ".*$"));
                i++;
            }
            Assert.assertEquals(true, i > 0);
        }
        finally {
            file.delete();
        }


    }

    @Test
    public void shouldFunctionWithConstantParameters() throws InvalidExpressionException {

        testLikeConstant("start%end", "^start.*end$", "start some text end", "\\", true);
        testLikeConstant("sstart%end", "^sstart.*end$", "start some text end", "\\", false);
        testLikeConstant("start%", "^start.*$", "start some text end", "\\", true);
        testLikeConstant("%end", "^.*end$", "start some text end", "\\", true);

        testLikeConstant("s_a_t%", "^s.a.t.*$", "start some text end", "\\", true);
        testLikeConstant("s\\_a_t%", "^s_a.t.*$", "s_art some text end", "\\", true);

        testLikeConstant("[start]%", "^\\[start\\].*$", "[start] some text end", "\\", true);
        testLikeConstant("(start)%", "^\\(start\\).*$", "(start) some text end", "\\", true);
        testLikeConstant("{start}%", "^\\{start\\}.*$", "{start} some text end", "\\", true);
        testLikeConstant(".start.%", "^\\.start\\..*$", ".start. some text end", "\\", true);
        testLikeConstant(".start.%", "^\\.start\\..*$", "xstartx some text end", "\\", false);
        testLikeConstant("+start+%", "^\\+start\\+.*$", "+start+ some text end", "\\", true);
        testLikeConstant("*start*%", "^\\*start\\*.*$", "*start* some text end", "\\", true);

    }

    @Test
    public void shouldFunctionWithDynamicParameters() throws InvalidExpressionException {

        testLikeDynamic("start%end", "^start.*end$", "start some text end", "\\", true);
        testLikeDynamic("sstart%end", "^sstart.*end$", "start some text end", "\\", false);
        testLikeDynamic("start%", "^start.*$", "start some text end", "\\", true);
        testLikeDynamic("%end", "^.*end$", "start some text end", "\\", true);

        testLikeDynamic("s_a_t%", "^s.a.t.*$", "start some text end", "\\", true);
        testLikeDynamic("s\\_a_t%", "^s_a.t.*$", "s_art some text end", "\\", true);

        testLikeDynamic("[start]%", "^\\[start\\].*$", "[start] some text end", "\\", true);
        testLikeDynamic("(start)%", "^\\(start\\).*$", "(start) some text end", "\\", true);
        testLikeDynamic("{start}%", "^\\{start\\}.*$", "{start} some text end", "\\", true);
        testLikeDynamic(".start.%", "^\\.start\\..*$", ".start. some text end", "\\", true);
        testLikeDynamic(".start.%", "^\\.start\\..*$", "xstartx some text end", "\\", false);
        testLikeDynamic("+start+%", "^\\+start\\+.*$", "+start+ some text end", "\\", true);
        testLikeDynamic("*start*%", "^\\*start\\*.*$", "*start* some text end", "\\", true);

    }

    private void testLike(final String p_sql, final String p_regex) throws IOException, DataSourceException, AmbiguousColumnNameException, ColumnNotFoundException {

        File file = new File("test.txt");
        HelperFunctions.createTestFile(file);
        try {
            int i = 0;
            IDataSource dataSource = DataSource.parse(p_sql);
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            for (Enumeration<IDataRow> rows = resultSet.getRows(); rows.hasMoreElements(); ) {
                IDataRow dataRow = rows.nextElement();
                Assert.assertTrue(((String) dataRow.get("Text")).matches(p_regex));
                i++;
            }
            Assert.assertEquals(true, i > 0);
        }
        finally {
            file.delete();
        }

    }

    private void testLikeConstant(final String p_pattern, String p_regexPattern, final String p_value, String p_escape, final boolean p_result) throws InvalidExpressionException {

        IExpression expression = new CompareLIKE(new StringConstant(p_value), new StringConstant(p_pattern), new StringConstant(p_escape));
        expression.validate(new ValidationContext());

        Assert.assertEquals(p_result, expression.getValue(new ExecutionContext()));
    }

    private void testLikeDynamic(final String p_pattern, String p_regexPattern, final String p_value, String p_escape, final boolean p_result) throws InvalidExpressionException {

        final ExecutionContext context = new ExecutionContext();

        context.setValue("", "pattern", p_pattern);
        context.setValue("", "escape", p_escape);

        IExpression expression1 = new CompareLIKE(new StringConstant(p_value), new ColumnReference(new ColumnInfo("", "pattern", DataType.STRING)), new StringConstant(p_escape));
        expression1.validate(new ValidationContext());
        Assert.assertEquals(p_result, expression1.getValue(context));

        IExpression expression2 = new CompareLIKE(new StringConstant(p_value), new StringConstant(p_pattern), new ColumnReference(new ColumnInfo("", "escape", DataType.STRING)));

        Assert.assertEquals(p_result, expression2.getValue(context));

        IExpression expression3 = new CompareLIKE(new StringConstant(p_value), new ColumnReference(new ColumnInfo("", "pattern", DataType.STRING)), new ColumnReference(new ColumnInfo("", "escape", DataType.STRING)));

        Assert.assertEquals(p_result, expression3.getValue(context));

    }


}
