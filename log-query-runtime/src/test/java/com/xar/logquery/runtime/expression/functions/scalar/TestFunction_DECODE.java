/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.functions.scalar;

import com.xar.logquery.runtime.HelperFunctions;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.expression.functions.exceptions.InvalidParametersException;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

/**
 *
 */
public class TestFunction_DECODE {

    @Test
    public void shouldSelectOptions() throws InvalidExpressionException, InstantiationException, IllegalAccessException {

        Assert.assertEquals(BigDecimal.valueOf(5), HelperFunctions.executeFunction(DecodeFunction.class, DataType.REAL, 10, 1, 10d, 2, 2f, 3, 3, 4, 4l, 5));
        Assert.assertEquals(BigDecimal.valueOf(10.0), HelperFunctions.executeFunction(DecodeFunction.class, DataType.REAL, 1, 1, 10d, 2, 2f, 3, 3, 4, 4l, 5));
        Assert.assertEquals(BigDecimal.valueOf(2.0), HelperFunctions.executeFunction(DecodeFunction.class, DataType.REAL, 2, 1, 10d, 2, 2f, 3, 3, 4, 4l, 5));
        Assert.assertEquals(BigDecimal.valueOf(3), HelperFunctions.executeFunction(DecodeFunction.class, DataType.REAL, 3, 1, 10d, 2, 2f, 3, 3, 4, 4l, 5));
        Assert.assertEquals(BigDecimal.valueOf(4), HelperFunctions.executeFunction(DecodeFunction.class, DataType.REAL, 4, 1, 10d, 2, 2f, 3, 3, 4, 4l, 5));
    }

    @Test
    public void shouldReturnNullIfNoOptionsAvailable() throws InvalidExpressionException, InstantiationException, IllegalAccessException {

        Assert.assertNull(HelperFunctions.executeFunction(DecodeFunction.class, DataType.REAL, 10, 1, 10d, 2, 2f, 3, 3, 4, 4l));
    }

    @Test
    public void shouldThrowExceptionIfIcorrectNumberOfParameters() throws InstantiationException, IllegalAccessException {
        DecodeFunction f = new DecodeFunction();
        try {
            Assert.assertNull(HelperFunctions.executeFunction(DecodeFunction.class, DataType.REAL, 10));

            Assert.fail("Expected error");
        }
        catch (InvalidExpressionException e) {
            Assert.assertTrue(e.getMessage(), e.getMessage().startsWith("DECODE - Invalid function parameters, requires at least 3 parameters"));
        }
    }

    @Test
    public void shouldThrowExceptionIfIncompatibleOptionTypes() throws InstantiationException, IllegalAccessException {
        try {
            HelperFunctions.executeFunction(DecodeFunction.class, DataType.REAL, 10, "1", 10d, 2, 2f, 3, 3, 4, 4l, 5);

            Assert.fail("Expected error");
        }
        catch (InvalidExpressionException e) {
            Assert.assertTrue(e.getMessage(), e.getMessage().startsWith("DECODE - Invalid function parameters, unable to cast STRING to INTEGER"));
        }
    }

    @Test
    public void shouldThrowExceptionIfIncompatibleValueTypes() throws InstantiationException, IllegalAccessException {
        try {
            HelperFunctions.executeFunction(DecodeFunction.class, DataType.REAL, 10, 1, "10d", 2, 2, 3, 3, 4, 4l, 5);

            Assert.fail("Expected error");
        }
        catch (InvalidExpressionException e) {
            Assert.assertTrue(e.getMessage(), e.getMessage().startsWith("DECODE - Invalid function parameters, unable to cast INTEGER to STRING"));
        }

    }
    @Test
    public void shouldThrowExceptionIfIncompatibleDefaultValueTypes() throws InstantiationException, IllegalAccessException {
        try {
            HelperFunctions.executeFunction(DecodeFunction.class, DataType.REAL, 10, 1, 10d, 2, 2, 3, 3, 4, 4l, "5");

            Assert.fail("Expected error");
        }
        catch (InvalidExpressionException e) {
            Assert.assertTrue(e.getMessage(), e.getMessage().startsWith("DECODE - Invalid function parameters, unable to cast STRING to REAL"));
        }

    }

}
