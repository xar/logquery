/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource;

import com.xar.logquery.runtime.DataSource;
import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.exceptions.AmbiguousColumnNameException;
import com.xar.logquery.runtime.exceptions.ColumnNotFoundException;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

/**
 * Created by akoekemo on 4/25/15.
 */
public class TestDataSource_DISTINCT {

    @Test
    public void shouldReturnDistinctRows() throws IOException, DataSourceException {

        try (PrintWriter out = new PrintWriter("test.txt")) {
            out.println("10");
            out.println("10");
            out.println("20");
            out.println("20");
            out.println("30");
            out.println("30");
            out.println("40");
            out.println("40");
            out.println("50");
            out.println("50");
        }
        try {
            int value = 10;
            IDataSource dataSource = DataSource.parse("SELECT DISTINCT Text from 'test.txt' ORDER BY Text");
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            for (Enumeration<IDataRow> rows = resultSet.getRows(); rows.hasMoreElements(); ) {
                IDataRow dataRow = rows.nextElement();
                Assert.assertEquals(""+value, dataRow.get("Text"));
                value += 10;
                System.out.println(dataRow);
            }
            Assert.assertEquals(60, value);
        }
        catch (ColumnNotFoundException e) {
            e.printStackTrace();
        }
        catch (AmbiguousColumnNameException e) {
            e.printStackTrace();
        }
        finally{
            new File("test.txt").delete();
        }
    }


}
