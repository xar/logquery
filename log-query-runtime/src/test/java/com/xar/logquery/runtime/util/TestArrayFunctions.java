/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.util;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by akoekemo on 3/15/15.
 */
public class TestArrayFunctions {

    @Test
    public void returnsArrayIfSizeToSameSize() {

        Integer[] a = new Integer[]{0, 1, 2, 3, 4};
        Assert.assertTrue(a == ArrayFunctions.aSize(a, a.length));
    }

    @Test
    public void shouldAddElementsTOArrays() {

        Integer[] a = new Integer[]{0, 1, 2};
        a = ArrayFunctions.aAdd(a, 3);
        Assert.assertEquals(4, a.length);
        for (int i = 0; i < a.length; i++) {
            Assert.assertEquals(i, a[i].intValue());
        }
    }

    @Test
    public void shouldDeleteElementsFromArrays() {

        Integer[] a = new Integer[]{0, 1, 3, 4, 2};
        a = ArrayFunctions.aDel(a, 2, 2);
        Assert.assertEquals(3, a.length);
        for (int i = 0; i < a.length; i++) {
            Assert.assertEquals(i, a[i].intValue());
        }

    }

    @Test
    public void shouldEnlargeArrays() {

        Integer[] a = new Integer[]{0, 1, 2, 3, 4};

        a = ArrayFunctions.aSize(a, a.length + 2);
        Assert.assertEquals(7, a.length);
        for (int i = 0; i < a.length - 2; i++) {
            Assert.assertEquals(i, a[i].intValue());
        }

        for (int i = a.length - 2; i < a.length; i++) {
            Assert.assertNull(a[i]);
        }

    }

    @Test
    public void delThrowsArrayIndexOutOfBounds() {

        Integer[] a = new Integer[]{0, 1, 3, 4, 2};
        try {
            a = ArrayFunctions.aDel(a, 6, 2);

            Assert.fail("Expected exception");
        }
        catch (ArrayIndexOutOfBoundsException ex){

        }
    }

    @Test
    public void shouldReturnSubArray() {

        Integer[] a = new Integer[]{0, 1, 2, 3, 4, 5, 6};

        a = ArrayFunctions.subArray(a, 2);
        Assert.assertEquals(5, a.length);
        for (int i = 0; i < a.length - 2; i++) {
            Assert.assertEquals(i + 2, a[i].intValue());
        }
    }

    @Test
    public void shouldShrinkArrays() {

        Integer[] a = new Integer[]{0, 1, 2, 3, 4, 5, 6};

        a = ArrayFunctions.aSize(a, a.length - 2);
        Assert.assertEquals(5, a.length);
        for (int i = 0; i < a.length - 2; i++) {
            Assert.assertEquals(i, a[i].intValue());
        }

    }


}
