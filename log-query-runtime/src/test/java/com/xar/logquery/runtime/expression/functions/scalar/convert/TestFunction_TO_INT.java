/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.functions.scalar.convert;

import com.xar.logquery.runtime.HelperFunctions;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.constant.BooleanConstant;
import com.xar.logquery.runtime.expression.constant.ConstantExpression;
import com.xar.logquery.runtime.expression.constant.StringConstant;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigInteger;

/**
 * Created by Anton Koekemoer on 2014/11/23.
 */
public class TestFunction_TO_INT {
    @Test
    public void shouldConvertNumbersToInt() throws InvalidExpressionException, InstantiationException, IllegalAccessException {

        Assert.assertEquals(BigInteger.valueOf(10), HelperFunctions.executeFunction(ToIntFunction.class, DataType.INTEGER, 10));
        Assert.assertEquals(BigInteger.valueOf(10), HelperFunctions.executeFunction(ToIntFunction.class, DataType.INTEGER, 10L));
        Assert.assertEquals(BigInteger.valueOf(10), HelperFunctions.executeFunction(ToIntFunction.class, DataType.INTEGER, 10.12f));
        Assert.assertEquals(BigInteger.valueOf(10), HelperFunctions.executeFunction(ToIntFunction.class, DataType.INTEGER, 10.12d));
        Assert.assertEquals(BigInteger.valueOf(10), HelperFunctions.executeFunction(ToIntFunction.class, DataType.INTEGER, "10.12"));
        Assert.assertEquals(null, HelperFunctions.executeFunction(ToIntFunction.class, DataType.INTEGER, new StringConstant(null)));
        Assert.assertEquals(null, HelperFunctions.executeFunction(ToIntFunction.class, DataType.INTEGER, new ConstantExpression<Integer>(DataType.INTEGER, null)));
        Assert.assertEquals(null, HelperFunctions.executeFunction(ToIntFunction.class, DataType.INTEGER, BooleanConstant.TRUE));

    }


}
