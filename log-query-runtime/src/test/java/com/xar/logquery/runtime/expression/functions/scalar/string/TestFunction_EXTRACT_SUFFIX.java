/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.functions.scalar.string;

import com.xar.logquery.runtime.HelperFunctions;
import com.xar.logquery.runtime.expression.constant.StringConstant;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.expression.functions.exceptions.InvalidParametersException;
import org.junit.Assert;
import org.junit.Test;

import static com.xar.logquery.runtime.enums.DataType.STRING;

/**
 * Created by Anton Koekemoer on 2014/11/22.
 */
public class TestFunction_EXTRACT_SUFFIX {
    @Test
    public void extractSuffix() throws IllegalAccessException, InvalidExpressionException, InstantiationException {

        String inputString = "01,02,03,04,05,06,07,08,09";
        Assert.assertEquals("02,03,04,05,06,07,08,09", HelperFunctions.executeFunction(ExtractSuffixFunction.class, STRING, inputString, 0, ","));
        Assert.assertEquals("03,04,05,06,07,08,09", HelperFunctions.executeFunction( ExtractSuffixFunction.class, STRING,  inputString, 1, ","));
        Assert.assertEquals("04,05,06,07,08,09", HelperFunctions.executeFunction( ExtractSuffixFunction.class, STRING,  inputString, 2, ","));
        Assert.assertEquals("05,06,07,08,09", HelperFunctions.executeFunction(ExtractSuffixFunction.class, STRING, inputString, 3, ","));
        Assert.assertEquals("06,07,08,09", HelperFunctions.executeFunction( ExtractSuffixFunction.class, STRING,  inputString, 4, ","));
        Assert.assertEquals("07,08,09", HelperFunctions.executeFunction( ExtractSuffixFunction.class, STRING,  inputString, 5, ","));
        Assert.assertEquals("08,09", HelperFunctions.executeFunction( ExtractSuffixFunction.class, STRING,  inputString, 6, ","));
        Assert.assertEquals("09", HelperFunctions.executeFunction( ExtractSuffixFunction.class, STRING,  inputString, 7, ","));
        Assert.assertEquals(null, HelperFunctions.executeFunction( ExtractSuffixFunction.class, STRING,  inputString, 8, ","));
        Assert.assertEquals(null, HelperFunctions.executeFunction( ExtractSuffixFunction.class, STRING,  new StringConstant(null), 8, ","));
        Assert.assertEquals("02,03,04,05,06,07,08,09", HelperFunctions.executeFunction( ExtractSuffixFunction.class, STRING,  inputString, -8, ","));
        Assert.assertEquals("03,04,05,06,07,08,09", HelperFunctions.executeFunction( ExtractSuffixFunction.class, STRING,  inputString, -7, ","));
        Assert.assertEquals("04,05,06,07,08,09", HelperFunctions.executeFunction( ExtractSuffixFunction.class, STRING,  inputString, -6, ","));
        Assert.assertEquals("05,06,07,08,09", HelperFunctions.executeFunction( ExtractSuffixFunction.class, STRING,  inputString, -5, ","));
        Assert.assertEquals("06,07,08,09", HelperFunctions.executeFunction( ExtractSuffixFunction.class, STRING,  inputString, -4, ","));
        Assert.assertEquals("07,08,09", HelperFunctions.executeFunction( ExtractSuffixFunction.class, STRING,  inputString, -3, ","));
        Assert.assertEquals("08,09", HelperFunctions.executeFunction( ExtractSuffixFunction.class, STRING,  inputString, -2, ","));
        Assert.assertEquals("09", HelperFunctions.executeFunction( ExtractSuffixFunction.class, STRING,  inputString, -1, ","));
        Assert.assertEquals(null, HelperFunctions.executeFunction( ExtractSuffixFunction.class, STRING,  inputString, -9, ","));
        Assert.assertEquals(null, HelperFunctions.executeFunction( ExtractSuffixFunction.class, STRING,  new StringConstant(null), -9, ","));
    }
}
