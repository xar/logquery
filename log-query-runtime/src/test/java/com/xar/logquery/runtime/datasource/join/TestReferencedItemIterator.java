/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource.join;

import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

/**
 *
 */
public class TestReferencedItemIterator {

    @Test
    public void test() {

        List<Integer> unref = new LinkedList<>();
        List<Integer> ref = new LinkedList<>();

        for (int i = 0; i < 10000; i++) {
            unref.add(i);
        }


        ReferencedItemIterator<Integer> it = new ReferencedItemIterator<>(unref, ref);
        int index = 0;
        while (it.hasNext()) {
            int value = it.next().intValue();
            Assert.assertEquals(index, value);
            if ((value % 2) == 0) { it.referenced(); }
            index++;
        }
        index = 1;
        Assert.assertEquals(5000, unref.size());
        for (Integer value : unref) {

            Assert.assertEquals(index, value.intValue());
            index += 2;
        }
        Assert.assertEquals(10001, index);
        index = 0;
        Assert.assertEquals(5000, ref.size());
        for (Integer value : ref) {

            Assert.assertEquals(index, value.intValue());
            index += 2;
        }
        Assert.assertEquals(10000, index);
    }


}
