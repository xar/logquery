/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression;

import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.ValidationContext;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.arithmitic.ModulusExpression;
import com.xar.logquery.runtime.expression.constant.ConstantExpression;
import com.xar.logquery.runtime.expression.constant.RealConstant;
import com.xar.logquery.runtime.expression.constant.IntegerConstant;
import com.xar.logquery.runtime.expression.constant.StringConstant;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.expression.exceptions.InvalidOperandsException;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * Created by koekemoera on 2014/11/13.
 */
public class TestExpression_MOD {

    @Test
    public void shouldModValues() throws InvalidExpressionException {
        testExpression(new ModulusExpression(new IntegerConstant(11), new IntegerConstant(2)), BigDecimal.class, BigDecimal.valueOf(1l));
        testExpression(new ModulusExpression(new IntegerConstant(11), new RealConstant(2d)), BigDecimal.class, BigDecimal.valueOf(1));


        testExpression(new ModulusExpression(new RealConstant(11), new RealConstant(2)), BigDecimal.class, BigDecimal.valueOf(1));
        testExpression(new ModulusExpression(new RealConstant(11), new IntegerConstant(2)), BigDecimal.class, BigDecimal.valueOf(1));
    }

    @Test
    public void NullModAnythingIsNull() throws InvalidExpressionException {

        final ModulusExpression modulusExpression = new ModulusExpression(new RealConstant(10), new ConstantExpression<Long>(DataType.INTEGER, null));
        modulusExpression.validate(new ValidationContext());
        Assert.assertNull(modulusExpression.getValue(new ExecutionContext()));
    }

    @Test
    public void addShouldThrowExceptionForInvalidOperands(){

        try {
            testExpression(new ModulusExpression(new StringConstant("10"), new IntegerConstant(2)), String.class, "102");
            Assert.fail("Expect error");
        }
        catch (InvalidExpressionException e) {
            Assert.assertEquals("Incompatible operands STRING and INTEGER", e.getMessage());
        }

    }

    private void testExpression(final IExpression p_expression, final Class<?> p_resultType, final Object p_expectedValue) throws InvalidExpressionException {
        p_expression.validate(new ValidationContext());
        Object value = p_expression.getValue(new ExecutionContext());

        Assert.assertEquals(p_resultType, value.getClass());
        Assert.assertEquals(p_expectedValue, value);


    }


}
