/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime;

import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.expression.exceptions.InvalidOperandsException;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Enumeration;

/**
 * Created by akoekemo on 2/27/15.
 */
public class TestArithmetic {

    @Test
    public void shouldAddValues() throws IOException, DataSourceException {

        testArithmetic("10+5", new BigInteger("15"));
        testArithmetic("10.5+5", new BigDecimal(15.5));
        testArithmetic("'10'+'5'", "105");

        // TODO: Implement add for String + anything else

    }

    @Test
    public void shouldDivideValues() throws IOException, DataSourceException {

        testArithmetic("10/5", new BigDecimal("2"));
    }

    @Test
    public void shouldModulusValues() throws IOException, DataSourceException {

        testArithmetic("10%5", new BigDecimal("0"));
        testArithmetic("10%6", new BigDecimal("4"));
    }

    @Test
    public void shouldMultiplyValues() throws IOException, DataSourceException {

        testArithmetic("10*5", new BigInteger("50"));
    }

    @Test
    public void shouldSubtractValues() throws IOException, DataSourceException {

        testArithmetic("10-5", new BigInteger("5"));
    }

    @Test
    public void shouldThrowExceptionForIncompatibleTypes() throws IOException, DataSourceException {

        try {
            testArithmetic("10/'5'", 2.0);
            Assert.fail("Expected error");
        }
        catch (InvalidOperandsException e) {
            Assert.assertEquals("[1, 7] Incompatible operands INTEGER and STRING", e.getMessage());
        }
    }

    private void testArithmetic(String p_expression, Object p_expected) throws IOException, DataSourceException {

        IDataSource dataSource = DataSource.parse("SELECT " + p_expression + " AS Result FROM DUAL");
        IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());

        Enumeration<IDataRow> rows = resultSet.getRows();

        Assert.assertTrue(rows.hasMoreElements());
        IDataRow row = rows.nextElement();

        final Object actual = row.get(0);
        Assert.assertEquals(p_expected.getClass(), actual.getClass());
        Assert.assertEquals(p_expected, actual);

        Assert.assertFalse(rows.hasMoreElements());
    }

}
