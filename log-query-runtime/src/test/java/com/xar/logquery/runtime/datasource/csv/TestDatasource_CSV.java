/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource.csv;


import com.xar.logquery.runtime.DataSource;
import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.ValidationContext;
import com.xar.logquery.runtime.datasource.csv.interfaces.ICSVParser;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.exceptions.AmbiguousColumnNameException;
import com.xar.logquery.runtime.exceptions.ColumnNotFoundException;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Enumeration;

/**
 * Created by Anton Koekemoer on 2014/11/27.
 */
public class TestDatasource_CSV {

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    private static SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Test
    public void shouldAcceptSettingFromProperties() throws FileNotFoundException, DataSourceException {

        createTestFile("test1.txt", false);
        try {
            int groupMod = 10;
            int lineNumber = 0;
            int group = 0;

            CSVDataSource dataSource = new CSVDataSource();

            dataSource.setFilePattern("test*.txt");
            dataSource.setHeading(false);

            dataSource.validate(new ValidationContext());
            IResultSet result = dataSource.getResultSet(new ExecutionContext());

            for (Enumeration<IDataRow> rows = result.getRows(); rows.hasMoreElements(); ) {

                // Calculate the number of rows for the group. A group is not allowed to have
                // less that 10 rows, with an additional number of rows of the mod 10 of the group
                int rowCount = groupMod + (group % groupMod);
                int startInt = rowCount;

                LocalDate date = new LocalDate(2014, 1, 1);

                DateTime dateTime = new DateTime(date.toDate());

                for (int row = 0; row < rowCount; row++) {

                    IDataRow dataRow = rows.nextElement();
//                    System.out.println(dataRow);
                    dateTime = dateTime.minusHours(1);

                    Assert.assertEquals(String.valueOf(group), dataRow.get("A"));
                    Assert.assertEquals("Line " + lineNumber, dataRow.get("B"));
                    Assert.assertEquals(String.valueOf(row), dataRow.get("C"));
                    Assert.assertEquals(String.valueOf(startInt--), dataRow.get("D"));
                    Assert.assertEquals(String.valueOf(((float) group) + (0.1 * row + 1)), dataRow.get("E"));
                    Assert.assertEquals(dateFormat.format(date.toDate()), dataRow.get("F"));
                    Assert.assertEquals(timeStampFormat.format(dateTime.toDate()), dataRow.get("G"));
                    Assert.assertEquals("Line1\nLine2\n\"Line3", dataRow.get("H"));
                    lineNumber++;
                    date = date.plusDays(1);
                }

                group++;
            }

            Assert.assertEquals(1450, lineNumber);
        }
        catch (ColumnNotFoundException e) {
            e.printStackTrace();
        }
        catch (AmbiguousColumnNameException e) {
            e.printStackTrace();
        }
        finally {
            new File("test1.txt").delete();
        }
    }

    @Test
    public void shouldEnsureDataLength() throws IOException, DataSourceException, AmbiguousColumnNameException, ColumnNotFoundException {

        try (PrintWriter out = new PrintWriter("right.csv")) {
            out.println("Table,Row");
            out.println("right,1");
            out.println("right,2");
            out.println("right,3");
            out.println("right,4");
            out.println("right,5");
            out.println("right,6");
            out.println("right,7");
            out.println("");
        }
        try {
            IDataSource dataSource = DataSource.parse("SELECT Table, Row FROM 'right.csv' ORDER BY 1");
            IResultSet result = dataSource.getResultSet(new ExecutionContext());
            int row = 0;
            for (Enumeration<IDataRow> rows = result.getRows(); rows.hasMoreElements(); ) {
                IDataRow dataRow = rows.nextElement();
                if (row < 1) { Assert.assertNull(dataRow.get("Row")); }
                else { Assert.assertEquals("" + row, dataRow.get("Row")); }
                row++;
                System.out.println(dataRow);
            }
            Assert.assertEquals(8, row);
        }
        finally {
            new File("right.csv").delete();
        }
    }

    @Test
    public void shouldParseCSV() throws DataSourceException, FileNotFoundException, ColumnNotFoundException {

        createTestFile("test1.txt", false);
        try {
            int groupMod = 10;
            int lineNumber = 0;
            int group = 0;

            CSVDataSource dataSource = new CSVDataSource("test*.txt", "");
            dataSource.setHeading(false);
            dataSource.validate(new ValidationContext());
            IResultSet result = dataSource.getResultSet(new ExecutionContext());

            for (Enumeration<IDataRow> rows = result.getRows(); rows.hasMoreElements(); ) {

                // Calculate the number of rows for the group. A group is not allowed to have
                // less that 10 rows, with an additional number of rows of the mod 10 of the group
                int rowCount = groupMod + (group % groupMod);
                int startInt = rowCount;

                LocalDate date = new LocalDate(2014, 1, 1);

                DateTime dateTime = new DateTime(date.toDate());

                for (int row = 0; row < rowCount; row++) {

                    IDataRow dataRow = rows.nextElement();
                    System.out.println(dataRow);
                    dateTime = dateTime.minusHours(1);

                    Assert.assertEquals(String.valueOf(group), dataRow.get("A"));
                    Assert.assertEquals("Line " + lineNumber, dataRow.get("B"));
                    Assert.assertEquals(String.valueOf(row), dataRow.get("C"));
                    Assert.assertEquals(String.valueOf(startInt--), dataRow.get("D"));
                    Assert.assertEquals(String.valueOf(((float) group) + (0.1 * row + 1)), dataRow.get("E"));
                    Assert.assertEquals(dateFormat.format(date.toDate()), dataRow.get("F"));
                    Assert.assertEquals(timeStampFormat.format(dateTime.toDate()), dataRow.get("G"));
                    Assert.assertEquals("Line1\nLine2\n\"Line3", dataRow.get("H"));
                    lineNumber++;
                    date = date.plusDays(1);
                }

                group++;
            }

            Assert.assertEquals(1450, lineNumber);
        }
        catch (AmbiguousColumnNameException e) {
            e.printStackTrace();
        }
        finally {
            new File("test1.txt").delete();
        }
    }

    @Test
    public void shouldParseHeading() throws FileNotFoundException, DataSourceException {

        createTestFile("test1.txt", true);

        try {
            int groupMod = 10;
            int lineNumber = 0;
            int group = 0;

            CSVDataSource dataSource = new CSVDataSource("test*.txt", "");
            dataSource.setHeading(true);

            dataSource.validate(new ValidationContext());
            IResultSet result = dataSource.getResultSet(new ExecutionContext());

            for (Enumeration<IDataRow> rows = result.getRows(); rows.hasMoreElements(); ) {

                // Calculate the number of rows for the group. A group is not allowed to have
                // less that 10 rows, with an additional number of rows of the mod 10 of the group
                int rowCount = groupMod + (group % groupMod);
                int startInt = rowCount;

                LocalDate date = new LocalDate(2014, 1, 1);

                DateTime dateTime = new DateTime(date.toDate());

                for (int row = 0; row < rowCount; row++) {

                    IDataRow dataRow = rows.nextElement();
//                    System.out.println(dataRow);
                    dateTime = dateTime.minusHours(1);

                    Assert.assertEquals(String.valueOf(group), dataRow.get("groupNumber"));
                    Assert.assertEquals("Line " + lineNumber, dataRow.get("line"));
                    Assert.assertEquals(String.valueOf(row), dataRow.get("groupRowNumber"));
                    Assert.assertEquals(String.valueOf(startInt--), dataRow.get("intCol"));
                    Assert.assertEquals(String.valueOf(((float) group) + (0.1 * row + 1)), dataRow.get("floatCol"));
                    Assert.assertEquals(dateFormat.format(date.toDate()), dataRow.get("dateCol"));
                    Assert.assertEquals(timeStampFormat.format(dateTime.toDate()), dataRow.get("timestampCol"));
                    Assert.assertEquals("Line1\nLine2\n\"Line3", dataRow.get("textCol"));
                    lineNumber++;
                    date = date.plusDays(1);
                }

                group++;
            }

            Assert.assertEquals(1450, lineNumber);
        }
        catch (ColumnNotFoundException e) {
            e.printStackTrace();
        }
        catch (AmbiguousColumnNameException e) {
            e.printStackTrace();
        }
        finally {
            new File("test1.txt").delete();
        }
    }

    @Test
    public void shouldThrowExceptionIfInvalidFilePattern() throws FileNotFoundException {

        CSVDataSource dataSource = new CSVDataSource("a\u0000bc", "");
        try {
            dataSource.validate(new ValidationContext());
        }
        catch (DataSourceException e) {
            Assert.assertEquals("Invalid file path", e.getMessage());
        }
    }

    @Test
    public void shouldThrowExceptionIfUnableToLoadHeaders() throws FileNotFoundException {

        createTestFile("test1.txt", false);
        try {
            CSVDataSource dataSource = new CSVDataSource("test*.txt", "") {

                /**
                 * Create the reader appropriate for this datasource
                 *
                 * @param p_fileName the file to be read
                 *
                 * @return the reader
                 *
                 * @throws java.io.FileNotFoundException the file not found exception
                 */
                @Override
                protected ICSVParser createFileReader(final File p_fileName) throws IOException {

                    throw new IOException("Failed!");
                }
            };
            dataSource.setHeading(false);
            try {
                dataSource.validate(new ValidationContext());
            }
            catch (DataSourceException e) {
                Assert.assertEquals("Error loading column info", e.getMessage());
            }
        }
        finally {
            new File("test1.txt").delete();
        }
    }

    /**
     * Create a test input file, that can be used to test all the features of the application
     *
     * @param p_fileName name of the file to create
     */
    public static void createTestFile(String p_fileName, boolean heading) throws FileNotFoundException {

        // Group, seq_int, seq_float, Date, Timestamp,
        int groupCount = 100;
        int groupMod = 10;
        int lineNumber = 0;

        try (PrintWriter out = new PrintWriter(p_fileName)) {
            if (heading) {
                out.println("groupNumber,line,groupRowNumber,intCol,floatCol,dateCol,timestampCol,textCol");
            }
            for (int group = 0; group < groupCount; group++) {
                // Calculate the number of rows for the group. A group is not allowed to have
                // less that 10 rows, with an additional number of rows of the mod 10 of the group
                int rowCount = groupMod + (group % groupMod);
                int startInt = rowCount;
                LocalDate date = new LocalDate(2014, 1, 1);
                DateTime dateTime = new DateTime(date.toDate());
                for (int row = 0; row < rowCount; row++) {
                    dateTime = dateTime.minusHours(1);
                    StringBuilder builder = new StringBuilder();
                    builder.append(String.valueOf(group)).append(',')
                            .append("\"Line ").append(lineNumber).append("\",")
                            .append(String.valueOf(row)).append(',')
                            .append(String.valueOf(startInt--)).append(",")
                            .append(String.valueOf(((float) group) + (0.1 * row + 1))).append(",")
                            .append(dateFormat.format(date.toDate())).append(',')
                            .append(timeStampFormat.format(dateTime.toDate())).append(',')
                            .append('"').append("Line1\nLine2\n").append('"').append('"').append("Line3").append('"');

                    out.println(builder);
                    lineNumber++;
                    date = date.plusDays(1);
                }

            }
        }
    }


}
