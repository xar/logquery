/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.functions.scalar;

import com.xar.logquery.runtime.HelperFunctions;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.constant.IntegerConstant;
import com.xar.logquery.runtime.expression.constant.StringConstant;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.expression.functions.exceptions.InvalidParametersException;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Anton Koekemoer on 2014/12/08.
 */
public class TestFunction_ISNULL {

    @Test
    public void shouldReturnTheReplacementValueIfExpressionIsNull() throws IllegalAccessException, InvalidExpressionException, InstantiationException {

        Assert.assertEquals("result", HelperFunctions.executeFunction(ISNULLFunction.class, DataType.STRING, new StringConstant(null), new StringConstant("result")));
    }

    @Test
    public void shouldInputValueIfExpressionIsNotNull() throws IllegalAccessException, InvalidExpressionException, InstantiationException {

        Assert.assertEquals("input", HelperFunctions.executeFunction(ISNULLFunction.class, DataType.STRING, new StringConstant("input"), new StringConstant("result")));
    }

    @Test
    public void shouldThrowExceptionIfParametersIncorrect() throws IllegalAccessException, InvalidParametersException, InstantiationException {
        try {
            Assert.assertEquals("input", HelperFunctions.executeFunction(ISNULLFunction.class, DataType.STRING, new StringConstant("input")));
            Assert.fail("Expected error");
        }
        catch (InvalidExpressionException ex){
            Assert.assertEquals("ISNULL requires two parameters", ex.getMessage());
        }

        try {
            Assert.assertEquals("input", HelperFunctions.executeFunction(ISNULLFunction.class, DataType.STRING, new StringConstant("input"), new IntegerConstant(10)));
            Assert.fail("Expected error");
        }
        catch (InvalidExpressionException ex){
            Assert.assertEquals("ISNULL data types STRING and INTEGER is not compatible", ex.getMessage());
        }

    }


}
