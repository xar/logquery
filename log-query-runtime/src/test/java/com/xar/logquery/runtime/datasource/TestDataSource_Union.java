/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource;

import com.xar.logquery.runtime.DataSource;
import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.exceptions.AmbiguousColumnNameException;
import com.xar.logquery.runtime.exceptions.ColumnNotFoundException;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.Enumeration;

/**
 * Created by akoekemo on 3/10/15.
 */
public class TestDataSource_Union {

    @Test
    public void unionSupported() throws IOException, DataSourceException, AmbiguousColumnNameException, ColumnNotFoundException {

        IDataSource dataSource = DataSource.parse("SELECT 1 as col1, 2 as col2, 3 as col3, 4 as col4, 5 as col5 from DUAL UNION SELECT 0, 1, 2, 3, 4 FROM DUAL UNION SELECT 2, 1, 2, 3, 4 FROM DUAL ORDER BY col1");
        int rowCount = 0;
        IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
        Enumeration<IDataRow> rows = resultSet.getRows();
        while (rows.hasMoreElements()) {

            IDataRow dataRow = rows.nextElement();
            Assert.assertEquals(rowCount, ((Number) dataRow.get("col1")).intValue());
            System.out.println(dataRow);
            rowCount++;
        }
        Assert.assertEquals(3, rowCount);
    }

    @Test
    public void shouldThrowExceptionIFUnionSourcesHaveDiffirentNumberOfColumns() throws IOException, DataSourceException {
        try {
            IDataSource dataSource = DataSource.parse("SELECT 1 as col1, 2 as col2, 3 as col3, 4 as col4, 5 as col5 from DUAL UNION SELECT 0, 1, 2, 3, 4 FROM DUAL UNION SELECT 2, 1, 2, 4 FROM DUAL ORDER BY col1");
            Assert.fail("Expected exception");
        }
        catch (DataSourceException ex){
            Assert.assertEquals("Number of columns in the select queries do not match!", ex.getMessage());
        }
    }

    @Test
    public void shouldThrowExceptionIFColumnDataTypesIncompatible() throws IOException, DataSourceException {
        try {
            IDataSource dataSource = DataSource.parse("SELECT 1 as col1, 2 as col2, 3 as col3, 4 as col4, 5 as col5 from DUAL UNION SELECT 0, 1.0, 2, 3, 4 FROM DUAL UNION SELECT 2, 1, 2, '3', 4 FROM DUAL ORDER BY col1");
            Assert.fail("Expected exception");
        }
        catch (DataSourceException ex){
            Assert.assertEquals("Data type error. Unable to case value of type STRING to INTEGER!", ex.getMessage());
        }
    }


}
