/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.output;

import com.xar.logquery.runtime.ColumnInfo;
import com.xar.logquery.runtime.datasource.StaticDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.output.csv.CSVOutputFormatter;
import com.xar.logquery.runtime.output.interfaces.IOutputFormatter;
import com.xar.logquery.runtime.output.text.TextOutputFormatter;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;

/**
 * Created by akoekemo on 4/15/15.
 */
public class Test_TextOutputFormatter {


    @Test
    public void shouldPageResults() throws DataSourceException, UnsupportedEncodingException {
        StaticDataSource ds = new StaticDataSource();
        ds.setColumnInfo(new IColumnInfo[]{
                new ColumnInfo("", "col1", DataType.STRING), new ColumnInfo("", "col2", DataType.STRING)});
        TextOutputFormatter txt = new TextOutputFormatter();
        txt.setPageSize(1);

        PrintStream out = System.out;
        InputStream in = System.in;

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(baos);
        System.setOut(printStream);

        String data = "\n";
        InputStream testInput = new ErrorInputStream();
        System.setIn(testInput);

        try {
            ds.addRow("r1\"c1", "r1c2");
            ds.addRow("r2\"c1", "r2c2");

            txt.format(ds);
        }
        finally {
            System.setOut(out);
            System.setIn(in);
        }
        Assert.assertEquals("col1  col2\n----- ----\nr1\"c1 r1c2\n\nEnter for more...\n\ncol1  col2\n" +
                "----- ----\n" +
                "r2\"c1 r2c2\n", new String(baos.toByteArray()));

    }

    private class ErrorInputStream extends InputStream {

        @Override
        public int read() throws IOException {

            throw new IOException();
        }
    }
}
