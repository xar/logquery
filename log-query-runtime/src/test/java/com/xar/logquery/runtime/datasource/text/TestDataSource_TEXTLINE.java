/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource.text;

import com.xar.logquery.runtime.DataSource;
import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.ValidationContext;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.exceptions.AmbiguousColumnNameException;
import com.xar.logquery.runtime.exceptions.ColumnNotFoundException;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.NoSuchElementException;

/**
 * Created by akoekemo on 2/24/15.
 */
public class TestDataSource_TEXTLINE {

    @Test
    public void shouldIterateFiles() throws IOException, DataSourceException {

        try {
            createFile("test1.txt");
            createFile("test2.txt");
            createFile("test3.txt");

            IDataSource dataSource = DataSource.parse("SELECT * FROM 'test*.txt'");

            IResultSet result = dataSource.getResultSet(new ExecutionContext());
            Enumeration<IDataRow> rows = result.getRows();
            int fno = 1;
            int i = 0;
            String fileName = "";
            int rowCount = 0;
            while (rows.hasMoreElements()) {

                IDataRow row = rows.nextElement();
                rowCount++;
                if (!row.getData()[0].equals(fileName)) {
                    fileName = (String) row.getData()[0];
                    Assert.assertEquals(new File("test" + fno + ".txt").getAbsolutePath(), fileName);
                    fno++;
                    i = 0;
                }
                Assert.assertEquals((long) i + 1, row.getData()[1]);
                Assert.assertEquals(fileName + " Line " + i, row.getData()[2]);
                i++;
            }
            Assert.assertEquals(3000, rowCount);
        }
        finally {
            for (int i = 1; i < 5; i++) { new File("test" + i + ".txt").delete(); }
        }
    }

    @Test
    public void shouldConstructManually() throws IOException, DataSourceException, ColumnNotFoundException, AmbiguousColumnNameException {

        try (PrintWriter writer = new PrintWriter(new FileWriter("test.txt"))) {
            for (int i = 1; i <= 10; i++) {
                writer.println("Line " + i);
                for (int j = 1; j <= 5; j++) { writer.println("  Sub " + i + "." + j); }
            }

        }

        try {
            TextLineDataSource dataSource = new TextLineDataSource();
            dataSource.setFilePattern("test.txt");
            dataSource.setLinePattern("^Line.+");
            dataSource.validate(new ValidationContext());
            IResultSet result = dataSource.getResultSet(new ExecutionContext());
            Enumeration<IDataRow> rows = result.getRows();

            int row = 0;
            while (rows.hasMoreElements()) {
                row++;
                IDataRow r = rows.nextElement();
                System.out.println(r);
                Assert.assertEquals("Line " + row + "\n  Sub " + row + ".1" + "\n  Sub " + row + ".2" + "\n  Sub " + row + ".3" + "\n  Sub " + row + ".4" + "\n  Sub " + row + ".5", r.get("Text"));
            }
            Assert.assertEquals(10, row);
        }
        finally {
            new File("test.txt").delete();
        }
    }

    @Test
    public void shouldMultiLines() throws IOException, DataSourceException, ColumnNotFoundException, AmbiguousColumnNameException {

        try (PrintWriter writer = new PrintWriter(new FileWriter("test.txt"))) {
            for (int i = 1; i <= 10; i++) {
                writer.println("Line " + i);
                for (int j = 1; j <= 5; j++) { writer.println("  Sub " + i + "." + j); }
            }

        }

        try {
            IDataSource dataSource = DataSource.parse("SELECT * FROM 'test.txt' USING TEXTLINE(linePattern='^Line.+')");
//            TextLineDataSource dataSource = new TextLineDataSource();
//            dataSource.setFilePattern("test.txt");
//            dataSource.setLinePattern("^Line.+");
            dataSource.validate(new ValidationContext());
            IResultSet result = dataSource.getResultSet(new ExecutionContext());
            Enumeration<IDataRow> rows = result.getRows();

            int row = 0;
            while (rows.hasMoreElements()) {
                row++;
                IDataRow r = rows.nextElement();
                System.out.println(r);
                Assert.assertEquals("Line " + row + "\n  Sub " + row + ".1" + "\n  Sub " + row + ".2" + "\n  Sub " + row + ".3" + "\n  Sub " + row + ".4" + "\n  Sub " + row + ".5", r.get("Text"));
            }
            Assert.assertEquals(10, row);
        }
        finally {
            new File("test.txt").delete();
        }
    }

    @Test
    public void shouldRecoverOffError() throws IOException, DataSourceException, ColumnNotFoundException {

        createFile("test1.txt");
        createFile("test2.txt");
        createFile("test3.txt");

        TextLineDataSource dataSource = new TextLineDataSource();
        dataSource.setFilePattern("test*.txt");

        IResultSet result = dataSource.getResultSet(new ExecutionContext());
        Enumeration<IDataRow> rows = result.getRows();

        new File("test1.txt").delete();

        IDataRow row = rows.nextElement();
        String fileName = (String) row.getData()[0];
        Assert.assertEquals(new File("test2.txt").getAbsolutePath(), fileName);

        Assert.assertEquals(true, new File("test2.txt").delete());

        while (rows.hasMoreElements()) {
            row = rows.nextElement();
        }
        fileName = (String) row.getData()[0];
        Assert.assertEquals(new File("test3.txt").getAbsolutePath(), fileName);

    }


    @Test
    public void shouldThrowExceptionIfNoMoreLines() throws IOException, DataSourceException {

        try {
            createFile("test1.txt");

            IDataSource dataSource = new TextLineDataSource("test*.txt", "");
            dataSource.validate(new ValidationContext());
            IResultSet result = dataSource.getResultSet(new ExecutionContext());
            Enumeration<IDataRow> rows = result.getRows();

            int fno = 1;
            int i = 0;
            String fileName = "";
            try {
                while (true) {

                    IDataRow row = rows.nextElement();
                    if (!row.getData()[0].equals(fileName)) {
                        fileName = (String) row.getData()[0];
                        Assert.assertEquals(new File("test" + fno + ".txt").getAbsolutePath(), fileName);
                        fno++;
                        i = 0;
                    }
                    Assert.assertEquals((long) i + 1, row.getData()[1]);
                    Assert.assertEquals(fileName + " Line " + i, row.getData()[2]);
                    i++;
                    if (false) { break; }
                }
                Assert.fail("Expected NoSuchElementException");
            }
            catch (NoSuchElementException ex) {

            }
            Assert.assertEquals(1000, i);
        }
        finally {
            for (int i = 0; i < 4; i++) { new File("test" + i + ".txt").delete(); }
        }
    }

    private File createFile(final String p_fileName) throws IOException {

        File f = new File(p_fileName);
        try (FileWriter writer = new FileWriter(f)) {

            for (int i = 0; i < 1000; i++) {
                writer.write(f.getAbsolutePath() + " Line " + i + "\n");
            }
        }
        return f;
    }

}
