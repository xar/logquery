/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource.join;

import com.xar.logquery.runtime.DataSource;
import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.exceptions.AmbiguousColumnNameException;
import com.xar.logquery.runtime.exceptions.ColumnNotFoundException;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

/**
 * Created by akoekemo on 3/19/15.
 */
public class TestJoin_OUTER {

    @Test
    public void supportsFULL_OUTER() throws IOException, DataSourceException, AmbiguousColumnNameException, ColumnNotFoundException {

        try (PrintWriter out = new PrintWriter("left.csv")) {
            out.println("Table,Row");
            out.println("left,1");
            out.println("left,2");
            out.println("left,3");
            out.println("left,4");
            out.println("left,5");
            out.println("left,6");
            out.println("left,7");
            out.println("left,8");
            out.println("left,9");
            out.println("left,10");
        }
        try (PrintWriter out = new PrintWriter("right.csv")) {
            out.println("Table,Row");
            out.println("right,1");
            out.println("right,3");
            out.println("right,5");
            out.println("right,7");
            out.println("right,9");
            out.println("right,11");
            out.println("right,13");
        }
        try {
            IDataSource dataSource = DataSource.parse("SELECT * FROM 'left.csv' USING CSV AS A FULL OUTER JOIN 'right.csv' USING CSV AS B ON A.Row = B.Row");
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            Enumeration<IDataRow> rows = resultSet.getRows();

            int rowCount = 0;
            while (rows.hasMoreElements()) {
                IDataRow dataRow = rows.nextElement();
                System.out.println(dataRow);
                if (rowCount < 10) {
                    if ((rowCount % 2) == 0) {
                        Assert.assertEquals(dataRow.get("A.Row"), dataRow.get("B.Row"));
                    }
                    else {
                        Assert.assertNull(dataRow.get("B.Row"));
                    }
                }
                else {
                    Assert.assertNull(dataRow.get("A.Row"));
                }

                rowCount++;
            }
            Assert.assertEquals(12, rowCount);
        }
        finally {
            new File("left.csv").delete();
            new File("right.csv").delete();
        }
    }

    @Test
    public void rightOUTERReturnsRowsWhenRigthTableIsEmpty() throws DataSourceException, ColumnNotFoundException, IOException, AmbiguousColumnNameException {
        try (PrintWriter out = new PrintWriter("left.csv")) {
            out.println("Table,Row");
        }
        try (PrintWriter out = new PrintWriter("right.csv")) {
            out.println("Table,Row");
            out.println("right,1");
            out.println("right,3");
            out.println("right,5");
            out.println("right,7");
            out.println("right,9");
            out.println("right,11");
            out.println("right,13");
        }
        try {
            IDataSource dataSource = DataSource.parse("SELECT * FROM 'left.csv' USING CSV AS A RIGHT OUTER JOIN 'right.csv' USING CSV AS B ON A.Row = B.Row");
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            Enumeration<IDataRow> rows = resultSet.getRows();

            int rowCount = 0;
            while (rows.hasMoreElements()) {
                IDataRow dataRow = rows.nextElement();
                System.out.println(dataRow);
                Assert.assertNull(dataRow.get("A.Row"));
                rowCount++;
            }
            Assert.assertEquals(7, rowCount);
        }
        finally {
            new File("left.csv").delete();
            new File("right.csv").delete();
        }
    }

    @Test
    public void leftOUTERReturnsRowsWhenRigthTableIsEmpty() throws DataSourceException, ColumnNotFoundException, IOException, AmbiguousColumnNameException {
        try (PrintWriter out = new PrintWriter("left.csv")) {
            out.println("Table,Row");
            out.println("left,1");
            out.println("left,2");
            out.println("left,3");
            out.println("left,4");
            out.println("left,5");
            out.println("left,6");
            out.println("left,7");
            out.println("left,8");
            out.println("left,9");
            out.println("left,10");
        }
        try (PrintWriter out = new PrintWriter("right.csv")) {
            out.println("Table,Row");
        }
        try {
            IDataSource dataSource = DataSource.parse("SELECT * FROM 'left.csv' USING CSV AS A LEFT OUTER JOIN 'right.csv' USING CSV AS B ON A.Row = B.Row");
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            Enumeration<IDataRow> rows = resultSet.getRows();

            int rowCount = 0;
            while (rows.hasMoreElements()) {
                IDataRow dataRow = rows.nextElement();
                System.out.println(dataRow);
                Assert.assertNull(dataRow.get("B.Row"));
                rowCount++;
            }
            Assert.assertEquals(10, rowCount);
        }
        finally {
            new File("left.csv").delete();
            new File("right.csv").delete();
        }
    }


    @Test
    public void supportsLEFT_OUTER() throws IOException, DataSourceException, AmbiguousColumnNameException, ColumnNotFoundException {

        try (PrintWriter out = new PrintWriter("left.csv")) {
            out.println("Table,Row");
            out.println("left,1");
            out.println("left,2");
            out.println("left,3");
            out.println("left,4");
            out.println("left,5");
            out.println("left,6");
            out.println("left,7");
            out.println("left,8");
            out.println("left,9");
            out.println("left,10");
        }
        try (PrintWriter out = new PrintWriter("right.csv")) {
            out.println("Table,Row");
            out.println("right,1");
            out.println("right,3");
            out.println("right,5");
            out.println("right,7");
            out.println("right,9");
            out.println("right,11");
            out.println("right,13");
        }
        try {
            IDataSource dataSource = DataSource.parse("SELECT * FROM 'left.csv' USING CSV AS A LEFT OUTER JOIN 'right.csv' USING CSV AS B ON A.Row = B.Row");
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            Enumeration<IDataRow> rows = resultSet.getRows();

            int rowCount = 0;
            while (rows.hasMoreElements()) {
                IDataRow dataRow = rows.nextElement();
                System.out.println(dataRow);
                if ((rowCount % 2) == 0) {
                    Assert.assertEquals(dataRow.get("A.Row"), dataRow.get("B.Row"));
                }
                else {
                    Assert.assertNull(dataRow.get("B.Row"));
                }
                rowCount++;
            }
            Assert.assertEquals(10, rowCount);
        }
        finally {
            new File("left.csv").delete();
            new File("right.csv").delete();
        }
    }

    @Test
    public void supportsRIGHT_OUTER() throws IOException, DataSourceException, AmbiguousColumnNameException, ColumnNotFoundException {

        try (PrintWriter out = new PrintWriter("left.csv")) {
            out.println("Table,Row");
            out.println("left,1");
            out.println("left,2");
            out.println("left,3");
            out.println("left,4");
            out.println("left,5");
            out.println("left,6");
            out.println("left,7");
            out.println("left,8");
            out.println("left,9");
            out.println("left,10");
        }
        try (PrintWriter out = new PrintWriter("right.csv")) {
            out.println("Table,Row");
            out.println("right,1");
            out.println("right,3");
            out.println("right,5");
            out.println("right,7");
            out.println("right,9");
            out.println("right,11");
            out.println("right,13");
        }
        try {
            IDataSource dataSource = DataSource.parse("SELECT * FROM 'right.csv' USING CSV AS A RIGHT OUTER JOIN 'left.csv' USING CSV AS B ON A.Row = B.Row");
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            Enumeration<IDataRow> rows = resultSet.getRows();

            int rowCount = 0;
            while (rows.hasMoreElements()) {
                IDataRow dataRow = rows.nextElement();
                System.out.println(dataRow);

                if (rowCount < 5) {
                    Assert.assertEquals(dataRow.get("A.Row"), dataRow.get("B.Row"));
                }
                else {
                    Assert.assertNull(dataRow.get("A.Row"));
                }
                rowCount++;
            }
            Assert.assertEquals(10, rowCount);
        }
        finally {
            new File("left.csv").delete();
            new File("right.csv").delete();
        }
    }

}
