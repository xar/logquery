/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource;

import com.xar.logquery.runtime.DataSource;
import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.HelperFunctions;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.exceptions.AmbiguousColumnNameException;
import com.xar.logquery.runtime.exceptions.ColumnNotFoundException;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Enumeration;
import java.util.NoSuchElementException;

/**
 * Created by akoekemo on 3/9/15.
 */
public class TestDataSource_Limit {

    /**
     * Test that the datasource returns all the rows from the input file, and that the data is returned correctly
     *
     * @throws IOException
     * @throws DataSourceException
     * @throws ColumnNotFoundException
     */
    @Test
    public void shouldLimitRowsFromDatasource() throws IOException, DataSourceException, ColumnNotFoundException, AmbiguousColumnNameException {

        File file = new File("test.txt");
        HelperFunctions.createTestFile(file);
        try {
            int lineNumber = 0;

            IDataSource dataSource = DataSource.parse("SELECT * from '" + file.getName() + "' TRANSFORM(STRTOK(Text, ',', 0) AS groupNumber, STRTOK(Text, ',', 1) AS line, STRTOK(Text, ',', 2) AS groupRowNumber, STRTOK(Text, ',', 3) AS intCol, STRTOK(Text, ',', 4) AS floatCol, STRTOK(Text, ',', 5) AS dateCol, STRTOK(Text, ',', 6) AS timestampCol) LIMIT 10");
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());

            for (Enumeration<IDataRow> rows = resultSet.getRows(); rows.hasMoreElements(); ) {
                IDataRow dataRow = rows.nextElement();
                System.out.println(dataRow);
                Assert.assertEquals("" + lineNumber, dataRow.get("groupRowNumber"));
                Assert.assertEquals("0", dataRow.get("groupNumber"));

                lineNumber++;
            }

            Assert.assertEquals(10, lineNumber);
        }
        finally {
            file.delete();
        }
    }

    @Test
    public void shouldSkipRowsFromDatasource() throws IOException, DataSourceException, ColumnNotFoundException, AmbiguousColumnNameException {

        File file = new File("test.txt");
        HelperFunctions.createTestFile(file);
        try {
            int lineNumber = 0;

            IDataSource dataSource = DataSource.parse("SELECT * from '" + file.getName() + "' TRANSFORM (STRTOK(Text, ',', 0) AS groupNumber, STRTOK(Text, ',', 1) AS line, STRTOK(Text, ',', 2) AS groupRowNumber, STRTOK(Text, ',', 3) AS intCol, STRTOK(Text, ',', 4) AS floatCol, STRTOK(Text, ',', 5) AS dateCol, STRTOK(Text, ',', 6) AS timestampCol) LIMIT 9, 10");

            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());

            for (Enumeration<IDataRow> rows = resultSet.getRows(); rows.hasMoreElements(); ) {
                IDataRow dataRow = rows.nextElement();
                System.out.println(dataRow);
                Assert.assertEquals("" + lineNumber, dataRow.get("groupRowNumber"));
                Assert.assertEquals("1", dataRow.get("groupNumber"));

                lineNumber++;
            }

            Assert.assertEquals(9, lineNumber);
        }
        finally {
            file.delete();
        }
    }

    @Test
    public void shouldRecogniseSkipSyntax() throws IOException, DataSourceException, ColumnNotFoundException, AmbiguousColumnNameException {

        File file = new File("test.txt");
        HelperFunctions.createTestFile(file);
        try {
            int lineNumber = 0;

            IDataSource dataSource = DataSource.parse("SELECT * from '" + file.getName() + "' TRANSFORM (STRTOK(Text, ',', 0) AS groupNumber, STRTOK(Text, ',', 1) AS line, STRTOK(Text, ',', 2) AS groupRowNumber, STRTOK(Text, ',', 3) AS intCol, STRTOK(Text, ',', 4) AS floatCol, STRTOK(Text, ',', 5) AS dateCol, STRTOK(Text, ',', 6) AS timestampCol) LIMIT 9 SKIP 10");

            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());

            for (Enumeration<IDataRow> rows = resultSet.getRows(); rows.hasMoreElements(); ) {
                IDataRow dataRow = rows.nextElement();
                System.out.println(dataRow);
                Assert.assertEquals("" + lineNumber, dataRow.get("groupRowNumber"));
                Assert.assertEquals("1", dataRow.get("groupNumber"));

                lineNumber++;
            }

            Assert.assertEquals(9, lineNumber);
        }
        finally {
            file.delete();
        }
    }

    // TODO: limitAcceptsOnlyNumbers()
//    @Test
//    public void limitAcceptsOnlyNumbers() throws IOException {
//
//        try {
//            IDataSource dataSource = DataSource.parse("SELECT * from 'file.txt' LIMIT '10', 9");
//            Assert.fail("Expected error");
//        }
//        catch (Exception ex) {
//            Assert.assertEquals("[1, 25] Invalid data type, found STRING expected NUMBER", ex.getMessage());
//        }
//        try {
//            IDataSource dataSource = DataSource.parse("SELECT * from 'file.txt' LIMIT 10, '9'");
//            Assert.fail("Expected error");
//        }
//        catch (Exception ex) {
//            Assert.assertEquals("[1, 25] Invalid data type, found STRING expected NUMBER", ex.getMessage());
//        }
//    }

    @Test
    public void shouldThrowExceptionIfAttemptToIteratePastEnd() throws IOException, DataSourceException {
        File file = new File("test.txt");
        HelperFunctions.createTestFile(file);
        try {
            int lineNumber = 0;

            IDataSource dataSource = DataSource.parse("SELECT * from '" + file.getName() + "' TRANSFORM (STRTOK(Text, ',', 0) AS groupNumber, STRTOK(Text, ',', 1) AS line, STRTOK(Text, ',', 2) AS groupRowNumber, STRTOK(Text, ',', 3) AS intCol, STRTOK(Text, ',', 4) AS floatCol, STRTOK(Text, ',', 5) AS dateCol, STRTOK(Text, ',', 6) AS timestampCol) LIMIT 9 SKIP 10");

            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            Enumeration<IDataRow> rows = resultSet.getRows();
            while (rows.hasMoreElements()) {
                IDataRow dataRow = rows.nextElement();
            }
            Assert.assertFalse(rows.hasMoreElements());
            try{
                rows.nextElement();
                Assert.fail("Expected exception");
            }
            catch (NoSuchElementException ex){}
        }
        finally {
            file.delete();
        }
    }



}
