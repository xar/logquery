/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.util;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Anton Koekemoer on 2014/12/12.
 */
public class TestTypeFunctions {

    @Test
    public void shouldDetermineIfAnObjectBelongsToOneOrMoreTypes(){

        Assert.assertTrue(TypeFunctions.isValueInteger(1));
        Assert.assertFalse(TypeFunctions.isValueInteger(1.0));
        Assert.assertTrue(TypeFunctions.isValueReal(1.0));
        Assert.assertFalse(TypeFunctions.isValueReal(1));
        Assert.assertTrue(TypeFunctions.isValueTypeOf(1, int.class, Integer.class));
        Assert.assertTrue(TypeFunctions.isValueTypeOf(new Integer(1), int.class, Integer.class));
        Assert.assertTrue(TypeFunctions.isValueTypeOf(1l, long.class, Long.class));
        Assert.assertTrue(TypeFunctions.isValueTypeOf(new Long(1), long.class, Long.class));
    }

}
