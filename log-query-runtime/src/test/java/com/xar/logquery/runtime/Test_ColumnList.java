/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime;

import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.exceptions.DuplicateColumnNameException;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by akoekemo on 3/15/15.
 */
public class Test_ColumnList {

    @Test
    public void shouldClear() throws DuplicateColumnNameException {

        ColumnList list = new ColumnList();
        list.addColumn(new ColumnInfo("a", "a", DataType.INTEGER));
        list.addColumn(new ColumnInfo("a", "b", DataType.INTEGER));
        list.addColumn(new ColumnInfo("a", "c", DataType.INTEGER));
        list.addColumn(new ColumnInfo("a", "d", DataType.INTEGER));

        Assert.assertEquals(4, list.getColumns().size());

        list.clear();

        Assert.assertEquals(0, list.getColumns().size());
    }

    @Test
    public void shouldReturnColumnsForSpecifiedContext() throws DuplicateColumnNameException {

        ColumnList list = new ColumnList();
        list.addColumn(new ColumnInfo("a", "a", DataType.INTEGER));
        list.addColumn(new ColumnInfo("a", "b", DataType.INTEGER));
        list.addColumn(new ColumnInfo("a", "c", DataType.INTEGER));
        list.addColumn(new ColumnInfo("a", "d", DataType.INTEGER));
        list.addColumn(new ColumnInfo("b", "a", DataType.INTEGER));
        list.addColumn(new ColumnInfo("b", "b", DataType.INTEGER));
        list.addColumn(new ColumnInfo("b", "c", DataType.INTEGER));
        list.addColumn(new ColumnInfo("b", "d", DataType.INTEGER));

        Assert.assertEquals(4, list.getColumnsForContext("a").size());
        for (IColumnInfo info : list.getColumnsForContext("a")) {
            Assert.assertEquals("a", info.getContext());
        }

    }

    @Test
    public void shouldThrowExceptionIfDuplicateColumnsAdded() throws DuplicateColumnNameException {

        ColumnList list = new ColumnList();
        list.addColumn(new ColumnInfo("a", "b", DataType.INTEGER));
        try {
            list.addColumn(new ColumnInfo("a", "b", DataType.INTEGER));
            Assert.fail("Expected exception");
        }
        catch (DuplicateColumnNameException ex) {
            Assert.assertEquals("Column name 'a.b' already exists", ex.getMessage());
        }
    }

    @Test
    public void shouldAddColumnsOnlyIfTheyDoNotExist() {
        ColumnList list = new ColumnList();
        Assert.assertTrue(list.addColumnIfNotExist(new ColumnInfo("a", "b", DataType.INTEGER)));
        Assert.assertFalse(list.addColumnIfNotExist(new ColumnInfo("a", "b", DataType.INTEGER)));
    }

    @Test
    public void shouldReturnColumnsFromThelist() throws DuplicateColumnNameException {
        ColumnList list = new ColumnList();
        ColumnInfo info = new ColumnInfo("a", "b", DataType.INTEGER);
        list.addColumn(info);
        Assert.assertEquals(info, list.getColumnInfo("a", "b"));
    }




}
