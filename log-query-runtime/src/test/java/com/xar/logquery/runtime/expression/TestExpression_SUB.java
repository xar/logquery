/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression;

import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.ValidationContext;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.arithmitic.SubtractExpression;
import com.xar.logquery.runtime.expression.constant.ConstantExpression;
import com.xar.logquery.runtime.expression.constant.RealConstant;
import com.xar.logquery.runtime.expression.constant.IntegerConstant;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

/**
 * Created by koekemoera on 2014/11/13.
 */
public class TestExpression_SUB {

    @Test
    public void shouldSubstractValues() throws InvalidExpressionException {
        testExpression(new SubtractExpression(new IntegerConstant(10), new IntegerConstant(2)), BigInteger.class, new BigInteger("8"));
        testExpression(new SubtractExpression(new IntegerConstant(10), new RealConstant(2)), BigDecimal.class, new BigDecimal("8.0"));

        testExpression(new SubtractExpression(new RealConstant(10), new RealConstant(2)), BigDecimal.class, new BigDecimal("8.0"));
        testExpression(new SubtractExpression(new RealConstant(10), new IntegerConstant(2)), BigDecimal.class, new BigDecimal("8.0"));

        Date start =new Date();
        Date end = new Date(start.getTime() + 86400000);

        testExpression(new ConstantExpression<>(DataType.DATE, start), new IntegerConstant(1), Date.class, LocalDate.fromDateFields(start).minusDays(1).toDate());
        testExpression(new ConstantExpression<>(DataType.DATE, start), new RealConstant(1.5f), Date.class, LocalDate.fromDateFields(start).minusDays(1).toDate());
        testExpression(new SubtractExpression(new ConstantExpression<>(DataType.DATE, start), new ConstantExpression<>(DataType.DATE, end)), Integer.class, Days.daysBetween(LocalDate.fromDateFields(start), LocalDate.fromDateFields(end)).getDays());
        testExpression(new SubtractExpression(new ConstantExpression<>(DataType.DATE, start), new ConstantExpression<>(DataType.TIMESTAMP, end)), Long.class, LocalDate.fromDateFields(start).toDate().getTime() - end.getTime());

        testExpression(new ConstantExpression<>(DataType.TIMESTAMP, start), new IntegerConstant(1000), Date.class, new Date(start.getTime() - 1000));
        testExpression(new ConstantExpression<>(DataType.TIMESTAMP, start), new RealConstant(1.5d), Date.class, new Date((long) (start.getTime() - (86400000 * 1.5d))));
        testExpression(new SubtractExpression(new ConstantExpression<>(DataType.TIMESTAMP, start), new ConstantExpression<>(DataType.TIMESTAMP, end)), Long.class, start.getTime() - end.getTime());
        testExpression(new SubtractExpression(new ConstantExpression<>(DataType.TIMESTAMP, start), new ConstantExpression<>(DataType.DATE, end)), Long.class, start.getTime() - LocalDate.fromDateFields(end).toDate().getTime());

    }

    private void testExpression(final IExpression p_expression, final Class<?> p_resultType, final Object p_expectedValue) throws InvalidExpressionException {
        p_expression.validate(new ValidationContext());
        Object value = p_expression.getValue(new ExecutionContext());

        Assert.assertEquals(p_resultType, value.getClass());
        Assert.assertEquals(p_expectedValue, value);


    }

    private void testExpression(final IExpression p_lhs,final IExpression p_rhs, final Class<?> p_resultType, final Object p_expectedValue) throws InvalidExpressionException {
        testExpression(new SubtractExpression(p_lhs, p_rhs), p_resultType, p_expectedValue);
        testExpression(new SubtractExpression(p_rhs, p_lhs), p_resultType, p_expectedValue);
    }


}
