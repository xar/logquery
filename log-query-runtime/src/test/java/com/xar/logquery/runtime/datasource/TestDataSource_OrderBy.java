/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource;

import com.xar.logquery.runtime.DataSource;
import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.HelperFunctions;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.exceptions.AmbiguousColumnNameException;
import com.xar.logquery.runtime.exceptions.ColumnNotFoundException;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.expression.OrderExpression;
import com.xar.logquery.runtime.expression.constant.IntegerConstant;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Enumeration;

/**
 * Created by akoekemo on 3/9/15.
 */
public class TestDataSource_OrderBy {

    @Test
    public void shouldOrderBy() throws DataSourceException, IOException, ColumnNotFoundException, AmbiguousColumnNameException {

        File file = new File("test.txt");
        HelperFunctions.createTestFile(file);
        try {


            IDataSource dataSource = DataSource.parse("SELECT * FROM '" + file.getName() + "' WHERE RowNumber BETWEEN 10 AND 100 ORDER BY RowNumber DESC");
            long last = 1000;
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            for (Enumeration<IDataRow> rows = resultSet.getRows(); rows.hasMoreElements(); ) {
                IDataRow dataRow = rows.nextElement();
                Assert.assertTrue(((Number) dataRow.get("RowNumber")).longValue() < last);
                last = ((Number) dataRow.get("RowNumber")).longValue();
                System.out.println(dataRow);
            }
        }
        finally {
            file.delete();
        }

    }

    @Test
    public void shouldOrderByColumnIndex() throws IOException, DataSourceException, AmbiguousColumnNameException, ColumnNotFoundException {
        File file = new File("test.txt");
        HelperFunctions.createTestFile(file);
        try {

            IDataSource dataSource = DataSource.parse("SELECT RowNumber, Text, Line from '" + file.getName() + "' TRANSFORM (TO_INT(STRTOK(STRTOK(Text, ',', 1), ' ', 1)) AS Line) ORDER BY 1 DESC");
            int last = Integer.MAX_VALUE;
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            for (Enumeration<IDataRow> rows = resultSet.getRows(); rows.hasMoreElements(); ) {
                IDataRow dataRow = rows.nextElement();
                int current = ((Number) dataRow.get("Line")).intValue();
                Assert.assertTrue(last > current);
                last = current;
            }
        }
        finally {
            file.delete();
        }
    }

    @Test
    public void shouldOrderRowsThatAreExactlyTheSame() throws IOException, DataSourceException, AmbiguousColumnNameException, ColumnNotFoundException {
        File file = new File("test.txt");
        HelperFunctions.createTestFile(file);
        try {

            IDataSource dataSource = DataSource.parse("SELECT RowNumber, Text, Line from '" + file.getName() + "' TRANSFORM (TO_INT(STRTOK(STRTOK(Text, ',', 1), ' ', 1)) AS Line) ORDER BY RowNumber%5 DESC");
            int last = Integer.MAX_VALUE;
            int mod = 5;

            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            for (Enumeration<IDataRow> rows = resultSet.getRows(); rows.hasMoreElements(); ) {
                IDataRow dataRow = rows.nextElement();

                int current = ((Number) dataRow.get("RowNumber")).intValue();
                if (current < last){
                    last = 0;
                    mod--;
                }

                Assert.assertEquals(mod, current%5);

//                System.out.println(dataRow);
                Assert.assertTrue(last + "<" +  current, last < current);
                last = current;
            }
        }
        finally {
            file.delete();
        }
    }


    @Test
    public void shouldOrderByUnreferencedColumns() throws IOException, DataSourceException, ColumnNotFoundException, AmbiguousColumnNameException {

        File file = new File("test.txt");
        HelperFunctions.createTestFile(file);
        try {

            IDataSource dataSource = DataSource.parse("SELECT Text, Line from '" + file.getName() + "' TRANSFORM (TO_INT(STRTOK(STRTOK(Text, ',', 1), ' ', 1)) AS Line) ORDER BY RowNumber DESC");
//            IDataSource dataSource = DataSource.parse("SELECT Text, Line from '" + file.getName() + "' ORDER BY RowNumber DESC");
            int last = Integer.MAX_VALUE;
            IResultSet resultSet = dataSource.getResultSet(new ExecutionContext());
            for (Enumeration<IDataRow> rows = resultSet.getRows(); rows.hasMoreElements(); ) {
                IDataRow dataRow = rows.nextElement();
                int current = ((Number) dataRow.get("Line")).intValue();
                Assert.assertTrue(last > current);
                last = current;
            }
        }
        finally {
            file.delete();
        }
    }


}
