/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.functions.scalar.convert;

import com.xar.logquery.runtime.HelperFunctions;
import com.xar.logquery.runtime.ValidationContext;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.constant.BooleanConstant;
import com.xar.logquery.runtime.expression.constant.ConstantExpression;
import com.xar.logquery.runtime.expression.constant.StringConstant;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.expression.functions.FunctionParameter;
import com.xar.logquery.runtime.expression.functions.exceptions.InvalidParametersException;
import org.joda.time.LocalDate;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Anton Koekemoer on 2014/11/23.
 */
public class TestFunction_TO_REAL {
    @Test
    public void shouldConvertNumbersToDouble() throws InvalidExpressionException, InstantiationException, IllegalAccessException {

        Assert.assertEquals(BigDecimal.valueOf(10), HelperFunctions.executeFunction(ToRealFunction.class, DataType.REAL, 10));
        Assert.assertEquals(BigDecimal.valueOf(10), HelperFunctions.executeFunction(ToRealFunction.class, DataType.REAL, 10L));
        Assert.assertEquals(BigDecimal.valueOf(10.12d), HelperFunctions.executeFunction(ToRealFunction.class, DataType.REAL, "10.12"));
        Assert.assertEquals(null, HelperFunctions.executeFunction(ToRealFunction.class, DataType.REAL, new StringConstant(null)));
        Assert.assertEquals(null, HelperFunctions.executeFunction(ToRealFunction.class, DataType.REAL, new ConstantExpression<Integer>(DataType.REAL, null)));
        Assert.assertEquals(null, HelperFunctions.executeFunction(ToRealFunction.class, DataType.REAL, BooleanConstant.TRUE));
        Assert.assertEquals(BigDecimal.valueOf(10.12d).doubleValue(), ((BigDecimal)HelperFunctions.executeFunction(ToRealFunction.class, DataType.REAL, 10.12f)).doubleValue(), 0.01d);
        Assert.assertEquals(BigDecimal.valueOf(10.12d).doubleValue(), ((BigDecimal)HelperFunctions.executeFunction(ToRealFunction.class, DataType.REAL, 10.12d)).doubleValue(), 0.01d);

        long now = System.currentTimeMillis();
        double dNow = now / (double) (86400000);
        Assert.assertEquals(BigDecimal.valueOf(dNow).doubleValue(), ((BigDecimal)HelperFunctions.executeFunction(ToRealFunction.class, DataType.REAL, new Date(now))).doubleValue(), 0.01d);


    }

    @Test
    public void shouldFailIfIncorrectNumberOfParameters() {

        ToRealFunction function = new ToRealFunction();
        function.setParameters(new FunctionParameter(new StringConstant("10")), new FunctionParameter(new StringConstant("##")));
        try {
            function.validate(new ValidationContext());
            Assert.fail("Expected exception");
        }
        catch (InvalidExpressionException e) {
            Assert.assertEquals("TO_REAL requires one parameter", e.getMessage());
        }
    }



}
