/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.util;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Anton Koekemoer on 2014/11/25.
 */
public class TestStringUtils {

    @Test
    public void isEmptyReturnsTrueIfStringIsNull(){
        Assert.assertTrue(StringUtils.isEmpty(null));
    }

    @Test
    public void isEmptyReturnsTrueIfStringIsEmpty(){
        Assert.assertTrue(StringUtils.isEmpty(""));
    }

    @Test
    public void isEmptyReturnsFalseIfStringHasData(){
        Assert.assertFalse(StringUtils.isEmpty(" "));
    }

    @Test
    public void expandTabsShouldExpandTabs(){
        Assert.assertEquals("    a", StringUtils.expandTabs("\ta", 4));
        Assert.assertEquals("    a", StringUtils.expandTabs(" \ta", 4));
        Assert.assertEquals("    a", StringUtils.expandTabs("  \ta", 4));
        Assert.assertEquals("    a", StringUtils.expandTabs("   \ta", 4));
        Assert.assertEquals("        a", StringUtils.expandTabs("    \ta", 4));
        Assert.assertEquals("        a", StringUtils.expandTabs("     \ta", 4));
    }

    @Test
    public void shouldRTrimString(){

        Assert.assertEquals("    aaaaa", StringUtils.rtrim("    aaaaa   "));
        Assert.assertEquals("    aaaaa", StringUtils.rtrim("    aaaaa   \t \t "));
        Assert.assertEquals("\t    aaaaa", StringUtils.rtrim("\t    aaaaa   \t \t "));
        Assert.assertEquals("", StringUtils.rtrim(""));
    }

    @Test
    public void shouldPadLeft(){
        Assert.assertEquals("   a", StringUtils.padl("a", 4, ' '));
        Assert.assertEquals("   a", StringUtils.padl("a", 4));
    }

    @Test
    public void shouldPadRight(){
        Assert.assertEquals("a   ", StringUtils.padr("a", 4, ' '));
        Assert.assertEquals("a   ", StringUtils.padr("a", 4));
    }





}
