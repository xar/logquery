/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.util;

import com.xar.logquery.runtime.config.RuntimeConfiguration;
import com.xar.logquery.runtime.enums.DataType;

import java.util.Date;

/**
 * Created by akoekemo on 3/10/15.
 */
public class FormatFunctions {

    /**
     * Convert the specified object to a string
     *
     * @param p_object   the object to convert to a string
     * @param p_dataType the data type of the input value
     *
     * @return a string representation of the object
     */
    public static String asString(final Object p_object, final DataType p_dataType) {

        if (p_object == null || p_dataType == DataType.NULL) {
            return RuntimeConfiguration.getNullText();
        }
        switch (p_dataType) {
            case STRING:
                return (String) p_object;
            case INTEGER:
            case REAL:
            case BOOLEAN:
                break;
            case DATE:
                return DateTimeFunctions.toString((Date) p_object, RuntimeConfiguration.getDateFormat());
            case TIMESTAMP:
                return DateTimeFunctions.toString((Date) p_object, RuntimeConfiguration.getTimestampFormat());
        }
        return String.valueOf(p_object);
    }


}
