/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.logic;

import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.BinaryExpression;
import com.xar.logquery.runtime.expression.exceptions.InvalidOperandsException;
import com.xar.logquery.runtime.expression.functions.exceptions.InvalidParametersException;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.expression.interfaces.IFunctionParameter;

/**
 * Created by koekemoera on 2014/08/28.
 */
public class OrCondition extends BinaryExpression {

    public OrCondition(final IExpression p_lhs, final IExpression p_rhs) {

        super(DataType.BOOLEAN, p_lhs, p_rhs);

        addCallback(new DataType[]{DataType.BOOLEAN, DataType.BOOLEAN}, new OrCallback());
    }

    @Override
    protected void throwInvalidParameters(final DataType[][] p_expected, final IFunctionParameter[] p_functionParameters) throws InvalidParametersException {

        DataType operandType = m_functionParameters[0].getDataType();
        if (operandType == DataType.BOOLEAN) { operandType = m_functionParameters[1].getDataType(); }
        throw new InvalidOperandsException(getParseData(), "Invalid data type, found " + operandType + ", expected " + DataType.BOOLEAN);
    }

    private static class OrCallback implements IExecuteCallback {

        @Override
        public Object execute(final Object[] p_parameters) {

            return (Boolean) p_parameters[0] || (Boolean) p_parameters[1];
        }
    }
}
