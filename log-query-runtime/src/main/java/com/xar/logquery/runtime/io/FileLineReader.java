/*
 * Copyright 2014 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Anton Koekemoer on 2014/11/27.
 */
public class FileLineReader {

    private File m_file;

    private DynamicBufferedReader m_reader;

    private int m_lineNumber;

    private int m_lineNumberAtmark;

    public FileLineReader(final File p_file) throws FileNotFoundException {

        m_file = p_file;
        m_reader = new DynamicBufferedReader(new FileReader(m_file));
    }

    /**
     * Get the file being read
     * @return the file being read
     */
    public File getFile() {

        return m_file;
    }

    /**
     * Get the current line number
     * @return the current line number
     */
    public int getLineNumber() {

        return m_lineNumber;
    }

    /**
     * Marks the present position in the stream.  Subsequent calls to reset()
     * will attempt to reposition the stream to this point.  Not all
     * character-input streams support the mark() operation.
     *
     * @throws java.io.IOException If the stream does not support mark(),
     *                             or if some other I/O error occurs
     */
    public void mark() throws IOException {

        m_lineNumberAtmark = m_lineNumber;
        m_reader.mark();
    }

    /**
     * Reads a line of text.  A line is considered to be terminated by any one
     * of a line feed ('\n'), a carriage return ('\r'), or a carriage return
     * followed immediately by a linefeed.
     *
     * @return A String containing the contents of the line, not including
     * any line-termination characters, or null if the end of the
     * stream has been reached
     *
     * @throws java.io.IOException If an I/O error occurs
     * @see java.nio.file.Files#readAllLines
     */
    public String readLine() throws IOException {

        String line = m_reader.readLine();
        if (line != null) { m_lineNumber++; }
        return line;
    }

    /**
     * Resets the stream.  If the stream has been marked, then attempt to
     * reposition it at the mark.  If the stream has not been marked, then
     * attempt to reset it in some way appropriate to the particular stream,
     * for example by repositioning it to its starting point.  Not all
     * character-input streams support the reset() operation, and some support
     * reset() without supporting mark().
     *
     * @throws java.io.IOException If the stream has not been marked,
     *                             or if the mark has been invalidated,
     *                             or if the stream does not support reset(),
     *                             or if some other I/O error occurs
     */
    public void reset() throws IOException {

        m_reader.reset();
        m_lineNumber = m_lineNumberAtmark;
    }

    /**
     * Closes the stream and releases any system resources associated with
     * it.  Once the stream has been closed, further read(), ready(),
     * mark(), reset(), or skip() invocations will throw an IOException.
     * Closing a previously closed stream has no effect.
     *
     * @throws java.io.IOException If an I/O error occurs
     */
    public void close() throws IOException {

        m_reader.close();
    }
}
