/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression;

import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;
import com.xar.logquery.runtime.parse.ParseData;

/**
 * A named expression implementation
 */
public class NamedExpression extends AbstractExpression {

    IExpression m_expression;

    String m_name;

    public NamedExpression(final IExpression p_expression, final String p_name) {

        this(p_expression, p_name, p_expression.getParseData());
    }

    public NamedExpression(final IExpression p_expression, final String p_name, ParseData p_parseData) {

        setExpression(p_expression);
        setName(p_name);
        setParseData(p_parseData);
    }

    /**
     * Get the child expressions of this expression
     *
     * @return the child expressions of this expression
     */
    @Override
    public IExpression[] getChildren() {

        return new IExpression[]{m_expression};
    }

    /**
     * Get the datatype of the expression
     *
     * @return the datatype of the expression
     */
    @Override
    public DataType getDataType() {

        return m_expression.getDataType();
    }

    public IExpression getExpression() {

        return m_expression;
    }

    public void setExpression(final IExpression p_expression) {

        m_expression = p_expression;
    }

    /**
     * Get the alias for the object
     *
     * @return the alias for the object
     */
    public String getName() {

        return m_name;
    }

    /**
     * Sets the alias for the object.
     *
     * @param p_name the alias
     */
    public void setName(final String p_name) {

        m_name = p_name;
    }

    /**
     * Get the parseData for the expression
     *
     * @return the parse data for the expression
     */
    @Override
    public ParseData getParseData() {

        ParseData parseData = super.getParseData();
        return parseData == null ? m_expression.getParseData() : parseData;
    }

    /**
     * Get the value of the expression, based on the current row context
     *
     * @param p_context the current row context
     *
     * @return the value of the expression.
     */
    @Override
    public Object getValue(final IExecutionContext p_context) {

        return m_expression.getValue(p_context);
    }

    @Override
    public String toString() {

        return "NamedExpression{" +
                "expression=" + m_expression +
                ", alias='" + m_name + '\'' +
                '}';
    }

    /**
     * Validates the expression against the specified context
     *
     * @param p_context the context to validate the expression against
     */
    @Override
    public void validateExpression(final IValidationContext p_context) throws InvalidExpressionException {
        // Nothing to do here
    }
}
