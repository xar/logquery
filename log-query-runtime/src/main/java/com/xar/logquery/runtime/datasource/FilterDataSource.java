/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource;

import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.ResultSet;
import com.xar.logquery.runtime.datasource.filter.ExpressionFilterFactory;
import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IFilter;
import com.xar.logquery.runtime.datasource.interfaces.IFilterFactory;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;

import java.util.Enumeration;
import java.util.NoSuchElementException;

/**
 * An implementation of the datasource object that supports filtering
 */
public class FilterDataSource extends AbstractDataSource {

    /**
     * The contained datasource
     */
    IDataSource m_dataSource;

    /**
     * Row filter
     */
    IFilterFactory m_filterFactory;

    /**
     * Instantiates a new Filter data source.
     *
     * @param p_dataSource the data source to filter
     * @param p_condition  the condition of the filter
     */
    public FilterDataSource(final IDataSource p_dataSource, final IExpression p_condition) {

        this(p_dataSource, new ExpressionFilterFactory(p_condition));
    }

    public FilterDataSource(final IDataSource p_dataSource, final IFilterFactory p_filterFactory) {

        m_dataSource = p_dataSource;
        m_filterFactory = p_filterFactory;
    }

    /**
     * Execute the query and returns a resultset
     *
     * @param p_context the execution context
     *
     * @return the resultset
     *
     * @throws com.xar.logquery.runtime.exceptions.DataSourceException if the execution fails
     */
    @Override
    protected IResultSet executeQuery(final IExecutionContext p_context) throws DataSourceException {

        IResultSet resultSet = m_dataSource.getResultSet(p_context);
        return new ResultSet(resultSet.getColumnInfo(), new FilterEnumeration(p_context, resultSet, m_filterFactory.create()));
    }

    /**
     * Validate the datasource
     *
     * @param p_context the binding context
     *
     * @throws com.xar.logquery.runtime.exceptions.DataSourceException if the datasource is invalid
     */
    @Override
    protected void validateQuery(final IValidationContext p_context) throws DataSourceException {

        m_dataSource.validate(p_context);
        m_filterFactory.validate(p_context);
    }

    /**
     * Filter enumeration
     */
    private class FilterEnumeration implements Enumeration<IDataRow> {

        /**
         * The expression context.
         */
        final IExecutionContext m_context;

        /**
         * THe columns returned
         */
        private final IColumnInfo[] m_columnInfo;

        /**
         * The enumeration being filtered.
         */
        Enumeration<IDataRow> m_enumeration;

        /**
         * The current row.
         */
        IDataRow m_currentRow;

        IFilter m_filter;

        /**
         * Initialise the filter enumeration
         *
         * @param p_context the execution context
         *
         * @throws DataSourceException
         */
        private FilterEnumeration(final IExecutionContext p_context, IResultSet p_resultSet, IFilter p_filter) throws DataSourceException {

            m_enumeration = p_resultSet.getRows();
            m_columnInfo = p_resultSet.getColumnInfo();
            m_context = new ExecutionContext(p_context);
            m_filter = p_filter;
            advance();
        }

        /**
         * Tests if this enumeration contains more elements.
         *
         * @return <code>true</code> if and only if this enumeration object
         * contains at least one more element to provide;
         * <code>false</code> otherwise.
         */
        @Override
        public boolean hasMoreElements() {

            if (m_currentRow == null) { advance(); }
            return m_currentRow != null;
        }

        /**
         * Returns the next element of this enumeration if this enumeration
         * object has at least one more element to provide.
         *
         * @return the next element of this enumeration.
         *
         * @throws java.util.NoSuchElementException if no more elements exist.
         */
        @Override
        public IDataRow nextElement() {

            if (m_currentRow == null) { advance(); }
            if (m_currentRow == null) { throw new NoSuchElementException();}
            try {
                return m_currentRow;
            } finally {
                m_currentRow = null;
            }
        }

        /**
         * Move to the next available row
         */
        private void advance() {

            m_currentRow = null;
            while (m_enumeration.hasMoreElements()) {
                IDataRow row = m_enumeration.nextElement();
                final Object[] rowData = row.getData();

                m_context.reset();
                if (m_filter.accept(m_context, m_columnInfo, rowData)) {
                    m_currentRow = row;
                    break;
                }
            }
        }
    }

}
