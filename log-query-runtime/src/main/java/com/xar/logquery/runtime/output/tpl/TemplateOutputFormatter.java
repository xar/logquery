/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.output.tpl;

import com.xar.cli.annotations.CommandLineArgument;
import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.config.RuntimeConfiguration;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.output.interfaces.IOutputFormatter;
import com.xar.logquery.runtime.output.tpl.directive.AvgDirective;
import com.xar.logquery.runtime.output.tpl.directive.BodyDirective;
import com.xar.logquery.runtime.output.tpl.directive.FirstDirective;
import com.xar.logquery.runtime.output.tpl.directive.LastDirective;
import com.xar.logquery.runtime.output.tpl.directive.MaxDirective;
import com.xar.logquery.runtime.output.tpl.directive.MinDirective;
import com.xar.logquery.runtime.output.tpl.directive.SumDirective;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;

import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Template based output formatting. Uses Freemarker template engine
 * <p/>
 * Created by Anton Koekemoer on 2014/12/01.
 */
public class TemplateOutputFormatter implements IOutputFormatter {

    @CommandLineArgument(name = "tpl", description = "Template file name")
    private String templateFile;

    @CommandLineArgument(name = "dsn", description = "Template variable name for the datasource object")
    private String dataSourceName = "dataSource";

    @Override
    public void format(final IDataSource p_dataSource) throws DataSourceException {

        File template = new File(templateFile).getAbsoluteFile();

        // Create your Configuration instance, and specify if up to what FreeMarker
        // version (here 2.3.21) do you want to apply the fixes that are not 100%
        // backward-compatible. See the Configuration JavaDoc for details.
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_21);

        // Specify the source where the template files come from. We use the
        // parent directory of the template we are using
        try {
            cfg.setDirectoryForTemplateLoading(template.getParentFile());

            cfg.setDefaultEncoding("UTF-8");
            cfg.setDateFormat(RuntimeConfiguration.getDateFormat());
            cfg.setDateTimeFormat(RuntimeConfiguration.getTimestampFormat());

            // Sets how errors will appear.
            cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);

            // Create the root hash
            Map root = new HashMap();


            root.put("dataSource", p_dataSource.getResultSet(new ExecutionContext()));
            // We define the body tag.
            root.put("body", new BodyDirective(p_dataSource, new ExecutionContext()));

            root.put("sum", new SumDirective());
            root.put("avg", new AvgDirective());
            root.put("min", new MinDirective());
            root.put("max", new MaxDirective());
            root.put("first", new FirstDirective());
            root.put("last", new LastDirective());

            Template temp = cfg.getTemplate(template.getName());

            Writer out = new OutputStreamWriter(System.out);
            temp.process(root, out);
        }
        catch (IOException | TemplateException e) {
            throw new DataSourceException(e.getMessage(), e);
        }

    }

    /**
     * Set the template for the formatter
     *
     * @param p_template the template file
     */
    public void setTemplate(String p_template) {

        this.templateFile = p_template;
    }


}
