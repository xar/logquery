/*
* Copyright 2014 Anton Koekemoer
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.xar.logquery.runtime.util;

import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.exceptions.UnconvertableValueException;
import org.joda.time.LocalDate;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.util.Date;

/**
 * Created by Anton Koekemoer on 2014/12/12.
 */
public class ConvertFunctions {

    private static ConvertFunctions _instance = new ConvertFunctions();

    private ConvertFunctions() {

    } // prevent instance creation

    /**
     * Used to convert a value to the specified data type for the purposes of a function call
     *
     * @param p_value      the value to convert
     * @param p_dataTypeTo the data type to convert the value to
     *
     * @return an object of a type appropriate to the requested data type
     */
    public static Object convertValue(Object p_value, final DataType p_dataTypeTo) {

        if (p_value == null) {
            return null;
        }
        if (p_dataTypeTo == DataType.DATE || p_dataTypeTo == DataType.TIMESTAMP) {
            if (p_value instanceof String) {
                try {
                    p_value = DateTimeFunctions.parseDate((String) p_value);
                }
                catch (ParseException e) {
                }
            }
        }
        try {
            switch (p_dataTypeTo) {
                case INTEGER: { return ConvertFunctions.toInteger(p_value); }
                case REAL: { return ConvertFunctions.toReal(p_value); }
                case STRING: { return p_value.toString(); }
                case DATE: { return ConvertFunctions.toDate(p_value); }
                case TIMESTAMP: { return ConvertFunctions.toTimestamp(p_value); }
            }
        }
        catch (UnconvertableValueException ignored) {
        }
        return p_value;
    }

    public static BigInteger safeToInteger(final Object p_value) {

        try {
            return toInteger(p_value);
        }
        catch (UnconvertableValueException e) {

        }
        return null;
    }

    /**
     * Converts the specified object to a real number
     *
     * @param p_value the value
     *
     * @return the value converted to a big decimal
     */
    public static BigDecimal safeToReal(final Object p_value) {

        try {
            return toReal(p_value);
        }
        catch (UnconvertableValueException ignored) {
        }
        return null;
    }

    /**
     * Convert the specified value to a date (without the time fields set)
     *
     * @param p_value the value to convert
     *
     * @return the specified value as a date (without the time fields set)
     */
    public static Date toDate(final Object p_value) throws UnconvertableValueException {

        if (p_value == null) { return null; }
        return LocalDate.fromDateFields(toTimestamp(p_value)).toDate();
    }

    /**
     * Converts the specified object to a big integer
     *
     * @param p_value the value to convert
     *
     * @return the value as a big integer
     *
     * @throws UnconvertableValueException if the value is not compatible with the big integer
     */
    public static BigInteger toInteger(Object p_value) throws UnconvertableValueException {

        try {
            if (p_value == null) { return null; }
            if (p_value instanceof BigInteger) {return (BigInteger) p_value;}
            if (p_value instanceof String) {
                return BigInteger.valueOf(new BigDecimal(((String) p_value)).longValue());
            }
            if (p_value instanceof Number) { return BigInteger.valueOf(((Number) p_value).longValue());}
            if (p_value instanceof Date) { return new BigInteger(String.valueOf(((Date) p_value).getTime())); }
        }
        catch (NumberFormatException ignored) {}
        throw new UnconvertableValueException("Value '" + p_value + "' could not be converted to an integer");

    }

    /**
     * Converts the specified object to a big decimal
     *
     * @param p_value the value to convert
     *
     * @return the value as a big decimal
     *
     * @throws UnconvertableValueException if the value is not compatible with the big decimal
     */
    public static BigDecimal toReal(Object p_value) throws UnconvertableValueException {

        try {
            if (p_value == null) { return null; }
            if (p_value instanceof BigDecimal) {return (BigDecimal) p_value;}
            if (p_value instanceof String) { return new BigDecimal((String) p_value); }
            if (p_value instanceof Double) { return new BigDecimal(((Double) p_value));}
            if (p_value instanceof Number) { return new BigDecimal(((Number) p_value).doubleValue());}
            if (p_value instanceof Date) {
                return new BigDecimal(((Date) p_value).getTime() / (double) DateTimeFunctions.MILLIS_PER_DAY);
            }
        }
        catch (NumberFormatException ignored) {
        }
        throw new UnconvertableValueException("Value '" + p_value + "' could not be converted to a real");
    }

    /**
     * Convert the specified value to a date
     *
     * @param p_value the value to convert
     *
     * @return the specified value as a date
     */
    public static Date toTimestamp(final Object p_value) throws UnconvertableValueException {

        if (p_value == null) { return null; }
        try {
            if (p_value instanceof String) {
                return DateTimeFunctions.parseDate((String) p_value);
            }
            else if (TypeFunctions.isValueTypeOf(p_value, int.class, Integer.class, long.class, Long.class, BigInteger.class)) {
                return new Date(((Number) p_value).longValue());
            }
            else if (TypeFunctions.isValueTypeOf(p_value, float.class, Float.class, double.class, Double.class, BigDecimal.class)) {
                return new Date((long) (((Number) p_value).doubleValue() * DateTimeFunctions.MILLIS_PER_DAY));
            }
            else if (p_value instanceof Date) {
                return (Date) p_value;
            }
        }
        catch (ParseException ignored) {

        }
        throw new UnconvertableValueException("Unable to convert '" + p_value + "' to a date");
    }


}
