/*
 * Copyright 2014 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.util;

import com.xar.logquery.runtime.annotations.Function;
import com.xar.logquery.runtime.config.RuntimeConfiguration;
import com.xar.logquery.runtime.datasource.annotations.DataSourceAttribute;
import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.text.TextLineDataSource;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.exceptions.RequiredAttributeMissing;
import com.xar.logquery.runtime.exceptions.UnknownAttributeException;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.expression.interfaces.IFunctionParameter;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.io.ParseBuffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Runtime Functions
 */
public class RuntimeFunctions {

    /**
     * The shared logger instance.
     */
    private static final Logger logger = LoggerFactory.getLogger(RuntimeFunctions.class);


    private static RuntimeFunctions _instance = new RuntimeFunctions();

    private RuntimeFunctions() {

    } // Prevent instantiation

    /**
     * Applies a data row to an execution context. Copies the values from the row into the context
     *
     * @param p_context the context to copy the values to
     * @param p_row     the row to copy the values from
     *
     * @return The expression context
     */
    public static IExecutionContext applyRowToContext(final IExecutionContext p_context, final IDataRow p_row) {

        p_context.reset();

        IColumnInfo[] columnInfo = p_row.getColumnInfo();
        final Object[] rowData = p_row.getData();

        for (int i = 0; i < rowData.length; i++) {

            p_context.setValue(columnInfo[i].getContext(), columnInfo[i].getName(), rowData[i]);
        }
        return p_context;
    }


    /**
     * Build the parameter error message.
     *
     * @param p_class              the function class
     * @param p_functionParameters the function parameters
     * @param p_expectedParameters the expected parameters
     *
     * @return the string builder
     */
    public static String buildParameterErrorMessage(final Class<?> p_class, final IFunctionParameter[] p_functionParameters, final DataType[][] p_expectedParameters) {

        String functionName = getFunctionName(p_class);
        StringBuilder parameters = new StringBuilder();
        parameters.append(getFunctionCallPrototype(functionName, p_functionParameters));
        parameters.append(" expected one of\n");

        for (int i = 0; i < p_expectedParameters.length; i++) {
            parameters.append("\n").append(functionName).append("(");
            for (DataType type : p_expectedParameters[i]) {
                parameters.append(type).append(", ");
            }
            if (p_expectedParameters[i].length > 0) { parameters.delete(parameters.length() - 2, parameters.length()); }
            parameters.append(")");
        }
        return parameters.toString();
    }

    /**
     * Compare the two values.
     *
     * @param p_left  the left value
     * @param p_right the right value
     *
     * @return the value 0 if the right value is equal to the left value; a value less than 0 if the left value is smaller than the right value; and a value greater than 0 if the left value is greater than the right value.
     */
    public static int compare(final Object p_left, final Object p_right) {

        if (p_left == p_right) { return 0; }
        if (p_left == null && p_right != null) {
            return -1;
        }
        if (p_left != null && p_right == null) {
            return 1;
        }
        if (p_left instanceof Number && p_right instanceof Number) {
            double v1 = ((Number) p_left).doubleValue();
            double v2 = ((Number) p_right).doubleValue();

            if (v1 < v2) { return -1; }
            if (v1 > v2) { return 1; }
            return 0;
        }

        if (p_left instanceof String && p_right instanceof String) {
            return ((String) p_left).compareTo(((String) p_right));
        }

        if (p_left instanceof Date && p_right instanceof Date) {
            long v1 = ((Date) p_left).getTime();
            long v2 = ((Date) p_right).getTime();

            if (v1 < v2) { return -1; }
            if (v1 > v2) { return 1; }
            return 0;
        }

        if (p_left instanceof Boolean) {
            return ((Boolean) p_left ? 1 : 0) - ((Boolean) p_right ? 1 : 0);
        }
        return -1;
    }

    /**
     * Convert the object specified to a value of the specified type
     *
     * @param p_value
     * @param p_toType
     *
     * @return
     */
    public static Object convert(final Object p_value, final Class<?> p_toType) {

        if (p_toType == String.class) { return p_value.toString();}

        if (p_value instanceof Number) {
            if (p_toType == byte.class || p_toType == Byte.class) {return ((Number) p_value).byteValue();}
            else if (p_toType == char.class || p_toType == Character.class) {
                return (char) ((Number) p_value).intValue();
            }
            else if (p_toType == short.class || p_toType == Short.class) { return ((Number) p_value).shortValue();}
            else if (p_toType == int.class || p_toType == Integer.class) { return ((Number) p_value).intValue();}
            else if (p_toType == long.class || p_toType == Long.class) { return ((Number) p_value).longValue();}
            else if (p_toType == float.class || p_toType == Float.class) { return ((Number) p_value).floatValue();}
            else if (p_toType == double.class || p_toType == Double.class) { return ((Number) p_value).doubleValue();}
        }

        if (p_value instanceof String) {
            String value = (String) p_value;
            if (p_toType == byte.class || p_toType == Byte.class) {return value.getBytes()[0];}
            else if (p_toType == char.class || p_toType == Character.class) {return value.toCharArray()[0];}
            else if (p_toType == short.class || p_toType == Short.class) { return Short.parseShort(value);}
            else if (p_toType == int.class || p_toType == Integer.class) { return Integer.parseInt(value);}
            else if (p_toType == long.class || p_toType == Long.class) { return Long.parseLong(value);}
            else if (p_toType == float.class || p_toType == Float.class) { return Float.parseFloat(value);}
            else if (p_toType == double.class || p_toType == Double.class) { return Double.parseDouble(value);}
        }
        return p_value;
    }

    /**
     * Construct a new Custom datasource
     *
     * @param p_name the name of the datasource to construct
     *
     * @return a new custom datasource
     */
    public static IDataSource createCustomDataSource(String p_name) {

        logger.debug("Find type for DataSource: " + p_name);
        Class<?> type = RuntimeConfiguration.getDatasource(p_name);

        if (type != null) {
            try {
                logger.debug("Construct new datasource for DataSource: " + p_name + "; Type: " + type);
                return (IDataSource) type.newInstance();
            }
            catch (InstantiationException | IllegalAccessException e) {
                logger.error("Unable to construct type: " + type, e);
            }
        }
        else {
            logger.debug("Unable to find type for DataSource: " + p_name + " - Default to textline");
        }
        return null;
    }

    /**
     * Create an instance of the named datasource
     *
     * @param p_name the name of a datasource
     *
     * @return a new instance of the datasource
     */
    public static IDataSource createDataSource(final String p_name) {

        logger.debug("Find type for DataSource: " + p_name);
        Class<?> type = RuntimeConfiguration.getDatasource(p_name);

        if (type != null) {
            try {
                logger.debug("Construct new datasource for DataSource: " + p_name + "; Type: " + type);
                return (IDataSource) type.newInstance();
            }
            catch (InstantiationException | IllegalAccessException e) {
                logger.error("Unable to construct type: " + type, e);
            }
        }
        else {
            logger.debug("Unable to find type for DataSource: " + p_name + " - Default to textline");
        }
        // CONSOLE: Unable to determine file type, default to TEXTLINE
        return new TextLineDataSource();

    }

    public static List<Object[]> getData(final Enumeration<IDataRow> p_rowsLeft) {

        return getData(p_rowsLeft, new ArrayList<Object[]>());
    }

    public static <T extends List> T getData(final Enumeration<IDataRow> p_rowsLeft, T p_list) {

        while (p_rowsLeft.hasMoreElements()) {
            p_list.add(p_rowsLeft.nextElement().getData());
        }
        return p_list;
    }

    /**
     * Get the fully qualified name of the column
     *
     * @param p_columnInfo the column info for which to get the fully qualified name
     *
     * @return the fully qualified name
     */
    public static String getFQN(IColumnInfo p_columnInfo) {

        return getFQN(p_columnInfo.getContext(), p_columnInfo.getName());
    }

    /**
     * Get the fully qualified name based on the context and name
     *
     * @param p_context the context
     * @param p_name    the name
     *
     * @return the fully qualified name
     */
    public static String getFQN(String p_context, String p_name) {

        if (StringUtils.isEmpty(p_context)) {
            return p_name;
        }
        return p_context + "." + p_name;
    }

    /**
     * Get a string representation fro a function call prototype for the function parameter
     *
     * @param p_functionName       the name of the function
     * @param p_functionParameters the parameters to the function
     *
     * @return a string representation fro a function call prototype for the function parameter
     */
    public static String getFunctionCallPrototype(final String p_functionName, final DataType[] p_functionParameters) {

        StringBuilder parameters = new StringBuilder();
        parameters.append(p_functionName).append("(");
        if (p_functionParameters != null) {
            for (DataType parameter : p_functionParameters) {
                parameters.append(parameter).append(", ");
            }
            if (p_functionParameters.length > 0) { parameters.delete(parameters.length() - 2, parameters.length()); }
        }
        parameters.append(")");

        return parameters.toString();
    }

    /**
     * Get a string representation fro a function call prototype for the function parameter
     *
     * @param p_functionName       the name of the function
     * @param p_functionParameters the parameters to the function
     *
     * @return a string representation fro a function call prototype for the function parameter
     */
    public static String getFunctionCallPrototype(final String p_functionName, final IFunctionParameter[] p_functionParameters) {

        DataType[] parameters = new DataType[p_functionParameters.length];
        for (int i = 0; i < p_functionParameters.length; i++) {
            parameters[i] = p_functionParameters[i].getDataType();
        }
        return getFunctionCallPrototype(p_functionName, parameters);
    }

    /**
     * Get the name of the function defined by the class.
     * <p/>
     * Will examine the class for the Function annotation, and use the first name, otherwise returns the function class
     *
     * @param p_class the class
     *
     * @return the name of the function
     */
    public static String getFunctionName(final Class<?> p_class) {

        String functionName = p_class.getSimpleName();
        Function annotation = p_class.getAnnotation(Function.class);
        if (annotation != null) {
            functionName = annotation.names()[0];
        }
        return functionName;
    }

    /**
     * Initialise the datasource with the values in the map
     *
     * @param p_dataSource the datasource
     * @param p_attributes the attributes
     *
     * @throws RequiredAttributeMissing
     */
    public static void initialiseDataSource(IDataSource p_dataSource, Map<String, IExpression> p_attributes, IExecutionContext p_context) throws RequiredAttributeMissing, UnknownAttributeException {

        HashMap<String, IExpression> attributes = new HashMap<>(p_attributes);
        Class<?> argumentClass = p_dataSource.getClass();

        while (argumentClass != Object.class) {

            Field[] fields = argumentClass.getDeclaredFields();

            for (Field field : fields) {
                DataSourceAttribute annotation = field.getAnnotation(DataSourceAttribute.class);
                String name;
                IExpression expression;
                field.setAccessible(true);
                if (annotation != null) {
                    name = annotation.name();
                    expression = attributes.get(annotation.name());
                    try {
                        if (expression == null && annotation.required() && field.get(p_dataSource) == null) {
                            throw new RequiredAttributeMissing(annotation.name() + " - Is a required attribute");
                        }
                    }
                    catch (IllegalAccessException e) {
                        throw new InternalError(e.getMessage());
                    }
                }
                else {
                    name = field.getName();
                    expression = attributes.get(field.getName());
                }

                if (expression != null) {
                    attributes.remove(name);
                    try {
                        field.set(p_dataSource, convert(expression.getValue(p_context), field.getType()));
                    }
                    catch (IllegalAccessException e) {
                        throw new InternalError(e.getMessage());
                    }
                }
            }
            argumentClass = argumentClass.getSuperclass();
        }
        if (attributes.size() > 0) {
            for (Map.Entry<String, IExpression> entry : attributes.entrySet()) {
                throw new UnknownAttributeException(entry.getValue().getParseData(), "Unknown attribute: " + entry.getKey());
            }
        }
    }

    /**
     * Convert a LIKE string to an equivalent regular expression
     *
     * @param p_pattern the like patter
     * @param p_escape  the escape sequence for the like
     *
     * @return a regular expression
     */
    public static String like2Regex(String p_pattern, String p_escape) {

        StringBuilder regex = new StringBuilder();
        ParseBuffer buffer = new ParseBuffer(p_pattern);
        regex.append('^');

        while (true) {
            int c;
            if (buffer.la(p_escape)) {
                buffer.skip(p_escape.length());
                c = buffer.next();
                if (c != -1) {
                    escapeChar(regex, (char) c);
                }
                continue;
            }
            c = buffer.next();
            if (c == -1) { break; }
            else if (c == '_') { regex.append('.'); }
            else if (c == '%') { regex.append(".*"); }
            else { escapeChar(regex, (char) c); }
        }

        regex.append('$');

        return regex.toString();

    }

    /**
     * Escapes a regular expression character
     *
     * @param p_regex
     * @param p_char
     */
    private static void escapeChar(final StringBuilder p_regex, final char p_char) {

        final char[] escapes = {'(', ')', '[', ']', '.', '*', '+', '{', '}', '$', '^'};
        for (int i = 0; i < escapes.length; i++) {
            if (p_char == escapes[i]) {
                p_regex.append("\\").append(escapes[i]);
                return;
            }
        }
        p_regex.append(p_char);
    }


}
