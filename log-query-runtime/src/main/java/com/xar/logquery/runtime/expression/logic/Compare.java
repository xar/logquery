/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.logic;

import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.BinaryExpression;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.parse.ParseData;
import com.xar.logquery.runtime.util.RuntimeFunctions;

/**
 * Comparison operator base class
 */
public class Compare extends BinaryExpression {

    public Compare(final IExpression p_lhs, final IExpression p_rhs) {

        super(DataType.BOOLEAN, p_lhs, p_rhs);

        IExecuteCallback executeCallback = new CompareCallback();

        addCallback(new DataType[]{DataType.REAL, DataType.REAL}, executeCallback);
        addCallback(new DataType[]{DataType.TIMESTAMP, DataType.TIMESTAMP}, executeCallback);
        addCallback(new DataType[]{DataType.DATE, DataType.DATE}, executeCallback);
        addCallback(new DataType[]{DataType.STRING, DataType.STRING}, executeCallback);
        addCallback(new DataType[]{DataType.BOOLEAN, DataType.BOOLEAN}, executeCallback);
    }

    public Compare(final IExpression p_lhs, final IExpression p_rhs, final ParseData p_parseData) {

        this(p_lhs, p_rhs);
        setParseData(p_parseData);

    }


    private static class CompareCallback implements IExecuteCallback {

        @Override
        public Object execute(final Object[] p_parameters) {

            return RuntimeFunctions.compare(p_parameters[0], p_parameters[1]);
        }
    }
}
