/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.logic;

import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.ValidationContext;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.expression.AbstractExpression;
import com.xar.logquery.runtime.expression.exceptions.EvaluationException;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;
import com.xar.logquery.runtime.util.RuntimeFunctions;

import java.util.Enumeration;

/**
 * Implementation of the IN predicate
 */
public class CompareIN extends AbstractExpression {

    IExpression m_value;

    IDataSource m_values;

    public CompareIN(final IExpression p_value, final IDataSource p_values) {

        m_value = p_value;
        m_values = p_values;
    }

    /**
     * Get the child expressions of this expression
     *
     * @return the child expressions of this expression
     */
    @Override
    public IExpression[] getChildren() {

        return new IExpression[]{m_value};
    }

    /**
     * Get the datatype of the expression
     *
     * @return the datatype of the expression
     */
    @Override
    public DataType getDataType() {

        return DataType.BOOLEAN;
    }

    /**
     * Get the value of the expression, based on the current row context
     *
     * @param p_context the current row context
     *
     * @return the value of the expression.
     */
    @Override
    public Object getValue(final IExecutionContext p_context) {

        Object value = m_value.getValue(p_context);

        try {
            IResultSet resultSet = m_values.getResultSet(new ExecutionContext(p_context));
            Enumeration<IDataRow> rows = resultSet.getRows();
            while (rows.hasMoreElements()) {
                if (RuntimeFunctions.compare(value, rows.nextElement().get(0)) == 0) {
                    return true;
                }
            }
        }
        catch (DataSourceException e) {
            throw new EvaluationException(e.getMessage());
        }
        return false;
    }

    /**
     * Validates the expression against the specified context
     *
     * @param p_context the context to validate the expression against
     */
    @Override
    public void validateExpression(final IValidationContext p_context) throws InvalidExpressionException {

        try {
            m_values.validate(new ValidationContext(p_context));
        }
        catch (DataSourceException e) {
            throw new InvalidExpressionException(e.getParseData(), e.getBasicMessage());
        }
    }
}
