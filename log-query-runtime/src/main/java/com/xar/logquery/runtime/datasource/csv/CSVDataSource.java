/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource.csv;

import com.xar.logquery.runtime.AliasableColumnInfo;
import com.xar.logquery.runtime.DataRow;
import com.xar.logquery.runtime.datasource.annotations.DataSourceAttribute;
import com.xar.logquery.runtime.datasource.csv.interfaces.ICSVParser;
import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.text.TextFileDataSource;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.interfaces.IValidationContext;
import com.xar.logquery.runtime.util.FileFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

/**
 * Implementation of a data source that reads data from a comma separated file
 */
public class CSVDataSource extends TextFileDataSource<ICSVParser> {

    /**
     * The shared logger instance.
     */
    private static final Logger logger = LoggerFactory.getLogger(CSVDataSource.class);

    /**
     * If set to true, the first line of the file has the headings
     */
    @DataSourceAttribute(name = "heading", description = "Indicates wether the first row of the input file contains headings", required = false)
    private boolean heading = true;

    /**
     * The file currently being processed
     */
    private File m_currentFile;

    /**
     * Instantiates a new Text file datasource.
     *
     * @param p_filePattern the file pattern
     * @param p_alias       the alias
     */
    public CSVDataSource(final String p_filePattern, final String p_alias) {

        super(p_filePattern, p_alias);
    }

    /**
     * Closes the file reader
     *
     * @param p_icsvParser the file reader
     */
    @Override
    protected void closeFileReader(final ICSVParser p_icsvParser) {

        try {
            p_icsvParser.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Instantiates a new Text file datasource.
     */
    public CSVDataSource() {

    }

    /**
     * Set wether the first row in the fil represents a heading
     *
     * @param p_heading true if the first row in the file is a heading, false otherwise
     */
    public void setHeading(final boolean p_heading) {

        heading = p_heading;
    }

    /**
     * Create the reader appropriate for this datasource
     *
     * @param p_fileName the file to be read
     *
     * @return the reader
     *
     * @throws java.io.FileNotFoundException the file not found exception
     */
    @Override
    protected ICSVParser createFileReader(final File p_fileName) throws IOException {

        logger.debug("Create reader for file - {}", p_fileName.getName());
        // We can now create a file reader based on some selected format
        // For now we always use RFC-4180
        ICSVParser parser = new CSVParser4180(p_fileName);
        m_currentFile = p_fileName;

        IColumnInfo[] columnInfo = getColumnInfo();
        // No column info yet, this is the first file
        if (heading) {
            String[] headings = parser.nextRow();
            if (columnInfo == null) {
                if (headings != null) {
                    logger.debug("Create headings from first row data in file {}", p_fileName.getName());
                    columnInfo = new IColumnInfo[headings.length];

                    for (int i = 0; i < headings.length; i++) {
                        columnInfo[i] = new AliasableColumnInfo(this, headings[i], DataType.STRING);
                    }
                }
                setColumnInfo(columnInfo);
            }
        }
        else if (columnInfo == null) {
            parser.mark();
            String[] headings = parser.nextRow();
            if (headings != null) {
                logger.debug("Create default headings");
                columnInfo = new IColumnInfo[headings.length];

                for (int i = 0; i < headings.length; i++) {
                    columnInfo[i] = new AliasableColumnInfo(this, columnName(i), DataType.STRING);
                }
            }
            parser.reset();
            setColumnInfo(columnInfo);
        }


        return parser;
    }

    /**
     * Construct a row from the data
     *
     * @param p_text the text
     *
     * @return a new data row
     */
    @Override
    protected IDataRow readLine(final ICSVParser p_text) throws IOException {

        final String[] data = p_text.nextRow();
        if (data != null) {
            IColumnInfo[] columnInfo = getColumnInfo();

            Object[] cells = new Object[columnInfo.length];
            cells[0] = m_currentFile.getAbsolutePath();
            cells[1] = p_text.getLineNumber();
            System.arraycopy(data, 0, cells, 2, data.length);
            return new DataRow(columnInfo, cells);
        }
        return null;
    }

    /**
     * Set the column info for the datasource
     *
     * @param p_columns the column info
     */
    @Override
    protected void setColumnInfo(final IColumnInfo[] p_columns) {

        IColumnInfo[] columns = new IColumnInfo[p_columns.length + 2];

        columns[0] = new AliasableColumnInfo(this, "FileName", DataType.STRING);
        columns[1] = new AliasableColumnInfo(this, "RowNumber", DataType.INTEGER);

        System.arraycopy(p_columns, 0, columns, 2, p_columns.length);

        super.setColumnInfo(columns);
    }

    /**
     * Bind the datasource to the execution context
     *
     * @param p_context the binding context
     *
     * @throws com.xar.logquery.runtime.exceptions.DataSourceException if the datasource is invalid
     */
    @Override
    protected void validateQuery(final IValidationContext p_context) throws DataSourceException {

        try {
            createColumnInfo();
        }
        catch (IOException e) {
            throw new DataSourceException(e.getMessage());
        }
        super.validateQuery(p_context);
    }

    /**
     * Synthesize a columnName
     *
     * @param p_index The columnIndex
     *
     * @return a synthetic name for the column
     */
    private String columnName(final int p_index) {

        int v = p_index + 1;
        StringBuilder builder = new StringBuilder();
        while (v > 0) {
            builder.insert(0, (char) ('A' + ((v % 26) - 1)));
            v /= 26;
        }
        return builder.toString();
    }

    /**
     * Creates the column info
     *
     * @throws IOException
     */
    private void createColumnInfo() throws IOException, DataSourceException {

        if (getColumnInfo() == null) {
            for (File f : FileFunctions.listFiles(getFilePattern())) {
                try {
                    if (createFileReader(f) != null) {
                        break;
                    }
                }
                catch (Exception ex) {
                    logger.error("Unable to open file", ex);                }

            }
        }
        if (getColumnInfo() == null) {
            throw new DataSourceException("Error loading column info");
        }
    }


}
