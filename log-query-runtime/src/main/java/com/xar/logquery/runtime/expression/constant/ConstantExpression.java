/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.constant;

import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.AbstractExpression;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;

/**
 * Expression that returns a constant value
 */
public class ConstantExpression<T> extends AbstractExpression {

    public static final IExpression NULL = new ConstantExpression<>(DataType.NULL, null);

    /**
     * The data type of the expression
     */
    private DataType m_dataType;

    /**
     * The value of the expression
     */
    private T m_value;

    /**
     * Instantiates a new Constant expression.
     *
     * @param p_dataType the data type of the constant
     * @param p_value    the constant value
     */
    public ConstantExpression(final DataType p_dataType, final T p_value) {

        m_dataType = p_dataType;
        m_value = p_value;
    }

    /**
     * Get the child expressions of this expression
     *
     * @return the child expressions of this expression
     */
    @Override
    public IExpression[] getChildren() {

        return new IExpression[0];
    }

    /**
     * Get the datatype of the expression
     *
     * @return the datatype of the expression
     */
    @Override
    public DataType getDataType() {

        return m_dataType;
    }

    /**
     * Get the value of the expression, based on the current row context
     *
     * @param p_context the current row context
     *
     * @return the value of the expression.
     */
    @Override
    public T getValue(final IExecutionContext p_context) {

        return m_value;
    }

    @Override
    public String toString() {

        return "Constant{" + m_dataType +
                " " + m_value +
                '}';
    }

    /**
     * Validates the expression against the specified context
     *
     * @param p_context the context to validate the expression against
     */
    @Override
    public void validateExpression(final IValidationContext p_context) throws InvalidExpressionException {
        // Always valid.
    }
}
