/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource.csv;

import com.xar.logquery.runtime.datasource.csv.interfaces.ICSVParser;
import com.xar.logquery.runtime.io.DynamicBufferedReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

/**
 * Simple CSV Parser using RFC-4180
 * <p/>
 * Created by Anton Koekemoer on 2014/11/28.
 */
public class CSVParser4180 implements ICSVParser {

    /**
     * The reader.
     */
    private DynamicBufferedReader m_reader;

    /**
     * The current row.
     */
    private List<String> m_currentRow = new ArrayList<>();

    /**
     * The line number.
     */
    private int m_lineNumber = 0;

    /**
     * Instantiates a new CSV parser 4180.
     *
     * @param p_file the file
     *
     * @throws java.io.FileNotFoundException the file not found exception
     */
    public CSVParser4180(File p_file) throws FileNotFoundException {

        this(new FileReader(p_file));
    }

    /**
     * Instantiates a new CSV parser 4180.
     *
     * @param p_reader the reader
     */
    public CSVParser4180(final Reader p_reader) {

        m_reader = p_reader instanceof DynamicBufferedReader ? (DynamicBufferedReader) p_reader :
                   new DynamicBufferedReader(p_reader);
    }

    /**
     * The current line number
     *
     * @return the current line number
     */
    @Override
    public int getLineNumber() {

        return m_lineNumber;
    }

    @Override
    public void mark() throws IOException {

        m_reader.mark();
    }


    /**
     * Read the next row
     *
     * @return a string array with the cells of the next row
     *
     * @throws java.io.IOException the iO exception
     */
    @Override
    public String[] nextRow() throws IOException {

        m_currentRow.clear();

        StringBuilder cell = new StringBuilder();

        boolean incomplete = true;
        boolean eof = false;
        int cellStart = -1;
//                        else {
//                            cellStart = -1;
//                        }

        while (incomplete) {
            String line = m_reader.readLine();
            if (line == null) {
                eof = true;
                // add column
                break;
            }
            char[] characters = line.toCharArray();
            int pos = 0;
            while (pos < characters.length) {

                char current = characters[pos++];

                switch (current) {
                    case '\'': // My own flavour, not in the official RFC
                    case '"':
                        if (cellStart == -1) {
                            if (cell.length() == 0) {
                                cellStart = current;
                                continue;
                            }
                            // throw exception here
                        }
                        else if (cellStart == current) {
                            if (pos < characters.length) {
                                if (characters[pos] == current) {
                                    cell.append(current);
                                    pos++;
                                }
                                else {
                                    cellStart = -1;
                                    // The very next character MUST be a comma.
                                }
                            }
                            else {
                                cellStart = -1;
                            }
                        }
                        break;
                    case ',':
                        if (cellStart == -1) {
                            m_currentRow.add(cell.toString());
                            cell.setLength(0);
                            break;
                        }
                    default:
                        cell.append(current);
                }
            }
            if (cellStart != -1) {
                incomplete = true;
                cell.append(System.lineSeparator());
                continue;
            }
            incomplete = false;
        }

        if (cell.length() > 0) {
            m_currentRow.add(cell.toString());
        }
        else if (eof && m_currentRow.size() == 0) {
            return null;
        }

        m_lineNumber++;
        return m_currentRow.toArray(new String[m_currentRow.size()]);


    }

    @Override
    public void reset() throws IOException {

        m_reader.reset();
    }

    @Override
    public void close() throws IOException {
        m_reader.close();
    }

}
