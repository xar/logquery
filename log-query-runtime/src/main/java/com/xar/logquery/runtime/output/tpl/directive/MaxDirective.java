/*
 * Copyright 2014 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.output.tpl.directive;

import com.xar.logquery.runtime.util.TypeFunctions;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;

/**
 * Freemarker User defined directive to keep the maximum value of a field.
 * <p/>
 * Parameters:
 * <ul>
 * <li><code>field</code>: The name of the field to be kept. This field can contain any type of value, and is compulsory</li>
 * <li><code>output</code>: The name field into which the result is to be placed. This field is optional, and the output will be defaulted into a field names MaxOf_&lt;fieldName&gt;</li>
 * </ul>
 * <p/>
 * Loop Variables: None
 * Mested content: No
 * <p/>
 * Created by Anton Koekemoer on 2014/12/04.
 */
public class MaxDirective extends AggregateDirectiveBase<Object> {

    public MaxDirective() {

        super("MaxOf_");
    }

    @Override
    protected IAggregate<Object> createAggregate(final String p_fieldName) {

        return new Max(p_fieldName);
    }

    private class Max implements IAggregate<Object> {

        private Object m_maximum;

        private String m_fieldName;

        public Max(final String p_fieldName) {

            m_fieldName = p_fieldName;
        }

        @Override
        public void aggregate(final TemplateModel p_model) throws TemplateModelException {

            Object value = getValueOf(p_model);

            if (m_maximum == null) { m_maximum = value; }

            if (value instanceof Number) {
                if (TypeFunctions.isValueReal(value) || TypeFunctions.isValueReal(m_maximum)) {

                    m_maximum = Math.max(((Number) m_maximum).doubleValue(), ((Number) value).doubleValue());
                }
                else { m_maximum = Math.max(((Number) m_maximum).longValue(), ((Number) value).longValue()); }
            }
            else if (m_maximum instanceof Comparable) {
                if (value.getClass().isAssignableFrom(m_maximum.getClass())) {
                    m_maximum = ((Comparable) value).compareTo(m_maximum) > 0 ? value : m_maximum;
                }
                else {
                    m_maximum = ((Comparable) m_maximum).compareTo(value) > 0 ? m_maximum : value;
                }
            }
            else {
                throw new TemplateModelException("Unable to determine minimum foe field " + m_fieldName + "!");
            }

        }

        @Override
        public Object getValue() {

            return m_maximum;
        }
    }
}
