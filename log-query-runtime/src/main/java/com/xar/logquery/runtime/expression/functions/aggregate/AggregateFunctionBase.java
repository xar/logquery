/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.functions.aggregate;

import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.AbstractExpression;
import com.xar.logquery.runtime.expression.functions.aggregate.interfaces.IAggregateExpression;
import com.xar.logquery.runtime.expression.functions.aggregate.interfaces.IAggregator;
import com.xar.logquery.runtime.expression.functions.aggregate.interfaces.IAggregatorFactory;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.expression.interfaces.IFunction;
import com.xar.logquery.runtime.expression.interfaces.IFunctionParameter;
import com.xar.logquery.runtime.interfaces.IExecutionContext;

import java.util.UUID;

/**
 * Base class for implementing aggregate functions
 * <p/>
 * Created by koekemoera on 2014/11/10.
 */
public abstract class AggregateFunctionBase extends AbstractExpression implements IAggregateExpression, IFunction {

    protected String m_key = UUID.randomUUID().toString();

    protected IFunctionParameter[] m_functionParameters;

    private DataType m_dataType;

    private IAggregatorFactory m_aggregatorFactory;

    /**
     * Instantiates a new Aggregate function base.
     *
     * @param p_dataType          the data type
     * @param p_aggregatorFactory the aggregator factory
     */
    public AggregateFunctionBase(final DataType p_dataType, final IAggregatorFactory p_aggregatorFactory) {

        setDataType(p_dataType);
        setAggregatorFactory(p_aggregatorFactory);
    }

    /**
     * Instantiates a new Aggregate function base.
     */
    public AggregateFunctionBase() {

        this(DataType.NULL, null);
    }

    /**
     * Aggregate the value
     *
     * @param p_state   the aggregate state
     * @param p_context the current execution context
     *
     * @return a new aggregate state
     */
    @Override
    public Object aggregate(final Object p_state, final IExecutionContext p_context) {

        ((IAggregator) p_state).aggregate(p_context);
        return p_state;
    }

    /**
     * Get the aggregator factory
     *
     * @return the aggregator factory
     */
    public IAggregatorFactory getAggregatorFactory() {

        return m_aggregatorFactory;
    }

    /**
     * Set  the aggregator factory
     *
     * @param p_aggregatorFactory the aggregator factory
     */
    public void setAggregatorFactory(final IAggregatorFactory p_aggregatorFactory) {

        m_aggregatorFactory = p_aggregatorFactory;
    }

    /**
     * Get the child expressions of this expression
     *
     * @return the child expressions of this expression
     */
    @Override
    public final IExpression[] getChildren() {

        return m_functionParameters;
    }

    /**
     * Get the datatype of the expression
     *
     * @return the datatype of the expression
     */
    @Override
    public DataType getDataType() {

        return m_dataType;
    }

    /**
     * Set the datatype of the expression
     *
     * @return the datatype of the expression
     */
    public void setDataType(final DataType p_dataType) {

        m_dataType = p_dataType;
    }

    /**
     * A euneque instance key for the aggregate expression. Implementers must ensure that this value is
     * euneque for every instance of the expression.
     *
     * @return a key used to identify the specific instance of the expression.
     */
    @Override
    public String getKey() {

        return m_key;
    }

    /**
     * Get the value of the expression, based on the current row context
     *
     * @param p_context the current row context
     *
     * @return the value of the expression.
     */
    @Override
    public Object getValue(final IExecutionContext p_context) {

        IAggregator aggregator = (IAggregator) p_context.getValue(getClass().getName(), m_key);

        return aggregator == null ? null : aggregator.getValue();
    }

    /**
     * Creates a new aggregator
     *
     * @return a new instance of the aggregator
     */
    @Override
    public Object newState() {

        return getAggregatorFactory().create();
    }

    /**
     * Set the parameters of the function
     *
     * @param p_functionParameters the parameters
     */
    @Override
    public final void setParameters(final IFunctionParameter... p_functionParameters) {

        m_functionParameters = p_functionParameters;
    }
}
