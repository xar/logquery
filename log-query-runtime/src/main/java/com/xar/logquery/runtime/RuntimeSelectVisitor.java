/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime;

import com.xar.logquery.parser.exceptions.ParseException;
import com.xar.logquery.parser.generated.SelectBaseVisitor;
import com.xar.logquery.parser.generated.SelectParser;
import com.xar.logquery.runtime.config.RuntimeConfiguration;
import com.xar.logquery.runtime.datasource.DistinctDatasource;
import com.xar.logquery.runtime.datasource.ExpressionDataSource;
import com.xar.logquery.runtime.datasource.FilterDataSource;
import com.xar.logquery.runtime.datasource.GroupedDataSource;
import com.xar.logquery.runtime.datasource.LimitDataSource;
import com.xar.logquery.runtime.datasource.OrderedDataSource;
import com.xar.logquery.runtime.datasource.SelectDataSource;
import com.xar.logquery.runtime.datasource.TransformDataSource;
import com.xar.logquery.runtime.datasource.UnionDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IFileDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IOrderable;
import com.xar.logquery.runtime.datasource.interfaces.ITransformer;
import com.xar.logquery.runtime.datasource.join.JoinDataSource;
import com.xar.logquery.runtime.datasource.pivot.PivotDataSource;
import com.xar.logquery.runtime.datasource.text.TextLineDataSource;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.ColumnReference;
import com.xar.logquery.runtime.expression.NamedExpression;
import com.xar.logquery.runtime.expression.OrderExpression;
import com.xar.logquery.runtime.expression.WildcardExpression;
import com.xar.logquery.runtime.expression.arithmitic.AddExpression;
import com.xar.logquery.runtime.expression.arithmitic.DivideExpression;
import com.xar.logquery.runtime.expression.arithmitic.ModulusExpression;
import com.xar.logquery.runtime.expression.arithmitic.MultiplyExpression;
import com.xar.logquery.runtime.expression.arithmitic.SubtractExpression;
import com.xar.logquery.runtime.expression.arithmitic.UnaryMINUS;
import com.xar.logquery.runtime.expression.constant.BooleanConstant;
import com.xar.logquery.runtime.expression.constant.ConstantExpression;
import com.xar.logquery.runtime.expression.constant.IntegerConstant;
import com.xar.logquery.runtime.expression.constant.RealConstant;
import com.xar.logquery.runtime.expression.constant.StringConstant;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.expression.functions.FunctionParameter;
import com.xar.logquery.runtime.expression.functions.scalar.DecodeFunction;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.expression.interfaces.IFunction;
import com.xar.logquery.runtime.expression.interfaces.IFunctionParameter;
import com.xar.logquery.runtime.expression.logic.AndCondition;
import com.xar.logquery.runtime.expression.logic.CompareBETWEEN;
import com.xar.logquery.runtime.expression.logic.CompareEQ;
import com.xar.logquery.runtime.expression.logic.CompareGT;
import com.xar.logquery.runtime.expression.logic.CompareGTE;
import com.xar.logquery.runtime.expression.logic.CompareIN;
import com.xar.logquery.runtime.expression.logic.CompareLIKE;
import com.xar.logquery.runtime.expression.logic.CompareLT;
import com.xar.logquery.runtime.expression.logic.CompareLTE;
import com.xar.logquery.runtime.expression.logic.NotExpression;
import com.xar.logquery.runtime.expression.logic.OrCondition;
import com.xar.logquery.runtime.interfaces.IAliasable;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;
import com.xar.logquery.runtime.parse.ParseData;
import com.xar.logquery.runtime.util.FileFunctions;
import com.xar.logquery.runtime.util.ParseFunctions;
import com.xar.logquery.runtime.util.RuntimeFunctions;
import com.xar.logquery.runtime.util.StringUtils;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTree;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import static com.xar.logquery.runtime.datasource.join.JoinDataSource.JoinType.*;

/**
 * Implementation for the select visitor, used to construct the runtime execution tree.
 */
public class RuntimeSelectVisitor extends SelectBaseVisitor<Object> {

    /**
     * The shared logger instance.
     */
    private static final Logger logger = LoggerFactory.getLogger(RuntimeSelectVisitor.class);

    VisitorState currentState;

    private Stack<VisitorState> visitorStates = new Stack<>();

    private IExecutionContext m_executionContext;

    private IValidationContext m_validationContext = new ValidationContext();


    public RuntimeSelectVisitor() {

        this(new ExecutionContext());
    }

    public RuntimeSelectVisitor(final IExecutionContext p_executionContext) {

        m_executionContext = p_executionContext;
        pushState();
    }

    /**
     * Get the dataSource
     *
     * @return the dataSource
     */
    public IDataSource getDataSource() {

        return currentState.getDataSource();
    }

    /**
     * Sets the datasource
     *
     * @param p_dataSource the datasource
     */
    public void setDataSource(final IDataSource p_dataSource) {

        currentState.setDataSource(p_dataSource);
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public IExpression visitArithmeticExpression(@NotNull final SelectParser.ArithmeticExpressionContext ctx) {

        super.visitArithmeticExpression(ctx);

        if (ctx.getChildCount() == 3) {
            IExpression rhs = popExpression();
            IExpression lhs = popExpression();

            switch (ctx.getChild(1).getText().charAt(0)) {
                case '*':
                    return pushExpression(new MultiplyExpression(lhs, rhs), ctx);
                case '/':
                    return pushExpression(new DivideExpression(lhs, rhs), ctx);
                case '%':
                    return pushExpression(new ModulusExpression(lhs, rhs), ctx);
                case '+':
                    return pushExpression(new AddExpression(lhs, rhs), ctx);
                case '-':
                    return pushExpression(new SubtractExpression(lhs, rhs), ctx);
            }
        }

        return currentState().peek();
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public IFunction visitCaseFunction(@NotNull final SelectParser.CaseFunctionContext ctx) {

        super.visitCaseFunction(ctx);

        DecodeFunction function = new DecodeFunction();
        int parameter_start = 0;
        int parameter_count = 1;

        parameter_count += ctx.whenExpression().size() * 2;
        IExpression elseAction = null;
        if (ctx.elseExpression() != null) {
            parameter_count++;
            elseAction = popExpression();
        }
        FunctionParameter[] parameters = new FunctionParameter[parameter_count];

        if (elseAction != null) { parameters[--parameter_count] = new FunctionParameter(elseAction); }

        if (ctx.simpleExpression() == null) {
            parameter_start++;
            parameters[0] = new FunctionParameter(BooleanConstant.TRUE);
        }
        while (parameter_count > parameter_start) {
            parameters[--parameter_count] = new FunctionParameter(popExpression());
        }
        function.setParameters(parameters);
        pushExpression(function, ctx);
        return function;
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Object visitColumnName(@NotNull final SelectParser.ColumnNameContext ctx) {

        List<SelectParser.IdentifierContext> identifiers = ctx.identifier();

        String nameSpace;
        String columnName;

        if (identifiers.size() == 1) {
            nameSpace = null;
            columnName = ParseFunctions.getIdentifierValue(identifiers.get(0));
        }
        else {
            nameSpace = ParseFunctions.getIdentifierValue(identifiers.get(0));
            columnName = ParseFunctions.getIdentifierValue(identifiers.get(1));
        }

        return pushExpression(new ColumnReference(nameSpace, columnName), ctx);
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public IDataSource visitCustomDatasource(@NotNull final SelectParser.CustomDatasourceContext ctx) {

        String sourceName = visitIdentifier(ctx.identifier());

        IDataSource dataSource = RuntimeFunctions.createCustomDataSource(sourceName);

        if (dataSource == null) {
            throw new ParseException("Unbable to load datasource - " + sourceName, ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
        }

        if (ctx.propertyList() != null) {
            dataSource.setAttributes(visitPropertyList(ctx.propertyList()));
        }

        if (ctx.usingClause() != null) {
            IDataSource using = visitUsingClause(ctx.usingClause());
            if (!(using instanceof ITransformer)) {
                final Token start = ctx.usingClause().identifier().getStart();
                throw new ParseException(ParseFunctions.getIdentifierValue(ctx.identifier()) + " - Is not a transformation provider", start.getLine(), start.getCharPositionInLine());
            }
            ((ITransformer) using).setDataSource(dataSource);
            dataSource = using;
        }

        setDataSource(dataSource);

        return dataSource;

    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public IExpression[] visitExpressionList(@NotNull final SelectParser.ExpressionListContext ctx) {

        super.visitExpressionList(ctx);

        IExpression[] expressionList = new IExpression[ctx.simpleExpression().size()];

        for (int i = expressionList.length - 1; i >= 0; i--) {
            expressionList[i] = popExpression();
        }
        return expressionList;
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public IFileDataSource visitFileDatasource(@NotNull final SelectParser.FileDatasourceContext ctx) {

        String filePattern = ctx.ASCIIStringLiteral().getText();
        filePattern = filePattern.substring(1, filePattern.length() - 1);

        IFileDataSource dataSource;
        if (ctx.usingClause() != null) {
            IDataSource using = visitUsingClause(ctx.usingClause());
            if (!(using instanceof IFileDataSource)) {
                final Token start = ctx.usingClause().identifier().getStart();
                throw new ParseException(ParseFunctions.getIdentifierValue(ctx.usingClause().identifier()) + " - Is not a file based format provider", start.getLine(), start.getCharPositionInLine());
            }
            dataSource = (IFileDataSource) using;
        }
        else {

            String extension = FileFunctions.getExtension(filePattern);
            String source = RuntimeConfiguration.getDatasourceForExtension(extension);
            if (source != null) {
                IDataSource work = RuntimeFunctions.createCustomDataSource(source);

                if (work instanceof IFileDataSource) { dataSource = (IFileDataSource) work; }
                else {
                    // TODO: Unable to load formatter, using textline
                    dataSource = new TextLineDataSource();
//                        throw new ParseException("Unable to load formatter - " + source, ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
                }
            }
            else {
                dataSource = new TextLineDataSource();
            }
        }

        dataSource.setFilePattern(filePattern);

        setDataSource(dataSource);

        return dataSource;

    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public IDataSource visitFromClause(@NotNull final SelectParser.FromClauseContext ctx) {

        super.visitFromClause(ctx);

        return currentState.getDataSource();
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public IExpression visitFunction(@NotNull final SelectParser.FunctionContext ctx) {

        String functionName = ParseFunctions.getIdentifierValue(ctx.identifier());

        // Do a lookup for the function
        Class<?> functionClass = RuntimeConfiguration.getFunction(functionName);
        if (functionClass == null) {
            throw new ParseException("Function " + functionName + " does not exist", ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
        }

        try {
            IFunction function = (IFunction) functionClass.newInstance();

            if (ctx.functionParameterList() != null) {
                function.setParameters(visitFunctionParameterList(ctx.functionParameterList()));
            }
            return pushExpression(function, ctx);

        }
        catch (Exception e) {
            throw new ParseException(e.getMessage(), ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine(), e);
        }

    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public IFunctionParameter visitFunctionParameter(@NotNull final SelectParser.FunctionParameterContext ctx) {

        IFunctionParameter parameter;
        if (ctx.wildcardItem() != null) {
            visitWildcardItem(ctx.wildcardItem());
            parameter = new FunctionParameter(popExpression());
        }
        else {
            visitExpression(ctx.expression());
            parameter = new FunctionParameter(popExpression(), ctx.getChild(0).getText().equalsIgnoreCase("DISTINCT"));
        }
        pushExpression(parameter, ctx);

        return parameter;
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public IFunctionParameter[] visitFunctionParameterList(@NotNull final SelectParser.FunctionParameterListContext ctx) {

        super.visitFunctionParameterList(ctx);
        IFunctionParameter[] parameters = new IFunctionParameter[ctx.functionParameter().size()];

        for (int i = parameters.length - 1; i >= 0; i--) {
            parameters[i] = (IFunctionParameter) popExpression();
        }

        return parameters;
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    public IDataSource visitGroupByClause(@NotNull final SelectParser.GroupByClauseContext ctx, ParseFunctions.ReferenceInfo p_referenceInfo) {

        IExpression[] groupBy = new IExpression[ctx == null ? 0 : ctx.groupExpression().size()];
        for (int i = 0; i < groupBy.length; i++) {
            visitGroupExpression(ctx.groupExpression(i));
            groupBy[i] = popExpression();
        }

        ParseFunctions.ReferenceInfo groupReference = ParseFunctions.getReferenceInfo(groupBy);
        if (groupReference.getAggregateExpressions().length > 0) {
            throw new ParseException("No aggregate functions allowed in GROUP BY", ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
        }
        List<IColumnInfo> work = new ArrayList<>(Arrays.asList(p_referenceInfo.getColumnInfo()));
        for (IColumnInfo info : groupReference.getColumnInfo()) {
            for (int j = 0; j < work.size(); j++) {
                if (ColumnInfo.equals(info, work.get(j))) {
                    work.remove(j);
                    break;
                }
            }
        }
        if (work.size() > 0) {
            throw new ParseException("Group by invalid. Referenced column [" + work.get(0).getName() + "] does not appear in the group by", 0, 0);
        }
        IDataSource dataSource = new GroupedDataSource(getDataSource(), p_referenceInfo.getColumnInfo(), p_referenceInfo.getAggregateExpressions(), groupBy);
        setDataSource(dataSource);

        if (ctx != null && ctx.havingClause() != null) {
            dataSource = visitHavingClause(ctx.havingClause());
        }
        return dataSource;
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public IDataSource visitHavingClause(@NotNull final SelectParser.HavingClauseContext ctx) {

        super.visitHavingClause(ctx);

        currentState.setDataSource(new FilterDataSource(currentState.getDataSource(), popExpression()));

        return currentState.getDataSource();
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Object visitHexLiteral(@NotNull final SelectParser.HexLiteralContext ctx) {

        return pushExpression(new IntegerConstant(Long.parseLong(ctx.getText().substring(2), 16)), ctx);
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public String visitIdentifier(@NotNull final SelectParser.IdentifierContext ctx) {

        return ParseFunctions.getIdentifierValue(ctx);
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Object visitIntegerLiteral(@NotNull final SelectParser.IntegerLiteralContext ctx) {

        return pushExpression(new IntegerConstant(Long.parseLong(ctx.getText())), ctx);
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public IDataSource visitJoinClause(@NotNull final SelectParser.JoinClauseContext ctx) {

        IDataSource dataSource;
        // 'CROSS' 'JOIN' subTableSource
        // (('INNER' | ('LEFT' | 'RIGHT' | 'FULL') ('OUTER') ?))?'JOIN' tableSource 'ON' searchCondition
        IDataSource left = getDataSource();
        IDataSource right;

        if (left instanceof IAliasable && StringUtils.isEmpty(((IAliasable) left).getAlias())) {
            throw new ParseException("Datasource requires an alias", ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
        }
        String joinText = ctx.getChild(0).getText().toUpperCase();
        if (joinText.equals("CROSS")) {
            right = visitSubTableSource(ctx.subTableSource());
            dataSource = new JoinDataSource(left, right, BooleanConstant.TRUE, INNER);
        }
        else {

            visitTableSource(ctx.tableSource());
            visitSearchCondition(ctx.searchCondition());

            right = getDataSource();

            IExpression searchCondition = popExpression();

            switch (joinText) {
                case "LEFT":
                    dataSource = new JoinDataSource(left, right, searchCondition, LEFT_OUTER);
                    break;
                case "RIGHT":
                    dataSource = new JoinDataSource(left, right, searchCondition, RIGHT_OUTER);
                    break;
                case "FULL":
                    dataSource = new JoinDataSource(left, right, searchCondition, EnumSet.of(LEFT_OUTER, RIGHT_OUTER));
                    break;
                case "INNER":
                case "JOIN":
                default:
                    dataSource = new JoinDataSource(left, right, searchCondition, INNER);
                    break;
            }
        }
        if (right instanceof IAliasable && StringUtils.isEmpty(((IAliasable) right).getAlias())) {
            throw new ParseException("Datasource requires an alias", ctx.subTableSource().getStart().getLine(), ctx.getStart().getCharPositionInLine());
        }

        setDataSource(dataSource);
        return dataSource;

    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public IDataSource visitLimitClause(@NotNull final SelectParser.LimitClauseContext ctx) {

        int skip = 0;
        int take;

        int expressionCount = ctx.integerLiteral().size();

        if (expressionCount > 1) {
            skip = Integer.parseInt(ctx.integerLiteral(1).getText());
            take = Integer.parseInt(ctx.integerLiteral(0).getText());
        }
        else {
            take = Integer.parseInt(ctx.integerLiteral(0).getText());
        }

        IDataSource dataSource = new LimitDataSource(getDataSource(), skip, take);

        setDataSource(dataSource);

        return dataSource;
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public NamedExpression visitNamedExpression(@NotNull final SelectParser.NamedExpressionContext ctx) {

        visitSimpleExpression(ctx.simpleExpression());
        final NamedExpression namedExpression = new NamedExpression(popExpression(), ParseFunctions.getIdentifierValue(ctx.identifier()));

        pushExpression(namedExpression, ctx);

        return namedExpression;
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public IExpression visitNegateCondition(@NotNull final SelectParser.NegateConditionContext ctx) {

        super.visitNegateCondition(ctx);

        return pushExpression(new NotExpression(currentState().pop()), ctx);
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public OrderExpression[] visitOrderByClause(@NotNull final SelectParser.OrderByClauseContext ctx) {

        super.visitOrderByClause(ctx);

        OrderExpression[] orderBy = new OrderExpression[ctx.orderExpression().size()];

        for (int i = orderBy.length - 1; i >= 0; i--) {
            orderBy[i] = (OrderExpression) currentState().pop();
        }

        return orderBy;
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public OrderExpression visitOrderExpression(@NotNull final SelectParser.OrderExpressionContext ctx) {

        super.visitOrderExpression(ctx);

        IExpression expression = popExpression();

        boolean ascending = true;

        if (ctx.getChildCount() > 1) {
            ascending = !ctx.getChild(1).getText().equalsIgnoreCase("DESC");
        }

        final OrderExpression orderExpression = new OrderExpression(expression, ascending);

        pushExpression(orderExpression, ctx);

        return orderExpression;
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public IExpression visitPredicateBETWEEN(@NotNull final SelectParser.PredicateBETWEENContext ctx) {

        super.visitPredicateBETWEEN(ctx);
        IExpression upper = currentState().pop();
        IExpression lower = currentState().pop();
        IExpression value = currentState().pop();

        return pushExpression(new CompareBETWEEN(value, lower, upper), ctx);
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public IExpression visitPredicateCompare(@NotNull final SelectParser.PredicateCompareContext ctx) {

        super.visitPredicateCompare(ctx);

        IExpression rhs = currentState().pop();
        IExpression lhs = currentState().pop();

        String operator = ctx.getChild(1).getText();

        switch (operator) {
            case "!=":
            case "<>":
                return pushExpression(new NotExpression(new CompareEQ(lhs, rhs)), ctx);
            case ">":
                return pushExpression(new CompareGT(lhs, rhs), ctx);
            case ">=":
                return pushExpression(new CompareGTE(lhs, rhs), ctx);
            case "<":
                return pushExpression(new CompareLT(lhs, rhs), ctx);
            case "<=":
                return pushExpression(new CompareLTE(lhs, rhs), ctx);
            case "=":
            default: // Stupid I know, but we default to compare
                return pushExpression(new CompareEQ(lhs, rhs), ctx);
        }

    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public IExpression visitPredicateIN(@NotNull final SelectParser.PredicateINContext ctx) {

        // TODO: https://trello.com/c/IKOXAIM2/1-optimise-predicatein
        IDataSource dataSource;
        visitSimpleExpression(ctx.simpleExpression());
        IExpression value = currentState().pop();
        DataType dataType = value.getDataType();

        VisitorState child = pushState();
        try {
            if (ctx.expressionList() != null) {
                IExpression[] list = visitExpressionList(ctx.expressionList());
                IExpression[][] expressions = new IExpression[list.length][];
                for (int i = 0; i < list.length; i++) {
                    expressions[i] = new IExpression[]{list[i]};
                }
                dataSource = new ExpressionDataSource(new ColumnInfo[]{
                        new ColumnInfo("", "col1", dataType)}, expressions);
            }
            else {
                visitSelectStatement(ctx.selectStatement());
                dataSource = child.getDataSource();
            }
        }
        finally {
            popState();
        }

        return pushExpression(new CompareIN(value, dataSource), ctx);
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public IExpression visitPredicateLIKE(@NotNull final SelectParser.PredicateLIKEContext ctx) {

        super.visitPredicateLIKE(ctx);

        IExpression escape;
        if (ctx.simpleExpression().size() == 3) {
            escape = currentState().pop();
        }
        else { escape = new StringConstant("\\"); }
        IExpression pattern = currentState().pop();
        IExpression value = currentState().pop();

        return pushExpression(new CompareLIKE(value, pattern, escape), ctx);
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public IExpression visitPredicateNULL(@NotNull final SelectParser.PredicateNULLContext ctx) {

        visitSimpleExpression(ctx.simpleExpression());

        IExpression value = new CompareEQ(currentState().pop(), ConstantExpression.NULL);

        if (ctx.getChild(2).getText().equalsIgnoreCase("NOT")) {
            value = new NotExpression(value);
        }

        return pushExpression(value, ctx);
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Object visitPropertyAssign(@NotNull final SelectParser.PropertyAssignContext ctx) {

        super.visitPropertyAssign(ctx);
        final IExpression expression = currentState().pop();
        try {
            expression.validate(m_validationContext);
        }
        catch (InvalidExpressionException e) {
            final ParseData parseData = expression.getParseData();
            throw new ParseException(e.getBasicMessage(), parseData.getRowNumber(), parseData.getColumnNumber());
        }
        return pushExpression(new NamedExpression(expression, visitIdentifier(ctx.identifier())), ctx);
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Map<String, IExpression> visitPropertyList(@NotNull final SelectParser.PropertyListContext ctx) {
//    public Map<String, Object> visitPropertyList(@NotNull final SelectParser.PropertyListContext ctx) {

        super.visitPropertyList(ctx);

        Map<String, IExpression> properties = new HashMap<>();
        int count = ctx.propertyAssign().size();
        for (int i = 0; i < count; i++) {
            NamedExpression expression = (NamedExpression) popExpression();
            properties.put(expression.getName(), expression.getExpression());
        }

        return properties;
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Object visitRealLiteral(@NotNull final SelectParser.RealLiteralContext ctx) {

        return pushExpression(new RealConstant(new BigDecimal(ctx.getText())), ctx);
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Object visitReservedConstant(@NotNull final SelectParser.ReservedConstantContext ctx) {

        String constant = ctx.getText().toLowerCase();
        switch (constant) {
            case "true":
                return pushExpression(BooleanConstant.TRUE, ctx);
            case "false":
                return pushExpression(BooleanConstant.FALSE, ctx);
            case "null":
            default:
                return pushExpression(ConstantExpression.NULL, ctx);
        }
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Object visitSearchCondition(@NotNull final SelectParser.SearchConditionContext ctx) {

        super.visitSearchCondition(ctx);

        if (ctx.getChildCount() > 1) {
            IExpression rhs = popExpression();
            IExpression lhs = popExpression();

            String operator = ctx.getChild(1).getText().toLowerCase();

            switch (operator) {
                case "or":
                    pushExpression(new OrCondition(lhs, rhs), ctx);
                    break;
                case "and":
                    pushExpression(new AndCondition(lhs, rhs), ctx);
                    break;
            }
        }
        return null;

    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public IExpression visitSelectExpression(@NotNull final SelectParser.SelectExpressionContext ctx) {

        visitSearchCondition(ctx.searchCondition());
        if (ctx.identifier() != null) {
            pushExpression(new NamedExpression(popExpression(), visitIdentifier(ctx.identifier())), ctx);
        }
        return currentState.peek();
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public IExpression[] visitSelectList(@NotNull final SelectParser.SelectListContext ctx) {

        super.visitSelectList(ctx);

        final IExpression[] expressionList = new IExpression[ctx.selectItem().size()];

        for (int i = expressionList.length - 1; i >= 0; i--) {
            expressionList[i] = popExpression();
        }

        return expressionList;
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Object visitSelectStatement(@NotNull final SelectParser.SelectStatementContext ctx) {

        visitSimpleQueryStatement(ctx.simpleQueryStatement());
        IDataSource dataSource = getDataSource();

        if (ctx.orderByClause() != null) {
            final OrderExpression[] orderBy = visitOrderByClause(ctx.orderByClause());
            if (dataSource instanceof IOrderable) {
                ((IOrderable) dataSource).setOrderBy(orderBy);
            }
            else {
                dataSource = new OrderedDataSource(dataSource, orderBy);
                setDataSource(dataSource);
            }
        }
        if (ctx.limitClause() != null) { visitLimitClause(ctx.limitClause()); }

        return dataSource;
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public IDataSource visitSimpleSelectStatement(@NotNull final SelectParser.SimpleSelectStatementContext ctx) {

        visitFromClause(ctx.fromClause());
        if (ctx.whereClause() != null) { visitWhereClause(ctx.whereClause()); }

        IExpression[] expressionList = visitSelectList(ctx.selectList());

        ParseFunctions.ReferenceInfo referenceInfo = ParseFunctions.getReferenceInfo(expressionList);

        if (ctx.pivotClause() != null) {
            setDataSource(visitPivotClause(ctx.pivotClause(), expressionList));
        }
        else {
            if (referenceInfo.getAggregateExpressions().length > 0) {
                IDataSource groupBy = visitGroupByClause(ctx.groupByClause(), referenceInfo);

                setDataSource(groupBy);

            }
            final SelectDataSource selectDataSource = new SelectDataSource(getDataSource(), expressionList);
            setDataSource(selectDataSource);

        }
        if (ctx.getChild(1).getText().equalsIgnoreCase("DISTINCT")){
            setDataSource(new DistinctDatasource(getDataSource()));
        }

        return currentState.getDataSource();
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Object visitStringLiteral(@NotNull final SelectParser.StringLiteralContext ctx) {

        String literal = ctx.getText();

        return pushExpression(new StringConstant(literal.substring(1, literal.length() - 1)), ctx);
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public IExpression visitSubExpression(@NotNull final SelectParser.SubExpressionContext ctx) {

        visitPrimaryExpression(ctx.primaryExpression());

        if (ctx.unaryOperator() != null) {
            visitUnaryOperator(ctx.unaryOperator());
            switch (ctx.unaryOperator().getText()) {
                case "-":
                    return pushExpression(new UnaryMINUS(currentState().pop()), ctx);
                case "+":
                default:
                    // A Unary plus means nothing
            }
        }
        return null;
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public IDataSource visitSubTableSource(@NotNull final SelectParser.SubTableSourceContext ctx) {

        visitDataSource(ctx.dataSource());
        if (ctx.identifier() != null) {
            IDataSource dataSource = getDataSource();
            if (dataSource instanceof IAliasable) {
                String alias = ParseFunctions.getIdentifierValue(ctx.identifier());
                if (currentState().addAlias(alias)) {
                    ((IAliasable) dataSource).setAlias(alias);
                }
                else {
                    throw new ParseException("Alias " + alias + " already in use!", ctx.identifier().getStart().getLine(), ctx.identifier().getStart().getCharPositionInLine());
                }
            }
        }
        if (ctx.transformClause() != null) { visitTransformClause(ctx.transformClause()); }

        return getDataSource();
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public TransformDataSource visitTransformClause(@NotNull final SelectParser.TransformClauseContext ctx) {

        NamedExpression[] transform = visitUsingList(ctx.usingList());
        final TransformDataSource transformDataSource = new TransformDataSource(currentState.getDataSource(), transform);
        currentState.setDataSource(transformDataSource);

        return transformDataSource;
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public UnionDataSource visitUnionStatement(@NotNull final SelectParser.UnionStatementContext ctx) {

        int count = ctx.queryStatement().size();

        IDataSource[] sources = new IDataSource[count];

        for (int i = 0; i < sources.length; i++) {
            pushState();
            visitQueryStatement(ctx.queryStatement(i));
            sources[i] = popState().getDataSource();
        }
        UnionDataSource dataSource = new UnionDataSource(sources);

        setDataSource(dataSource);

        return dataSource;
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public IDataSource visitUsingClause(@NotNull final SelectParser.UsingClauseContext ctx) {

        String sourceName = visitIdentifier(ctx.identifier());

        IDataSource dataSource = RuntimeFunctions.createCustomDataSource(sourceName);

        if (dataSource == null) {
            final Token start = ctx.identifier().getStart();
            throw new ParseException("Unable to load formatter - " + sourceName, start.getLine(), start.getCharPositionInLine());
        }

        if (ctx.propertyList() != null) {
            dataSource.setAttributes(visitPropertyList(ctx.propertyList()));
        }

        return dataSource;
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public NamedExpression[] visitUsingList(@NotNull final SelectParser.UsingListContext ctx) {

        super.visitUsingList(ctx);

        NamedExpression[] list = new NamedExpression[ctx.namedExpression().size()];

        for (int i = list.length - 1; i >= 0; i--) {
            list[i] = (NamedExpression) popExpression();
        }
        return list;
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public IDataSource visitWhereClause(@NotNull final SelectParser.WhereClauseContext ctx) {

        super.visitWhereClause(ctx);

        currentState.setDataSource(new FilterDataSource(currentState.getDataSource(), popExpression()));

        return currentState.getDataSource();
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Object visitWildcardItem(@NotNull final SelectParser.WildcardItemContext ctx) {

        pushExpression(new WildcardExpression(
                ctx.identifier() == null ? "" : ParseFunctions.getIdentifierValue(ctx.identifier())), ctx);
        return null;
    }

    /**
     * Get the current visitor state
     *
     * @return the current visitor state
     */
    private VisitorState currentState() {

        return visitorStates.peek();
    }

    /**
     * Pop an expression from the stack
     *
     * @return the expression at the top of the stack
     */
    private IExpression popExpression() {

        return currentState.pop();
    }

    /**
     * Pop the previous visitor state
     */
    private VisitorState popState() {

        VisitorState state = visitorStates.pop();
        currentState = currentState();
        return state;
    }

    /**
     * Push an expression onto the stack
     *
     * @param p_expression the expression to push onto the stack
     * @param p_ctx        the parser rule context
     *
     * @return the expression that was pushed onto thew stack
     */
    private IExpression pushExpression(IExpression p_expression, final ParserRuleContext p_ctx) {

        p_expression.setParseData(new ParseData(p_ctx.getStart().getLine(), p_ctx.getStart().getCharPositionInLine(), p_ctx.getText()));
        currentState.push(p_expression);
        return p_expression;
    }

    /**
     * Pushes the current visitor state onto the stack
     */
    private VisitorState pushState() {

        currentState = new VisitorState();
        visitorStates.push(currentState);
        return currentState;
    }

    private IDataSource visitPivotClause(final SelectParser.PivotClauseContext ctx, final IExpression[] p_rowHeaders) {

        visitSearchCondition(ctx.searchCondition(0));
        visitSearchCondition(ctx.searchCondition(1));

        // expression list will from the row headers, this is also the group by. For this we can just get all the column references from there
        // we also disallow aggregates in the row headers.
        int pivotPoint = p_rowHeaders.length;
        boolean after = true;
        if (ctx.integerLiteral() != null || ctx.identifier() != null) {

            // We need to determine if it is a before or after
            after = ctx.children.get(ctx.children.size() - 2).getText().equalsIgnoreCase("AFTER");

            if (ctx.integerLiteral() != null) {
                pivotPoint = Integer.parseInt(ctx.integerLiteral().getText());
                if (pivotPoint <= 0 || pivotPoint > p_rowHeaders.length) {
                    throw new ParseException("Pivot point must be an INTEGER value between 1 and " + p_rowHeaders.length, ctx.integerLiteral().getStart().getLine(), ctx.integerLiteral().getStart().getCharPositionInLine());
                }
            }
            else {
                String identifier = visitIdentifier(ctx.identifier());
                // Now we need to find a column with this name.
                boolean found = false;
                for (int i = 0; i < p_rowHeaders.length; i++) {
                    IExpression rowHeader = p_rowHeaders[i];
                    String name;
                    if (rowHeader instanceof NamedExpression) {
                        name = ((NamedExpression) rowHeader).getName();
                    }
                    else {
                        name = rowHeader.getParseData().getText();
                    }
                    if (name.equals(identifier)) {
                        pivotPoint = i + 1;
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    throw new ParseException("Unable to find column with name - " + identifier, ctx.identifier().getStart().getLine(), ctx.identifier().getStart().getCharPositionInLine());
                }
            }
        }
        if (after) { pivotPoint++; }
        PivotDataSource.SortOrder order = PivotDataSource.SortOrder.NONE;

        ParseTree orderTree = ctx.getChild(4);
        if (orderTree != null) {
            if (orderTree.getText().equalsIgnoreCase("ASC")) {
                order = PivotDataSource.SortOrder.ASC;
            }
            else if (orderTree.getText().equalsIgnoreCase("DESC")) {
                order = PivotDataSource.SortOrder.DESC;
            }
        }

        return new PivotDataSource(getDataSource(), p_rowHeaders, popExpression(), popExpression(), pivotPoint, order);
    }

    /**
     * A State class used during the visitor process
     */
    private class VisitorState {

        /**
         * The Stack.
         */
        Stack<IExpression> m_expressions = new Stack<>();

        /**
         * THe data source for the state
         */
        private IDataSource m_dataSource;

        private Set<String> m_aliases = new HashSet<>();

        public boolean addAlias(final String p_alias) {

            return m_aliases.add(p_alias);
        }

        /**
         * Get the current datasource
         *
         * @return the current datasource
         */
        public IDataSource getDataSource() {


            return m_dataSource;
        }

        /**
         * Set the current datasource
         *
         * @param p_dataSource the current datasource
         */
        public void setDataSource(final IDataSource p_dataSource) {

            m_dataSource = p_dataSource;
        }

        /**
         * Peek at the expression stack
         *
         * @return the item at the top of the expression stack
         */
        public IExpression peek() {

            if (this.m_expressions.size() == 0) { return null; }

            return this.m_expressions.peek();
        }

        /**
         * Pop the item at the top of the expression stack from the expression stack
         *
         * @return the item that was popped off the expression stack
         */
        public IExpression pop() {

            IExpression o = this.m_expressions.pop();
            if (logger.isDebugEnabled()) { logger.debug("Pop expression: {}", o); }
            return o;
        }

        /**
         * Push an item onto the expression stack
         *
         * @param p_expression the object to push onto the expression stack
         */
        public void push(IExpression p_expression) {

            if (logger.isDebugEnabled()) { logger.debug("Push expression: {}", p_expression); }

            this.m_expressions.push(p_expression);
        }
    }
}
