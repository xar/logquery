/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.enums;

import java.util.EnumSet;

/**
 * Enumeration of data types.
 */
public enum DataType {

    UNDEFINED,
    STRING,
    INTEGER,
    REAL,
    BOOLEAN,
    DATE,
    TIMESTAMP,
    NULL;

    /**
     * Data types that describes a number
     */
    public static EnumSet<DataType> NUMBER = EnumSet.of(INTEGER, REAL);

}
