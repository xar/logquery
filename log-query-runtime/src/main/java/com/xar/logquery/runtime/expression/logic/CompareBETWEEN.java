/*
 * Copyright 2014 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.logic;

import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.functions.exceptions.InvalidParametersException;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.expression.interfaces.IFunctionParameter;

/**
 * Compares a value to see if it is between an upper nad a lower bound.
 */
public class CompareBETWEEN extends AndCondition {

    public CompareBETWEEN(final IExpression p_value, final IExpression p_lower, final IExpression p_upper) {

        super(new CompareGTE(p_value, p_lower, p_lower.getParseData()), new CompareLTE(p_value, p_upper, p_upper.getParseData()));
    }

}
