/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.parse;

/**
 * Parse data
 */
public class ParseData {

    public static final ParseData EMPTY = new ParseData(0, 0, "");

    private int rowNumber;

    private int columnNumber;

    private String text;

    public ParseData(final int p_rowNumber, final int p_columnNumber, final String p_text) {

        rowNumber = p_rowNumber;
        columnNumber = p_columnNumber;
        text = p_text;
    }

    public int getColumnNumber() {

        return columnNumber;
    }

    public int getRowNumber() {

        return rowNumber;
    }

    public String getText() {

        return text;
    }
}
