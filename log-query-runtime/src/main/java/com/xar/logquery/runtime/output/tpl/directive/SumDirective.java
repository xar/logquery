/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.output.tpl.directive;

import com.xar.logquery.runtime.expression.exceptions.UnconvertableValueException;
import com.xar.logquery.runtime.util.ConvertFunctions;
import com.xar.logquery.runtime.util.NumberFunctions;
import com.xar.logquery.runtime.util.TypeFunctions;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateNumberModel;
import freemarker.template.TemplateScalarModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Freemarker User defined directive to sum the values of a field.
 * <p/>
 * Parameters:
 * <ul>
 * <li><code>field</code>: The name of the field to be summed. This field must be a number, or contain a string that can be parsed to a number, and is compulsory</li>
 * <li><code>output</code>: The name of the result  of the sum. This field is optional, and the output will be defaulted into a field names SumOf_&lt;fieldName&gt;</li>
 * </ul>
 * <p/>
 * Loop Variables: None
 * Mested content: No
 * <p/>
 * Created by Anton Koekemoer on 2014/12/04.
 */
public class SumDirective extends AggregateDirectiveBase<Number> {

    /**
     * The shared logger instance.
     */
    private static final Logger logger = LoggerFactory.getLogger(SumDirective.class);


    public SumDirective() {

        super("SumOf_");
    }

    @Override
    protected IAggregate<Number> createAggregate(final String p_fieldName) {

        return new Sum(p_fieldName);
    }

    private class Sum implements IAggregate<Number> {

        Number sum;

        private String m_fieldName;

        public Sum(final String p_fieldName) {


            m_fieldName = p_fieldName;
        }

        public void aggregate(TemplateModel p_valueModel) throws TemplateModelException {

            Number p_sum = null;
            if (p_valueModel instanceof TemplateNumberModel) {
                p_sum = ((TemplateNumberModel) p_valueModel).getAsNumber();
            }
            else if (p_valueModel instanceof TemplateScalarModel) {
                String value = ((TemplateScalarModel) p_valueModel).getAsString();

                try {
                    if (NumberFunctions.isIntegral(value)) {
                        p_sum = ConvertFunctions.toInteger(value);
                    }
                    else {
                        p_sum = ConvertFunctions.toReal(value);
                    }
                }
                catch (UnconvertableValueException e) {
                    e.printStackTrace();
                }
            }
            else {
                throw new TemplateModelException("Unable to SUM non numeric value " + m_fieldName + "!");
            }

            if (p_sum == null) { return; }
            try {
                if (sum == null) { sum = p_sum; }

                // If one of the values is a double, we manage both as doubles
                else if (TypeFunctions.isValueReal(p_sum) || TypeFunctions.isValueReal(sum)) {
                    sum = ConvertFunctions.toReal(sum).add(ConvertFunctions.toReal(p_sum));
                }
                else {
                    sum = ConvertFunctions.toInteger(sum).add(ConvertFunctions.toInteger(p_sum));
                }
            }
            catch (UnconvertableValueException e) {
                logger.error("Error adding values - " + sum + " and " + p_sum);
            }

        }

        public Number getValue() {

            return sum;
        }
    }
}
