/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.output.tpl.directive;

import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.util.FormatFunctions;
import freemarker.core.Environment;
import freemarker.ext.beans.BeanModel;
import freemarker.ext.beans.BeansWrapperBuilder;
import freemarker.ext.beans.BooleanModel;
import freemarker.template.Configuration;
import freemarker.template.SimpleDate;
import freemarker.template.SimpleNumber;
import freemarker.template.SimpleScalar;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;

import java.io.IOException;
import java.io.Writer;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Enumeration;
import java.util.Map;

/**
 * FreeMarker user-defined directive for the template body. Iterates over the datasource, and set the
 * values accordingly
 * <p>Nested content: Yes
 */
public class BodyDirective implements TemplateDirectiveModel {


    private IDataSource m_dataSource;

    private IExecutionContext m_executionContext;

    public BodyDirective(final IDataSource p_dataSource, final IExecutionContext p_executionContext) {


        m_dataSource = p_dataSource;
        m_executionContext = p_executionContext;
    }

    public void execute(Environment env,
                        Map params, TemplateModel[] loopVars,
                        TemplateDirectiveBody body)
            throws TemplateException, IOException {

        // ---------------------------------------------------------------------
        // Do the actual directive execution:
        if (loopVars.length > 2) {
            throw new TemplateModelException(
                    "At most two loop variable is allowed.");
        }

        Writer out = env.getOut();
        if (body != null) {
            try {
                IResultSet m_resultSet = m_dataSource.getResultSet(m_executionContext);
                IColumnInfo[] columnInfo = m_resultSet.getColumnInfo();
                Enumeration<IDataRow> rows = m_resultSet.getRows();
                while (rows.hasMoreElements()) {
                    IDataRow row = rows.nextElement();
                    Object[] data = row.getData();
                    if (loopVars.length > 0) {
                        loopVars[0] = new BeanModel(row, new BeansWrapperBuilder(Configuration.VERSION_2_3_21).build());
                    }
                    if (loopVars.length > 1) {
                        loopVars[1] = new BooleanModel(rows.hasMoreElements(), new BeansWrapperBuilder(Configuration.VERSION_2_3_21).build());
                    }
                    for (int i = 0; i < columnInfo.length; i++) {
                        IColumnInfo column = columnInfo[i];
                        addVariable(env, column.getDataType(), column.getName(), data[i]);
                    }

                    body.render(out);
                }
            }
            catch (DataSourceException e) {
                throw new TemplateException(e, env);
            }
        }
    }

    private void addVariable(final Environment env, final DataType p_dataType, final String p_name, final Object p_value) {

        if (p_value == null) {
            env.setVariable(p_name, new SimpleScalar((String) p_value));
        }
        else {
            switch (p_dataType) {
                case INTEGER:
                case REAL:
                    env.setVariable(p_name, new SimpleNumber((Number) p_value));
                    break;
                case STRING:
                    env.setVariable(p_name, new SimpleScalar((String) p_value));
                    break;
                case DATE:
                    env.setVariable(p_name, new SimpleDate(new Date(((java.util.Date) p_value).getTime())));
                    break;
                case TIMESTAMP:
                    env.setVariable(p_name, new SimpleDate(new Timestamp(((java.util.Date) p_value).getTime())));
                    break;
                default:
                    env.setVariable(p_name, new SimpleScalar(FormatFunctions.asString(p_value, p_dataType)));
                    break;
            }
        }

    }

}
