/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.functions.aggregate;


import com.xar.logquery.runtime.annotations.Function;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.expression.functions.aggregate.interfaces.IAggregator;
import com.xar.logquery.runtime.expression.functions.aggregate.interfaces.IAggregatorFactory;
import com.xar.logquery.runtime.expression.functions.exceptions.InvalidParametersException;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;
import com.xar.logquery.runtime.util.RuntimeFunctions;

/**
 * Implementation for the max function. Finds the maximum value of the selected expression
 */
@Function(names = {"MIN"})
public class MinFunction extends AggregateFunctionBase {


    /**
     * Validates the expression against the specified context
     *
     * @param p_context the context to validate the expression against
     */
    @Override
    public void validateExpression(final IValidationContext p_context) throws InvalidExpressionException {
        // We only accept one
        if (m_functionParameters.length != 1) {
            String functionName = RuntimeFunctions.getFunctionName(getClass());
            throw new InvalidParametersException(getParseData(), functionName + " requires one parameter\nCalled with: " + RuntimeFunctions.getFunctionCallPrototype(functionName, m_functionParameters));
        }

        setDataType(m_functionParameters[0].getDataType());
        setAggregatorFactory(new IAggregatorFactory() {

            @Override
            public IAggregator create() {

                return new MinAggregator();
            }
        });
    }


    /**
     * Aggregator for the first function
     */
    private class MinAggregator implements IAggregator {

        Object value;

        /**
         * Aggregate the value
         *
         * @param p_context the current expression context
         */
        @Override
        public void aggregate(final IExecutionContext p_context) {

            Object expressionValue = m_functionParameters[0].getValue(p_context);
            if (expressionValue != null) {
                if (value == null) {
                    value = expressionValue;
                }
                else if (RuntimeFunctions.compare(value, expressionValue) > 0) {
                    value = expressionValue;
                }
            }
        }

        /**
         * Get the value for the aggregate
         *
         * @return the value of the aggregate
         */
        @Override
        public Object getValue() {

            return value;
        }
    }
}
