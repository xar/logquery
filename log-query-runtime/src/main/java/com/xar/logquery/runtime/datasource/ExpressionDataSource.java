/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource;

import com.xar.logquery.runtime.ColumnInfo;
import com.xar.logquery.runtime.DataRow;
import com.xar.logquery.runtime.ResultSet;
import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;
import com.xar.logquery.runtime.util.ConvertFunctions;

import java.util.Enumeration;
import java.util.NoSuchElementException;

/**
 * A datasource that creates it's data based on the evaluation of a number of expressions.
 * The expressions are one for column in each row required in the result.
 */
public class ExpressionDataSource extends AbstractDataSource {

    private IColumnInfo[] m_columnInfo;

    private IExpression[][] m_data;

    /**
     * Construct a new expression datasource with the specified columns and data
     *
     * @param p_columnInfo the column info
     * @param p_data       the expressions to evaluate for the data
     */
    public ExpressionDataSource(final IColumnInfo[] p_columnInfo, final IExpression[][] p_data) {

        m_columnInfo = p_columnInfo;
        m_data = p_data;
    }


    /**
     * Execute the query and returns a resultset
     *
     * @param p_context the execution context
     *
     * @return the resultset
     *
     * @throws com.xar.logquery.runtime.exceptions.DataSourceException if the execution fails
     */
    @Override
    protected IResultSet executeQuery(final IExecutionContext p_context) throws DataSourceException {

        return new ResultSet(m_columnInfo, new ExpressionEnumeration(p_context));
    }

    /**
     * Validate the datasource
     *
     * @param p_context the binding context
     *
     * @throws com.xar.logquery.runtime.exceptions.DataSourceException if the datasource is invalid
     */
    @Override
    protected void validateQuery(final IValidationContext p_context) throws DataSourceException {

        for (IExpression[] row : m_data) { for (IExpression expression : row) { expression.validate(p_context); } }
    }

    private class ExpressionEnumeration implements Enumeration<IDataRow> {

        int rowNumber = 0;

        private IExecutionContext m_context;

        public ExpressionEnumeration(final IExecutionContext p_context) {

            m_context = p_context;
        }

        /**
         * Tests if this enumeration contains more elements.
         *
         * @return <code>true</code> if and only if this enumeration object
         * contains at least one more element to provide;
         * <code>false</code> otherwise.
         */
        @Override
        public boolean hasMoreElements() {

            return rowNumber < m_data.length;
        }

        /**
         * Returns the next element of this enumeration if this enumeration
         * object has at least one more element to provide.
         *
         * @return the next element of this enumeration.
         *
         * @throws java.util.NoSuchElementException if no more elements exist.
         */
        @Override
        public IDataRow nextElement() {

            if (!hasMoreElements()) {
                throw new NoSuchElementException();
            }
            Object[] data = new Object[m_columnInfo.length];

            final IExpression[] row = m_data[rowNumber];

            for (int i = 0; i < row.length; i++) {
                data[i] = ConvertFunctions.convertValue(row[i].getValue(m_context), m_columnInfo[i].getDataType());
            }
            rowNumber++;
            return new DataRow(m_columnInfo, data);
        }
    }
}
