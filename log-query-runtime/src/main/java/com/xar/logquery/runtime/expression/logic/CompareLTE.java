/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.logic;

import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.parse.ParseData;

/**
 * Created by koekemoera on 2014/08/29.
 */
public class CompareLTE extends Compare {

    public CompareLTE(final IExpression p_lhs, final IExpression p_rhs) {

        super(p_lhs, p_rhs);
    }

    public CompareLTE(final IExpression p_value, final IExpression p_upper, final ParseData p_parseData) {

        super(p_value, p_upper, p_parseData);
    }


    /**
     * Get the valueExpression of the expression, based on the current row context
     *
     * @param p_context the current row context
     *
     * @return the valueExpression of the expression.
     */
    @Override
    public Object getValue(final IExecutionContext p_context) {

        return ((Number) super.getValue(p_context)).intValue() <= 0;
    }

}
