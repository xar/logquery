/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource;

import com.xar.logquery.runtime.ColumnInfo;
import com.xar.logquery.runtime.DataRow;
import com.xar.logquery.runtime.ResultSet;
import com.xar.logquery.runtime.ValidationContext;
import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;
import com.xar.logquery.runtime.util.ConvertFunctions;
import com.xar.logquery.runtime.util.TypeFunctions;

import java.util.Enumeration;
import java.util.NoSuchElementException;

/**
 * Created by akoekemo on 2/17/15.
 */
public class UnionDataSource extends AbstractDataSource {

    private IColumnInfo[] m_columnInfo;

    private IDataSource[] m_dataSources;


    /**
     * Initialize a new Union datasource
     *
     * @param p_dataSource1 the data sources
     */
    public UnionDataSource(final IDataSource... p_dataSource1) {

        m_dataSources = p_dataSource1;
    }

    /**
     * Execute the query and returns a resultset
     *
     * @param p_context the execution context
     *
     * @return the resultset
     *
     * @throws com.xar.logquery.runtime.exceptions.DataSourceException if the execution fails
     */
    @Override
    protected IResultSet executeQuery(final IExecutionContext p_context) throws DataSourceException {

        return new ResultSet(m_columnInfo, new UnionEnumeration(p_context));
    }

    /**
     * Validate the datasource
     *
     * @param p_context the binding context
     *
     * @throws com.xar.logquery.runtime.exceptions.DataSourceException if the datasource is invalid
     */
    @Override
    protected void validateQuery(final IValidationContext p_context) throws DataSourceException {


        for (IDataSource dataSource : m_dataSources) {
            ValidationContext context = new ValidationContext(p_context);
            dataSource.validate(context);
            IColumnInfo[] columns = context.getColumns();
            if (m_columnInfo == null) {
                m_columnInfo = columns;
                continue;
            }
            else if (columns.length != m_columnInfo.length) {
                throw new DataSourceException("Number of columns in the select queries do not match!");
            }

            for (int i = 0; i < m_columnInfo.length; i++) {
                IColumnInfo columnInfo = m_columnInfo[i];
                final DataType type1 = columnInfo.getDataType();
                final DataType type2 = columns[i].getDataType();

                if (!TypeFunctions.isTypeCompatible(type1, type2)) {
                    throw new DataSourceException("Data type error. Unable to case value of type " + type2 + " to " + type1 + "!");
                }
                DataType type = TypeFunctions.compatibleType(type1, type2);
                if (type != type1) {
                    m_columnInfo[i] = new ColumnInfo(columnInfo.getContext(), columnInfo.getName(), type);
                }
            }

        }

        for (IColumnInfo info : m_columnInfo) { p_context.addColumn(info); }

    }


    class UnionEnumeration implements Enumeration<IDataRow> {

        int m_idx = 0;

        Enumeration<IDataRow>[] m_current;

        private IExecutionContext m_context;

        public UnionEnumeration(final IExecutionContext p_context) throws DataSourceException {

            m_context = p_context;

            m_current = new Enumeration[m_dataSources.length];

            for (int i = 0; i < m_dataSources.length; i++) {
                m_current[i] = m_dataSources[i].getResultSet(m_context).getRows();
            }
        }

        /**
         * Tests if this enumeration contains more elements.
         *
         * @return <code>true</code> if and only if this enumeration object
         * contains at least one more element to provide;
         * <code>false</code> otherwise.
         */
        @Override
        public boolean hasMoreElements() {

            return getEnumeration().hasMoreElements();
        }

        /**
         * Returns the next element of this enumeration if this enumeration
         * object has at least one more element to provide.
         *
         * @return the next element of this enumeration.
         *
         * @throws NoSuchElementException if no more elements exist.
         */
        @Override
        public IDataRow nextElement() {

            final Enumeration<IDataRow> rowEnumeration = getEnumeration();
            if (!rowEnumeration.hasMoreElements()) { throw new NoSuchElementException();}

            IDataRow row = rowEnumeration.nextElement();

            Object[] data = row.getData();
            for (int i = 0; i < data.length; i++) {
                data[i] = ConvertFunctions.convertValue(data[i], m_columnInfo[i].getDataType());
            }

            return new DataRow(m_columnInfo, data);
        }

        private Enumeration<IDataRow> getEnumeration() {

            if (!m_current[m_idx].hasMoreElements()) {
                if (m_idx + 1 < m_dataSources.length) {
                    m_idx++;
                }
            }
            return m_current[m_idx];
        }
    }
}
