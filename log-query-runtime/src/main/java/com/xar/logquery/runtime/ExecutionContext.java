/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime;

import com.xar.logquery.runtime.interfaces.IExecutionContext;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by akoekemo on 2/23/15.
 */
public class ExecutionContext implements IExecutionContext {

    /**
     * The parent context
     */
    private IExecutionContext m_parentContext;

    /**
     * The values in the context
     */
    private Map<String, Object> m_values = new HashMap<>();


    /**
     * Initialize a new instance of the execution context
     */
    public ExecutionContext() {

    }

    /**
     * Initialize a new instance of the execution context, and set the parent context
     *
     * @param p_parentContext the parent context
     */
    public ExecutionContext(final IExecutionContext p_parentContext) {


        m_parentContext = p_parentContext;
    }

    /**
     * Get the value of the object associated with the specified context and name
     *
     * @param p_context the context
     * @param p_name    the name
     *
     * @return the value of the object associated with the specified context and name
     */
    @Override
    public Object getValue(final String p_context, final String p_name) {

        String key = "[" + p_context + "].[" + p_name + "]";

        if (m_values.containsKey(key)) {
            return m_values.get(key);
        }

        return m_parentContext == null ? null : m_parentContext.getValue(p_context, p_name);
    }

    /**
     * Reset the context and clears all values
     */
    public void reset() {

        m_values.clear();
    }

    /**
     * Associated the value of an object with the specified context and name
     *
     * @param p_context the context
     * @param p_name    the name
     * @param p_value   the value
     */
    @Override
    public void setValue(final String p_context, final String p_name, final Object p_value) {

        m_values.put("[" + p_context + "].[" + p_name + "]", p_value);
    }

}
