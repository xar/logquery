/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime;

import com.xar.logquery.parser.CaseInsensitiveANTLRInputStream;
import com.xar.logquery.parser.generated.SelectLexer;
import com.xar.logquery.parser.generated.SelectParser;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

/**
 * Created by akoekemo on 2/24/15.
 */
public class DataSource {

    private static DataSource _instance = new DataSource();
    // Prevent construction of instances of this type
    private DataSource() {}

    /**
     * Parses the SQL and returns a datasource
     *
     * @param p_sql the sql to parse
     *
     * @return a datasource
     *
     * @throws IOException
     * @throws DataSourceException
     */
    public static IDataSource parse(String p_sql) throws IOException, DataSourceException {

        return parse(new StringReader(p_sql));
    }

    /**
     * Parses the SQL in the reader and returns a datasource
     *
     * @param p_reader the sql to parse
     *
     * @return a datasource
     *
     * @throws IOException
     * @throws DataSourceException
     */
    public static IDataSource parse(Reader p_reader) throws IOException, DataSourceException {
        // create a CharStream that reads from scalar input
        ANTLRInputStream input = new CaseInsensitiveANTLRInputStream(p_reader);

        // create a lexer that feeds off of input CharStream
        SelectLexer lexer = new SelectLexer(input);

        // create a buffer of tokens pulled from the lexer
        CommonTokenStream tokens = new CommonTokenStream(lexer);

        // create a parser that feeds off the tokens buffer
        SelectParser parser = new SelectParser(tokens);


        ParseTree tree = parser.statement(); // begin parsing at init rule

        RuntimeSelectVisitor builder = new RuntimeSelectVisitor();
        builder.visit(tree);

        IDataSource dataSource = builder.getDataSource();

        dataSource.validate(new ValidationContext());

        return dataSource;
    }

}
