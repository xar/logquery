/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.functions.scalar;

import com.xar.logquery.runtime.annotations.Function;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.AbstractExpression;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.expression.functions.exceptions.InvalidParametersException;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.expression.interfaces.IFunction;
import com.xar.logquery.runtime.expression.interfaces.IFunctionParameter;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;
import com.xar.logquery.runtime.util.ConvertFunctions;
import com.xar.logquery.runtime.util.RuntimeFunctions;
import com.xar.logquery.runtime.util.TypeFunctions;

/**
 * Equivalent to the Oracle Decode function
 */
@Function(names = {"DECODE"})
public class DecodeFunction extends AbstractExpression implements IFunction {

    private IExpression[] m_options;

    private DataType m_dataType = DataType.NULL;

    /**
     * Get the child expressions of this expression
     *
     * @return the child expressions of this expression
     */
    @Override
    public IExpression[] getChildren() {

        return m_options;
    }

    /**
     * Get the datatype of the expression
     *
     * @return the datatype of the expression
     */
    @Override
    public DataType getDataType() {

        return m_dataType;
    }

    /**
     * Get the value of the expression, based on the current row context
     *
     * @param p_context the current row context
     *
     * @return the value of the expression.
     */
    @Override
    public Object getValue(final IExecutionContext p_context) {

        Object value = m_options[0].getValue(p_context);

        int index = 1;
        while (index < m_options.length) {
            Object option = m_options[index++].getValue(p_context);
            if (index >= m_options.length) {
                return ConvertFunctions.convertValue(option, m_dataType);
            }
            if (RuntimeFunctions.compare(value, option) == 0) {
                return ConvertFunctions.convertValue(m_options[index].getValue(p_context), m_dataType);
            }
            index++;
        }
        return null;
    }

    /**
     * Set the parameters of the function
     *
     * @param p_functionParameters the parameters
     */
    @Override
    public void setParameters(final IFunctionParameter... p_functionParameters) {

        m_options = p_functionParameters;
    }

    /**
     * Validates the expression against the specified context
     *
     * @param p_context the context to validate the expression against
     */
    @Override
    public void validateExpression(final IValidationContext p_context) throws InvalidExpressionException {

        if (m_options.length < 3) {
            throw new InvalidParametersException(getParseData(), RuntimeFunctions.getFunctionName(getClass()) + " - Invalid function parameters, requires at least 3 parameters");
        }

        m_dataType = m_options[2].getDataType();

        int index = 1;
        while (index < m_options.length) {
            IExpression option = m_options[index++];

            if (index >= m_options.length) {
                if (!TypeFunctions.isTypeCompatible(m_dataType, option.getDataType())) {
                    throw new InvalidParametersException(getParseData(), RuntimeFunctions.getFunctionName(getClass()) + " - Invalid function parameters, unable to cast " + option.getDataType() + " to " + m_dataType);
                }
                break;
            }
            else if (!TypeFunctions.isTypeCompatible(m_options[0].getDataType(), option.getDataType())) {
                throw new InvalidParametersException(getParseData(), RuntimeFunctions.getFunctionName(getClass()) + " - Invalid function parameters, unable to cast " + option.getDataType() + " to " + m_options[0].getDataType());
            }
            option = m_options[index++];
            if (option.getDataType() != DataType.NULL) {
                if (!TypeFunctions.isTypeCompatible(m_dataType, option.getDataType())) {
                    throw new InvalidParametersException(getParseData(), RuntimeFunctions.getFunctionName(getClass()) + " - Invalid function parameters, unable to cast " + option.getDataType() + " to " + m_dataType);
                }
                m_dataType = TypeFunctions.compatibleType(m_dataType, option.getDataType());
            }

        }

    }
}
