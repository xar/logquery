package com.xar.logquery.runtime.datasource.filter;

import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.datasource.interfaces.IFilter;
import com.xar.logquery.runtime.datasource.interfaces.IFilterFactory;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;

/**
 * Created by akoekemo on 11/13/15.
 */
public class ExpressionFilterFactory implements IFilterFactory {

    private IExpression m_expression;

    public ExpressionFilterFactory(IExpression p_expression) {

        m_expression = p_expression;
    }

    /**
     * Create a filter
     *
     * @return a new filter
     */
    @Override
    public IFilter create() {

        return new ExpressionFilter(m_expression);
    }

    /**
     * Validate the filter against the validation context
     *
     * @param p_context the binding context
     *
     * @throws com.xar.logquery.runtime.exceptions.DataSourceException if the datasource is invalid
     */
    @Override
    public void validate(final IValidationContext p_context) throws DataSourceException {

        m_expression.validate(p_context);
        if (m_expression.getDataType() != DataType.BOOLEAN) {
            throw new InvalidExpressionException(m_expression.getParseData(), "Expression does not evaluate to a BOOLEAN result");
        }
    }

    private static class ExpressionFilter implements IFilter {

        private final IExpression m_condition;

        public ExpressionFilter(final IExpression p_condition) {

            m_condition = p_condition;
        }

        /**
         * Tests the row against the condition
         *
         * @param p_context    An execution context for the current operation. Implementers do not need to wrap it or reset. This context is ready to use.
         * @param p_columnInfo the column info for the data
         * @param p_rowData    the data
         *
         * @return true if the row is accepted; false otherwise
         */
        public boolean accept(final IExecutionContext p_context, final IColumnInfo[] p_columnInfo, final Object[] p_rowData) {

            for (int i = 0; i < p_rowData.length; i++) {

                p_context.setValue(p_columnInfo[i].getContext(), p_columnInfo[i].getName(), p_rowData[i]);
            }
            return ((Boolean) m_condition.getValue(p_context));
        }
    }

}
