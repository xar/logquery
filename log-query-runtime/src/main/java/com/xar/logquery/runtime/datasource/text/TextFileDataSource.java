/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource.text;

import com.xar.logquery.runtime.ResultSet;
import com.xar.logquery.runtime.datasource.AliasableDataSource;
import com.xar.logquery.runtime.datasource.annotations.DataSourceAttribute;
import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IFileDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;
import com.xar.logquery.runtime.util.FileFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Datasource used to read text files.
 */
public abstract class TextFileDataSource<TReader> extends AliasableDataSource implements IFileDataSource {

    /**
     * The shared logger instance.
     */
    private static final Logger logger = LoggerFactory.getLogger(TextFileDataSource.class);

    /**
     * File pattern
     */
    @DataSourceAttribute(name = "filePattern", description = "A file pattern used to match the files. Can include wildcards * and ?", required = true)
    private String m_filePattern;

    /**
     * Column info for the data source
     */
    private IColumnInfo[] m_columnInfo;


    /**
     * Instantiates a new Text file datasource.
     */
    public TextFileDataSource() {

    }

    /**
     * Instantiates a new Text file datasource.
     *
     * @param p_filePattern the file pattern
     * @param p_alias       the alias
     */
    public TextFileDataSource(final String p_filePattern, final String p_alias) {

        this();

        setFilePattern(p_filePattern);
        setAlias(p_alias);
    }

    /**
     * Open the data source and return a resultset. This method can be called multiple times
     *
     * @param p_context the execution context
     *
     * @return a new resultset
     */
    @Override
    public IResultSet executeQuery(final IExecutionContext p_context) throws DataSourceException {

        if (m_columnInfo == null) { throw new DataSourceException("DataSource has not been validated!"); }
        try {
            return new ResultSet(getColumnInfo(), new TextFileEnumeration(p_context));
        }
        catch (IOException e) {
            throw new DataSourceException("Error opening files", e);
        }
    }

    /**
     * Get the file pattern used
     *
     * @return the file pattern used
     */
    @Override
    public String getFilePattern() {

        return m_filePattern;
    }

    /**
     * Set the file pattern used
     *
     * @param p_filePattern the file pattern used
     */
    @Override
    public void setFilePattern(final String p_filePattern) {

        m_filePattern = p_filePattern;
    }

    /**
     * Closes the file reader
     *
     * @param p_reader the file reader
     */
    protected abstract void closeFileReader(final TReader p_reader);

    /**
     * Create the reader appropriate for this datasource
     *
     * @param p_fileName the file to be read
     *
     * @return the reader
     *
     * @throws java.io.FileNotFoundException the file not found exception
     */
    protected abstract TReader createFileReader(final File p_fileName) throws IOException;

    /**
     * Get the column info for the datasource
     *
     * @return the column info
     */
    protected IColumnInfo[] getColumnInfo() {

        return m_columnInfo;
    }

    /**
     * Set the column info for the datasource
     *
     * @param p_columnInfo the column info
     */
    protected void setColumnInfo(final IColumnInfo[] p_columnInfo) {

        m_columnInfo = p_columnInfo;
    }

    /**
     * Construct a row from the data
     *
     * @param p_text the text
     *
     * @return a new data row
     */
    protected abstract IDataRow readLine(TReader p_text) throws IOException;

    /**
     * Validate the datasource
     *
     * @param p_context the binding context
     *
     * @throws com.xar.logquery.runtime.exceptions.DataSourceException if the datasource is invalid
     */
    @Override
    protected void validateQuery(final IValidationContext p_context) throws DataSourceException {

        for (IColumnInfo columnInfo : m_columnInfo) {
            p_context.addColumn(columnInfo);
        }
    }

    public class TextFileEnumeration implements Enumeration<IDataRow> {

        /**
         * List of files
         */
        List<File> files;

        /**
         * Reader for the current file
         */
        TReader reader;

        /**
         * Current line
         */
        IDataRow line;

        private IExecutionContext m_parentContext;

        /**
         * Instantiates a new Text line enumeration.
         *
         * @param p_context
         *
         * @throws java.io.IOException the iO exception
         */
        public TextFileEnumeration(final IExecutionContext p_context) throws IOException {

            m_parentContext = p_context;

            files = FileFunctions.listFiles(getFilePattern());
        }

        /**
         * Tests if this enumeration contains more elements.
         *
         * @return <code>true</code> if and only if this enumeration object
         * contains at least one more element to provide;
         * <code>false</code> otherwise.
         */
        @Override
        public boolean hasMoreElements() {

            return peek() != null;
        }

        /**
         * Returns the next element of this enumeration if this enumeration
         * object has at least one more element to provide.
         *
         * @return the next element of this enumeration.
         *
         * @throws java.util.NoSuchElementException if no more elements exist.
         */
        @Override
        public IDataRow nextElement() {

            IDataRow line = takeLine();
            if (line == null) { throw new NoSuchElementException(); }

            return line;
        }

        /**
         * Advances the line pointer to the next line, and set it as the current line
         *
         * @return the next line
         */
        private IDataRow advance() {

            line = null;
            while (true) {
                if (reader == null) {
                    if (files.size() == 0) { break; }
                    try {
                        File fileName = files.get(0);
                        reader = createFileReader(fileName);
                    }
                    catch (IOException e) {
                        logger.error("File not found: " + files.get(0).getAbsolutePath());
                        popFile();
                        continue;
                    }
                }
                try {
                    if ((line = readLine(reader)) != null) {
                        break;
                    }
                }
                catch (IOException e) {
                    logger.error("Error reading file: " + files.get(0).getAbsolutePath());
                }
                popFile();
            }
            return line;
        }

        /**
         * Returns the current line without advancing to the next line
         *
         * @return
         */
        private IDataRow peek() {

            if (line == null) {
                advance();
            }
            return line;
        }

        /**
         * Moves on the the next file
         *
         * @return true if there are more files, false otherwise
         */
        private boolean popFile() {

            if (reader != null) { closeFileReader(reader); }
            reader = null;
            if (files.size() > 0) {
                files.remove(0);
            }
            return files.size() > 0;
        }

        /**
         * Return the current line in the file set, and advance to the next line in the file(s)
         *
         * @return the current line
         */
        private IDataRow takeLine() {

            try {
                return peek();
            }
            finally {
                line = null;
            }
        }
    }


}
