/*
 * Copyright 2014 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.output.tpl.directive;

import com.xar.logquery.runtime.util.TypeFunctions;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;

/**
 * Freemarker User defined directive to keep the minimum value of a field.
 * <p/>
 * Parameters:
 * <ul>
 * <li><code>field</code>: The name of the field to be kept. This field can contain any type of value, and is compulsory</li>
 * <li><code>output</code>: The name field into which the result is to be placed. This field is optional, and the output will be defaulted into a field names MinOf_&lt;fieldName&gt;</li>
 * </ul>
 * <p/>
 * Loop Variables: None
 * Mested content: No
 * <p/>
 * Created by Anton Koekemoer on 2014/12/04.
 */
public class MinDirective extends AggregateDirectiveBase<Object> {

    public MinDirective() {

        super("MinOf_");
    }

    @Override
    protected IAggregate<Object> createAggregate(final String p_fieldName) {

        return new Min(p_fieldName);
    }

    private class Min implements IAggregate<Object> {

        private Object m_minimum;

        private String m_fieldName;

        public Min(final String p_fieldName) {

            m_fieldName = p_fieldName;
        }

        @Override
        public void aggregate(final TemplateModel p_model) throws TemplateModelException {

            Object value = getValueOf(p_model);

            if (m_minimum == null) { m_minimum = value; }

            if (value instanceof Number) {
                if (TypeFunctions.isValueReal(value) || TypeFunctions.isValueReal(m_minimum)) {
                    m_minimum = Math.min(((Number) m_minimum).doubleValue(), ((Number) value).doubleValue());
                }
                else { m_minimum = Math.min(((Number) m_minimum).longValue(), ((Number) value).longValue()); }
            }
            else if (m_minimum instanceof Comparable) {
                if (value.getClass().isAssignableFrom(m_minimum.getClass())) {
                    m_minimum = ((Comparable) value).compareTo(m_minimum) < 0 ? value : m_minimum;
                }
                else {
                    m_minimum = ((Comparable) m_minimum).compareTo(value) < 0 ? m_minimum : value;
                }
            }
            else {
                throw new TemplateModelException("Unable to determine minimum foe field " + m_fieldName + "!");
            }

        }

        @Override
        public Object getValue() {

            return m_minimum;
        }
    }
}
