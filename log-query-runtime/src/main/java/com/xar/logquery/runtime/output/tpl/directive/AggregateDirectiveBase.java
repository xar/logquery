/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.output.tpl.directive;

import freemarker.core.Environment;
import freemarker.ext.beans.BeanModel;
import freemarker.template.SimpleDate;
import freemarker.template.SimpleNumber;
import freemarker.template.SimpleScalar;
import freemarker.template.TemplateBooleanModel;
import freemarker.template.TemplateDateModel;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateNumberModel;
import freemarker.template.TemplateScalarModel;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Base class for aggregate template directives
 * <p/>
 * Created by Anton Koekemoer on 2014/12/04.
 */
public abstract class AggregateDirectiveBase<T> implements TemplateDirectiveModel {

    private static final String FIELD_NAME = "field";

    private static final String OUTPUT_NAME = "output";

    Map<String, IAggregate> m_sums = new HashMap<>();

    private String m_prefix;

    /**
     * Instantiates a new Aggregate directive base.
     *
     * @param p_prefix the p _ prefix
     */
    public AggregateDirectiveBase(final String p_prefix) {

        m_prefix = p_prefix;
    }

    /**
     * Executes the directive
     *
     * @param p_environment           the template environment
     * @param p_map                   the map with parameters
     * @param p_templateModels        template models for directive variables
     * @param p_templateDirectiveBody the body of the directive
     *
     * @throws TemplateException if something goes wrong
     * @throws java.io.IOException       if an input output error occurs
     */
    @Override
    public void execute(final Environment p_environment, final Map p_map, final TemplateModel[] p_templateModels, final TemplateDirectiveBody p_templateDirectiveBody) throws TemplateException, IOException {

        String fieldName = getString(p_map, FIELD_NAME, true);
        String outputName = getString(p_map, OUTPUT_NAME, false);

        if (outputName == null) { outputName = m_prefix + fieldName; }

        TemplateModel valueModel = p_environment.getVariable(fieldName);

        IAggregate aggregate = m_sums.get(outputName);

        if (aggregate == null) {
            aggregate = createAggregate(fieldName);
            m_sums.put(outputName, aggregate);
        }
        aggregate.aggregate(valueModel);

        p_environment.setVariable(outputName, createModel(aggregate.getValue()));

    }

    /**
     * Called to create a new aggregate for the field
     *
     * @param p_fieldName te field the aggregate is created for
     *
     * @return a new aggregate for the field
     */
    protected abstract IAggregate<T> createAggregate(final String p_fieldName);

    /**
     * Get the object value for the supplied template model
     *
     * @param p_model the template model
     *
     * @return the object value for the supplied template model
     *
     * @throws TemplateModelException
     */
    protected Object getValueOf(TemplateModel p_model) throws TemplateModelException {

        if (p_model instanceof TemplateNumberModel) {
            return ((TemplateNumberModel) p_model).getAsNumber();
        }
        if (p_model instanceof TemplateDateModel) {
            return ((TemplateDateModel) p_model).getAsDate();
        }
        if (p_model instanceof TemplateBooleanModel) {
            return ((TemplateBooleanModel) p_model).getAsBoolean();
        }
        if (p_model instanceof TemplateScalarModel) {
            return ((TemplateScalarModel) p_model).getAsString();
        }
        if (p_model instanceof BeanModel) {
            return ((BeanModel) p_model).getWrappedObject();
        }
        return null;
    }

    /**
     * Creates a TemplateModel appropriate to the type of bject.
     *
     * @param p_value the value to create the model for
     *
     * @return a TemplateModel appropriate to the type of bject.
     */
    private TemplateModel createModel(final Object p_value) {

        if (p_value instanceof Number) { return new SimpleNumber((Number) p_value); }
        if (p_value instanceof Boolean) {
            return (Boolean) p_value ? TemplateBooleanModel.TRUE : TemplateBooleanModel.FALSE;
        }
        if (p_value instanceof String) { return new SimpleScalar((String) p_value); }
        if (p_value instanceof java.sql.Date) { return new SimpleDate((java.sql.Date) p_value); }
        if (p_value instanceof java.sql.Timestamp) { return new SimpleDate((java.sql.Timestamp) p_value); }
        if (p_value instanceof java.util.Date) {
            return new SimpleDate(new java.sql.Date(((java.util.Date) p_value).getTime()));
        }

        return new SimpleScalar(String.valueOf(p_value));
    }

    /**
     * Get a string parameter
     *
     * @param p_map           the map containg the paramters
     * @param p_parameterName the name of the required parameter
     * @param p_required      true idf the parameter is compulsory; false otherwise
     *
     * @return the value of the parameter as a string
     *
     * @throws TemplateModelException
     */
    private String getString(final Map p_map, String p_parameterName, boolean p_required) throws TemplateModelException {

        Object fieldModel = p_map.get(p_parameterName);

        if (fieldModel == null) {
            if (p_required) {
                throw new TemplateModelException("The " + p_parameterName + " parameter is compulsory!");
            }
            return null;
        }
        if (!(fieldModel instanceof TemplateScalarModel)) {
            throw new TemplateModelException("The " + p_parameterName + " parameter must be a string!");
        }
        return ((TemplateScalarModel) fieldModel).getAsString();
    }


    /**
     * Minimum required interface for an aggregate
     *
     * @param <T>
     */
    protected interface IAggregate<T> {

        public void aggregate(TemplateModel value) throws TemplateModelException;

        public T getValue();

    }
}
