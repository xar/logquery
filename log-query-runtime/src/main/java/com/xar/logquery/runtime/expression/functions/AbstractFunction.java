/*
* Copyright 2015 Anton Koekemoer
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.xar.logquery.runtime.expression.functions;

import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.AbstractExpression;
import com.xar.logquery.runtime.expression.exceptions.EvaluationException;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.expression.functions.exceptions.InvalidParametersException;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.expression.interfaces.IFunction;
import com.xar.logquery.runtime.expression.interfaces.IFunctionParameter;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;
import com.xar.logquery.runtime.util.ArrayFunctions;
import com.xar.logquery.runtime.util.ConvertFunctions;
import com.xar.logquery.runtime.util.RuntimeFunctions;
import com.xar.logquery.runtime.util.TypeFunctions;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Abstract function implementation. This implementation uses a callback mechanism to perform the
 * operation. Operations are selected by input parameters.
 */
public abstract class AbstractFunction extends AbstractExpression implements IFunction {

    /**
     * Input parameters
     */
    protected IFunctionParameter[] m_functionParameters;

    /**
     * List of available callbacks
     */
    private List<Callback> m_callbacks = new ArrayList<>();

    /**
     * Callback that was selected based on the input parameter types for the operation
     */
    private Callback m_selectedCallback = null;

    /**
     * The data type of the operation
     */
    private DataType m_dataType;


    /**
     * Instantiates a new Abstract function.
     *
     * @param p_dataType the data type of the operation
     */
    protected AbstractFunction(final DataType p_dataType, IFunctionParameter... p_parameters) {

        setDataType(p_dataType);

        if (p_parameters != null) { m_functionParameters = p_parameters; }
    }

    /**
     * Get the child expressions of this expression
     *
     * @return the child expressions of this expression
     */
    @Override
    public final IExpression[] getChildren() {

        return m_functionParameters;
    }

    /**
     * Get the datatype of the expression
     *
     * @return the datatype of the expression
     */
    @Override
    public DataType getDataType() {

        return m_selectedCallback != null ? m_selectedCallback.result : m_dataType;
    }

    /**
     * Set the datatype of the function
     *
     * @param p_dataType the datatype of the function
     */
    protected void setDataType(final DataType p_dataType) {

        m_dataType = p_dataType;
    }

    /**
     * Get the value of the expression, based on the current row context
     *
     * @param p_context the current row context
     *
     * @return the value of the expression.
     */
    @Override
    public Object getValue(final IExecutionContext p_context) {

        Object[] parameters = new Object[m_selectedCallback.parameters.length];
        for (int i = 0; i < parameters.length; i++) {
            parameters[i] = ConvertFunctions.convertValue(m_functionParameters[i].getValue(p_context), m_selectedCallback.parameters[i]);
        }

        return m_selectedCallback.executeCallback.execute(parameters);
    }

    /**
     * Set the parameters of the function
     *
     * @param p_functionParameters the parameters
     */
    @Override
    public void setParameters(final IFunctionParameter... p_functionParameters) {

        m_functionParameters = p_functionParameters;
    }

    /**
     * Validates the expression against the specified context
     *
     * @param p_context the context to validate the expression against
     */
    @Override
    public void validateExpression(final IValidationContext p_context) throws InvalidExpressionException {

        for (Callback callback : m_callbacks) {
            DataType[] callbackParameters = callback.parameters;

            if (callbackParameters.length != m_functionParameters.length) { continue; }

            m_selectedCallback = callback;
            for (int j = 0; j < callbackParameters.length && j < m_functionParameters.length; j++) {
                if (callback.parameters[j] != m_functionParameters[j].getDataType()) {
                    m_selectedCallback = null;
                    break;
                }
            }
            if (m_selectedCallback != null) { break; }
        }
        if (m_selectedCallback == null) {
            for (Callback callback : m_callbacks) {
                if (isCompatible(m_functionParameters, callback.parameters)) {
                    m_selectedCallback = callback;
                    break;
                }
            }
        }
        if (m_selectedCallback == null) {

            DataType[][] expected = new DataType[m_callbacks.size()][];
            for (int i = 0; i < m_callbacks.size(); i++) {
                expected[i] = m_callbacks.get(i).parameters;
            }
            throwInvalidParameters(expected, m_functionParameters);
        }

    }

    /**
     * Add a callback for the specified parameter types, that will invoke the secified method on the first parameter
     *
     * @param p_parameters the input parameters to match
     * @param p_method     the method to invoke
     */
    protected void addCallback(DataType[] p_parameters, Method p_method) {

        addCallback(p_parameters, new MethodCallback(p_method));
    }

    protected void addCallback(DataType[] p_parameters, IExecuteCallback p_callback) {

        addCallback(m_dataType, p_parameters, p_callback);
    }

    protected void addCallback(DataType p_result, DataType[] p_parameters, IExecuteCallback p_callback) {

        Callback callback = new Callback(p_result, p_parameters, p_callback);
        m_callbacks.add(callback);
    }

    protected void throwInvalidParameters(final DataType[][] p_expected, final IFunctionParameter[] p_functionParameters) throws InvalidExpressionException {
        throw new InvalidParametersException(getParseData(), "Invalid parameters in function call.\n\n" + RuntimeFunctions.buildParameterErrorMessage(getClass(), p_functionParameters, p_expected));
    }

    /**
     * Validates the supplied parameters against the function parameters to determine if they are compatible.
     *
     * @param p_functionParameters the function parameters
     * @param p_dataType           the data types to validate against
     *
     * @return true if the parameters are compatible, false otherwise
     */
    public static boolean isCompatible(final IExpression[] p_functionParameters, final DataType[] p_dataType) {

        DataType[] dataType = p_dataType;

        if (p_functionParameters.length == dataType.length) {
            boolean valid = true;
            for (int j = 0; j < dataType.length; j++) {
                if (!TypeFunctions.isTypeCompatible(p_functionParameters[j].getDataType(), dataType[j])) {
                    valid = false;
                    break;
                }
            }
            if (valid) { return true; }
        }
        return false;
    }

    protected interface IExecuteCallback {


        public Object execute(Object[] p_parameters);
    }

    protected static class MethodCallback implements IExecuteCallback {

        Method m_method;

        public MethodCallback(final Method p_method) {

            m_method = p_method;
        }

        @Override
        public Object execute(final Object[] p_parameters) {

            try {
                return m_method.invoke(p_parameters[0], ArrayFunctions.subArray(p_parameters, 1));
            }
            catch (Exception e) {
                throw new EvaluationException(e.getCause() != null ? e.getCause().getMessage() : e.getMessage());
            }
        }
    }

    private class Callback {

        DataType[] parameters;


        IExecuteCallback executeCallback;

        private DataType result;

        private Callback(final DataType p_result, final DataType[] p_parameters, final IExecuteCallback p_executeCallback) {

            result = p_result;

            parameters = p_parameters;
            executeCallback = p_executeCallback;
        }

//        @Override
//        public String toString() {
//
//            return RuntimeFunctions.getFunctionCallPrototype(RuntimeFunctions.getFunctionName(ScalarFunctionBase.this.getClass()), parameters);
//
//        }
    }
}
