/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource;///*

import com.xar.logquery.runtime.ResultSet;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;

import java.util.Enumeration;
import java.util.NoSuchElementException;

/**
 * Datasource used to limit output
 */
public class LimitDataSource extends AbstractDataSource {

    private IDataSource m_dataSource;

    private int m_skip;

    private int m_take;

    public LimitDataSource(final IDataSource p_dataSource, final int p_skip, final int p_take) {

        m_dataSource = p_dataSource;
        m_skip = p_skip;
        m_take = p_take;
    }

    /**
     * Execute the query and returns a resultset
     *
     * @param p_context the execution context
     *
     * @return the resultset
     *
     * @throws com.xar.logquery.runtime.exceptions.DataSourceException if the execution fails
     */
    @Override
    protected IResultSet executeQuery(final IExecutionContext p_context) throws DataSourceException {

        IResultSet resultSet = m_dataSource.getResultSet(p_context);

        return new ResultSet(resultSet.getColumnInfo(), new LimitEnumeration<>(resultSet.getRows(), m_skip, m_take));
    }

    /**
     * Validate the datasource
     *
     * @param p_context the binding context
     *
     * @throws com.xar.logquery.runtime.exceptions.DataSourceException if the datasource is invalid
     */
    @Override
    protected void validateQuery(final IValidationContext p_context) throws DataSourceException {

        m_dataSource.validate(p_context);
    }

    private class LimitEnumeration<T> implements Enumeration<T> {

        Enumeration<T> inner;

        int skip;

        int take;

        public LimitEnumeration(final Enumeration<T> p_inner, final int p_skip, final int p_take) {

            inner = p_inner;
            skip = p_skip;
            take = p_take;
        }

        /**
         * Tests if this enumeration contains more elements.
         *
         * @return <code>true</code> if and only if this enumeration object
         * contains at least one more element to provide;
         * <code>false</code> otherwise.
         */
        @Override
        public boolean hasMoreElements() {

            skip();

            return skip == 0 && take > 0 && inner.hasMoreElements();
        }

        /**
         * Returns the next element of this enumeration if this enumeration
         * object has at least one more element to provide.
         *
         * @return the next element of this enumeration.
         *
         * @throws NoSuchElementException if no more elements exist.
         */
        @Override
        public T nextElement() {

            skip();
            if (take > 0) {
                T item = inner.nextElement();
                take--;
                return item;
            }
            throw new NoSuchElementException();
        }

        private void skip() {

            while (skip > 0 && inner.hasMoreElements()) {
                skip--;
                inner.nextElement();
            }
        }
    }
}
