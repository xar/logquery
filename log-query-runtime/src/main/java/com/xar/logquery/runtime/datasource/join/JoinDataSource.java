/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource.join;

import com.xar.logquery.runtime.DataRow;
import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.ResultSet;
import com.xar.logquery.runtime.datasource.AbstractDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;
import com.xar.logquery.runtime.util.ArrayFunctions;
import com.xar.logquery.runtime.util.RuntimeFunctions;

import java.util.EnumSet;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import static com.xar.logquery.runtime.datasource.join.JoinDataSource.JoinType.LEFT_OUTER;
import static com.xar.logquery.runtime.datasource.join.JoinDataSource.JoinType.RIGHT_OUTER;

/**
 * Created by akoekemo on 3/16/15.
 */
public class JoinDataSource extends AbstractDataSource {

    private IDataSource m_datasourceLeft;

    private IDataSource m_datasourceRight;

    private IExpression m_searchCondition;

    private EnumSet<JoinType> m_joinType;

    public JoinDataSource(final IDataSource p_left, final IDataSource p_right, final IExpression p_searchCondition, final JoinType p_joinType) {

        this(p_left, p_right, p_searchCondition, EnumSet.of(p_joinType));
    }

    /**
     * Initialise a new instance of the inner join datasource
     *
     * @param p_left            the left hand datasource
     * @param p_right           the right hand datasource
     * @param p_searchCondition
     * @param p_joinType
     */

    public JoinDataSource(final IDataSource p_left, final IDataSource p_right, final IExpression p_searchCondition, final EnumSet<JoinType> p_joinType) {

        m_datasourceLeft = p_left;
        m_datasourceRight = p_right;
        m_searchCondition = p_searchCondition;
        m_joinType = p_joinType;
    }

    /**
     * Execute the query and returns a resultset
     *
     * @param p_context the execution context
     *
     * @return the resultset
     *
     * @throws com.xar.logquery.runtime.exceptions.DataSourceException if the execution fails
     */
    @Override
    protected IResultSet executeQuery(final IExecutionContext p_context) throws DataSourceException {

        IResultSet resultsetLeft = m_datasourceLeft.getResultSet(p_context);
        IResultSet resultsetRight = m_datasourceRight.getResultSet(p_context);

        IColumnInfo[] columnInfo = ArrayFunctions.concatenate(resultsetLeft.getColumnInfo(), resultsetRight.getColumnInfo());

        List<Object[]> dataLeft = RuntimeFunctions.getData(resultsetLeft.getRows());
        ReferencedItemList<Object[]> dataRight = RuntimeFunctions.getData(resultsetRight.getRows(), new ReferencedItemList<Object[]>());

        return new ResultSet(columnInfo, new JoinEnumeration(p_context, columnInfo, dataLeft, dataRight, resultsetLeft.getColumnInfo().length));
    }

    /**
     * Validate the datasource
     *
     * @param p_context the binding context
     *
     * @throws com.xar.logquery.runtime.exceptions.DataSourceException if the datasource is invalid
     */
    @Override
    protected void validateQuery(final IValidationContext p_context) throws DataSourceException {

        m_datasourceLeft.validate(p_context);
        m_datasourceRight.validate(p_context);
        m_searchCondition.validate(p_context);
    }

    public enum JoinType {
        LEFT_OUTER,
        RIGHT_OUTER,
        INNER
    }

    private class JoinEnumeration implements Enumeration<IDataRow> {

        private final Iterator<Object[]> m_iteratorLeft;

        private final ReferencedItemList<Object[]> m_dataRight;

        private final IExecutionContext m_context;

        private int m_rightOffset;

        private Iterator<Object[]> m_iteratorUnreferenced;

        private Object[] m_rowLeft;

        private ReferencedItemIterator<Object[]> m_iteratorRight;

        private IColumnInfo[] m_columnInfo;

        private Object[] m_currentRow;

        private boolean m_leftReferenced;

        public JoinEnumeration(final IExecutionContext p_context, final IColumnInfo[] p_columnInfo, final List<Object[]> p_dataLeft, final ReferencedItemList<Object[]> p_dataRight, final int p_rightOffset) {

            m_columnInfo = p_columnInfo;

            m_dataRight = p_dataRight;
            m_rightOffset = p_rightOffset;
            m_iteratorLeft = p_dataLeft.iterator();
            m_iteratorRight = p_dataRight.iterator();

            m_context = new ExecutionContext(p_context);

        }

        /**
         * Tests if this enumeration contains more elements.
         *
         * @return <code>true</code> if and only if this enumeration object
         * contains at least one more element to provide;
         * <code>false</code> otherwise.
         */
        @Override
        public boolean hasMoreElements() {

            if (m_currentRow == null) { advance(); }
            return m_currentRow != null;
        }

        /**
         * Returns the next element of this enumeration if this enumeration
         * object has at least one more element to provide.
         *
         * @return the next element of this enumeration.
         *
         * @throws java.util.NoSuchElementException if no more elements exist.
         */
        @Override
        public IDataRow nextElement() {

            if (m_currentRow == null) { advance(); }
            if (m_currentRow == null) { throw new NoSuchElementException();}
            try {
                return new DataRow(m_columnInfo, m_currentRow);
            }
            finally {
                advance();
            }
        }

        private void advance() {

            m_currentRow = null;
            while (m_currentRow == null) {
                if (m_rowLeft == null && !advanceLeft()) { break; }
                if (!advanceRight()) {
                    if (!m_leftReferenced && m_joinType.contains(LEFT_OUTER)) {
                        m_currentRow = ArrayFunctions.concatenate(m_rowLeft, new Object[m_columnInfo.length - m_rightOffset]);
                        m_rowLeft = null;
                        return;
                    }
                    m_rowLeft = null;
                }
            }
            if (m_currentRow == null && m_joinType.contains(RIGHT_OUTER)) {
                if (m_iteratorUnreferenced == null) {
                    m_iteratorUnreferenced = m_dataRight.unreferencedItems.iterator();
                }
                if (m_iteratorUnreferenced.hasNext()) {
                    Object[] rowRight = m_iteratorUnreferenced.next();
                    m_currentRow = ArrayFunctions.concatenate(new Object[m_columnInfo.length - rowRight.length], rowRight);
                }
            }

        }

        private boolean advanceLeft() {

            if (m_iteratorLeft.hasNext()) {
                m_rowLeft = m_iteratorLeft.next();
                m_leftReferenced = false;
                m_iteratorRight = m_dataRight.iterator();
                applyToContext(m_rowLeft, 0, m_rightOffset);
                return true;
            }
            return false;
        }

        private boolean advanceRight() {

            while (m_iteratorRight.hasNext()) {
                Object[] rowRight = m_iteratorRight.next();
                applyToContext(rowRight, m_rightOffset, m_columnInfo.length - m_rightOffset);

                if ((Boolean) m_searchCondition.getValue(m_context)) {
                    m_leftReferenced = true;
                    m_iteratorRight.referenced();
                    m_currentRow = ArrayFunctions.concatenate(m_rowLeft, rowRight);
                    return true;
                }
            }
            return false;
        }

        private String a2s(final Object[] p_array) {
            StringBuilder builder = new StringBuilder();
            for (Object o : p_array){
                builder.append(',').append(o);
            }
            return builder.length() > 0 ? builder.substring(1) : "";
        }

        private void applyToContext(final Object[] p_rowdata, final int p_offset, final int p_length) {

            for (int i = 0; i < p_length; i++) {
                IColumnInfo columnInfo = m_columnInfo[i + p_offset];
                m_context.setValue(columnInfo.getContext(), columnInfo.getName(), p_rowdata[i]);
            }
        }
    }
}
