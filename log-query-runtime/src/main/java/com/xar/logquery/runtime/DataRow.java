/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime;


import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.exceptions.AmbiguousColumnNameException;
import com.xar.logquery.runtime.exceptions.ColumnNotFoundException;
import com.xar.logquery.runtime.util.StringUtils;

/**
 * Implementation of the data row interface
 */
public class DataRow implements IDataRow {

    private IColumnInfo[] m_columnInfo;

    private Object[] m_data;

    /**
     * Instantiates a new Data row.
     *
     * @param p_columnInfo the column info
     * @param p_data       the data
     */
    public DataRow(final IColumnInfo[] p_columnInfo, final Object[] p_data) {

        m_columnInfo = p_columnInfo;
        m_data = p_data;

        if (m_columnInfo.length != m_data.length) {
            throw new RuntimeException("Invalid number of columns. Expected " + m_columnInfo.length + ", but found " + p_data.length);
        }
    }

    public Object get(String p_columnName) throws ColumnNotFoundException, AmbiguousColumnNameException {

        return get("", p_columnName);
    }

    /**
     * Get the value of the column with the specified name
     *
     * @param p_context    the column context
     * @param p_columnName the name of the column
     *
     * @return the value of the column with the specified name
     *
     * @throws com.xar.logquery.runtime.exceptions.ColumnNotFoundException if the column name does not exist
     */
    public Object get(final String p_context, final String p_columnName) throws ColumnNotFoundException, AmbiguousColumnNameException {

        int idx = -1;
        IColumnInfo[] columnInfo = getColumnInfo();
        for (int i = 0; i < columnInfo.length; i++) {
            final IColumnInfo info = columnInfo[i];
            if (info.getName().equals(p_columnName)) {
                if (StringUtils.isEmpty(p_context) && idx >= 0) {
                    throw new AmbiguousColumnNameException("Ambiguous column name - " + p_columnName);
                }
                idx = i;
                if (info.getContext().equals(p_context)) {
                    break;
                }
            }
        }
        if (idx >= 0) { return get(idx); }
        throw new ColumnNotFoundException("Column not found - ColumnName: " + p_columnName);
    }

    public Object get(final int p_index) {

        return m_data[p_index];

    }

    /**
     * Get the column Info of the row
     *
     * @return an array containing the column Info of the row
     */
    public IColumnInfo[] getColumnInfo() {

        return m_columnInfo;
    }

    /**
     * Get the data associated with the row
     *
     * @return the data associated with the row
     */
    public Object[] getData() {

        return m_data;
    }

    /**
     * Returns a string representation of the object.
     *
     * @return a string representation of the object.
     */
    @Override
    public String toString() {

        StringBuffer buffer = new StringBuffer();
        IColumnInfo[] columnInfo = getColumnInfo();

        for (int i = 0; i < columnInfo.length; i++) {

            IColumnInfo iColumnInfo = columnInfo[i];
            Object value = i < m_data.length ? m_data[i] : null;
            if (!StringUtils.isEmpty(iColumnInfo.getContext())) {
                buffer.append(iColumnInfo.getContext()).append('.');
            }
            buffer.append(iColumnInfo.getName()).append("=[").append(value).append("], ");
        }

        return "DataRow{" + buffer.substring(0, buffer.length() - 2) + '}';
    }
}
