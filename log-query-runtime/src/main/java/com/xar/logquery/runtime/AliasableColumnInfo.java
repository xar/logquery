/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime;

import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.interfaces.IAliasable;
import com.xar.logquery.runtime.parse.ParseData;

/**
 * Created by akoekemo on 2/23/15.
 */
public class AliasableColumnInfo implements IColumnInfo {

    IAliasable m_parent;

    /**
     * Name of the column
     */
    private String m_name;

    /**
     * Data type of the column
     */
    private DataType m_dataType;

    private ParseData m_parseData;

    /**
     * Instantiates a new Column info.
     *
     * @param p_parent   the  datasource that implements the column
     * @param p_name     the column name
     * @param p_dataType the data type of the column
     */
    public AliasableColumnInfo(final IAliasable p_parent, final String p_name, final DataType p_dataType) {

        this(p_parent, p_name, p_dataType, ParseData.EMPTY);
    }

    /**
     * Instantiates a new Column info.
     *
     * @param p_parent   the  datasource that implements the column
     * @param p_name     the column name
     * @param p_dataType the data type of the column
     */
    public AliasableColumnInfo(final IAliasable p_parent, final String p_name, final DataType p_dataType, ParseData p_parseData) {

        m_parent = p_parent;
        this.m_name = p_name;
        this.m_dataType = p_dataType;
        this.m_parseData = p_parseData;
    }


    /**
     * Indicates whether some other object is "equal to" this one.
     *
     * @param o the object to compare to this one
     *
     * @return true if this object is the same as the obj argument; false otherwise.
     */
    @Override
    public boolean equals(final Object o) {

        if (this == o) { return true; }
        if (!(o instanceof IColumnInfo)) { return false; }

        final IColumnInfo that = (IColumnInfo) o;

        if (!this.getContext().equals(that.getContext())) { return false; }
        if (this.m_dataType != that.getDataType()) { return false; }
        if (!this.m_name.equals(that.getName())) { return false; }

        return true;
    }

    /**
     * Get the column context (alias of the datasource)
     *
     * @return the column context or alias of the datasource
     */
    public String getContext() {

        return m_parent.getAlias();
    }

    /**
     * Get the data type of the column
     *
     * @return the data type of the column
     */
    public DataType getDataType() {

        return this.m_dataType;
    }

    /**
     * Get the name of the column
     *
     * @return the name of the column
     */
    public String getName() {

        return this.m_name;
    }

}
