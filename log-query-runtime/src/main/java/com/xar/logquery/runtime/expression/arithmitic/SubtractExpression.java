/*
 * Copyright 2014 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.arithmitic;

import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.BinaryExpression;
import com.xar.logquery.runtime.expression.functions.FunctionParameter;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.util.ConvertFunctions;
import org.joda.time.Days;
import org.joda.time.LocalDate;

import java.util.Date;

/**
 * Created by koekemoera on 2014/08/27.
 */
public class SubtractExpression extends BinaryExpression {

    public SubtractExpression() {

        LongCallback longCallback = new LongCallback();
        DoubleCallback doubleCallback = new DoubleCallback();
        LongDateCallback longDateCallback = new LongDateCallback();
        LongTimeStampCallback longTimeStampCallback = new LongTimeStampCallback();
        DoubleTimeStampCallback doubleTimeStampCallback = new DoubleTimeStampCallback();
        TimestampCallback timestampCallback = new TimestampCallback();

        addInvertedCallback(DataType.INTEGER, new DataType[]{DataType.INTEGER, DataType.INTEGER}, longCallback);
        addInvertedCallback(DataType.REAL, new DataType[]{DataType.INTEGER, DataType.REAL}, doubleCallback);
        addInvertedCallback(DataType.DATE, new DataType[]{DataType.INTEGER, DataType.DATE}, longDateCallback);
        addInvertedCallback(DataType.TIMESTAMP, new DataType[]{DataType.INTEGER,
                                                               DataType.TIMESTAMP}, longTimeStampCallback);

        addInvertedCallback(DataType.DATE, new DataType[]{DataType.REAL, DataType.DATE}, longDateCallback);
        addInvertedCallback(DataType.TIMESTAMP, new DataType[]{DataType.REAL,
                                                               DataType.TIMESTAMP}, doubleTimeStampCallback);

        addInvertedCallback(DataType.REAL, new DataType[]{DataType.REAL, DataType.REAL}, doubleCallback);

        addInvertedCallback(DataType.INTEGER, new DataType[]{DataType.DATE, DataType.DATE}, new DateCallback());
        addInvertedCallback(DataType.INTEGER, new DataType[]{DataType.DATE, DataType.TIMESTAMP}, timestampCallback);

        addInvertedCallback(DataType.INTEGER, new DataType[]{DataType.TIMESTAMP,
                                                             DataType.TIMESTAMP}, timestampCallback);

    }

    /**
     * Instantiates a new Add m_expression.
     *
     * @param p_lhs the left hand side expresion
     * @param p_rhs the right hand side expresion
     */
    public SubtractExpression(final IExpression p_lhs, final IExpression p_rhs) {

        this();
        setParameters(new FunctionParameter(p_lhs), new FunctionParameter(p_rhs));

    }

    /**
     * Callback for operations that produce a double value as result
     */
    private static class DoubleCallback extends BinaryCallback {

        @Override
        protected Object doOperation(final Object p_lhs, final Object p_rhs) {

            return ConvertFunctions.safeToReal(p_lhs).subtract(ConvertFunctions.safeToReal(p_rhs));
        }
    }

    /**
     * Callback for operations add a double value to a date
     */
    private static class DoubleTimeStampCallback extends BinaryCallback {

        @Override
        protected Object doOperation(final Object p_lhs, final Object p_rhs) {

            if (p_lhs instanceof Date) {
                return new Date((long) (((Date) p_lhs).getTime() - (((Number) p_rhs).doubleValue() * 86400000)));
            }
            return new Date((long) (((Date) p_rhs).getTime() - (((Number) p_lhs).doubleValue() * 86400000)));
        }
    }

    /**
     * Callback for operations that add two long values together
     */
    private static class LongCallback extends BinaryCallback {

        @Override
        protected Object doOperation(final Object p_lhs, final Object p_rhs) {

            return ConvertFunctions.safeToInteger(p_lhs).subtract(ConvertFunctions.safeToInteger(p_rhs));
        }
    }

    /**
     * Callback for operations that subtract a long value from a date
     */
    private static class LongDateCallback extends BinaryCallback {

        @Override
        protected Object doOperation(final Object p_lhs, final Object p_rhs) {

            if (p_lhs instanceof Date) {
                return LocalDate.fromDateFields((Date) p_lhs).minusDays(((Number) p_rhs).intValue()).toDate();
            }
            return LocalDate.fromDateFields((Date) p_rhs).minusDays(((Number) p_lhs).intValue()).toDate();
        }
    }

    /**
     * Callback for operations that subtract a long value from a date
     */
    private static class LongTimeStampCallback extends BinaryCallback {

        @Override
        protected Object doOperation(final Object p_lhs, final Object p_rhs) {

            if (p_lhs instanceof Date) {
                return new Date(((Date) p_lhs).getTime() - ((Number) p_rhs).longValue());
            }
            return new Date(((Date) p_rhs).getTime() - ((Number) p_lhs).longValue());
        }
    }

    private class DateCallback extends BinaryCallback {

        @Override
        protected Object doOperation(final Object p_lhs, final Object p_rhs) {

            return Days.daysBetween(LocalDate.fromDateFields((Date) p_lhs), LocalDate.fromDateFields((Date) p_rhs)).getDays();
        }
    }

    private class TimestampCallback extends BinaryCallback {

        @Override
        protected Object doOperation(final Object p_lhs, final Object p_rhs) {

            return ((Date) p_lhs).getTime() - ((Date) p_rhs).getTime();
        }
    }

}
