/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.functions.scalar.string;

import com.xar.logquery.runtime.annotations.Function;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.functions.AbstractFunction;

import static com.xar.logquery.runtime.enums.DataType.INTEGER;
import static com.xar.logquery.runtime.enums.DataType.STRING;

/**
 * Created by koekemoera on 2014/09/05.
 */
@Function(
        names = {"EXTRACT_SUFFIX"}
)
public class ExtractSuffixFunction extends AbstractFunction {

    public ExtractSuffixFunction() {

        super(STRING);

        addCallback(new DataType[]{STRING, INTEGER, STRING}, new IExecuteCallback() {

                    @Override
                    public Object execute(final Object[] p_parameters) {

                        String text = (String) p_parameters[0];
                        if (text == null) { return null; }
                        int instance = ((Number) p_parameters[1]).intValue();
                        String token = (String) p_parameters[2];

                        // We start from the right
                        if (instance >= 0) {
                            int pos = 0;
                            while (pos >= 0 && instance >= 0) {
                                int idx = text.indexOf(token, pos);
                                if (idx >= 0) {
                                    pos = idx + token.length();
                                    instance--;
                                }
                                else {
                                    return null;
                                }
                            }
                            return pos >= 0 ? text.substring(pos) : null;
                        }
                        else {
                            int pos = text.length();
                            while (pos >= 0 && instance < 0) {
                                int idx = text.lastIndexOf(token, pos);
                                if (idx >= 0) {
                                    pos = idx - 1;
                                    instance++;
                                }
                                else {
                                    return null;
                                }
                            }
                            return pos > 0 ? text.substring(pos + token.length() + 1) : null;
                        }
                    }
                }
        );
    }
}
