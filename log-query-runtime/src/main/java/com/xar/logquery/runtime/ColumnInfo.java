/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime;

import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.parse.ParseData;

/**
 * Implementation of the columnInfo interface
 */
public class ColumnInfo implements IColumnInfo {

    /**
     * Context of the column
     */
    private String m_context;

    /**
     * Name of the column
     */
    private String m_name;

    /**
     * Data type of the column
     */
    private DataType m_dataType;

    private ParseData m_parseData;

    /**
     * Instantiates a new Column info.
     *
     * @param p_context  the  context
     * @param p_name     the column name
     * @param p_dataType the data type of the column
     */
    public ColumnInfo(final String p_context, final String p_name, final DataType p_dataType) {

        this.m_context = p_context;
        this.m_name = p_name;
        setDataType(p_dataType);
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     *
     * @param o the object to compare to this one
     *
     * @return true if this object is the same as the obj argument; false otherwise.
     */
    @Override
    public boolean equals(final Object o) {

        if (this == o) { return true; }
        if (!(o instanceof IColumnInfo)) { return false; }

        final IColumnInfo that = (IColumnInfo) o;

        if (!this.m_context.equals(that.getContext())) { return false; }
        if (this.m_dataType != that.getDataType()) { return false; }
        if (!this.m_name.equals(that.getName())) { return false; }

        return true;
    }

    /**
     * Get the column context (alias of the datasource)
     *
     * @return the column context or alias of the datasource
     */
    public String getContext() {

        return this.m_context == null ? "" : this.m_context;
    }

    /**
     * Get the data type of the column
     *
     * @return the data type of the column
     */
    public DataType getDataType() {

        return this.m_dataType;
    }

    /**
     * Set the column data type
     *
     * @param p_dataType
     */
    public void setDataType(final DataType p_dataType) {

        m_dataType = p_dataType;
    }

    /**
     * Get the name of the column
     *
     * @return the name of the column
     */
    public String getName() {

        return this.m_name;
    }

    public static boolean equals(final IColumnInfo p_info1, final IColumnInfo p_info2) {

        if (p_info1 == p_info2) { return true; }

        if (!p_info1.getContext().equals(p_info2.getContext())) { return false; }
        if (!p_info1.getName().equals(p_info2.getName())) { return false; }
//        if (this.m_dataType != that.getDataType()) { return false; }

        return true;
    }
}
