/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.functions.aggregate;

import com.xar.logquery.runtime.annotations.Function;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.expression.functions.aggregate.interfaces.IAggregator;
import com.xar.logquery.runtime.expression.functions.aggregate.interfaces.IAggregatorFactory;
import com.xar.logquery.runtime.expression.functions.exceptions.InvalidParametersException;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;
import com.xar.logquery.runtime.util.RuntimeFunctions;

/**
 * Average aggregate function implementation. Will return the average of all non null values of a numeric expression
 */
@Function(names = {"AVG"})
public class AverageFunction extends AggregateFunctionBase {

    /**
     * Instantiates a new Aggregate function base.
     */
    public AverageFunction() {

        super(DataType.REAL, null);
    }

    /**
     * Validates the expression against the specified context
     *
     * @param p_context the context to validate the expression against
     */
    @Override
    public void validateExpression(final IValidationContext p_context) throws InvalidExpressionException {

        if (m_functionParameters.length == 1 && DataType.NUMBER.contains(m_functionParameters[0].getDataType())) {
        }
        else {
            // Not optimal, but no need to optimise here, as this will almost never be called, and will
            // not affect performance
            DataType[] numberTypes = DataType.NUMBER.toArray(new DataType[DataType.NUMBER.size()]);
            DataType[][] expected = new DataType[numberTypes.length][];
            for (int i = 0; i < numberTypes.length; i++) {
                expected[i] = new DataType[]{numberTypes[i]};
            }

            throw new InvalidParametersException(getParseData(), "Invalid parameters in function call.\n\n" + RuntimeFunctions.buildParameterErrorMessage(getClass(), m_functionParameters, expected));
        }
        setAggregatorFactory(new IAggregatorFactory() {

            @Override
            public IAggregator create() {

                return new AverageAggregator();
            }
        });

    }


    /**
     * Average value aggregator. Ignores nulls
     */
    private class AverageAggregator implements IAggregator {

        private int count;

        private double total;

        /**
         * Aggregate the value
         *
         * @param p_context the current expression context
         */
        @Override
        public void aggregate(final IExecutionContext p_context) {

            Object value = m_functionParameters[0].getValue(p_context);
            if (value instanceof Number) {
                count++;
                total += ((Number) value).doubleValue();
            }
        }

        /**
         * Get the value for the aggregate
         *
         * @return the value of the aggregate
         */
        @Override
        public Object getValue() {

            return total / count;
        }
    }
}
