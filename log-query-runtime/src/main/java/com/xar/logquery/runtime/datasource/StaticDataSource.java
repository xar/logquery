/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource;

import com.xar.logquery.runtime.DataRow;
import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
// TODO: Validate input data, do not allow setting column info if there is already data, check data consistent with column info

/**
 * Static datasource implementation
 */
public class StaticDataSource extends AliasableDataSource {

    private IColumnInfo[] m_columnInfo;

    private List<IDataRow> m_data = new ArrayList<>();

    /**
     * Initialise a new Static datasource
     */
    public StaticDataSource() {

    }


    /**
     * Initialise a new Static datasource
     *
     * @param p_columnInfo the column info for the datasource
     * @param p_data       the data for the dastasource
     */
    public StaticDataSource(final IColumnInfo[] p_columnInfo, final Object[][] p_data) {

        m_columnInfo = p_columnInfo;
        for (Object[] data : p_data) { m_data.add(new DataRow(m_columnInfo, data)); }
    }

    /**
     * Initialise a new Static datasource
     *
     * @param p_columnInfo the column info for the datasource
     * @param p_data       the data for the dastasource
     */
    public StaticDataSource(final IColumnInfo[] p_columnInfo, final List<Object[]> p_data) {

        m_columnInfo = p_columnInfo;
        for (Object[] data : p_data) { m_data.add(new DataRow(m_columnInfo, data)); }
    }

    /**
     * Add a row to the datasource
     *
     * @param p_data the row to add
     */
    public void addRow(Object... p_data) {

        m_data.add(new DataRow(m_columnInfo, p_data));
    }

    /**
     * Get the column info
     *
     * @return the column info
     */
    public IColumnInfo[] getColumnInfo() {


        return m_columnInfo;
    }

    /**
     * Set  the column info
     *
     * @param p_columnInfo the column info
     */
    public void setColumnInfo(final IColumnInfo[] p_columnInfo) {

        m_columnInfo = p_columnInfo;
    }

    /**
     * Execute the query and returns a resultset
     *
     * @param p_context the execution context
     *
     * @return the resultset
     *
     * @throws com.xar.logquery.runtime.exceptions.DataSourceException if the execution fails
     */
    @Override
    protected IResultSet executeQuery(final IExecutionContext p_context) throws DataSourceException {

        return new IResultSet() {

            @Override
            public IColumnInfo[] getColumnInfo() {

                return m_columnInfo;
            }

            @Override
            public Enumeration<IDataRow> getRows() throws DataSourceException {

                return new IteratorEnumeration<>(m_data.iterator());
            }
        };
    }

    /**
     * Validate the datasource
     *
     * @param p_context the binding context
     *
     * @throws com.xar.logquery.runtime.exceptions.DataSourceException if the datasource is invalid
     */
    @Override
    protected void validateQuery(final IValidationContext p_context) throws DataSourceException {

        for (IColumnInfo info : m_columnInfo) {
            p_context.addColumn(info);
        }
    }
}
