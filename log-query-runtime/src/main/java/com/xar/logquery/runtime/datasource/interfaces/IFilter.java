/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource.interfaces;

import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;

/**
 * Created by akoekemo on 4/25/15.
 */
public interface IFilter {

    /**
     * Tests the row for acceptability onm the filter
     *
     * @param p_context    An execution context for the current operation. Implementers do not need to wrap it or reset. This context is ready to use.
     * @param p_columnInfo the column info for the data
     * @param p_rowData    the data
     *
     * @return true if the row is accepted; false otherwise
     */
    boolean accept(final IExecutionContext p_context, IColumnInfo[] p_columnInfo, Object[] p_rowData);
}
