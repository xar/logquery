/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource;

import com.xar.logquery.runtime.AliasableColumnInfo;
import com.xar.logquery.runtime.ColumnList;
import com.xar.logquery.runtime.DataRow;
import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.ResultSet;
import com.xar.logquery.runtime.ValidationContext;
import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.exceptions.DuplicateColumnNameException;
import com.xar.logquery.runtime.expression.ColumnReference;
import com.xar.logquery.runtime.expression.NamedExpression;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.interfaces.IAliasable;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;
import com.xar.logquery.runtime.util.StringUtils;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 * Created by akoekemo on 3/2/15.
 */
public class TransformDataSource extends AliasableDataSource {

    /**
     * List of transformations
     */
    protected List<NamedExpression> m_transformations = new ArrayList<>();

    /**
     * THe column info for the datasource
     */
    IColumnInfo[] m_columnInfo;

    IExpression[] m_expressions;

    /**
     * The child datasource
     */
    IDataSource m_dataSource;


    /**
     * Initialises a new Select data source
     *
     * @param p_dataSource  the datasource to select from
     * @param p_expressions the transform expressions
     */
    public TransformDataSource(final IDataSource p_dataSource, final NamedExpression[] p_expressions) {

        m_dataSource = p_dataSource;
        for (NamedExpression expression : p_expressions) { addExpression(expression); }
    }

    /**
     * Add the specified expression top the datasource
     *
     * @param p_expression the datasource
     */
    public void addExpression(NamedExpression p_expression) {

        m_transformations.add(p_expression);
    }

    /**
     * Get the alias for the object
     *
     * @return the alias for the object
     */
    @Override
    public String getAlias() {

        if (m_dataSource instanceof IAliasable) {
            return StringUtils.isEmpty(super.getAlias()) ? ((IAliasable) m_dataSource).getAlias() : super.getAlias();
        }
        return super.getAlias();
    }

    /**
     * Sets the alias for the object.
     *
     * @param p_alias the alias
     */
    @Override
    public void setAlias(final String p_alias) {

        super.setAlias(p_alias);
        if (m_dataSource instanceof IAliasable) { ((IAliasable) m_dataSource).setAlias(p_alias); }
    }

    /**
     * Execute the query and returns a resultset
     *
     * @param p_context the execution context
     *
     * @return the resultset
     *
     * @throws com.xar.logquery.runtime.exceptions.DataSourceException if the execution fails
     */
    @Override
    protected IResultSet executeQuery(final IExecutionContext p_context) throws DataSourceException {

        return new ResultSet(m_columnInfo, new TransformEnumeration(m_columnInfo, p_context));
    }

    /**
     * Validate the datasource
     *
     * @param p_context the binding context
     *
     * @throws com.xar.logquery.runtime.exceptions.DataSourceException if the datasource is invalid
     */
    @Override
    protected void validateQuery(final IValidationContext p_context) throws DataSourceException {

        ValidationContext validationContext = new ValidationContext(p_context);


        m_dataSource.validate(validationContext);

        List<IExpression> expressions = new ArrayList<>();

        // So, now we will have a list of all the columns produced by the datasource
        ColumnList columns = new ColumnList();
        for (IColumnInfo column : validationContext.getColumns()) {
            columns.addColumnIfNotExist(column);
            expressions.add(new ColumnReference(column));
            p_context.addColumn(column);
        }

        for (NamedExpression expression : m_transformations) {

            String alias = expression.getName();

            expression.validate(validationContext);

            try {
                final AliasableColumnInfo column = new AliasableColumnInfo(this, alias, expression.getDataType());
                columns.addColumn(column);
                p_context.addColumn(column);

            }
            catch (DuplicateColumnNameException ex) {
                throw new DuplicateColumnNameException(expression.getParseData(), ex.getMessage());
            }
            expressions.add(expression);
        }


        List<IColumnInfo> list = columns.getColumns();
        m_columnInfo = list.toArray(new IColumnInfo[list.size()]);
        m_expressions = expressions.toArray(new IExpression[expressions.size()]);

    }

    /**
     * Enumeration for the datasource
     */
    private class TransformEnumeration implements Enumeration<IDataRow> {

        private final Enumeration<IDataRow> m_enumeration;

        private final ExecutionContext m_executionContext;

        private final IResultSet m_resultSet;

        private final IColumnInfo[] m_sourceColumns;

        private IColumnInfo[] m_columnInfo;

        public TransformEnumeration(final IColumnInfo[] p_columnInfo, final IExecutionContext p_context) throws DataSourceException {

            m_executionContext = new ExecutionContext(p_context);
            m_columnInfo = p_columnInfo;

            m_resultSet = m_dataSource.getResultSet(p_context);
            m_sourceColumns = m_resultSet.getColumnInfo();
            m_enumeration = m_resultSet.getRows();
        }

        /**
         * Tests if this enumeration contains more elements.
         *
         * @return <code>true</code> if and only if this enumeration object
         * contains at least one more element to provide;
         * <code>false</code> otherwise.
         */
        @Override
        public boolean hasMoreElements() {

            return m_enumeration.hasMoreElements();
        }

        /**
         * Returns the next element of this enumeration if this enumeration
         * object has at least one more element to provide.
         *
         * @return the next element of this enumeration.
         *
         * @throws java.util.NoSuchElementException if no more elements exist.
         */
        @Override
        public IDataRow nextElement() {

            return transformRow(m_executionContext, m_enumeration.nextElement());
        }

        /**
         * Transform the specified row, using the expression list
         *
         * @param p_row the row to transform
         *
         * @return the transformed row
         */
        private IDataRow transformRow(IExecutionContext p_context, final IDataRow p_row) {

            final Object[] rowData = p_row.getData();
            final Object[] data = new Object[m_columnInfo.length];

            p_context.reset();

            for (int i = 0; i < rowData.length; i++) {

                data[i] = rowData[i];
                p_context.setValue(m_sourceColumns[i].getContext(), m_sourceColumns[i].getName(), data[i]);
            }

            int i = 0;
            for (IExpression expression : m_expressions) {
                IColumnInfo info = m_columnInfo[i];

                data[i] = expression.getValue(p_context);

                p_context.setValue(info.getContext(), info.getName(), data[i]);
                i++;
            }

            return new DataRow(m_columnInfo, data);

        }
    }
}
