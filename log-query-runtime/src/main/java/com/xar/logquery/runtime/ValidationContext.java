/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime;

import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.exceptions.DuplicateColumnNameException;
import com.xar.logquery.runtime.interfaces.IValidationContext;

import java.util.List;

/**
 * Created by akoekemo on 2/23/15.
 */
public class ValidationContext implements IValidationContext {

    /**
     * The parent context
     */
    private IValidationContext m_parentContext;

    /**
     * The values in the context
     */
    private ColumnList m_values = new ColumnList();


    public ValidationContext() {

    }

    public ValidationContext(final IValidationContext p_parentContext) {

        m_parentContext = p_parentContext;
    }

    /**
     * Associated the data type with the specified context and name
     *
     * @param p_columnInfo the column
     */
    @Override
    public void addColumn(final IColumnInfo p_columnInfo) throws DuplicateColumnNameException {

        m_values.addColumn(p_columnInfo);
    }

    /**
     * Get the data type of the value with the specified name and alias
     *
     * @param p_context the alias
     * @param p_name    the name
     *
     * @return the data type of the calue, or DataType.UNDEFINED if not found
     */
    @Override
    public IColumnInfo getColumnInfo(final String p_context, final String p_name) {

        IColumnInfo info = m_values.getColumnInfo(p_context, p_name);
        if (info != null) { return info; }
        return m_parentContext == null ? null : m_parentContext.getColumnInfo(p_context, p_name);
    }

    /**
     * Get all the columns in the context
     *
     * @return all the columns in the context
     */
    @Override
    public IColumnInfo[] getColumns() {

        List<IColumnInfo> columns = m_values.getColumns();

        return columns.toArray(new IColumnInfo[columns.size()]);
    }
}
