package com.xar.logquery.runtime.datasource.interfaces;

import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.interfaces.IValidationContext;

/**
 * Interface for a filter factory
 */
public interface IFilterFactory {

    /**
     * Create a filter
     * @return a new filter
     */
    IFilter create();

    void validate(IValidationContext p_context) throws DataSourceException;
}
