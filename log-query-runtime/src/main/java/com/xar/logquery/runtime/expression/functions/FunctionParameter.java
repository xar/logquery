/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.functions;

import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.AbstractExpression;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.expression.interfaces.IFunctionParameter;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;

/**
 * Function parameter
 */
public class FunctionParameter extends AbstractExpression implements IFunctionParameter {

    /**
     * The expression.
     */
    IExpression m_expression;

    /**
     * The is distinct.
     */
    boolean m_isDistinct;

    /**
     * Instantiates a new Function parameter.
     *
     * @param p_expression the expression
     * @param p_isDistinct the is the parameter marked as distinct
     */
    public FunctionParameter(final IExpression p_expression, final boolean p_isDistinct) {

        m_expression = p_expression;
        setParseData(p_expression.getParseData());
        setDistinct(p_isDistinct);
    }

    /**
     * Instantiates a new non distinct Function parameter.
     *
     * @param p_expression the  expression
     */
    public FunctionParameter(final IExpression p_expression) {

        this(p_expression, false);
    }

    /**
     * Get the child expressions of this expression
     *
     * @return the child expressions of this expression
     */
    @Override
    public IExpression[] getChildren() {

        return new IExpression[]{m_expression};
    }

    /**
     * Get the datatype of the expression
     *
     * @return the datatype of the expression
     */
    public DataType getDataType() {

        return m_expression.getDataType();
    }

    /**
     * Get the value of the expression, based on the current row context
     *
     * @param p_context the current row context
     *
     * @return the value of the expression.
     */
    public Object getValue(final IExecutionContext p_context) {

        return m_expression.getValue(p_context);
    }

    /**
     * Returns whether the parameter is noted as distinct
     *
     * @return true if the parameter is distinct, false otherwise
     */
    @Override
    public boolean isDistinct() {

        return m_isDistinct;
    }

    /**
     * Sets whether the parameter is noted as distinct
     *
     * @param p_isDistinct true if the parameter is distinct, false otherwise
     */
    @Override
    public void setDistinct(final boolean p_isDistinct) {

        m_isDistinct = p_isDistinct;
    }

    /**
     * Set the expression
     *
     * @param p_expression the expression
     */
    public void setExpression(final IExpression p_expression) {

        m_expression = p_expression;
    }

    /**
     * Validates the expression against the specified context
     *
     * @param p_context the context to validate the expression against
     */
    @Override
    public void validateExpression(final IValidationContext p_context) throws InvalidExpressionException {
        // Nothing to do here
    }
}
