/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.functions.scalar.convert;

import com.xar.logquery.runtime.annotations.Function;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.AbstractExpression;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.expression.exceptions.UnconvertableValueException;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.expression.interfaces.IFunction;
import com.xar.logquery.runtime.expression.interfaces.IFunctionParameter;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;
import com.xar.logquery.runtime.util.ConvertFunctions;
import com.xar.logquery.runtime.util.RuntimeFunctions;

/**
 * Function to convert a value to an integer
 */
@Function(
        names = {"TO_INTEGER", "TO_INT"}
)
public class ToIntFunction extends AbstractExpression implements IFunction {


    private IFunctionParameter[] m_functionParameters;

    public ToIntFunction() {

    }

    /**
     * Get the child expressions of this expression
     *
     * @return the child expressions of this expression
     */
    @Override
    public IExpression[] getChildren() {

        return m_functionParameters;
    }

    /**
     * Get the datatype of the expression
     *
     * @return the datatype of the expression
     */
    @Override
    public DataType getDataType() {

        return DataType.INTEGER;
    }

    /**
     * Get the value of the expression, based on the current row context
     *
     * @param p_context the current row context
     *
     * @return the value of the expression.
     */
    @Override
    public Object getValue(final IExecutionContext p_context) {

        try {
            return ConvertFunctions.toInteger(m_functionParameters[0].getValue(p_context));
        }
        catch (UnconvertableValueException ignored) {
        }
        return null;
    }

    /**
     * Set the parameters of the function
     *
     * @param p_functionParameters the parameters
     */
    @Override
    public void setParameters(final IFunctionParameter... p_functionParameters) {

        m_functionParameters = p_functionParameters;
    }

    /**
     * Validates the expression against the specified context
     *
     * @param p_context the context to validate the expression against
     */
    @Override
    public void validateExpression(final IValidationContext p_context) throws InvalidExpressionException {

        if (m_functionParameters.length != 1) {
            throw new InvalidExpressionException(getParseData(), RuntimeFunctions.getFunctionName(this.getClass()) + " requires one parameter");
        }

    }
}
