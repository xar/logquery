/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource.interfaces;


import com.xar.logquery.runtime.exceptions.AmbiguousColumnNameException;
import com.xar.logquery.runtime.exceptions.ColumnNotFoundException;

/**
 * Minimum interface requirement for a data row
 */
public interface IDataRow {

    /**
     * Get the value of the column at the specified index
     *
     * @param p_index the index of the column
     *
     * @return the value of the column at the specified index
     */
    Object get(int p_index);

    /**
     * Get the value of the column with the specified name
     *
     * @param p_columnName the name of the column
     *
     * @return the value of the column with the specified name
     *
     * @throws ColumnNotFoundException                                          if the column name does not exist
     * @throws com.xar.logquery.runtime.exceptions.AmbiguousColumnNameException if the column name is ambiguous (the same name is found in different contexts)
     */
    Object get(String p_columnName) throws ColumnNotFoundException, AmbiguousColumnNameException;

    /**
     * Get the value of the column with the specified name
     *
     * @param p_context    the column context
     * @param p_columnName the name of the column
     *
     * @return the value of the column with the specified name
     *
     * @throws ColumnNotFoundException if the column name does not exist
     */
    Object get(String p_context, String p_columnName) throws ColumnNotFoundException, AmbiguousColumnNameException;

    /**
     * Get the column info associated with teh row
     *
     * @return the column info associated with the row
     */
    public IColumnInfo[] getColumnInfo();

    /**
     * Get the data associated with the row
     *
     * @return the data associated with the row
     */
    public Object[] getData();

}
