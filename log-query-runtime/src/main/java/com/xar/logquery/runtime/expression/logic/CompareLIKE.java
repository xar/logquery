/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.logic;

import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.AbstractExpression;
import com.xar.logquery.runtime.expression.constant.ConstantExpression;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;
import com.xar.logquery.runtime.util.RuntimeFunctions;

/**
 * Matches text to a pattern.
 */
public class CompareLIKE extends AbstractExpression {


    String regex;

    boolean m_dynamic;

    private IExpression m_value;

    private IExpression m_pattern;

    private IExpression m_escape;

    /**
     * Initialize a new instance of CompareLIKE
     *
     * @param p_value   the value expression
     * @param p_pattern the pattern expression
     * @param p_escape  the escape expression
     */
    public CompareLIKE(final IExpression p_value, final IExpression p_pattern, final IExpression p_escape) {

        m_value = p_value;
        m_pattern = p_pattern;
        m_escape = p_escape;

        m_dynamic = !(m_pattern instanceof ConstantExpression && m_escape instanceof ConstantExpression);

    }

    /**
     * Get the child expressions of this expression
     *
     * @return the child expressions of this expression
     */
    @Override
    public IExpression[] getChildren() {

        return new IExpression[]{m_escape, m_pattern, m_value};
    }

    /**
     * Get the datatype of the expression
     *
     * @return the datatype of the expression
     */
    @Override
    public DataType getDataType() {

        return DataType.BOOLEAN;
    }

    /**
     * Get the valueExpression of the expression, based on the current row context.
     *
     * @param p_context the current row context
     *
     * @return the valueExpression of the expression.
     */
    @Override
    public Object getValue(final IExecutionContext p_context) {

        String value = (String) m_value.getValue(p_context);

        if (value == null) { return false; }

        if (m_dynamic) {
            regex = RuntimeFunctions.like2Regex((String) m_pattern.getValue(p_context), (String) m_escape.getValue(p_context));
        }
        else if (regex == null) {
            regex = RuntimeFunctions.like2Regex((String) m_pattern.getValue(p_context), (String) m_escape.getValue(p_context));
        }

        return value.matches(regex);
    }

    /**
     * Validates the expression against the specified context
     *
     * @param p_context the context to validate the expression against
     */
    @Override
    public void validateExpression(final IValidationContext p_context) throws InvalidExpressionException {

        checkType("value", m_value, DataType.STRING);
        checkType("pattern", m_pattern, DataType.STRING);
        checkType("escape", m_escape, DataType.STRING);
    }

    /**
     * Check the typeof the expression
     *
     * @param p_expression the expressionto check
     * @param p_dataType   the data type expected
     */
    private static void checkType(final String argumentName, final IExpression p_expression, final DataType p_dataType) throws InvalidExpressionException {

        if (p_expression.getDataType() != p_dataType) {
            throw new InvalidExpressionException(p_expression.getParseData(), "Expected " + argumentName + " expression of type " + p_dataType);
        }

    }


}
