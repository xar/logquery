/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.config;

import com.xar.logquery.runtime.config.entities.DateFormat;
import com.xar.logquery.runtime.datasource.DualDataSource;
import com.xar.logquery.runtime.datasource.csv.CSVDataSource;
import com.xar.logquery.runtime.datasource.text.TextLineDataSource;
import com.xar.logquery.runtime.expression.functions.aggregate.AverageFunction;
import com.xar.logquery.runtime.expression.functions.aggregate.CountFunction;
import com.xar.logquery.runtime.expression.functions.aggregate.FirstFunction;
import com.xar.logquery.runtime.expression.functions.aggregate.LastFunction;
import com.xar.logquery.runtime.expression.functions.aggregate.MaxFunction;
import com.xar.logquery.runtime.expression.functions.aggregate.MinFunction;
import com.xar.logquery.runtime.expression.functions.aggregate.SumFunction;
import com.xar.logquery.runtime.expression.functions.scalar.DecodeFunction;
import com.xar.logquery.runtime.expression.functions.scalar.IIFFunction;
import com.xar.logquery.runtime.expression.functions.scalar.ISNULLFunction;
import com.xar.logquery.runtime.expression.functions.scalar.convert.ToDateFunction;
import com.xar.logquery.runtime.expression.functions.scalar.convert.ToIntFunction;
import com.xar.logquery.runtime.expression.functions.scalar.convert.ToRealFunction;
import com.xar.logquery.runtime.expression.functions.scalar.convert.ToStringFunction;
import com.xar.logquery.runtime.expression.functions.scalar.convert.ToTimestampFunction;
import com.xar.logquery.runtime.expression.functions.scalar.date.DayFunction;
import com.xar.logquery.runtime.expression.functions.scalar.date.MonthFunction;
import com.xar.logquery.runtime.expression.functions.scalar.date.YearFunction;
import com.xar.logquery.runtime.expression.functions.scalar.string.ExtractPrefixFunction;
import com.xar.logquery.runtime.expression.functions.scalar.string.ExtractSuffixFunction;
import com.xar.logquery.runtime.expression.functions.scalar.string.LengthFunction;
import com.xar.logquery.runtime.expression.functions.scalar.string.StrTokenFunction;
import com.xar.logquery.runtime.expression.functions.scalar.string.SubstringFunction;
import com.xar.logquery.runtime.expression.functions.scalar.string.ToLowerFunction;
import com.xar.logquery.runtime.expression.functions.scalar.string.ToUpperFunction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Run time Configuration
 */
public class RuntimeConfiguration {

    private static RuntimeConfiguration _instance = new RuntimeConfiguration();

    /**
     * Default date format used when formatting dates for output
     */
    private static String s_dateFormat = "yyyy-MM-dd";

    /**
     * Default format used for formatting timestamps for output
     */
    private static String s_timestampFormat = "yyyy-MM-dd HH:mm:ss.SSS";

    /**
     * Date formats
     */
    private static List<DateFormat> s_dateFormats = new ArrayList<>();

    /**
     * The data sources for file extensions
     */
    private static Map<String, String> s_fileTypes = new HashMap<>();

    /**
     * Data sources
     */
    private static Map<String, Class<?>> s_dataSources = new HashMap<>();

    /**
     * Null text
     */
    private static String nullText = "<NULL>";

    /**
     * Supported functions
     */
    private static Map<String, Class<?>> s_functions = new HashMap<>();

    static {
        registerDateFormat("\\d{2,4}\\-\\d{1,2}\\-\\d{1,2}", "yyyy-M-d");
        registerDateFormat("\\d{4}\\-\\d{2}\\-\\d{2}", "yyyy-MM-dd");
        registerDateFormat("\\d{2,4}\\-\\d{1,2}\\-\\d{1,2} \\d{1,2}:\\d{1,2}", "yyyy-MM-dd H:m");
        registerDateFormat("\\d{2,4}\\-\\d{1,2}\\-\\d{1,2} \\d{1,2}:\\d{1,2}:\\d{1,2}", "yyyy-MM-dd H:m:s");
        registerDateFormat("\\d{2,4}\\-\\d{1,2}\\-\\d{1,2} \\d{1,2}:\\d{1,2}:\\d{1,2}\\.\\d{1,3}", "yyyy-MM-dd H:m:s.SSS");

        addDatasource("CSV", CSVDataSource.class, "csv");
        addDatasource("TEXTLINE", TextLineDataSource.class, "txt");
        addDatasource("DUAL", DualDataSource.class);
        addDatasource("DUMMY", DualDataSource.class);

        addFunction("IIF", IIFFunction.class);
        addFunction("DECODE", DecodeFunction.class);
        addFunction("ISNULL", ISNULLFunction.class);
        addFunction("NVL", ISNULLFunction.class);

        addFunction("AVG", AverageFunction.class);
        addFunction("EXTRACT_PREFIX", ExtractPrefixFunction.class);
        addFunction("EXTRACT_SUFFIX", ExtractSuffixFunction.class);
        addFunction("EXTRACT_TOKEN", StrTokenFunction.class);
        addFunction("COUNT", CountFunction.class);
        addFunction("FIRST", FirstFunction.class);
        addFunction("LAST", LastFunction.class);
        addFunction("LCASE", ToLowerFunction.class);
        addFunction("LEN", LengthFunction.class);
        addFunction("LENGTH", LengthFunction.class);
        addFunction("MAX", MaxFunction.class);
        addFunction("MIN", MinFunction.class);
        addFunction("PARSE_DATE", ToDateFunction.class);
        addFunction("PARSE_TIMESTAMP", ToTimestampFunction.class);
        addFunction("STRTOK", StrTokenFunction.class);
        addFunction("SUBSTRING", SubstringFunction.class);
        addFunction("SUM", SumFunction.class);
        addFunction("TO_DATE", ToDateFunction.class);
        addFunction("TO_REAL", ToRealFunction.class);
        addFunction("TO_DOUBLE", ToRealFunction.class);
        addFunction("TO_INT", ToIntFunction.class);
        addFunction("TO_INTEGER", ToIntFunction.class);
        addFunction("TO_LOWER", ToLowerFunction.class);
        addFunction("TO_STRING", ToStringFunction.class);
        addFunction("TO_TIMESTAMP", ToTimestampFunction.class);
        addFunction("TO_UPPER", ToUpperFunction.class);
        addFunction("UCASE", ToUpperFunction.class);

        addFunction("YEAR", YearFunction.class);
        addFunction("MONTH", MonthFunction.class);
        addFunction("DAY", DayFunction.class);


    }

    private RuntimeConfiguration() {

    }

    /**
     * Adds a datasource to the context
     *
     * @param p_name     the name of the data source
     * @param p_function the class of the data source
     */
    public static void addDatasource(final String p_name, final Class<?> p_function, String... p_extension) {

        s_dataSources.put(p_name.toLowerCase(), p_function);

        if (p_extension != null) {
            for (int i = 0; i < p_extension.length; i++) {
                registerExtension(p_extension[i], p_name.toLowerCase());
            }
        }
    }

    /**
     * Adds a function to the context
     *
     * @param p_name     the name of the function
     * @param p_function the class of the function
     */
    public static void addFunction(final String p_name, final Class<?> p_function) {

        s_functions.put(p_name.toLowerCase(), p_function);
    }

    /**
     * Get the data source with the specified name
     *
     * @param p_name the name of the data source
     *
     * @return the data source with the specified name
     */
    public static Class<?> getDatasource(final String p_name) {

        return s_dataSources.get(p_name.toLowerCase());
    }

    /**
     * Get the datasource for the file extension
     *
     * @param p_ext the extension
     *
     * @return the datasource for the extension, or null if not found
     */
    public static String getDatasourceForExtension(final String p_ext) {

        return s_fileTypes.get(p_ext);
    }

    public static String getDateFormat() {

        return s_dateFormat;
    }

    public static void setDateFormat(final String p_dateFormat) {

        s_dateFormat = p_dateFormat;
    }

    /**
     * List of date formats
     *
     * @return list of supported date formats
     */
    public static List<DateFormat> getDateFormats() {

        return Collections.unmodifiableList(s_dateFormats);
    }

    public static void setDateFormats(final List<DateFormat> p_dateFormats) {

        s_dateFormats = new ArrayList<>(p_dateFormats);
    }

    /**
     * Get the function with the specified name
     *
     * @param p_name the name of the function
     *
     * @return the function with the specified name
     */
    public static Class<?> getFunction(final String p_name) {

        return s_functions.get(p_name.toLowerCase());
    }

    public static String getNullText() {

        return nullText;
    }

    public static void setNullText(final String p_nullText) {

        nullText = p_nullText;
    }

    public static String getTimestampFormat() {

        return s_timestampFormat;
    }

    public static void setTimestampFormat(final String p_timestampFormat) {

        s_timestampFormat = p_timestampFormat;
    }

    /**
     * Register a new date format
     *
     * @param p_match  a regex to match against a date string
     * @param p_format the format string
     */
    public static void registerDateFormat(String p_match, String p_format) {

        registerDateFormat(new DateFormat(p_match, p_format));
    }

    /**
     * Register a new date format
     *
     * @param p_dateFormat the dateformat to register
     */
    public static void registerDateFormat(final DateFormat p_dateFormat) {

        for (int i = 0; i < s_dateFormats.size(); i++) {
            DateFormat dateFormat = s_dateFormats.get(i);
            if (p_dateFormat.getPattern().equals(dateFormat.getPattern())) {
                s_dateFormats.set(i, p_dateFormat);
                return;
            }
        }
        s_dateFormats.add(p_dateFormat);
    }

    public static void registerExtension(final String p_extension, final String p_dataSourceName) {

        s_fileTypes.put(p_extension, p_dataSourceName.toLowerCase());
    }

    public static void unregisterDateFormat(String p_pattern) {

        s_dataSources.remove(p_pattern);
    }
}
