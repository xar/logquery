/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource.interfaces;

import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.parse.ParseData;

/**
 * Minimum interface requirements for column information
 */
public interface IColumnInfo {

    /**
     * Get the column context (alias of the datasource) where the column originated
     *
     * @return the column context or alias of the datasource
     */
    public String getContext();

    /**
     * Get the data type of the column
     *
     * @return the data type of the column
     */
    public DataType getDataType();

    /**
     * Get the name of the column
     *
     * @return the name of the column
     */
    public String getName();

}
