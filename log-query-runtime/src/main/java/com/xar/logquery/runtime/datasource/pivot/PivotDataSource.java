/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource.pivot;

import com.xar.logquery.runtime.ColumnInfo;
import com.xar.logquery.runtime.DataRow;
import com.xar.logquery.runtime.ValidationContext;
import com.xar.logquery.runtime.datasource.AbstractDataSource;
import com.xar.logquery.runtime.datasource.IteratorEnumeration;
import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.exceptions.DuplicateColumnNameException;
import com.xar.logquery.runtime.expression.NamedExpression;
import com.xar.logquery.runtime.expression.WildcardExpression;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.expression.functions.aggregate.interfaces.IAggregateExpression;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.interfaces.IAliasable;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;
import com.xar.logquery.runtime.parse.ParseData;
import com.xar.logquery.runtime.util.ParseFunctions;
import com.xar.logquery.runtime.util.RuntimeFunctions;
import com.xar.logquery.runtime.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Pivot datasource. Combines a group by with a select
 */
public class PivotDataSource extends AbstractDataSource {

    private final ParseFunctions.ReferenceInfo m_pivotReferenceInfo;

    private final ParseFunctions.ReferenceInfo m_headerReferenceInfo;

    private final IDataSource m_dataSource;

    private final IExpression[] m_rowHeaders;

    private final IExpression m_columnHeaders;

    private final IExpression m_aggregate;

    private int m_pivotPoint;

    private SortOrder m_order = SortOrder.NONE;

    /**
     * Initialises a new PivotDataSource
     *
     * @param p_dataSource    the inner datasource
     * @param p_rowHeaders    row headers
     * @param p_columnHeaders expression used to obtain column headers
     * @param p_aggregate     the aggregation expression
     * @param p_pivotPoint
     * @param p_order
     */
    public PivotDataSource(final IDataSource p_dataSource, final IExpression[] p_rowHeaders, final IExpression p_columnHeaders, final IExpression p_aggregate, final int p_pivotPoint, final SortOrder p_order) {

        m_dataSource = p_dataSource;
        m_rowHeaders = p_rowHeaders;
        m_columnHeaders = p_columnHeaders;
        m_aggregate = p_aggregate;
        m_order = p_order;


        m_pivotReferenceInfo = ParseFunctions.getReferenceInfo(m_aggregate);
        m_headerReferenceInfo = ParseFunctions.getReferenceInfo(m_rowHeaders);
        m_pivotPoint = Math.max(0, Math.min(m_rowHeaders.length + 1, p_pivotPoint) - 1);

    }

    /**
     * Execute the query and returns a resultset
     *
     * @param p_context the execution context
     *
     * @return the resultset
     *
     * @throws com.xar.logquery.runtime.exceptions.DataSourceException if the execution fails
     */
    @Override
    protected IResultSet executeQuery(final IExecutionContext p_context) throws DataSourceException {

        return new PivotResultSet(p_context);
    }

    /**
     * Validate the datasource
     *
     * @param p_context the binding context
     *
     * @throws com.xar.logquery.runtime.exceptions.DataSourceException if the datasource is invalid
     */
    @Override
    protected void validateQuery(final IValidationContext p_context) throws DataSourceException {

        IValidationContext context = new ValidationContext(p_context);
        m_dataSource.validate(context);

        for (int i = 0; i < m_rowHeaders.length; i++) {
            m_rowHeaders[i].validate(context);
            if (m_rowHeaders[i] instanceof WildcardExpression) {
                throw new DataSourceException(m_rowHeaders[i].getParseData(), "Wildcards are not allowed in pivot");
            }

        }
        m_columnHeaders.validate(context);

        if (m_columnHeaders.getDataType() != DataType.STRING) {
            throw new InvalidExpressionException(m_columnHeaders.getParseData(), "Column headers in PIVOT must be string");
        }

        m_aggregate.validate(context);

        for (IColumnInfo column : context.getColumns()) {
            p_context.addColumn(column);
        }


    }

    /**
     * Get the alias for the object
     *
     * @return the alias for the object
     */
    private String getAlias() {

        return m_dataSource instanceof IAliasable ? ((IAliasable) m_dataSource).getAlias() : "";
    }

    public static enum SortOrder {
        NONE,
        ASC,
        DESC
    }

    private class NamedPointer {

        private String name;

        private int index;

        public NamedPointer(final String p_name, final int p_index) {

            name = p_name;
            index = p_index;
        }
    }

    private class NamedPointerComparator implements Comparator<NamedPointer> {

        /**
         * The order modifier
         */
        int m_orderModifier = 1;

        public NamedPointerComparator(final int p_m_orderModifier) {

            m_orderModifier = p_m_orderModifier;
        }

        /**
         * Compares its two arguments for order.  Returns a negative integer,
         * zero, or a positive integer as the first argument is less than, equal
         * to, or greater than the second.<p>
         * <p/>
         * In the foregoing description, the notation
         * <tt>sgn(</tt><i>expression</i><tt>)</tt> designates the mathematical
         * <i>signum</i> function, which is defined to return one of <tt>-1</tt>,
         * <tt>0</tt>, or <tt>1</tt> according to whether the value of
         * <i>expression</i> is negative, zero or positive.<p>
         * <p/>
         * The implementor must ensure that <tt>sgn(compare(x, y)) ==
         * -sgn(compare(y, x))</tt> for all <tt>x</tt> and <tt>y</tt>.  (This
         * implies that <tt>compare(x, y)</tt> must throw an exception if and only
         * if <tt>compare(y, x)</tt> throws an exception.)<p>
         * <p/>
         * The implementor must also ensure that the relation is transitive:
         * <tt>((compare(x, y)&gt;0) &amp;&amp; (compare(y, z)&gt;0))</tt> implies
         * <tt>compare(x, z)&gt;0</tt>.<p>
         * <p/>
         * Finally, the implementor must ensure that <tt>compare(x, y)==0</tt>
         * implies that <tt>sgn(compare(x, z))==sgn(compare(y, z))</tt> for all
         * <tt>z</tt>.<p>
         * <p/>
         * It is generally the case, but <i>not</i> strictly required that
         * <tt>(compare(x, y)==0) == (x.equals(y))</tt>.  Generally speaking,
         * any comparator that violates this condition should clearly indicate
         * this fact.  The recommended language is "Note: this comparator
         * imposes orderings that are inconsistent with equals."
         *
         * @param o1 the first object to be compared.
         * @param o2 the second object to be compared.
         *
         * @return a negative integer, zero, or a positive integer as the
         * first argument is less than, equal to, or greater than the
         * second.
         *
         * @throws NullPointerException if an argument is null and this
         *                              comparator does not permit null arguments
         * @throws ClassCastException   if the arguments' types prevent them from
         *                              being compared by this comparator.
         */
        @Override
        public int compare(final NamedPointer o1, final NamedPointer o2) {

            return o1.name.compareTo(o2.name) * m_orderModifier;
        }
    }

    /**
     * Enumeration used to build row data from an enumeration of pivot rows.
     */
    private class PivotEnumeration implements Enumeration<IDataRow> {

        private IColumnInfo[] m_columnInfo;

        private NamedPointer[] m_pivotedHeaders;

        private IteratorEnumeration<PivotRow> m_enumeration;

        private IExecutionContext m_executionContext;

        public PivotEnumeration(final IExecutionContext p_executionContext, final IColumnInfo[] p_columnInfo, final NamedPointer[] p_pivotedHeaders, IteratorEnumeration<PivotRow> p_enumeration) {

            m_executionContext = p_executionContext;
            m_columnInfo = p_columnInfo;
            m_pivotedHeaders = p_pivotedHeaders;
            m_enumeration = p_enumeration;
        }

        /**
         * Tests if this enumeration contains more elements.
         *
         * @return <code>true</code> if and only if this enumeration object
         * contains at least one more element to provide;
         * <code>false</code> otherwise.
         */
        @Override
        public boolean hasMoreElements() {

            return m_enumeration.hasMoreElements();
        }

        /**
         * Returns the next element of this enumeration if this enumeration
         * object has at least one more element to provide.
         *
         * @return the next element of this enumeration.
         *
         * @throws java.util.NoSuchElementException if no more elements exist.
         */
        @Override
        public IDataRow nextElement() {

            PivotRow row = m_enumeration.nextElement();
            Object[] data = new Object[m_columnInfo.length];

            int headingCount = m_rowHeaders.length;
            IColumnInfo[] headerColumnInfo = m_headerReferenceInfo.getColumnInfo();

            for (int i = 0; i < row.rowHeading.length; i++) {
                m_executionContext.setValue(headerColumnInfo[i].getContext(), headerColumnInfo[i].getName(), row.rowHeading[i]);
            }
            IAggregateExpression[] aggregates = m_headerReferenceInfo.getAggregateExpressions();
            for (int i = 0; i < aggregates.length; i++) {
                IAggregateExpression aggregate = aggregates[i];
                m_executionContext.setValue(aggregate.getClass().getName(), aggregate.getKey(), row.headingAggregates[i]);
            }

            for (int i = 0; i < headingCount; i++) {
                if (i < m_pivotPoint) { data[i] = m_rowHeaders[i].getValue(m_executionContext); }
                else {
                    data[i + row.pivotedColumns.size()] = m_rowHeaders[i].getValue(m_executionContext);
                }
            }

            aggregates = m_pivotReferenceInfo.getAggregateExpressions();
            for (int i = 0; i < m_columnInfo.length - headingCount; i++) {

                Object[] pivotAggregates = row.pivotedColumns.get(m_pivotedHeaders[i].index);
                for (int j = 0; j < aggregates.length; j++) {
                    IAggregateExpression aggregate = aggregates[j];
                    m_executionContext.setValue(aggregate.getClass().getName(), aggregate.getKey(), pivotAggregates[j]);
                }
                data[i + m_pivotPoint] = m_aggregate.getValue(m_executionContext);
            }
            return new DataRow(m_columnInfo, data);
        }
    }

    /**
     * Resultset implemented for the pivot datasource
     */
    private class PivotResultSet implements IResultSet {

        /**
         * The pivot data
         */
        private Map<String, PivotRow> m_pivotData = new LinkedHashMap<>();

        /**
         * Column information
         */
        private IColumnInfo[] m_columnInfo = new IColumnInfo[0];

        /**
         * List of headers as they are identified
         */
        private List<NamedPointer> m_pivotedHeaders = new ArrayList<>();

        private IExecutionContext m_context;

        public PivotResultSet(IExecutionContext p_context) throws DataSourceException {

            m_context = p_context;

            // Yes, this is ugly and we should not be doing this amount of
            // heavy lifting in a constructor. It is what it is, however,
            // so deal with it.
            doPivot(p_context);
        }

        /**
         * Get the column info from the resultset.
         *
         * @return an array that describes the columns of the resultset
         */
        @Override
        public IColumnInfo[] getColumnInfo() {

            return m_columnInfo;
        }


        /**
         * Get an enumeration of data rows in the resultset
         *
         * @return
         *
         * @throws com.xar.logquery.runtime.exceptions.DataSourceException
         */
        @Override
        public Enumeration<IDataRow> getRows() throws DataSourceException {

            return new PivotEnumeration(m_context, m_columnInfo, m_pivotedHeaders.toArray(new NamedPointer[m_pivotedHeaders.size()]), new IteratorEnumeration<>(m_pivotData.values().iterator()));
        }

        /**
         * Aggregates the row
         *
         * @param p_context             the execution context
         * @param p_columnInfo
         * @param p_headerReferenceInfo
         * @param p_dataRow             the row to aggregate
         */
        private void aggregateRow(final IExecutionContext p_context, final List<IColumnInfo> p_columnInfo, final ParseFunctions.ReferenceInfo p_headerReferenceInfo, final IDataRow p_dataRow) {

            RuntimeFunctions.applyRowToContext(p_context, p_dataRow);

            PivotRow row = getOrCreateRow(p_context, p_headerReferenceInfo);

            String heading = String.valueOf(m_columnHeaders.getValue(p_context));

            int idx = getOrCreateHeaderIndex(heading, p_columnInfo);

            Object[] aggregates = row.pivotedColumns.get(idx);
            IAggregateExpression[] aggregateExpressions = m_pivotReferenceInfo.getAggregateExpressions();

            for (int i = 0; i < aggregates.length; i++) {
                aggregates[i] = aggregateExpressions[i].aggregate(aggregates[i], p_context);
            }

            aggregates = row.headingAggregates;
            aggregateExpressions  = m_headerReferenceInfo.getAggregateExpressions();
            for (int i = 0; i < aggregates.length; i++) {
                aggregates[i] = aggregateExpressions[i].aggregate(aggregates[i], p_context);
            }

        }

        /**
         * Calculates a grouping key from the current row, and add the values for the key data to the supplied object array
         * the passed keydata array MUST be the same length as the row headers
         *
         * @param p_context   the execution context
         * @param p_rowHeader
         * @param p_keyData   the keydata array to put the values into
         *
         * @return a string representation of the row key
         */
        private String calculateKey(final IExecutionContext p_context, final IColumnInfo[] p_rowHeader, final Object[] p_keyData) {

            StringBuilder buffer = new StringBuilder();

            for (int i = 0; i < p_rowHeader.length; i++) {
                p_keyData[i] = p_context.getValue(p_rowHeader[i].getContext(), p_rowHeader[i].getName());
                buffer.append("|").append(p_keyData[i]);
            }

            return buffer.toString();
        }

        /**
         * Pivots the data in the datasource
         *
         * @param p_context the execution context
         *
         * @throws DataSourceException
         */
        private void doPivot(final IExecutionContext p_context) throws DataSourceException {

            IResultSet resultSet = m_dataSource.getResultSet(p_context);

            List<IColumnInfo> columnInfo = new ArrayList<>();

            Enumeration<IDataRow> rows = resultSet.getRows();
            while (rows.hasMoreElements()) {
                aggregateRow(p_context, columnInfo, m_headerReferenceInfo, rows.nextElement());
            }

            // We should now order the pivoted headers, after that we can add the column info
            // by using the index element
            if (m_order == SortOrder.ASC) {
                Collections.sort(m_pivotedHeaders, new NamedPointerComparator(1));
            }
            else if (m_order == SortOrder.DESC) {
                Collections.sort(m_pivotedHeaders, new NamedPointerComparator(-1));
            }
            Set<String> names = new HashSet<>();
            for (IColumnInfo info : columnInfo) {
                names.add(info.getName());
            }

            int headerSize = m_pivotedHeaders.size();
            m_columnInfo = new IColumnInfo[m_rowHeaders.length + headerSize];
            for (int i = 0; i < headerSize; i++) {
                NamedPointer pointer = m_pivotedHeaders.get(i);
                m_columnInfo[m_pivotPoint + i] = columnInfo.get(pointer.index);
            }
            columnInfo.clear();

            // First we need to make all the expressions in the list are in fact
            // Named expressions, and we add the aliased named expression first, as
            // explicitly named column names are fixed. We can synthesize names for
            // unnamed columns, but named columns are absolute, and should not clash
            List<NamedExpression> columns = new LinkedList<>();

            final List<IExpression> expressionList = new ArrayList<>(Arrays.asList(m_rowHeaders));

            for (IExpression expression : expressionList) {
                if (expression instanceof NamedExpression) {
                    columns.add((NamedExpression) expression);

                    String alias = ((NamedExpression) expression).getName();

                    if (StringUtils.isEmpty(alias)) { continue; }

                    if (names.add(alias)) { continue; }

                    throw new DuplicateColumnNameException(expression.getParseData(), "Duplicate column name - " + alias);
                }
                else {
                    columns.add(new NamedExpression(expression, ""));
                }
            }

            // Now we add the non aliased columns, for them we can make up new names
            // we add this second because the assigned aliases take presidence
            for (NamedExpression expression : columns) {

                String alias = expression.getName();

                if (!StringUtils.isEmpty(alias)) { continue;}

                final ParseData parseData = expression.getParseData();
                if (parseData != null) {
                    alias = parseData.getText();
                }

                if (!names.add(alias)) {
                    String prefix = alias;
                    int postFix = 1;
                    while (!names.add(alias)) {
                        alias = prefix + "_" + postFix;
                        postFix++;
                    }
                }

                expression.setName(alias);
            }

            for (int i = 0; i < columns.size(); i++) {
                NamedExpression expression = columns.get(i);

                if (i < m_pivotPoint) {
                    m_columnInfo[i] = new ColumnInfo(getAlias(), expression.getName(), expression.getDataType());
                }
                else {
                    m_columnInfo[headerSize + i] = new ColumnInfo(getAlias(), expression.getName(), expression.getDataType());
                }
            }
        }

        /**
         * Finds the index of the heading if it exists, or adds the heading and returns the new index
         *
         * @param p_heading    the heading to find/add
         * @param p_columnInfo
         *
         * @return the index of the heading if it exists, or adds the heading and returns the new index
         */
        private int getOrCreateHeaderIndex(final String p_heading, final List<IColumnInfo> p_columnInfo) {

            int size = m_pivotedHeaders.size();

            for (NamedPointer heading : m_pivotedHeaders) {
                if (heading.name.equals(p_heading)) { return heading.index; }
            }

            p_columnInfo.add(new ColumnInfo(getAlias(), p_heading, /*m_columnHeaders.getDataType()*/m_aggregate.getDataType()));

            m_pivotedHeaders.add(new NamedPointer(p_heading, m_pivotedHeaders.size()));

            for (PivotRow row : m_pivotData.values()) { row.addPivot(m_pivotedHeaders.size()); }

            return size;
        }

        /**
         * Finds the row that matches the key for the current datarow, if not found one is created
         *
         * @param p_context       the execution context
         * @param p_referenceInfo
         *
         * @return the row that matches the key for the current datarow
         */
        private PivotRow getOrCreateRow(final IExecutionContext p_context, final ParseFunctions.ReferenceInfo p_referenceInfo) {

            IColumnInfo[] columnInfo = p_referenceInfo.getColumnInfo();

            Object[] keyData = new Object[columnInfo.length];
            String key = calculateKey(p_context, columnInfo, keyData);

            PivotRow row = m_pivotData.get(key);

            if (row == null) {
                row = new PivotRow(keyData, p_referenceInfo.getAggregateExpressions(), m_pivotedHeaders.size(), m_pivotReferenceInfo.getAggregateExpressions());
                m_pivotData.put(key, row);
            }
            return row;
        }
    }

}
