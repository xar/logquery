/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.interfaces;

/**
 * Minimum interface requirement for an execution context
 */
public interface IExecutionContext {

    /**
     * Get the value of the object associated with the specified context and name
     *
     * @param p_context the context
     * @param p_name    the name
     *
     * @return the value of the object associated with the specified context and name
     */
    Object getValue(String p_context, String p_name);

    /**
     * Reset the context
     */
    void reset();

    /**
     * Associated the value of an object with the specified context and name
     *
     * @param p_context the context
     * @param p_name    the name
     * @param p_value   the value
     */
    void setValue(String p_context, String p_name, Object p_value);

}
