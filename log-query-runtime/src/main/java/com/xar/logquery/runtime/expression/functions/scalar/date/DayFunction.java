/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.functions.scalar.date;

import com.xar.logquery.runtime.annotations.Function;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.functions.AbstractFunction;
import com.xar.logquery.runtime.util.DateTimeFunctions;
import org.joda.time.LocalDate;

import java.text.ParseException;
import java.util.Date;

/**
 * Created by Anton Koekemoer on 2014/12/07.
 */
@Function(
        names = {"DAY"}
)
public class DayFunction extends AbstractFunction {

    /**
     * Instantiates a new Day function.
     */
    public DayFunction() {

        super(DataType.INTEGER);

        final IExecuteCallback dateCallback = new IExecuteCallback() {

            @Override
            public Object execute(final Object[] p_parameters) {

                return LocalDate.fromDateFields((Date) p_parameters[0]).dayOfMonth().get();
            }
        };
        addCallback(new DataType[]{DataType.DATE}, dateCallback);
        addCallback(new DataType[]{DataType.TIMESTAMP}, dateCallback);
        addCallback(new DataType[]{DataType.STRING}, new IExecuteCallback() {

            @Override
            public Object execute(final Object[] p_parameters) {

                try {
                    return dateCallback.execute(new Object[]{DateTimeFunctions.parseDate((String) p_parameters[0])});
                }
                catch (ParseException e) {
                }
                return null;
            }
        });
    }
}
