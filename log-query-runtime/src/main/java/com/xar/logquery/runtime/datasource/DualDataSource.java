/*
 * Copyright 2014 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource;

import com.xar.logquery.runtime.AliasableColumnInfo;
import com.xar.logquery.runtime.DataRow;
import com.xar.logquery.runtime.ResultSet;
import com.xar.logquery.runtime.datasource.annotations.DataSourceAttribute;
import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;

import java.util.Enumeration;
import java.util.NoSuchElementException;

/**
 * Created by Anton Koekemoer on 2014/11/21.
 */
public class DualDataSource extends AliasableDataSource {

    private IColumnInfo[] m_columnInfo = new IColumnInfo[]{
            new AliasableColumnInfo(this, "RowNumber", DataType.INTEGER)};

    @DataSourceAttribute(name = "rows", description = "The number of rows the datasource will return", required = false)
    private int rows;

    public DualDataSource(final int p_rows) {

        setRows(p_rows);
    }

    public DualDataSource() {

        this(1);
    }

    /**
     * Open the data source and return a resultset. This method can be called multiple times
     *
     * @param p_context the execution context
     *
     * @return a new resultset
     */
    @Override
    public IResultSet executeQuery(final IExecutionContext p_context) throws DataSourceException {

        return new ResultSet(m_columnInfo, new Enumeration<IDataRow>() {

            int counter = 0;


            /**
             * Tests if this enumeration contains more elements.
             *
             * @return <code>true</code> if and only if this enumeration object
             * contains at least one more element to provide;
             * <code>false</code> otherwise.
             */
            @Override
            public boolean hasMoreElements() {

                return counter < getRows();
            }

            /**
             * Returns the next element of this enumeration if this enumeration
             * object has at least one more element to provide.
             *
             * @return the next element of this enumeration.
             *
             * @throws NoSuchElementException if no more elements exist.
             */
            @Override
            public IDataRow nextElement() {

                if (counter < getRows()) {
                    counter++;
                    return new DataRow(m_columnInfo, new Object[]{counter});
                }
                throw new NoSuchElementException();
            }
        });
    }

    /**
     * Get the number of rows the datasource will return
     *
     * @return the number of rows the datasource will return
     */
    public int getRows() {

        return rows;
    }

    /**
     * Set the number of rows the datasource will return
     *
     * @param p_rows the number of rows the datasource will return
     */
    public void setRows(final int p_rows) {

        rows = p_rows;
    }

    /**
     * Bind the datasource to the execution context
     *
     * @param p_context the binding context
     *
     * @throws com.xar.logquery.runtime.exceptions.DataSourceException if the datasource is invalid
     */
    @Override
    protected void validateQuery(final IValidationContext p_context) throws DataSourceException {

        for (IColumnInfo info : m_columnInfo) {
            p_context.addColumn(info);
        }
    }
}
