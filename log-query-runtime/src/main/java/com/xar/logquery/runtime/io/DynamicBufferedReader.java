/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.io;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Reader;

/**
 * A Buffered reader with dynamic buffer growth of the buffer if a mark is in place and read progresses beyond the end of the buffer.
 * <p/>
 * Created by Anton Koekemoer on 2014/11/24.
 */
public class DynamicBufferedReader extends Reader {

    public static final char CHAR_CR = '\r';

    public static final char CHAR_LF = '\n';

    /**
     * The shared logger instance.
     */
    private static final Logger logger = LoggerFactory.getLogger(DynamicBufferedReader.class);

    /**
     * Constant indicating that there is no mark
     */
    private static final int UNMARKED = -1;

    /**
     * Inner reader
     */
    private final Reader m_reader;

    /**
     * The number of bytes to grow the buffer by if more space is required
     */
    private int m_growBy = 1024;

    /**
     * The character buffer
     */
    private char[] m_buffer;

    /**
     * The current position in the buffer
     */
    private int m_pos = 0;

    /**
     * The current starting location for the buffer
     */
    private int m_start = 0;

    /**
     * The end of the buffer
     */
    private int m_end = 0;

    /**
     * The mark location of the buffer
     */
    private int m_mark = UNMARKED;

//    /**
//     * Ignores the next linefeed (\n), used when reading \r characters. This is only on the first character read
//     */
//    private boolean m_skipNextLF = false;

    /**
     * Instantiates a new Dynamic buffered reader with an initial buffer size of 1024 bytes, and an auto grow by of 1024 bytes.
     *
     * @param p_reader the reader to read from
     */
    public DynamicBufferedReader(final Reader p_reader) {

        this(p_reader, 1024);

    }

    /**
     * Instantiates a new Dynamic buffered reader with an auto growby of 1024 bytes
     *
     * @param p_reader        the reader
     * @param p_initialBuffer the initial buffer size
     */
    public DynamicBufferedReader(final Reader p_reader, int p_initialBuffer) {

        this(p_reader, p_initialBuffer, 1024);
    }

    /**
     * Instantiates a new Dynamic buffered reader witht eh specified initial buffer size and auto growby value.
     *
     * @param p_reader        the reader
     * @param p_initialBuffer the initial buffer size
     * @param p_growBy        the amount by which the buffer should grow if more space is required
     */
    public DynamicBufferedReader(final Reader p_reader, int p_initialBuffer, int p_growBy) {

        m_reader = p_reader;
        m_buffer = new char[p_initialBuffer];
        m_growBy = p_growBy;

    }


    /**
     * Closes the stream and releases any system resources associated with
     * it.  Once the stream has been closed, further read(), ready(),
     * mark(), reset(), or skip() invocations will throw an IOException.
     * Closing a previously closed stream has no effect.
     *
     * @throws java.io.IOException If an I/O error occurs
     */
    @Override
    public void close() throws IOException {

        m_reader.close();
    }

    /**
     * Fills the buffer with more data from the reader
     *
     * @return true if the buffer is at end of file, false otherwise
     *
     * @throws java.io.IOException
     */
    public boolean fill() throws IOException {

        // First move all read (or all characters preceding the mark) characters from the buffer
        // and set the positions accordingly
        //
        if (m_mark != UNMARKED) {
            // With a mark in place
            //
            // Before...
            //
            //  1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 0 |
            //      ^           ^                   ^
            //      |           |                   |
            //    Mark         Pos                 End
            //
            // After...
            ///
            //  2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 0 |   |
            //  ^           ^                   ^
            //  |           |                   |
            // Mark         Pos                 End
            //
            // We need to ensure the mark is at the start of the buffer, so we copy mark to 0, and adjust m_pos accordingly
            if (m_mark > 0) {
                System.arraycopy(m_buffer, m_mark, m_buffer, 0, m_end - m_mark);
                m_end -= m_mark;
                m_pos -= m_mark;
                m_mark = 0;
                m_start = 0;
            }
        }
        else if (m_pos > 0) {
            //
            // With no mark in place
            //
            // Before...
            //
            //  1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 0 |
            //                  ^                   ^
            //                  |                   |
            //                 Pos                 End
            //
            // After...
            ///
            //  5 | 6 | 7 | 8 | 9 | 0 |   |   |   |   |
            //  ^                   ^
            //  |                   |
            // Pos                 End
            //
            System.arraycopy(m_buffer, m_pos, m_buffer, 0, m_end - m_pos);
            m_end -= m_pos;
            m_pos -= m_pos;
            m_start = 0;
        }

        // If we require more space
        if (m_end >= m_buffer.length) {
            // There is no space, we need to allocate a new buffer and read into that
            char[] temp = new char[m_buffer.length + m_growBy];
            System.arraycopy(m_buffer, 0, temp, 0, m_buffer.length);
            m_buffer = temp;
        }
        int read = m_reader.read(m_buffer, m_end, m_buffer.length - m_end);
        if (read > 0) { m_end += read; }

        return m_pos < m_end;
    }

    /**
     * Marks the present position in the stream.  Subsequent calls to reset()
     * will attempt to reposition the stream to this point.  Not all
     * character-input streams support the mark() operation.
     *
     * @param readAheadLimit Limit on the number of characters that may be
     *                       read while still preserving the mark.  This reader
     *                       grows it's buffer dynamically, so this value will
     *                       be used as the grow by setting. Use a sensible value,
     *                       as you can cause severe memory problems by setting this
     *                       value too large.
     *
     * @throws java.io.IOException If the stream does not support mark(),
     *                             or if some other I/O error occurs
     */
    @Override
    public void mark(final int readAheadLimit) throws IOException {

        m_growBy = readAheadLimit;
        m_mark = m_pos;
    }

    /**
     * Marks the present position in the stream.  Subsequent calls to reset()
     * will attempt to reposition the stream to this point.  Not all
     * character-input streams support the mark() operation.
     *
     * @throws java.io.IOException If the stream does not support mark(),
     *                             or if some other I/O error occurs
     */
    public void mark() throws IOException {

        mark(1024);
    }

    /**
     * Tells whether this stream supports the mark() operation. The default
     * implementation always returns false. Subclasses should override this
     * method.
     *
     * @return true if and only if this stream supports the mark operation.
     */
    @Override
    public boolean markSupported() {

        return true;
    }

    /**
     * Peeks at the next character in the sequence
     *
     * @return the next character in the sequence
     *
     * @throws java.io.IOException if there is an error reading from the stream
     */
    public int peek() throws IOException {

        if (m_pos < m_end || fill()) { return m_buffer[m_pos]; }
        return -1;
    }

    /**
     * Reads characters into a portion of an array.  This method will block
     * until some input is available, an I/O error occurs, or the end of the
     * stream is reached.
     *
     * @param cbuf Destination buffer
     * @param off  Offset at which to start storing characters
     * @param len  Maximum number of characters to read
     *
     * @return The number of characters read, or -1 if the end of the
     * stream has been reached
     *
     * @throws java.io.IOException If an I/O error occurs
     */
    @Override
    public int read(final char[] cbuf, final int off, final int len) throws IOException {

        int left = len;
        while (left > 0) {
            int available = m_end - m_pos;

            if (available > left) {
                System.arraycopy(m_buffer, m_pos, cbuf, len - left, left);
                m_pos += left;
                left = 0;
            }
            else {
                System.arraycopy(m_buffer, m_pos, cbuf, len - left, available);
                left -= available;
                m_pos += available;
            }

            if (!fill()) {
                if (left == len) { return -1; }
                break;
            }
        }
        return len - left;
    }

    /**
     * Reads a line of text.  A line is considered to be terminated by any one
     * of a line feed ('\n'), a carriage return ('\r'), or a carriage return
     * followed immediately by a linefeed.
     *
     * @return A String containing the contents of the line, not including
     * any line-termination characters, or null if the end of the
     * stream has been reached
     *
     * @throws java.io.IOException If an I/O error occurs
     * @see java.nio.file.Files#readAllLines
     */
    public String readLine() throws IOException {

        StringBuilder builder = new StringBuilder();
        boolean found = false;
        while (!found) {
            int start = m_pos;
            int length = 0;

            while (m_pos < m_end) {
                char c = m_buffer[m_pos++];
                if (c == CHAR_CR){
                    if (peek() == CHAR_LF){
                        m_pos++;
                    }
                    found = true;
                    break;
                }
                if (c == CHAR_LF) {
                    found = true;
                    break;
                }

                length++;
            }
            builder.append(m_buffer, start, length);
            if (!fill()) {
                if (m_pos < m_end || found || builder.length() > 0) {
                    break;
                }
                return null;
            }
        }

        return builder.toString();
    }

    /**
     * Resets the stream.  If the stream has been marked, then attempt to
     * reposition it at the mark.  If the stream has not been marked, then
     * attempt to reset it in some way appropriate to the particular stream,
     * for example by repositioning it to its starting point.  Not all
     * character-input streams support the reset() operation, and some support
     * reset() without supporting mark().
     *
     * @throws java.io.IOException If the stream has not been marked,
     *                             or if the mark has been invalidated,
     *                             or if the stream does not support reset(),
     *                             or if some other I/O error occurs
     */
    @Override
    public void reset() throws IOException {

        if (m_mark == UNMARKED) { throw new IOException("Stream has not been marked!"); }
        m_pos = m_mark;
        m_mark = -1;
    }

}
