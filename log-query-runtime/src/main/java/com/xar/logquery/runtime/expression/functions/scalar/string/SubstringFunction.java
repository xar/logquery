/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.functions.scalar.string;

import com.xar.logquery.runtime.annotations.Function;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.functions.AbstractFunction;

import static com.xar.logquery.runtime.enums.DataType.INTEGER;
import static com.xar.logquery.runtime.enums.DataType.STRING;

/**
 * Returns a substring of the input string.
 * <p/>
 * Syntax:
 * <p/>
 * SUBSTRING(text, startIndex[, length])
 * <p/>
 * Usage:
 * <p/>
 * SUBSTRING('1234567890', 1)
 * <p/>
 * Will return the substring of the input string, starting at startIndex, up to the last index of the string
 * Returns '234567890'
 * <p/>
 * SUBSTRING('1234567890', 1, 3)
 * <p/>
 * Will return the substring of the input string, starting at startIndex 1, continuing for three characters
 * Returns '234'
 */
@Function(
        names = {"SUBSTRING"}
)
public class SubstringFunction extends AbstractFunction {

    public SubstringFunction() {

        super(STRING);

        IExecuteCallback substring = new SubstringCallback();
        addCallback(new DataType[]{STRING, INTEGER}, substring);
        addCallback(new DataType[]{STRING, INTEGER, INTEGER}, substring);
    }

    private static class SubstringCallback implements IExecuteCallback {

        @Override
        public Object execute(final Object[] p_parameters) {

            String text = (String) p_parameters[0];
            int start = ((Number) p_parameters[1]).intValue();

            if (text == null) { return null; }

            if (start >= text.length()) { return ""; }
            if (p_parameters.length == 2) { return text.substring(start); }
            int length = ((Number) p_parameters[2]).intValue();


            return text.substring(start, Math.min(text.length(), start + length));

        }
    }
}
