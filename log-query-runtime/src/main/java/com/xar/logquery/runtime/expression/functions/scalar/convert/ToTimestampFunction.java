/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.functions.scalar.convert;

import com.xar.logquery.runtime.annotations.Function;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.functions.AbstractFunction;
import com.xar.logquery.runtime.util.DateTimeFunctions;

import java.text.ParseException;
import java.util.Date;

import static com.xar.logquery.runtime.enums.DataType.STRING;

/**
 * Function to convert a value to a timestamp
 */
@Function(names = {"TO_TIMESTAMP", "PARSE_TIMESTAMP"})
public class ToTimestampFunction extends AbstractFunction {

    public ToTimestampFunction() {

        super(DataType.TIMESTAMP);

        addCallback(new DataType[]{STRING}, new StringConverter());
        addCallback(new DataType[]{STRING, STRING}, new StringConverter());
        addCallback(new DataType[]{DataType.INTEGER}, new LongConverter());
        addCallback(new DataType[]{DataType.REAL}, new DoubleConverter());
        addCallback(new DataType[]{DataType.TIMESTAMP}, new DateConverter());
        addCallback(new DataType[]{DataType.DATE}, new DateConverter());
    }

    private static class DateConverter extends DateConverterBase {

        @Override
        public Date convert(final Object[] p_parameters) {

            return (Date) p_parameters[0];
        }
    }


    public static abstract class DateConverterBase implements AbstractFunction.IExecuteCallback {

        @Override
        public final Object execute(final Object[] p_parameters) {

            if (p_parameters[0] == null) { return null; }

            return convert(p_parameters);
        }

        protected abstract Date convert(final Object[] p_parameters);

    }

    private static class DoubleConverter extends DateConverterBase {

        @Override
        public Date convert(final Object[] p_parameters) {

            return new Date((long) (((Number) p_parameters[0]).doubleValue() * 86400000));
        }
    }

    private static class LongConverter extends DateConverterBase {

        @Override
        public Date convert(final Object[] p_parameters) {

            return new Date(((Number) p_parameters[0]).longValue());
        }
    }

    private static class StringConverter extends DateConverterBase {

        @Override
        public Date convert(final Object[] p_parameters) {

            String text = (String) p_parameters[0];

            try {
                if (p_parameters.length == 1) {
                    return DateTimeFunctions.parseDate(text);
                }
                else {
                    return DateTimeFunctions.parseDate(text, (String) p_parameters[1]);
                }
            }
            catch (ParseException e) {
                return null;
            }
        }
    }
}
