/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource;

import com.xar.logquery.runtime.AliasableColumnInfo;
import com.xar.logquery.runtime.DataRow;
import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.ResultSet;
import com.xar.logquery.runtime.ValidationContext;
import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IOrderable;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.exceptions.DuplicateColumnNameException;
import com.xar.logquery.runtime.expression.ColumnReference;
import com.xar.logquery.runtime.expression.NamedExpression;
import com.xar.logquery.runtime.expression.OrderExpression;
import com.xar.logquery.runtime.expression.WildcardExpression;
import com.xar.logquery.runtime.expression.constant.IntegerConstant;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;
import com.xar.logquery.runtime.parse.ParseData;
import com.xar.logquery.runtime.util.RuntimeFunctions;
import com.xar.logquery.runtime.util.StringUtils;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Created by akoekemo on 3/2/15.
 */
public class SelectDataSource extends AliasableDataSource implements IOrderable {

    protected List<IExpression> m_expressionList = new ArrayList<>();

    IDataSource m_dataSource;

    NamedExpression[] m_columns;

    /**
     * Initialises a new Select data source
     *
     * @param p_dataSource  the datasource to select from
     * @param p_expressions the select list
     */
    public SelectDataSource(final IDataSource p_dataSource, final IExpression[] p_expressions) {

        m_dataSource = p_dataSource;
        for (IExpression expression : p_expressions) { addExpression(expression); }
    }

    /**
     * Add the specified expression top the datasource
     *
     * @param p_expression the datasource
     */
    public void addExpression(IExpression p_expression) {

        m_expressionList.add(p_expression);
    }

    /**
     * Applies order to the datasource
     *
     * @param orderBy the order
     */
    @Override
    public void setOrderBy(final OrderExpression[] orderBy) {

        for (OrderExpression orderExpression : orderBy) {
            int idx = -1;

            if (orderExpression.getExpression() instanceof IntegerConstant) {
                idx = ((IntegerConstant) orderExpression.getExpression()).getValue(null).intValue();
            }
            if (idx > 0 && idx <= m_expressionList.size()) {
                IExpression expression = m_expressionList.get(idx - 1);
                // TODO: Maybe need an exception here for wildcard expressions
                orderExpression.setExpression(expression);
            }
        }

        if (m_dataSource instanceof IOrderable) {

            ((IOrderable) m_dataSource).setOrderBy(orderBy);
        }
        else {

            m_dataSource = new OrderedDataSource(m_dataSource, orderBy);
        }

    }

    /**
     * Execute the query and returns a resultset
     *
     * @param p_context the execution context
     *
     * @return the resultset
     *
     * @throws com.xar.logquery.runtime.exceptions.DataSourceException if the execution fails
     */
    @Override
    protected IResultSet executeQuery(final IExecutionContext p_context) throws DataSourceException {

        final IColumnInfo[] columnInfo = createColumnInfo();

        return new ResultSet(columnInfo, new TransformEnumeration(columnInfo, p_context));
    }

    /**
     * Validate the datasource
     *
     * @param p_context the binding context
     *
     * @throws com.xar.logquery.runtime.exceptions.DataSourceException if the datasource is invalid
     */
    @Override
    protected void validateQuery(final IValidationContext p_context) throws DataSourceException {

        ValidationContext validationContext = new ValidationContext(p_context);

        m_dataSource.validate(validationContext);

        Set<String> names = new HashSet<>();


        // First we need to make all the expressions in the list are in fact
        // Named expressions, and we add the aliased named expression first, as
        // explicitly maned column names are fixed. We can synthesize names for
        // unnamed columns, but named columns are absolute, and should not clash
        List<NamedExpression> columns = new LinkedList<>();

        final List<IExpression> expressionList = m_expressionList;

        for (IExpression expression : expressionList) {
            if (expression instanceof NamedExpression) {
                columns.add((NamedExpression) expression);

                String alias = ((NamedExpression) expression).getName();

                if (StringUtils.isEmpty(alias)) { continue; }

                expression.validate(validationContext);

                if (names.add(alias)) { continue; }

                throw new DuplicateColumnNameException(expression.getParseData(), "Duplicate column name - " + alias);
            }
            else if (expression instanceof WildcardExpression) {
                WildcardExpression wildCard = (WildcardExpression) expression;
                addColumns(columns, wildCard.getContext(), validationContext.getColumns(), expression.getParseData());
            }
            else {
                columns.add(new NamedExpression(expression, ""));
            }
        }

        // Now we add the non aliased columns, for them we can make up new names
        // we add this second because the assigned aliases take presidence
        for (NamedExpression expression : columns) {

            String alias = expression.getName();

            if (!StringUtils.isEmpty(alias)) { continue;}

            expression.validate(validationContext);

            final ParseData parseData = expression.getParseData();
            if (parseData != null) {
                alias = parseData.getText();
            }

            if (!names.add(alias)) {
                String prefix = alias;
                int postFix = 1;
                while (!names.add(alias)) {
                    alias = prefix + "_" + postFix;
                    postFix++;
                }
            }

            expression.setName(alias);
        }

        m_columns = columns.toArray(new NamedExpression[columns.size()]);

        for (int i = 0; i < m_columns.length; i++) {
            NamedExpression column = m_columns[i];
            p_context.addColumn(new AliasableColumnInfo(this, column.getName(), column.getDataType()));
        }


    }

    private void addColumns(final List<NamedExpression> p_columns, final String p_context, final IColumnInfo[] p_columnInfo, ParseData p_parseData) {

        for (IColumnInfo info : p_columnInfo) {
            if (StringUtils.isEmpty(p_context) || info.getContext().equals(p_context)) {
                final NamedExpression expression = new NamedExpression(new ColumnReference(info.getContext(), info.getName()), "");
                expression.setParseData(new ParseData(p_parseData.getRowNumber(), p_parseData.getColumnNumber(), RuntimeFunctions.getFQN(info.getContext(), info.getName())));
                p_columns.add(expression);
            }
        }
    }

    /**
     * Create the column info for the datasource
     *
     * @return an array with the columns of the datasource
     */
    private IColumnInfo[] createColumnInfo() {

        IColumnInfo[] columnInfo = new IColumnInfo[m_columns.length];

        for (int i = 0; i < m_columns.length; i++) {
            NamedExpression namedExpression = m_columns[i];

            columnInfo[i] = new AliasableColumnInfo(this, namedExpression.getName(), namedExpression.getDataType());

        }
        return columnInfo;
    }

    /**
     * Enumeration for the datasource
     */
    private class TransformEnumeration implements Enumeration<IDataRow> {

        private final Enumeration<IDataRow> m_enumeration;

        private final ExecutionContext m_executionContext;

        private final IResultSet m_resultSet;

        private final IColumnInfo[] m_sourceColumns;

        private IColumnInfo[] m_columnInfo;

        public TransformEnumeration(final IColumnInfo[] p_columnInfo, final IExecutionContext p_context) throws DataSourceException {

            m_executionContext = new ExecutionContext(p_context);
            m_columnInfo = p_columnInfo;

            m_resultSet = m_dataSource.getResultSet(p_context);
            m_sourceColumns = m_resultSet.getColumnInfo();
            m_enumeration = m_resultSet.getRows();
        }

        /**
         * Tests if this enumeration contains more elements.
         *
         * @return <code>true</code> if and only if this enumeration object
         * contains at least one more element to provide;
         * <code>false</code> otherwise.
         */
        @Override
        public boolean hasMoreElements() {

            return m_enumeration.hasMoreElements();
        }

        /**
         * Returns the next element of this enumeration if this enumeration
         * object has at least one more element to provide.
         *
         * @return the next element of this enumeration.
         *
         * @throws java.util.NoSuchElementException if no more elements exist.
         */
        @Override
        public IDataRow nextElement() {

            return transformRow(m_executionContext, m_enumeration.nextElement());
        }

        /**
         * Transform the specified row, using the expression list
         *
         * @param p_row the row to transform
         *
         * @return the transformed row
         */
        private IDataRow transformRow(IExecutionContext p_context, final IDataRow p_row) {

            final Object[] rowData = p_row.getData();
            final Object[] data = new Object[m_columnInfo.length];

            p_context.reset();

            for (int i = 0; i < rowData.length; i++) {

                p_context.setValue(m_sourceColumns[i].getContext(), m_sourceColumns[i].getName(), rowData[i]);
            }

            int i = 0;
            for (NamedExpression expression : m_columns) {
                IColumnInfo info = m_columnInfo[i];

                data[i] = expression.getValue(p_context);

                p_context.setValue(info.getContext(), info.getName(), data[i]);
                i++;
            }

            return new DataRow(m_columnInfo, data);

        }
    }

}
