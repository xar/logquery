/*
 * Copyright 2014 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.arithmitic;

import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.BinaryExpression;
import com.xar.logquery.runtime.expression.interfaces.IExpression;

import java.math.BigDecimal;

/**
 * Divide m_expression.
 */
public class DivideExpression extends BinaryExpression {

    public DivideExpression(final IExpression p_lhs, final IExpression p_rhs) {

        super(p_lhs, p_rhs);

        DoubleCallback doubleCallback = new DoubleCallback();

        addInvertedCallback(DataType.REAL, new DataType[]{DataType.REAL, DataType.REAL}, doubleCallback);


    }

    private static class DoubleCallback extends BinaryCallback {

        @Override
        protected Object doOperation(final Object p_lhs, final Object p_rhs) {

            return new BigDecimal(((Number) p_lhs).doubleValue() / ((Number) p_rhs).doubleValue());
        }

    }
}
