/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.interfaces;

import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.exceptions.DuplicateColumnNameException;

/**
 * Minimum interface requirements for a validation context.
 */
public interface IValidationContext {

    /**
     * Associated the data type with the specified context and name
     *
     * @param p_columnInfo the column
     */
    void addColumn(final IColumnInfo p_columnInfo) throws DuplicateColumnNameException;

    /**
     * Get the data type of the value with the specified name and alias
     *
     * @param p_alias the alias
     * @param p_name  the name
     *
     * @return the data type of the calue, or DataType.UNDEFINED if not found
     */
    IColumnInfo getColumnInfo(String p_alias, String p_name);

    /**
     * Get the columns already added to this context
     *
     * @return the columns already added to this context
     */
    IColumnInfo[] getColumns();

}
