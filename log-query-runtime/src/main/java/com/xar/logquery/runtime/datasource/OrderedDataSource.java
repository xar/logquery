/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource;///*

import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.ResultSet;
import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.expression.OrderExpression;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;
import com.xar.logquery.runtime.util.RuntimeFunctions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.List;

/**
 * A Datasource implementation that allows for the ordering data from another datasource
 */
public class OrderedDataSource extends AbstractDataSource {

    /**
     * The expressionlist to order by
     */
    OrderExpression[] m_expressionList = new OrderExpression[0];

    /**
     * The datasource to order
     */
    private IDataSource m_dataSource;


    /**
     * Instantiates a new Ordered data source.
     *
     * @param p_dataSource     the data source to order
     * @param p_expressionList the expression list to use when ordering
     */
    public OrderedDataSource(final IDataSource p_dataSource, final OrderExpression[] p_expressionList) {

        m_dataSource = p_dataSource;
        m_expressionList = p_expressionList;
    }

    /**
     * Execute the query and returns a resultset
     *
     * @param p_context the execution context
     *
     * @return the resultset
     *
     * @throws com.xar.logquery.runtime.exceptions.DataSourceException if the execution fails
     */
    @Override
    protected IResultSet executeQuery(final IExecutionContext p_context) throws DataSourceException {

        List<IDataRow> rows = new ArrayList<>();

        final IResultSet resultSet = m_dataSource.getResultSet(p_context);
        final Enumeration<IDataRow> re = resultSet.getRows();
        final IColumnInfo[] columnInfo = resultSet.getColumnInfo();

        while (re.hasMoreElements()) { rows.add(re.nextElement()); }

        Collections.sort(rows, new DataRowComparator(p_context, columnInfo));

        return new ResultSet(columnInfo, new IteratorEnumeration<>(rows.iterator()));
    }

    /**
     * Validate the datasource
     *
     * @param p_context the binding context
     *
     * @throws com.xar.logquery.runtime.exceptions.DataSourceException if the datasource is invalid
     */
    @Override
    protected void validateQuery(final IValidationContext p_context) throws DataSourceException {

        m_dataSource.validate(p_context);

        for (IExpression expression : m_expressionList) { expression.validate(p_context); }

    }

    private class DataRowComparator implements Comparator<IDataRow> {

        private final IExecutionContext m_context;

        private final IColumnInfo[] m_columnInfo;

        IExecutionContext m_context1;

        IExecutionContext m_context2;

        public DataRowComparator(final IExecutionContext p_context, final IColumnInfo[] p_columnInfo) {

            m_context = p_context;
            m_columnInfo = p_columnInfo;
            m_context1 = new ExecutionContext(m_context);
            m_context2 = new ExecutionContext(m_context);
        }

        @Override
        public int compare(final IDataRow p_row1, final IDataRow p_row2) {

            m_context1.reset();
            m_context2.reset();
            Object[] rowData1 = p_row1.getData();
            Object[] rowData2 = p_row2.getData();

            if (rowData1.length != m_columnInfo.length) { System.out.println(p_row1); }
            if (rowData2.length != m_columnInfo.length) { System.out.println(p_row2); }
            for (int i = 0; i < rowData1.length; i++) {

                m_context1.setValue(m_columnInfo[i].getContext(), m_columnInfo[i].getName(), rowData1[i]);
                m_context2.setValue(m_columnInfo[i].getContext(), m_columnInfo[i].getName(), rowData2[i]);
            }

            for (OrderExpression iExpression : m_expressionList) {

                int compare = RuntimeFunctions.compare(iExpression.getValue(m_context1), iExpression.getValue(m_context2));

                if (compare == 0) { continue; }

                return compare * (iExpression.isAscending() ? 1 : -1);
            }

            return 0;
        }
    }
}
