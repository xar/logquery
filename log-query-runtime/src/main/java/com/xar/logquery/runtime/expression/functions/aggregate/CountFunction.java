/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.functions.aggregate;

import com.xar.logquery.runtime.annotations.Function;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.WildcardExpression;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.expression.functions.aggregate.interfaces.IAggregator;
import com.xar.logquery.runtime.expression.functions.aggregate.interfaces.IAggregatorFactory;
import com.xar.logquery.runtime.expression.functions.exceptions.InvalidParametersException;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;
import com.xar.logquery.runtime.util.RuntimeFunctions;

import java.util.HashSet;

/**
 * Function used to count the number of records
 */
@Function(
        names = {"COUNT"}
)
public class CountFunction extends AggregateFunctionBase {

    /**
     * Instantiates a new Aggregate function base.
     */
    public CountFunction() {

        super(DataType.INTEGER, null);
    }


    /**
     * Validates the expression against the specified context
     *
     * @param p_context the context to validate the expression against
     */
    @Override
    public void validateExpression(final IValidationContext p_context) throws InvalidExpressionException {

        if (m_functionParameters.length != 1) {
            String functionName = RuntimeFunctions.getFunctionName(getClass());
            throw new InvalidParametersException(getParseData(), functionName + " requires one parameter, could be a column name or a wildcard\nCalled with: " + RuntimeFunctions.getFunctionCallPrototype(functionName, m_functionParameters));
        }


        if (m_functionParameters[0].getChildren()[0] instanceof WildcardExpression) {
            setAggregatorFactory(new WildcardAggregatorFactory());
        }
        else if (m_functionParameters[0].isDistinct()) {
            setAggregatorFactory(new DistinctValueAggregateFactory());
        }
        else {
            setAggregatorFactory(new ValueAggregateFactory());
        }

    }

    /**
     * Distinct value aggregator for count, counts all distinct values
     */
    private class DistinctValueAggregate extends WildcardAggregate {

        HashSet<Object> m_set = new HashSet<>();

        public DistinctValueAggregate() {

        }

        @Override
        public void aggregate(final IExecutionContext p_context) {

            Object value = m_functionParameters[0].getValue(p_context);
            if (value != null) {
                if (m_set.add(value)) {
                    super.aggregate(p_context);
                }
            }
        }
    }

    /**
     * Factory for the Distinct value aggregate
     */
    private class DistinctValueAggregateFactory implements IAggregatorFactory {

        /**
         * Creates a new Aggregator
         *
         * @return the aggregator
         */
        @Override
        public IAggregator create() {

            return new DistinctValueAggregate();
        }
    }

    /**
     * Value aggregator for count, counts all non null values
     */
    private class ValueAggregate extends WildcardAggregate {

        public ValueAggregate() {

        }

        @Override
        public void aggregate(final IExecutionContext p_context) {

            Object value = m_functionParameters[0].getValue(p_context);
            if (value != null) {
                super.aggregate(p_context);
            }
        }
    }

    /**
     * Factory for the Value aggregate
     */
    private class ValueAggregateFactory implements IAggregatorFactory {

        /**
         * Creates a new Aggregator
         *
         * @return the aggregator
         */
        @Override
        public IAggregator create() {

            return new ValueAggregate();
        }
    }

    /**
     * Wildcard aggregator for count, merely counts all;
     */
    private class WildcardAggregate implements IAggregator<Long> {

        long count;

        @Override
        public void aggregate(final IExecutionContext p_context) {

            count++;
        }

        @Override
        public Long getValue() {

            return count;
        }
    }

    /**
     * Factory for the Wildcard aggregate
     */
    private class WildcardAggregatorFactory implements IAggregatorFactory {

        /**
         * Creates a new Aggregator
         *
         * @return the aggregator
         */
        @Override
        public IAggregator create() {

            return new WildcardAggregate();
        }
    }
}
