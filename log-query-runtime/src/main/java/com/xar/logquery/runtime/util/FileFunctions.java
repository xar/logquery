/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.util;

import org.apache.commons.io.filefilter.WildcardFileFilter;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * File functions
 */
public class FileFunctions {

    /**
     * Get the file extension
     * @param p_fileName
     * @return
     */
    public static String getExtension(final String p_fileName) {
        int i = p_fileName.lastIndexOf('.');
        if (i > 0){
            return p_fileName.substring(i+1);
        }
        return "";
    }


    public static String getNameWithoutExtension(String p_fileName){
        int i = p_fileName.lastIndexOf('.');
        if (i > 0){
            return p_fileName.substring(0, i);
        }
        return p_fileName;
    }

    /**
     * List files according to a pattern.
     *
     * @param p_pattern the pattern to match
     *
     * @return the list of files matching the pattern
     *
     * @throws java.io.IOException the iO exception
     */
    public static List<File> listFiles(String p_pattern) throws IOException {

        File f = new File(p_pattern);

        List<String> path = new ArrayList<>();

        path.add(f.getName());


        File part = f.getCanonicalFile().getParentFile();
        while (part != null) {
            if (part.getParent() != null) {
                path.add(0, part.getName());
                part = part.getParentFile();
                continue;
            }
            break;
        }

        while (path.size() > 1) {
            if (path.get(0).contains("*") || path.get(0).contains("?")) {
                break;
            }
            part = new File(part, path.remove(0));
        }
        List<File> files = listFiles(part, path);
        Collections.sort(files);
        return files;
    }

    /**
     * Lists files valueIn the root, matching the patterns valueIn the path
     *
     * @param p_directory the root directory
     * @param p_path      a list of path patterns to match
     *
     * @return a list of files matching the patterns
     *
     * @throws java.io.IOException
     */
    private static List<File> listFiles(final File p_directory, final List<String> p_path) throws IOException {

        File path = new File(p_path.remove(0));
        FilenameFilter filter = new WildcardFileFilter(path.getName());

        List<File> result = new ArrayList<>();

        File[] files = p_directory.listFiles(filter);
        if (files != null && files.length > 0) {
            for (File file : files) {
                if (file.isDirectory()) {
                    if (p_path.size() > 0) {
                        result.addAll(listFiles(file.getCanonicalFile(), new ArrayList<String>(p_path)));
                    }
                }
                else if (p_path.size() == 0 && file.isFile()) {
                    result.add(file.getAbsoluteFile());
                }

            }
        }
        else if (path.exists()) {
            result.addAll(listFiles(new File(p_directory, path.getName()).getCanonicalFile(), new ArrayList<String>(p_path)));
        }
        return result;

    }

}
