/*
 * Copyright 2014 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.util;

import com.xar.logquery.parser.generated.SelectParser;
import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.expression.ColumnReference;
import com.xar.logquery.runtime.expression.functions.aggregate.interfaces.IAggregateExpression;
import com.xar.logquery.runtime.expression.interfaces.IExpression;

import java.util.ArrayList;
import java.util.List;


/**
 * Functions used during parse
 */
public class ParseFunctions {

    private static final ParseFunctions _instance = new ParseFunctions();

    private ParseFunctions() {

    }

    /**
     * Get the reference info for an aggregate expression.
     *
     * @param p_aggregateExpressions All aggregate expressions found in the child tree will be added to this list
     * @param p_aggregatedColumns    all column references found in the child tree will be added to this list
     * @param p_expressions          the expressions to be analysed
     */
    public static void getAggregateReferenceInfo(List<IAggregateExpression> p_aggregateExpressions, final List<IColumnInfo> p_aggregatedColumns, IExpression... p_expressions) {

        for (IExpression expression : p_expressions) {
            if (expression instanceof IAggregateExpression) {
                p_aggregateExpressions.add((IAggregateExpression) expression);
                getAggregateReferenceInfo(p_aggregateExpressions, p_aggregatedColumns, expression.getChildren());
            }
            else if (expression instanceof ColumnReference) {
                p_aggregatedColumns.add(((ColumnReference) expression).getColumnInfo());
            }
        }

    }

    /**
     * Get the value of an identifier from the identifier context
     *
     * @param ctx the context
     *
     * @return the identifier text
     */
    public static String getIdentifierValue(final SelectParser.IdentifierContext ctx) {

        if (ctx == null) { return null; }

        if (ctx.NonQuotedIdentifier() != null) {
            return ctx.NonQuotedIdentifier().getText();
        }
        else {
            String value = ctx.QuotedIdentifier().getText();
            return value.substring(1, value.length() - 1);
        }
    }

    /**
     * Get the reference Info for a series of expressions
     *
     * @param p_expressions the expressions to analyse
     *
     * @return a reference info object
     */
    public static ReferenceInfo getReferenceInfo(IExpression... p_expressions) {

        List<IColumnInfo> columns = new ArrayList<>();
        List<IAggregateExpression> aggregateExpressions = new ArrayList<>();
        List<IColumnInfo> aggregatedColumns = new ArrayList<>();

        getReferenceInfo(columns, aggregateExpressions, aggregatedColumns, p_expressions);

        return new ReferenceInfo(columns.toArray(new IColumnInfo[columns.size()]), aggregateExpressions.toArray(new IAggregateExpression[aggregateExpressions.size()]), aggregatedColumns.toArray(new IColumnInfo[aggregatedColumns.size()]));
    }

    public static void getReferenceInfo(List<IColumnInfo> p_references, List<IAggregateExpression> p_aggregateExpressions, List<IColumnInfo> p_aggregatedColumns, IExpression... p_expressions) {

        for (IExpression expression : p_expressions) {
            if (expression instanceof IAggregateExpression) {
                p_aggregateExpressions.add((IAggregateExpression) expression);
                getAggregateReferenceInfo(p_aggregateExpressions, p_aggregatedColumns, expression.getChildren());
            }
            else if (expression instanceof ColumnReference) {
                p_references.add(((ColumnReference) expression).getColumnInfo());
            }
            else {
                getReferenceInfo(p_references, p_aggregateExpressions, p_aggregatedColumns, expression.getChildren());
            }
        }

    }

    /**
     * Expression reference info object
     */
    public static class ReferenceInfo {

        /**
         * The columns referenced outside of an aggregate context
         */
        private IColumnInfo[] m_columnInfo;

        /**
         * The aggregate expressions
         */
        private IAggregateExpression[] m_aggregateExpressions;

        /**
         * The columns referenced inside of an aggregate context
         */
        private IColumnInfo[] m_aggregateColumns;

        /**
         * Initialise the reference info
         *
         * @param p_columnInfos          The columns referenced outside of an aggregate context
         * @param p_aggregateExpressions The aggregate expressions
         * @param p_aggregateColumns     The columns referenced inside of an aggregate context
         */
        public ReferenceInfo(final IColumnInfo[] p_columnInfos, final IAggregateExpression[] p_aggregateExpressions, final IColumnInfo[] p_aggregateColumns) {


            m_columnInfo = p_columnInfos;
            m_aggregateExpressions = p_aggregateExpressions;
            m_aggregateColumns = p_aggregateColumns;
        }

        /**
         * Get the columns referenced inside of an aggregate context
         *
         * @return the columns referenced inside of an aggregate context
         */
        public IColumnInfo[] getAggregateColumns() {

            return m_aggregateColumns;
        }

        /**
         * Get the aggregate expressions
         *
         * @return the aggregate expressions
         */
        public IAggregateExpression[] getAggregateExpressions() {

            return m_aggregateExpressions;
        }

        /**
         * Get the columns referenced outside of an aggregate context
         *
         * @return the columns referenced outside of an aggregate context
         */
        public IColumnInfo[] getColumnInfo() {

            return m_columnInfo;
        }
    }
}
