/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression;

import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.interfaces.IValidationContext;
import com.xar.logquery.runtime.parse.ParseData;

/**
 * Abstract implementation of the abstract expression.
 */
public abstract class AbstractExpression implements IExpression {

    private ParseData m_parseData;

    /**
     * Get the parseData for the expression
     *
     * @return the parse data for the expression
     */
    @Override
    public ParseData getParseData() {

        return m_parseData;
    }

    /**
     * Get the parseData for the expression
     *
     * @param p_parseData
     *
     * @return the parse data for the expression
     */
    @Override
    public void setParseData(final ParseData p_parseData) {

        m_parseData = p_parseData;
    }

    /**
     * Validates the expression against the specified context
     *
     * @param p_context the context to validate the expression against
     */
    @Override
    public final void validate(final IValidationContext p_context) throws InvalidExpressionException {

        for (IExpression child : getChildren()) { child.validate(p_context); }

        validateExpression(p_context);
    }

    /**
     * Validates the expression against the specified context
     *
     * @param p_context the context to validate the expression against
     */
    public abstract void validateExpression(final IValidationContext p_context) throws InvalidExpressionException;

}
