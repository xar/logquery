/*
 * Copyright 2014 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.util;

/**
 * String utilities
 */
public class StringUtils {

    private static StringUtils _instance = new StringUtils();

    private StringUtils() {

    } // Prevent instantiation


    /**
     * Expands tabs to spaces
     *
     * @param p_text   the text to expand tabs in
     * @param p_spaces the number of spaces per tab
     *
     * @return
     */
    public static String expandTabs(final String p_text, final int p_spaces) {

        int start = 0;
        int pos = 0;
        char[] spaces = new char[p_spaces];

        for (int i = 0; i < p_spaces; i++) { spaces[i] = ' '; }

        char[] chars = p_text.toCharArray();
        StringBuilder builder = new StringBuilder();
        for (pos = 0; pos < chars.length; pos++) {
            if (chars[pos] == '\t') {
                builder.append(chars, start, pos - start)
                        .append(spaces, 0, p_spaces - (pos % p_spaces));
                start = pos + 1;
            }
        }
        builder.append(chars, start, pos - start);

        return builder.toString();
    }

    /**
     * Check to see if a string is null or empty
     *
     * @param p_text the string to check
     *
     * @return true, if the string is either null or ehas a length of 0; false otherwise
     */
    public static boolean isEmpty(final String p_text) {

        return p_text == null || p_text.length() == 0;
    }

    public static void padl(final StringBuilder p_builder, final int p_length, char p_char) {

        while (p_builder.length() < p_length) {
            p_builder.insert(0, p_char);
        }
    }

    public static String padl(final String p_string, final int p_width, final char p_char) {

        StringBuilder builder = new StringBuilder(p_string);
        padl(builder, p_width, p_char);
        return builder.toString();
    }

    public static String padl(final String p_string, final int p_length) {

        return padl(p_string, p_length, ' ');
    }

    public static void padr(final StringBuilder p_builder, final int p_length, char p_char) {

        while (p_builder.length() < p_length) {
            p_builder.append(p_char);
        }
    }

    public static String padr(final String p_string, final int p_width, final char p_char) {

        StringBuilder builder = new StringBuilder(p_string);
        padr(builder, p_width, p_char);
        return builder.toString();
    }

    public static String padr(final String p_string, final int p_length) {

        return padr(p_string, p_length, ' ');
    }

    /**
     * Trim whitespace from the end of as string
     *
     * @param p_string the string from which to trim whitespace
     *
     * @return a new string without the trailing whitespace
     */
    public static String rtrim(String p_string) {

        char[] chars = p_string.toCharArray();

        for (int i = chars.length - 1; i >= 0; i--) {
            switch (chars[i]) {
                case ' ':
                case '\t':
                    continue;
                default:
                    return new String(chars, 0, i + 1);
            }
        }
        return p_string;
    }

}
