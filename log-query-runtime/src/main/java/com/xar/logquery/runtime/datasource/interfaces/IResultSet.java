/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource.interfaces;

import com.xar.logquery.runtime.exceptions.DataSourceException;

import java.util.Enumeration;

/**
 * Minimum interface requirement for a resultset.
 */
public interface IResultSet {

    /**
     * Get the column info from the resultset.
     *
     * @return an array that describes the columns of the resultset
     */
    public IColumnInfo[] getColumnInfo();

    /**
     * Get an enumeration of data rows in the resultset
     *
     * @return
     *
     * @throws DataSourceException
     */
    public Enumeration<IDataRow> getRows() throws DataSourceException;

}
