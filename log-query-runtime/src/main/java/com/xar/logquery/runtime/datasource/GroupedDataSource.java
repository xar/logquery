/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource;

import com.xar.logquery.runtime.ColumnInfo;
import com.xar.logquery.runtime.DataRow;
import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.ResultSet;
import com.xar.logquery.runtime.ValidationContext;
import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.expression.functions.aggregate.interfaces.IAggregateExpression;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;
import com.xar.logquery.runtime.util.RuntimeFunctions;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Datasource that supports grouping of data
 */
public class GroupedDataSource extends AbstractDataSource {

    /**
     * The group by expressions
     */
    private final IExpression[] m_groupBy;

    /**
     * Columns required from the datasource being grouped
     */
    private final IColumnInfo[] m_copyColumns;

    /**
     * Group m_rows
     */
    private Map<Object, Object[]> m_rows;

    /**
     * Aggregate expressions
     */
    private final IAggregateExpression[] m_aggregates;

    /**
     * Datasource being grouped
     */
    private IDataSource m_dataSource;

    /**
     * Columns being exposed
     */
    private IColumnInfo[] m_columnInfo;


    /**
     * Instantiates a new Grouped data source.
     *
     * @param p_dataSource  the data source to group
     * @param p_copyColumns the required columns from the datasource
     * @param p_aggregates  the aggregates
     * @param p_groupBy     the group by expressions
     */
    public GroupedDataSource(final IDataSource p_dataSource, final IColumnInfo[] p_copyColumns, final IAggregateExpression[] p_aggregates, final IExpression[] p_groupBy) {

        m_dataSource = p_dataSource;
        m_groupBy = p_groupBy;
        m_copyColumns = p_copyColumns;
        m_columnInfo = p_copyColumns;

        m_aggregates = p_aggregates;
    }

    /**
     * Execute the query and returns a resultset
     *
     * @param p_context the execution context
     *
     * @return the resultset
     *
     * @throws com.xar.logquery.runtime.exceptions.DataSourceException if the execution fails
     */
    @Override
    protected IResultSet executeQuery(final IExecutionContext p_context) throws DataSourceException {

        return new ResultSet(m_columnInfo, new GroupEnumerator(p_context));
    }

    /**
     * Validate the datasource
     *
     * @param p_context the binding context
     *
     * @throws com.xar.logquery.runtime.exceptions.DataSourceException if the datasource is invalid
     */
    @Override
    protected void validateQuery(final IValidationContext p_context) throws DataSourceException {

        IValidationContext context = new ValidationContext(p_context);
        m_dataSource.validate(context);

        m_columnInfo = new IColumnInfo[m_copyColumns.length + m_aggregates.length];

        for (int i = 0; i < m_copyColumns.length; i++) {
            IColumnInfo copyColumn = m_copyColumns[i];
            IColumnInfo columnInfo = context.getColumnInfo(copyColumn.getContext(), copyColumn.getName());
            m_columnInfo[i] = columnInfo;
            p_context.addColumn(columnInfo);
        }
        for (int i = 0; i < m_aggregates.length; i++) {
            IAggregateExpression aggregate = m_aggregates[i];
            aggregate.validate(context);
            ColumnInfo columnInfo = new ColumnInfo(aggregate.getClass().getName(), aggregate.getKey(), aggregate.getDataType());
            m_columnInfo[m_copyColumns.length + i] = columnInfo;
            p_context.addColumn(columnInfo);
        }


    }

    /**
     * Grouping enumerator
     */
    private class GroupEnumerator implements Enumeration<IDataRow> {

        /**
         * The Context.
         */
        ExecutionContext context = new ExecutionContext();

        /**
         * The M _ iterator.
         */
        Iterator<Object[]> m_iterator;


        /**
         * Instantiates a new Group enumerator.
         *
         * @param p_context
         */
        private GroupEnumerator(final IExecutionContext p_context) throws DataSourceException {

            m_iterator = getIterator();
        }

        /**
         * Tests if this enumeration contains more elements.
         *
         * @return <code>true</code> if and only if this enumeration object
         * contains at least one more element to provide;
         * <code>false</code> otherwise.
         */
        @Override
        public boolean hasMoreElements() {

            return m_iterator.hasNext();
        }

        /**
         * Returns the next element of this enumeration if this enumeration
         * object has at least one more element to provide.
         *
         * @return the next element of this enumeration.
         *
         * @throws java.util.NoSuchElementException if no more elements exist.
         */
        @Override
        public IDataRow nextElement() {

            Object[] row;

            row = m_iterator.next();

            context.reset();
            for (int i = 0; i < m_columnInfo.length; i++) {
                IColumnInfo columnInfo = m_columnInfo[i];
                context.setValue(columnInfo.getContext(), columnInfo.getName(), row[i]);
            }

            return new DataRow(m_columnInfo, row);
        }

        /**
         * Aggregates the specified row
         *
         * @param context     the expression context
         * @param p_columnIdx
         * @param p_dataRow   the datarow to aggregate
         */
        private void aggregateRow(ExecutionContext context, final int[] p_columnIdx, final IDataRow p_dataRow) {

            // Calculate row key
            RuntimeFunctions.applyRowToContext(context, p_dataRow);

            Object rowKey = calculateKey(context);

            // Find the row
            Object[] row = m_rows.get(rowKey);


            Object[] data = p_dataRow.getData();

            if (row == null) {
                row = new Object[m_columnInfo.length];
                for (int i = 0; i < p_columnIdx.length; i++) {
                    row[i] = data[p_columnIdx[i]];
                }
                for (int i = 0; i < m_aggregates.length; i++) {
                    row[p_columnIdx.length + i] = m_aggregates[i].newState();
                }
                m_rows.put(rowKey, row);
            }
            for (int i = 0; i < m_aggregates.length; i++) {
                row[p_columnIdx.length + i] = m_aggregates[i].aggregate(row[p_columnIdx.length + i], context);
            }
        }


        /**
         * Calculates a row key from the context, using the group by expressions
         *
         * @param p_context the expression context
         *
         * @return a row key from the context, using the group by expressions
         */
        private Object calculateKey(final IExecutionContext p_context) {

            StringBuilder buffer = new StringBuilder();
            for (IExpression expression : m_groupBy) {
                buffer.append('|').append(expression.getValue(p_context));
            }
            return buffer.toString();
        }

        /**
         * Get an iterator for the grouped values
         *
         * @return an iterator for the grouped values
         *
         * @throws DataSourceException if there is an error grouping the data
         */
        private Iterator<Object[]> getIterator() throws DataSourceException {

            if (m_rows == null) {
                m_rows = new HashMap<>();
                IResultSet resultSet = m_dataSource.getResultSet(context);
                IColumnInfo[] columnInfo = resultSet.getColumnInfo();

                int[] columnIdx = new int[m_copyColumns.length];

                for (int i = 0; i < columnInfo.length; i++) {
                    IColumnInfo outer = columnInfo[i];
                    for (int j = 0; j < m_copyColumns.length; j++) {
                        IColumnInfo inner = m_copyColumns[j];
                        if (ColumnInfo.equals(outer, inner)) {
                            columnIdx[j] = i;
                            break;
                        }
                    }
                }

                Enumeration<IDataRow> enumeration = resultSet.getRows();
                while (enumeration.hasMoreElements()) {
                    aggregateRow(context, columnIdx, enumeration.nextElement());
                }
            }
            return m_rows.values().iterator();
        }
    }
}
