/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.arithmitic;

import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.AbstractExpression;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.expression.exceptions.InvalidOperandsException;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;
import com.xar.logquery.runtime.util.ConvertFunctions;

/**
 * Created by akoekemo on 2/13/15.
 */
public class UnaryMINUS extends AbstractExpression {

    IExpression m_expression;

    public UnaryMINUS(final IExpression p_expression) {

        m_expression = p_expression;
    }

    /**
     * Get the child expressions of this expression
     *
     * @return the child expressions of this expression
     */
    @Override
    public IExpression[] getChildren() {

        return new IExpression[]{m_expression};
    }

    @Override
    public DataType getDataType() {

        return m_expression.getDataType();
    }

    @Override
    public Object getValue(final IExecutionContext p_context) {

        Object value = m_expression.getValue(p_context);
        if (value == null) { return value;}
        switch (getDataType()) {
            case INTEGER:
                return ConvertFunctions.convertValue(-((Number) value).longValue(), getDataType());
            case REAL:
            default:
                return ConvertFunctions.convertValue(-((Number) value).doubleValue(), getDataType());
        }
    }

    /**
     * Validates the expression against the specified context
     *
     * @param p_context the context to validate the expression against
     */
    @Override
    public void validateExpression(final IValidationContext p_context) throws InvalidExpressionException {

        if (!DataType.NUMBER.contains(m_expression.getDataType())) {
            throw new InvalidOperandsException(getParseData(), "Unable to apply '-' to an expression of type " + m_expression.getDataType());
        }
    }
}
