/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource.csv.interfaces;

import java.io.IOException;

/**
 * Created by Anton Koekemoer on 2014/11/28.
 */
public interface ICSVParser {

    int getLineNumber();

    void mark() throws IOException;

    String[] nextRow() throws IOException;

    void reset() throws IOException;

    void close() throws IOException;
}
