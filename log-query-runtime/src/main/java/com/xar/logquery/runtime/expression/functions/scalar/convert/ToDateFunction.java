/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.functions.scalar.convert;

import com.xar.logquery.runtime.annotations.Function;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.exceptions.UnconvertableValueException;
import com.xar.logquery.runtime.expression.functions.AbstractFunction;
import com.xar.logquery.runtime.util.ConvertFunctions;
import com.xar.logquery.runtime.util.DateTimeFunctions;
import org.joda.time.LocalDate;

import java.text.ParseException;
import java.util.Date;

import static com.xar.logquery.runtime.enums.DataType.STRING;

/**
 * Function to convert values to a date object.
 * String is parsed
 * Integer and Long is seen as milliseconds
 * Float and Double is seen as days
 */
@Function(names = {"TO_DATE", "PARSE_DATE"})
public class ToDateFunction extends AbstractFunction {

    public ToDateFunction() {

        super(DataType.DATE);

        DateConverter dateConverter = new DateConverter();
        addCallback(new DataType[]{STRING}, dateConverter);
        addCallback(new DataType[]{STRING, STRING}, new StringConverter());
        addCallback(new DataType[]{DataType.INTEGER}, dateConverter);
        addCallback(new DataType[]{DataType.REAL}, dateConverter);
        addCallback(new DataType[]{DataType.TIMESTAMP}, dateConverter);
        addCallback(new DataType[]{DataType.DATE}, dateConverter);
    }

    private static class DateConverter extends DateConverterBase {

        @Override
        public Date convert(final Object[] p_parameters) throws UnconvertableValueException {

            return ConvertFunctions.toDate(p_parameters[0]);
        }
    }

    public static abstract class DateConverterBase implements IExecuteCallback {

        @Override
        public final Object execute(final Object[] p_parameters) {

            if (p_parameters[0] == null) { return null; }

            try {
                return convert(p_parameters);
            }
            catch (UnconvertableValueException ignored) {
            }
            return null;
        }

        protected abstract Date convert(final Object[] p_parameters) throws UnconvertableValueException;

    }

    private static class StringConverter extends DateConverterBase {

        @Override
        public Date convert(final Object[] p_parameters) throws UnconvertableValueException {

            try {
                return LocalDate.fromDateFields(DateTimeFunctions.parseDate((String) p_parameters[0], (String) p_parameters[1])).toDate();
            }
            catch (ParseException ignored) {
                throw new UnconvertableValueException(ignored.getMessage(), ignored);
            }
        }
    }
}
