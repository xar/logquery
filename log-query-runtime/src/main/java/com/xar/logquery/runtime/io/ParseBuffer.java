/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.io;

/**
 * Buffer used to parse strings.
 */
public class ParseBuffer {

    /**
     * Characters
     */
    private final char[] m_chars;

    private final int m_length;

    private final int m_start;

    private int m_pos = 0;


    /**
     * Initialises the parse m_buffer with the specified string
     *
     * @param p_string The string to parse over
     */
    public ParseBuffer(final String p_string) {

        this(p_string.toCharArray());
    }

    /**
     * Initialises the parse m_buffer with the specified characters
     *
     * @param p_chars The characters to parse over
     */
    public ParseBuffer(final char[] p_chars) {

        this(p_chars, 0, p_chars.length);
    }

    /**
     * Initialises the parse m_buffer with the specified characters
     *
     * @param p_chars  The characters to parse over
     * @param p_start  The getStart position in the character array
     * @param p_length The length of the array
     */
    public ParseBuffer(final char[] p_chars, final int p_start, final int p_length) {

        m_chars = p_chars;
        m_start = p_start;
        m_length = p_length;
    }

    /**
     * Extracts the string up to the terminator
     *
     * @param p_terminator
     *
     * @return
     */
    public String extractString(final char p_terminator) {

        int start = m_pos;
        while (true) {
            int c = next();
            if (c == -1) { break; }

            if (c == p_terminator) {
                skip(-1);
                break;
            }

        }
        return new String(m_chars, start, m_pos - start);
    }

    /**
     * The the current position in the m_buffer
     *
     * @return
     */
    public int getPosition() {

        return m_pos;
    }

    /**
     * Checks ahead to see if the character following matches the character
     *
     * @param p_la The character to match
     *
     * @return true, if the character is a match, false otherwise
     */
    public boolean la(final char p_la) {

        return la(new char[]{p_la});
    }

    /**
     * Checks ahead to see if the characters following matches the string
     *
     * @param p_la The characters to match
     *
     * @return true, if te characters are a match, false otherwise
     */
    public boolean la(final String p_la) {

        return la(p_la.toCharArray());
    }

    /**
     * Returns the current character in the m_buffer, and advance by one
     *
     * @return The current character, or -1 if getEnd-of-file
     */
    public int next() {

        int c = peek(0);
        if (c >= 0) {
            m_pos++;
        }
        return c;
    }

    /**
     * Peek at the character at the specifed skip location
     *
     * @param p_skip Number of characters to skip
     *
     * @return The character at the location, or -1 id eof
     */
    public int peek(int p_skip) {

        if (m_pos + p_skip >= m_length) {
            return -1;
        }
        return m_chars[m_start + m_pos + p_skip];
    }

    /**
     * Set the position in the m_buffer
     *
     * @param p_pos The position in the m_buffer
     *
     * @return The position
     */
    public int setPosition(final int p_pos) {

        m_pos = Math.min(m_length, Math.max(0, p_pos));
        return m_pos;
    }

    /**
     * Skip the specified number of characters
     *
     * @param p_skip The number of characters to skip
     *
     * @return The number of characters skipped
     */
    public int skip(int p_skip) {

        p_skip = Math.min(m_length - m_pos, Math.max(-m_pos, p_skip));
        setPosition(m_pos + p_skip);
        return p_skip;
    }

    /**
     * Checks ahead to see if the characters following matches the string
     *
     * @param p_la The characters to match
     *
     * @return true, if the characters are a match, false otherwise
     */
    private boolean la(final char[] p_la) {

        for (int i = 0; i < p_la.length; i++) {
            if (peek(i) != p_la[i]) {
                return false;
            }
        }
        return true;
    }
}
