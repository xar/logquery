/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.functions.scalar.convert;

import com.xar.logquery.runtime.annotations.Function;
import com.xar.logquery.runtime.config.RuntimeConfiguration;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.functions.AbstractFunction;
import com.xar.logquery.runtime.util.DateTimeFunctions;
import org.joda.time.LocalDate;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.xar.logquery.runtime.enums.DataType.*;

/**
 * Function to convert a value to a string
 */
@Function(
        names = {"TO_STRING"}
)
public class ToStringFunction extends AbstractFunction {


    public ToStringFunction() {

        super(STRING);

        IExecuteCallback numberFormatter = new NumberFormatter();

        IExecuteCallback dateFormatter = new DateFormatter();

        IExecuteCallback timestampFormatter = new TimestampFormatter();

        addCallback(new DataType[]{INTEGER}, numberFormatter);
        addCallback(new DataType[]{REAL}, numberFormatter);
        addCallback(new DataType[]{DATE}, dateFormatter);
        addCallback(new DataType[]{TIMESTAMP}, timestampFormatter);
        addCallback(new DataType[]{INTEGER, STRING}, numberFormatter);
        addCallback(new DataType[]{REAL, STRING}, numberFormatter);
        addCallback(new DataType[]{DATE, STRING}, dateFormatter);
        addCallback(new DataType[]{TIMESTAMP, STRING}, timestampFormatter);


        addCallback(new DataType[]{STRING}, new IExecuteCallback() {

            @Override
            public Object execute(final Object[] p_parameters) {

                return p_parameters[0];
            }
        });

    }

    private static class DateFormatter implements IExecuteCallback {

        @Override
        public Object execute(final Object[] p_parameters) {

            if (p_parameters[0] == null) { return null; }

            if (p_parameters.length == 1) {
                return DateTimeFunctions.toString(LocalDate.fromDateFields((Date) p_parameters[0]).toDate(), RuntimeConfiguration.getDateFormat());
            }
            return DateTimeFunctions.toString(LocalDate.fromDateFields((Date) p_parameters[0]).toDate(), (String) p_parameters[1]);
        }
    }

    private static class NumberFormatter implements IExecuteCallback {

        Map<String, DecimalFormat> formats = new HashMap<>();

        @Override
        public Object execute(final Object[] p_parameters) {

            if (p_parameters[0] == null) { return null; }

            if (p_parameters.length == 1) {
                return String.valueOf(p_parameters[0]);
            }

            String format = (String) p_parameters[1];
            DecimalFormat fmt = formats.get(format);
            if (fmt == null) {
                fmt = new DecimalFormat(format);
                formats.put(format, fmt);
            }

            return fmt.format(((Number) p_parameters[0]).doubleValue());
        }
    }

    private static class TimestampFormatter implements IExecuteCallback {

        @Override
        public Object execute(final Object[] p_parameters) {

            if (p_parameters[0] == null) { return null; }

            if (p_parameters.length == 1) {
                return DateTimeFunctions.toString((Date) p_parameters[0], RuntimeConfiguration.getTimestampFormat());
            }
            return DateTimeFunctions.toString((Date) p_parameters[0], (String) p_parameters[1]);
        }
    }
}
