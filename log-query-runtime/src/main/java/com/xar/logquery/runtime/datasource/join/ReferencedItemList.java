/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource.join;

import java.util.AbstractList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by akoekemo on 3/18/15.
 */
public class ReferencedItemList<T> extends AbstractList<T> {

    List<T> unreferencedItems;

    List<T> referencedItems;


    public ReferencedItemList() {

        unreferencedItems = new LinkedList<>();
        referencedItems = new LinkedList<>();
    }

    /**
     * Appends the specified element to the end of this list (optional
     * operation).
     * <p/>
     * <p>Lists that support this operation may place limitations on what
     * elements may be added to this list.  In particular, some
     * lists will refuse to add null elements, and others will impose
     * restrictions on the type of elements that may be added.  List
     * classes should clearly specify in their documentation any restrictions
     * on what elements may be added.
     * <p/>
     * <p>This implementation calls {@code add(size(), e)}.
     * <p/>
     * <p>Note that this implementation throws an
     * {@code UnsupportedOperationException} unless
     * {@link #add(int, Object) add(int, E)} is overridden.
     *
     * @param e element to be appended to this list
     *
     * @return {@code true} (as specified by {@link Collection#add})
     *
     * @throws UnsupportedOperationException if the {@code add} operation
     *                                       is not supported by this list
     * @throws ClassCastException            if the class of the specified element
     *                                       prevents it from being added to this list
     * @throws NullPointerException          if the specified element is null and this
     *                                       list does not permit null elements
     * @throws IllegalArgumentException      if some property of this element
     *                                       prevents it from being added to this list
     */
    @Override
    public boolean add(final T e) {

        return unreferencedItems.add(e);
    }

    /**
     * {@inheritDoc}
     *
     * @param index
     *
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */
    @Override
    public T get(final int index) {

        if (index >= unreferencedItems.size()) {
            return referencedItems.get(index - unreferencedItems.size());
        }
        return unreferencedItems.get(index);
    }

    @Override
    public int size() {

        return unreferencedItems.size() + referencedItems.size();
    }

    /**
     * Returns an iterator over the elements in this list in proper sequence.
     * <p/>
     * <p>This implementation returns a straightforward implementation of the
     * iterator interface, relying on the backing list's {@code size()},
     * {@code get(int)}, and {@code remove(int)} methods.
     * <p/>
     * <p>Note that the iterator returned by this method will throw an
     * {@link UnsupportedOperationException} in response to its
     * {@code remove} method unless the list's {@code remove(int)} method is
     * overridden.
     * <p/>
     * <p>This implementation can be made to throw runtime exceptions in the
     * face of concurrent modification, as described in the specification
     * for the (protected) {@link #modCount} field.
     *
     * @return an iterator over the elements in this list in proper sequence
     */
    @Override
    public ReferencedItemIterator<T> iterator() {

        return new ReferencedItemIterator(unreferencedItems, referencedItems);
    }
}
