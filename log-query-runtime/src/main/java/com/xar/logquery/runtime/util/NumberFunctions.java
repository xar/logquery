/*
 * Copyright 2014 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

/**
 * Created by Anton Koekemoer on 2014/12/12.
 */
public class NumberFunctions {

    private static NumberFunctions _instance = new NumberFunctions();

    private NumberFunctions() {

    } // Prevent instance creation

    /**
     * Tests to see if the string represents a floating point value
     *
     * @param p_value the value o test
     *
     * @return true if the value can be parsed as a float, false otherwise
     */
    public static boolean isFloatingPoint(String p_value) {

        if (p_value == null) { return false; }
        return p_value.matches("^-?\\d+(\\.\\d+)?$");
    }

    /**
     * Tests to see if the string represents an integer type
     *
     * @param p_value the value o test
     *
     * @return true if the value can be parsed as an integer, false otherwise
     */
    public static boolean isIntegral(String p_value) {

        if (p_value == null) { return false; }
        return p_value.matches("^-?\\d+$");
    }

}
