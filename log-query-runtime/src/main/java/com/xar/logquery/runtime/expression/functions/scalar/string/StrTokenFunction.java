/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.functions.scalar.string;

import com.xar.logquery.runtime.annotations.Function;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.functions.AbstractFunction;

import static com.xar.logquery.runtime.enums.DataType.INTEGER;
import static com.xar.logquery.runtime.enums.DataType.STRING;

/**
 * Created by koekemoera on 2014/09/05.
 */
@Function(
        names = {"STRTOK", "EXTRACT_TOKEN"}
)
public class StrTokenFunction extends AbstractFunction {


    public StrTokenFunction() {

        super(STRING);

        super.addCallback(new DataType[]{STRING, STRING, INTEGER}, new IExecuteCallback() {

            @Override
            public Object execute(final Object[] p_parameters) {

                String text = (String) p_parameters[0];
                String token = (String) p_parameters[1];
                int index = ((Number) p_parameters[2]).intValue();

                return extractToken(text, token, index);
            }
        });
    }

    private Object extractToken(final String p_text, final String p_token, int p_index) {

        if (p_text == null) return null;

        String[] data = p_text.split(p_token);

        if (p_index < 0) {
            p_index = data.length + p_index;
        }
        if (p_index >= 0) {
            if (p_index < data.length) { return data[p_index]; }
        }
        return null;
    }
}
