/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource;

import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.exceptions.RequiredAttributeMissing;
import com.xar.logquery.runtime.exceptions.UnknownAttributeException;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;
import com.xar.logquery.runtime.util.RuntimeFunctions;

import java.util.Map;

/**
 * Created by akoekemo on 3/4/15.
 */
public abstract class AbstractDataSource implements IDataSource {

    private Map<String, IExpression> m_attributes;

    /**
     * Open the data source and return a resultset.
     *
     * @param p_context the execution context
     *
     * @return a new resultset
     */
    @Override
    public final IResultSet getResultSet(final IExecutionContext p_context) throws DataSourceException {

        if (m_attributes != null) {
            try {
                RuntimeFunctions.initialiseDataSource(this, m_attributes, p_context);
            }
            catch (RequiredAttributeMissing | UnknownAttributeException e) {
                throw new DataSourceException(e.getMessage(), e);
            }
        }
        return executeQuery(p_context);
    }

    /**
     * Set the attributes for the datasource. These are assigned to the datasource during execution
     *
     * @param p_attributes the attributes
     */
    @Override
    public void setAttributes(final Map<String, IExpression> p_attributes) {

        m_attributes = p_attributes;
    }

    /**
     * Bind the datasource to the execution context
     *
     * @param p_context the binding context
     *
     * @throws com.xar.logquery.runtime.exceptions.DataSourceException if the datasource is invalid
     */
    @Override
    public final void validate(final IValidationContext p_context) throws DataSourceException {

        if (m_attributes != null) {
            for (IExpression expression : m_attributes.values()) {
                expression.validate(p_context);
            }
        }
        validateQuery(p_context);
    }

    /**
     * Execute the query and returns a resultset
     *
     * @param p_context the execution context
     *
     * @return the resultset
     *
     * @throws com.xar.logquery.runtime.exceptions.DataSourceException if the execution fails
     */
    protected abstract IResultSet executeQuery(final IExecutionContext p_context) throws DataSourceException;

    /**
     * Validate the datasource
     *
     * @param p_context the binding context
     *
     * @throws com.xar.logquery.runtime.exceptions.DataSourceException if the datasource is invalid
     */
    protected abstract void validateQuery(final IValidationContext p_context) throws DataSourceException;
}
