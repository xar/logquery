/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.logic;

import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.AbstractExpression;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;

/**
 * Created by koekemoera on 2014/08/28.
 */
public class NotExpression extends AbstractExpression {

    IExpression m_expression;

    public NotExpression(final IExpression p_expression) {

        m_expression = p_expression;
    }

    /**
     * Get the child expressions of this expression
     *
     * @return the child expressions of this expression
     */
    @Override
    public IExpression[] getChildren() {

        return new IExpression[]{m_expression};
    }

    /**
     * Get the datatype of the expression
     *
     * @return the datatype of the expression
     */
    @Override
    public DataType getDataType() {

        return DataType.BOOLEAN;
    }

    /**
     * Get the valueExpression of the expression, based on the current row context
     *
     * @param p_context the current row context
     *
     * @return the valueExpression of the expression.
     */
    @Override
    public Object getValue(final IExecutionContext p_context) {

        return !((Boolean) m_expression.getValue(p_context));
    }

    /**
     * Validates the expression against the specified context
     *
     * @param p_context the context to validate the expression against
     */
    @Override
    public void validateExpression(final IValidationContext p_context) throws InvalidExpressionException {

        if (m_expression.getDataType() != DataType.BOOLEAN) {
            throw new InvalidExpressionException(m_expression.getParseData(), "Unable to apply NOT to " + m_expression.getDataType());
        }
    }
}
