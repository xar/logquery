/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource.pivot;

/**
 * Created by akoekemo on 3/12/15.
 */

import com.xar.logquery.runtime.expression.functions.aggregate.interfaces.IAggregateExpression;

import java.util.ArrayList;
import java.util.List;

/**
 * Class used to hold the data for a row, while the pivot is being calculated
 */
class PivotRow {

    /**
     * The row heading data
     */
    Object[] rowHeading;



    /**
     * The aggregate expressions used when building pivots
     */
    private IAggregateExpression[] pivotAggregates;

    /**
     * Aggregate state objects for each column in the pivot
     */
    List<Object[]> pivotedColumns = new ArrayList<>();

    public Object[] headingAggregates;

    /**
     * Initialise a new Pivot row
     * @param p_rowHeading the values of the row heading
     * @param p_numberOfPivots the number of columns to create
     */
    public PivotRow(final Object[] p_rowHeading, IAggregateExpression[] p_headingAggregates, final int p_numberOfPivots, IAggregateExpression[] p_pivotAggregates) {

        rowHeading = p_rowHeading;
        pivotAggregates = p_pivotAggregates;
        headingAggregates = new Object[p_headingAggregates.length];

        for (int i = 0; i < p_headingAggregates.length; i++) {
            headingAggregates[i] = p_headingAggregates[i].newState();
        }

        addPivot(p_numberOfPivots);
    }

    /**
     * Adds a new Pivot to the row
     */
    public void addPivot() {

        Object[] state = new Object[pivotAggregates.length];
        for (int i = 0; i < state.length; i++) {
            state[i] = pivotAggregates[i].newState();
        }
        pivotedColumns.add(state);
    }

    /**
     * Adds pivots to the row up to the specified number
     * @param p_numberOfPivots the number of pivots required
     */
    public void addPivot(final int p_numberOfPivots) {

        for (int i = pivotedColumns.size(); i < p_numberOfPivots; i++) { addPivot(); }
    }
}
