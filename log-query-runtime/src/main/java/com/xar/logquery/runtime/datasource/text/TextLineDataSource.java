/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource.text;

import com.xar.logquery.runtime.AliasableColumnInfo;
import com.xar.logquery.runtime.DataRow;
import com.xar.logquery.runtime.datasource.annotations.DataSourceAttribute;
import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.io.FileLineReader;
import com.xar.logquery.runtime.util.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class TextLineDataSource extends TextFileDataSource<FileLineReader> {

    /**
     * Pattern for line starts
     */
    @DataSourceAttribute(name = "linePattern", description = "A regular expression, used to recognise the start of a line", required = false)
    private String m_linePattern;

    /**
     * Instantiates a new Text file datasource.
     *
     * @param p_filePattern the file pattern
     * @param p_alias       the alias
     */
    public TextLineDataSource(final String p_filePattern, final String p_alias) {

        super(p_filePattern, p_alias);

        setColumnInfo(new IColumnInfo[]{
                new AliasableColumnInfo(this, "FileName", DataType.STRING),
                new AliasableColumnInfo(this, "RowNumber", DataType.INTEGER),
                new AliasableColumnInfo(this, "Text", DataType.STRING)
        });
    }

    /**
     * Closes the file reader
     *
     * @param p_fileLineReader the file reader
     */
    @Override
    protected void closeFileReader(final FileLineReader p_fileLineReader) {

        try {
            p_fileLineReader.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Instantiates a new Text file datasource.
     */
    public TextLineDataSource() {

        this("*.*", "");
    }

    /**
     * Assign the line pattern for the data source
     *
     * @param p_linePattern the line pattern
     */
    public void setLinePattern(final String p_linePattern) {

        m_linePattern = p_linePattern;
    }

    /**
     * Create the reader appropriate for this datasource
     *
     * @param p_fileName the file to be read
     *
     * @return the reader
     *
     * @throws java.io.FileNotFoundException the file not found exception
     */
    @Override
    protected FileLineReader createFileReader(final File p_fileName) throws FileNotFoundException {

        return new FileLineReader(p_fileName);
    }

    /**
     * Construct a row from the data
     *
     * @param p_text the text
     *
     * @return a new data row
     */
    @Override
    protected IDataRow readLine(final FileLineReader p_text) throws IOException {

        String line;
        int lineNumberAtStart = 0;
        if (m_linePattern == null) {
            line = p_text.readLine();
            lineNumberAtStart = p_text.getLineNumber();
        }
        else {
            StringBuilder builder = new StringBuilder();
            int lines = 0;
            while (true) {
                p_text.mark();
                line = p_text.readLine();

                if (line == null) { break; }
                if (line.matches(m_linePattern)) {
                    if (lines == 0) {
                        lineNumberAtStart = p_text.getLineNumber();
                        builder.append(line);
                        lines++;
                    }
                    else {
                        p_text.reset();
                        break;
                    }
                }
                else if (lines > 0) {
                    lines++;
                    builder.append('\n').append(line);
                }
            }

            line = lines > 0 ? builder.toString() : null;
        }
        if (line != null) {
            return new DataRow(getColumnInfo(), new Object[]{p_text.getFile().getAbsolutePath(),
                                                             (long) lineNumberAtStart,
                                                             StringUtils.expandTabs(line, 4)});
        }

        return null;
    }

}
