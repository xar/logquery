/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.interfaces;

import com.xar.logquery.runtime.exceptions.DataSourceException;

import java.util.Map;

/**
 * Minimum interface requirement for an initialisable datasource
 */
public interface IInitialisable {

    /**
     * Initialise the data source.
     * <p/>
     * SELECT * FROM &lt;name&gt;(param1=value, param2=value,...)
     *
     * @param p_parameters the parameters for the datasource
     *
     * @throws DataSourceException if there is a problem with the initialisation
     */
    public void initialise(final Map<String, Object> p_parameters) throws DataSourceException, DataSourceException;

}
