/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.output.csv;

import com.xar.cli.annotations.CommandLineArgument;
import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.output.interfaces.IOutputFormatter;
import com.xar.logquery.runtime.util.FormatFunctions;

import java.util.Enumeration;

/**
 * Formats a datasource into RFC-4180 compliant CSV format.
 * <p/>
 * Created by Anton Koekemoer on 2014/12/01.
 */
public class CSVOutputFormatter implements IOutputFormatter {

    @CommandLineArgument(name = "noheadings", description = "if set to true, will include headings in the output, otherwise no headings will be created")
    private boolean noheadings = false;

    @Override
    public void format(final IDataSource p_dataSource) throws DataSourceException {

        IResultSet resultSet = p_dataSource.getResultSet(new ExecutionContext());
        IColumnInfo[] columnInfo = resultSet.getColumnInfo();
        StringBuilder line = new StringBuilder();
        if (!noheadings) {
            for (int i = 0; i < columnInfo.length; i++) {
                line.append(formatValue(columnInfo[i].getName(), DataType.STRING)).append(',');
            }
            if (line.length() > 0) {
                System.out.println(line.delete(line.length() - 1, line.length()));
            }
            line.setLength(0);
        }
        for (Enumeration<IDataRow> rows = resultSet.getRows(); rows.hasMoreElements(); ) {
            IDataRow row = rows.nextElement();

            Object[] data = row.getData();

            for (int i = 0; i < columnInfo.length; i++) {
                line.append(formatValue(
                        data == null || i >= data.length ? null : data[i], columnInfo[i].getDataType())).append(',');
            }

            if (line.length() > 0) {
                System.out.println(line.delete(line.length() - 1, line.length()));
            }
            line.setLength(0);
        }
    }

    private String formatValue(final Object p_value, final DataType p_dataType) {

        switch (p_dataType) {
            case DATE:
            case TIMESTAMP:
            case INTEGER:
            case REAL:
                return FormatFunctions.asString(p_value, p_dataType);
            case STRING:
            default:

                char[] chars = p_value.toString().toCharArray();
                boolean delimit = false;
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < chars.length; i++) {
                    switch (chars[i]) {
                        case '\'':
                        case '"':
                            builder.append(chars[i]);
                        case '\r':
                        case '\n':
                            delimit = true;
                        default:
                            builder.append(chars[i]);
                    }
                }
                if (delimit) {
                    builder.insert(0, '"').append('"');
                }
                return builder.toString();
        }
    }
}
