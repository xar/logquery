/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource.join;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Iterator used to keep track of the referenced items in a list. If an itm is marked as referenced, it
 * will be removed from the unreferenced list, and added to the referenced list.
 */
public class ReferencedItemIterator<T> implements Iterator<T> {

    private final Iterator<T> m_unreferencedIterator;

    private final int m_itemCount;

    private final List<T> m_referencedItems;

    private Iterator<T> m_referencedIterator;

    private int itemNumber = 0;

    private T m_lastItem;

    /**
     * Initialises a new ReferencedItem iterator
     *
     * @param p_unreferencedItems the list containing the referenced items
     * @param p_referencedItems   the list containing the unreferenced items
     */
    public ReferencedItemIterator(final List<T> p_unreferencedItems, final List<T> p_referencedItems) {

        m_referencedItems = p_referencedItems;
        m_itemCount = p_unreferencedItems.size() + p_referencedItems.size();
        m_unreferencedIterator = p_unreferencedItems.iterator();
    }

    /**
     * Returns {@code true} if the iteration has more elements.
     * (In other words, returns {@code true} if {@link #next} would
     * return an element rather than throwing an exception.)
     *
     * @return {@code true} if the iteration has more elements
     */
    @Override
    public boolean hasNext() {

        return itemNumber < m_itemCount;
    }

    /**
     * Returns the next element in the iteration.
     *
     * @return the next element in the iteration
     *
     * @throws NoSuchElementException if the iteration has no more elements
     */
    @Override
    public T next() {

        m_lastItem = null;
        if (!hasNext()) { throw new NoSuchElementException(); }

        if (m_unreferencedIterator.hasNext()) {
            m_lastItem = m_unreferencedIterator.next();
            itemNumber++;
            return m_lastItem;
        }
        itemNumber++;
        if (m_referencedIterator == null) { m_referencedIterator = m_referencedItems.iterator(); }

        return m_referencedIterator.next();
    }

    /**
     * If the last item was returned from the unreferenced list, removes
     * from the underlying unreferenced collection the last element returned
     * by this iterator and adds it to the referenced collection.  This method
     * can be called only once per call to {@link #next}.  The behavior of an
     * iterator is unspecified if the underlying collection is modified while
     * the iteration is in progress in any way other than by calling this
     * method.
     *
     * @throws IllegalStateException if the {@code next} method has not
     *                               yet been called, or the {@code remove} method has already
     *                               been called after the last call to the {@code next}
     *                               method
     */
    public void referenced() {

        if (m_lastItem != null) {
            m_unreferencedIterator.remove();
            m_referencedItems.add(m_lastItem);
            m_lastItem = null;
        }
//        else
//            throw new IllegalStateException();
//        }
    }

    /**
     * Removes from the underlying collection the last element returned
     * by this iterator (optional operation).  This method can be called
     * only once per call to {@link #next}.  The behavior of an iterator
     * is unspecified if the underlying collection is modified while the
     * iteration is in progress in any way other than by calling this
     * method.
     *
     * @throws UnsupportedOperationException if the {@code remove}
     *                                       operation is not supported by this iterator
     * @throws IllegalStateException         if the {@code next} method has not
     *                                       yet been called, or the {@code remove} method has already
     *                                       been called after the last call to the {@code next}
     *                                       method
     */
    @Override
    public void remove() {

        throw new UnsupportedOperationException();
    }
}
