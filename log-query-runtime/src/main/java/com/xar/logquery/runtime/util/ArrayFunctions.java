/*
 * Copyright 2014 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.util;

import java.lang.reflect.Array;

/**
 * Created by Anton Koekemoer on 2014/11/26.
 */
@SuppressWarnings("SuspiciousSystemArraycopy")
public class ArrayFunctions {

    private static ArrayFunctions _instance = new ArrayFunctions();

    private ArrayFunctions() {

    }// Prevent construction

    /**
     * Add the specified value to the array
     *
     * @param p_array The add the value to
     * @param p_value The value to add to the array
     *
     * @return
     */
    public static <T> T[] aAdd(final T[] p_array, final T p_value) {

        T[] result = aSize(p_array, p_array.length + 1);
        result[p_array.length] = p_value;
        return result;
    }

    /**
     * Deletes a number of elements from the array
     */
    public static <T> T[] aDel(T[] array, int beginIndex, int length) {

        Class aClass = array.getClass();

        int endIndex = beginIndex + length;

        // assray size
        int size = Array.getLength(array);
        if (beginIndex > size - 1 || endIndex >= size) {
            throw new ArrayIndexOutOfBoundsException();
        }

        // Create an array of the same size as the supplied array
        T[] aReturn = (T[]) Array.newInstance(aClass.getComponentType(), size - (endIndex - beginIndex));

        System.arraycopy(array, 0, aReturn, 0, beginIndex);
        System.arraycopy(array, endIndex, aReturn, beginIndex, size - endIndex);


        // Return the array with the filters
        return aReturn;
    }

    /**
     * Resize the array to the specified length
     *
     * @param p_array
     * @param p_length
     *
     * @return
     */
    public static <T> T[] aSize(final T[] p_array, final int p_length) {

        if (p_array.length == p_length) {
            return p_array;
        }

        Object temp = Array.newInstance(p_array.getClass().getComponentType(), p_length);
        System.arraycopy(p_array, 0, temp, 0, Math.min(p_length, p_array.length));
        return (T[]) temp;
    }

    /**
     * Concatenate the two arrays
     *
     * @param a   the left side
     * @param b   the right side
     * @param <T>
     *
     * @return
     */
    public static <T> T[] concatenate(T[] a, T[] b) {

        int aLen = a.length;
        int bLen = b.length;

        @SuppressWarnings("unchecked")
        T[] c = (T[]) Array.newInstance(a.getClass().getComponentType(), aLen + bLen);
        System.arraycopy(a, 0, c, 0, aLen);
        System.arraycopy(b, 0, c, aLen, bLen);

        return c;
    }

    public static <T> T[] subArray(T[] p_array, int p_start) {

        return subArray(p_array, p_start, Array.getLength(p_array));
    }

    /**
     * Like substring for arrays
     *
     * @param p_array    the array
     * @param p_start    the start index
     * @param p_endIndex the length
     *
     * @return the requested sub-array
     */
    public static <T> T[] subArray(final T[] p_array, final int p_start, final int p_endIndex) {

        Class type = p_array.getClass();
        T[] result = (T[]) Array.newInstance(type.getComponentType(), p_endIndex - p_start);
        System.arraycopy(p_array, p_start, result, 0, p_endIndex - p_start);
        return result;
    }

}
