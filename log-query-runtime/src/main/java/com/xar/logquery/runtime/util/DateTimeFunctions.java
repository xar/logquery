/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.util;

import com.xar.logquery.runtime.config.RuntimeConfiguration;
import com.xar.logquery.runtime.config.entities.DateFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

/**
 * Functions for working with dates and times
 */
public class DateTimeFunctions {

    public static final int MILLIS_PER_DAY = 86400000;

    /**
     * The date formatter.
     */
    private static final ThreadLocal<Map<String, SimpleDateFormat>> s_dateFormatters = new ThreadLocal<Map<String, SimpleDateFormat>>() {

        @Override
        protected Map<String, SimpleDateFormat> initialValue() {

            return new HashMap<>();
        }
    };

    private static DateTimeFunctions _instance = new DateTimeFunctions();

    private DateTimeFunctions() {

    } // Prevent instantiation


    /**
     * Gets the formatter for the specified timezone and format.
     *
     * @param p_timeZone the p_time zone
     * @param p_format   the p_format
     *
     * @return the format
     */
    public static SimpleDateFormat getFormat(String p_timeZone, String p_format) {

        final Map<String, SimpleDateFormat> formats = s_dateFormatters.get();
        final String key = p_timeZone + " - " + p_format;
        SimpleDateFormat format = formats.get(key);
        if (format == null) {
            synchronized (formats) {
                format = formats.get(key);
                if (format == null) {
                    format = new SimpleDateFormat(p_format);
                    if (p_timeZone != null) {
                        format.setTimeZone(TimeZone.getTimeZone(p_timeZone));
                    }
                    formats.put(key, format);
                }

            }
        }
        return format;
    }

    public static Date parseDate(final String p_text) throws ParseException {

        return parseDate(p_text, selectTimestampFormat(p_text));
    }

    /**
     * Parses the date using the specified format and the default timezone
     *
     * @param p_date   the date to parse
     * @param p_format the format oft he date
     *
     * @return the date, parsed using the specified format and the default timezone
     *
     * @throws java.text.ParseException
     */
    public static Date parseDate(String p_date, String p_format) throws ParseException {

        return parseDate(p_date, p_format, null);
    }

    /**
     * Parses the date using the specified format and  timezone
     *
     * @param p_date     the date to parse
     * @param p_format   the format oft he date
     * @param p_timeZone the time zone
     *
     * @return the date, parsed using the specified format and timezone
     *
     * @throws java.text.ParseException
     */
    public static Date parseDate(String p_date, String p_format, String p_timeZone) throws ParseException {

        final SimpleDateFormat format = getFormat(p_timeZone, p_format);

        return format.parse(p_date);
    }

    /**
     * Matches the date to determine the format
     *
     * @param p_text the date text
     *
     * @return a format for the date
     */
    public static String selectTimestampFormat(final String p_text) {

        for (DateFormat format : RuntimeConfiguration.getDateFormats()) {
            if (p_text.matches("^" + format.getPattern() + "$")) { return format.getFormatString(); }
        }

        return "yyyy-MM-dd HH:mm:ss.SSS";
    }

    /**
     * Convert the date to a string, using the specified format and the default timezone
     *
     * @param p_date   the date to convert
     * @param p_format the format to use
     *
     * @return a string representation of the date, using the specified format and the default timezone
     */
    public static String toString(Date p_date, String p_format) {

        return toString(p_date, p_format, null);
    }

    /**
     * Convert the date to a string, using the specified format and timezone
     *
     * @param p_date     the date to convert
     * @param p_format   the format to use
     * @param p_timeZone the time zone
     *
     * @return a string representation of the date, using the specified format and time timezone
     */
    public static String toString(Date p_date, String p_format, String p_timeZone) {

        final SimpleDateFormat format = getFormat(p_timeZone, p_format);
        return format.format(p_date);
    }

}
