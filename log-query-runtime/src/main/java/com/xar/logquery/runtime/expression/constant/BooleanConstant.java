/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.constant;

import com.xar.logquery.runtime.enums.DataType;

/**
 * Created by koekemoera on 2014/11/19.
 */
public class BooleanConstant extends ConstantExpression<Boolean> {

    /**
     * True constant
     */
    public static final BooleanConstant TRUE = new BooleanConstant(true);

    /**
     * False constant
     */
    public static final BooleanConstant FALSE = new BooleanConstant(false);

    /**
     * Instantiates a new Boolean Constant expression with the specified value
     *
     * @param p_value the constant value
     */
    protected BooleanConstant(final Boolean p_value) {

        super(DataType.BOOLEAN, p_value);
    }
}
