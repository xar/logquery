package com.xar.logquery.runtime.datasource.filter;

import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.datasource.interfaces.IFilter;
import com.xar.logquery.runtime.datasource.interfaces.IFilterFactory;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by akoekemo on 11/13/15.
 */
public class DistinctFilterFactory implements IFilterFactory{

    /**
     * Create a filter
     *
     * @return a new filter
     */
    @Override
    public IFilter create() {

        return new DistinctFilter();
    }

    @Override
    public void validate(IValidationContext p_context) throws DataSourceException {

    }

    /**
     * Filter for Distinct row processing
     */
    private static class DistinctFilter implements IFilter {

        /**
         * StringBuilder to build a key
         */
        private StringBuilder m_builder = new StringBuilder();

        /**
         * Distinct hash set
         */
        private Set<String> rows = new HashSet<>();


        /**
         * Build a key for the row
         *
         * @param p_rowData the row data
         *
         * @return a key for the row
         */
        private String getKey(final Object[] p_rowData) {

            m_builder.setLength(0);

            for (Object value : p_rowData) {m_builder.append('|').append(value);}

            return m_builder.toString();
        }

        /**
         * Tests the row to see if we have already returned a row with these values
         *
         * @param p_context    An execution context for the current operation. Implementers do not need to wrap it or reset. This context is ready to use.
         * @param p_columnInfo the column info for the data
         * @param p_rowData    the data
         *
         * @return true if the row is accepted; false otherwise
         */
        @Override
        public boolean accept(final IExecutionContext p_context, final IColumnInfo[] p_columnInfo, final Object[] p_rowData) {

            String key = getKey(p_rowData);

            if (rows.contains(key)) { return false; }

            rows.add(key);

            return true;
        }
    }
    
}
