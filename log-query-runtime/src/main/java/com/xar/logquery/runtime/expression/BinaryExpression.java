/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression;

import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.exceptions.InvalidOperandsException;
import com.xar.logquery.runtime.expression.functions.AbstractFunction;
import com.xar.logquery.runtime.expression.functions.FunctionParameter;
import com.xar.logquery.runtime.expression.functions.exceptions.InvalidParametersException;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.expression.interfaces.IFunctionParameter;

/**
 * Base class fro all binary expressions
 */
public class BinaryExpression extends AbstractFunction {

    protected BinaryExpression(IExpression p_lhs, IExpression p_rhs) {

        this(DataType.UNDEFINED, p_lhs, p_rhs);
    }

    protected BinaryExpression(DataType p_dataType, IExpression p_lhs, IExpression p_rhs) {

        super(p_dataType, new FunctionParameter(p_lhs), new FunctionParameter(p_rhs));
    }

    protected BinaryExpression() {

        this(DataType.UNDEFINED);
    }

    public BinaryExpression(final DataType p_dataType) {

        super(p_dataType);
    }

    /**
     * Add the callback to the super, also inverts the parameters
     *
     * @param p_result     the result datatype
     * @param p_parameters the input parameter types
     * @param p_callback   the callback to invoke
     */
    protected void addInvertedCallback(final DataType p_result, DataType[] p_parameters, final IExecuteCallback p_callback) {

        addCallback(p_result, p_parameters, p_callback);
        if (p_parameters[0] != p_parameters[1]) {
            addCallback(p_result, new DataType[]{p_parameters[1], p_parameters[0]}, p_callback);
        }
    }

    @Override
    protected void throwInvalidParameters(final DataType[][] p_expected, final IFunctionParameter[] p_functionParameters) throws InvalidParametersException {

        throw new InvalidOperandsException(getParseData(), "Incompatible operands " + m_functionParameters[0].getDataType() + " and " + m_functionParameters[1].getDataType());
    }

    /**
     * Base class for binary operation callbacks
     */
    protected static abstract class BinaryCallback implements IExecuteCallback {

        @Override
        public Object execute(final Object[] p_parameters) {

            if (p_parameters[0] == null || p_parameters[1] == null) {
                return null;
            }
            return doOperation(p_parameters[0], p_parameters[1]);
        }

        protected abstract Object doOperation(final Object p_lhs, final Object p_rhs);
    }

}
