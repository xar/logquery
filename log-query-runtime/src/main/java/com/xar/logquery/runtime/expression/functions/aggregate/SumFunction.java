/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.functions.aggregate;


import com.xar.logquery.runtime.annotations.Function;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.expression.exceptions.UnconvertableValueException;
import com.xar.logquery.runtime.expression.functions.aggregate.interfaces.IAggregator;
import com.xar.logquery.runtime.expression.functions.aggregate.interfaces.IAggregatorFactory;
import com.xar.logquery.runtime.expression.functions.exceptions.InvalidParametersException;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;
import com.xar.logquery.runtime.util.ConvertFunctions;
import com.xar.logquery.runtime.util.RuntimeFunctions;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Created by koekemoera on 2014/08/28.
 */
@Function(names = {"SUM"})
public class SumFunction extends AggregateFunctionBase {

    /**
     * Validates the expression against the specified context
     *
     * @param p_context the context to validate the expression against
     */
    @Override
    public void validateExpression(final IValidationContext p_context) throws InvalidExpressionException {

        if (m_functionParameters.length == 1 && DataType.NUMBER.contains(m_functionParameters[0].getDataType())) {
        }
        else {
            // Not optimal, but no need to optimise here, as this will almost never be called, and will
            // not affect performance
            DataType[] numberTypes = DataType.NUMBER.toArray(new DataType[DataType.NUMBER.size()]);
            DataType[][] expected = new DataType[numberTypes.length][];
            for (int i = 0; i < numberTypes.length; i++) {
                expected[i] = new DataType[]{numberTypes[i]};
            }

            throw new InvalidParametersException(getParseData(), "Invalid parameters in function call.\n\n" + RuntimeFunctions.buildParameterErrorMessage(getClass(), m_functionParameters, expected));
        }

        switch (m_functionParameters[0].getDataType()) {
            case INTEGER:
                setDataType(DataType.INTEGER);
                break;
            case REAL:
            default:
                setDataType(DataType.REAL);
        }
        setAggregatorFactory(new Factory());
    }

    private class Factory implements IAggregatorFactory {


        /**
         * Creates a new Aggregator
         *
         * @return the aggregator
         */
        @Override
        public IAggregator create() {

            switch (getDataType()) {
                case INTEGER:
                    return new IntegerSumAggregator();
                case REAL:
                default:
                    return new RealSumAggregator();
            }
        }
    }

    /**
     * Average value aggregator. Ignores nulls
     */
    public class IntegerSumAggregator implements IAggregator {

        private BigInteger total;

        /**
         * Aggregate the value
         *
         * @param p_context the current expression context
         */
        @Override
        public void aggregate(final IExecutionContext p_context) {

            Object value = m_functionParameters[0].getValue(p_context);
            if (value == null) { return; }
            try {
                if (total == null) {
                    total = ConvertFunctions.toInteger(value);
                }
                else {
                    total = total.add(ConvertFunctions.toInteger(value));
                }
            }
            catch (UnconvertableValueException ignored) {
            }
        }

        /**
         * Get the value for the aggregate
         *
         * @return the value of the aggregate
         */
        @Override
        public Object getValue() {

            return total;
        }
    }

    /**
     * Average value aggregator. Ignores nulls
     */
    public class RealSumAggregator implements IAggregator {

        private BigDecimal total;

        /**
         * Aggregate the value
         *
         * @param p_context the current expression context
         */
        @Override
        public void aggregate(final IExecutionContext p_context) {

            Object value = m_functionParameters[0].getValue(p_context);
            if (value == null) { return; }
            try {
                if (total == null) {
                    total = ConvertFunctions.toReal(value);
                }
                else {
                    total = total.add(ConvertFunctions.toReal(value));
                }
            }
            catch (UnconvertableValueException ignored) {
            }
        }

        /**
         * Get the value for the aggregate
         *
         * @return the value of the aggregate
         */
        @Override
        public Object getValue() {

            return total;
        }
    }
}
