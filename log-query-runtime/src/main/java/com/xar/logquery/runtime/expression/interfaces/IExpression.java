/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression.interfaces;

import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;
import com.xar.logquery.runtime.parse.ParseData;

/**
 * Minimum interface requirement for an expression
 */
public interface IExpression {

    /**
     * Get the child expressions of this expression
     *
     * @return the child expressions of this expression
     */
    public IExpression[] getChildren();

    /**
     * Get the datatype of the expression
     *
     * @return the datatype of the expression
     */
    public DataType getDataType();

    /**
     * Get the parseData for the expression
     *
     * @return the parse data for the expression
     */
    public ParseData getParseData();

    /**
     * Get the parseData for the expression
     *
     * @return the parse data for the expression
     */
    public void setParseData(ParseData p_parseData);

    /**
     * Get the value of the expression, based on the current row context
     *
     * @param p_context the current row context
     *
     * @return the value of the expression.
     */
    public Object getValue(IExecutionContext p_context);

    /**
     * Validates the expression against the specified context
     *
     * @param p_context the context to validate the expression against
     */
    public void validate(IValidationContext p_context) throws InvalidExpressionException;

}
