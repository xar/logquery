/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.expression;

import com.xar.logquery.runtime.ColumnInfo;
import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.enums.DataType;
import com.xar.logquery.runtime.expression.exceptions.InvalidExpressionException;
import com.xar.logquery.runtime.expression.interfaces.IExpression;
import com.xar.logquery.runtime.interfaces.IExecutionContext;
import com.xar.logquery.runtime.interfaces.IValidationContext;
import com.xar.logquery.runtime.util.RuntimeFunctions;

/**
 * Implementation for a column reference
 */
public class ColumnReference extends AbstractExpression {

    private IColumnInfo m_columnInfo;

    /**
     * Initialise a new column reference instance
     *
     * @param p_context the column context
     * @param p_name    the column name
     */
    public ColumnReference(final String p_context, final String p_name) {

        this(new ColumnInfo(p_context, p_name, DataType.UNDEFINED));
    }

    /**
     * Initialise a new column reference instance
     *
     * @param p_column the column info
     */
    public ColumnReference(final IColumnInfo p_column) {

        m_columnInfo = p_column;
    }

    /**
     * Get the child expressions of this expression
     *
     * @return the child expressions of this expression
     */
    @Override
    public IExpression[] getChildren() {

        return new IExpression[0];
    }

    public IColumnInfo getColumnInfo() {

        return m_columnInfo;
    }

    /**
     * Get the column context (alias of the datasource) where the column originated
     *
     * @return the column context or alias of the datasource
     */
    public String getContext() {

        return m_columnInfo.getContext();
    }

    /**
     * Get the datatype of the expression
     *
     * @return the datatype of the expression
     */
    @Override
    public DataType getDataType() {

        return m_columnInfo.getDataType();
    }

    public String getName() {

        return m_columnInfo.getName();
    }

    /**
     * Get the value of the expression, based on the current row context
     *
     * @param p_context the current row context
     *
     * @return the value of the expression.
     */
    @Override
    public Object getValue(final IExecutionContext p_context) {

        return p_context.getValue(m_columnInfo.getContext(), m_columnInfo.getName());
    }

    @Override
    public String toString() {

        return "ColumnReference{" +
                "context='" + m_columnInfo.getContext() +
                "', columnName='" + m_columnInfo.getName() +
                "', dataType=" + m_columnInfo.getDataType() +
                '}';
    }

    /**
     * Validates the expression against the specified context
     *
     * @param p_context the context to validate the expression against
     */
    @Override
    public void validateExpression(final IValidationContext p_context) throws InvalidExpressionException {

        IColumnInfo info = p_context.getColumnInfo(m_columnInfo.getContext(), m_columnInfo.getName());

        m_columnInfo = info == null ? m_columnInfo : info;

        if (getDataType() == DataType.UNDEFINED) {
            throw new InvalidExpressionException(getParseData(), "Column not found - " + RuntimeFunctions.getFQN(m_columnInfo));
        }
    }
}
