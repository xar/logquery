/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.datasource;

import com.xar.logquery.runtime.interfaces.IAliasable;

/**
 * Aliasable data source
 */
public abstract class AliasableDataSource extends AbstractDataSource implements IAliasable {

    private String m_alias = "";

    /**
     * Get the alias for the object
     *
     * @return the alias for the object
     */
    @Override
    public String getAlias() {

        return m_alias;
    }

    /**
     * Sets the alias for the object.
     *
     * @param p_alias the alias
     */
    @Override
    public void setAlias(final String p_alias) {

        m_alias = p_alias == null ? "" : p_alias;
    }

}
