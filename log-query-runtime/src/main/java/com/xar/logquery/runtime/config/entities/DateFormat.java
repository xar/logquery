/*
 * Copyright 2014 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.config.entities;

/**
 * Used to represent a date format. The pattern is a regex that will be matched to the input to determine if the
 * format string is appropriate to use for parsing a date in the format
 * <p/>
 * Created by Anton Koekemoer on 2014/11/20.
 */
public class DateFormat {

    /**
     * Regex date pattern
     */
    String pattern;

    /**
     * Date format string
     */
    String formatString;

    /**
     * Instantiates a new Date format.
     */
    protected DateFormat() {
        // Serialiser constructor
    }

    /**
     * Instantiates a new Date format.
     *
     * @param p_pattern      the pattern used to match input
     * @param p_formatString the format string used to parse the input
     */
    public DateFormat(final String p_pattern, final String p_formatString) {
        this();
        pattern = p_pattern;
        formatString = p_formatString;
    }

    /**
     * Gets format string.
     *
     * @return the format string
     */
    public String getFormatString() {

        return formatString;
    }

    /**
     * Gets pattern.
     *
     * @return the pattern
     */
    public String getPattern() {

        return pattern;
    }

}
