/*
 * Copyright 2014 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.output.tpl.directive;

import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;

/**
 * Freemarker User defined directive to keep the last value of a field.
 * <p/>
 * Parameters:
 * <ul>
 * <li><code>field</code>: The name of the field to be kept. This field can contain any type of value, and is compulsory</li>
 * <li><code>output</code>: The name field into which the result is to be placed. This field is optional, and the output will be defaulted into a field names FirstOf_&lt;fieldName&gt;</li>
 * </ul>
 * <p/>
 * Loop Variables: None
 * Mested content: No
 * <p/>
 * Created by Anton Koekemoer on 2014/12/04.
 */
public class LastDirective extends AggregateDirectiveBase<Object> {

    public LastDirective() {

        super("LastOf_");
    }

    @Override
    protected IAggregate<Object> createAggregate(final String p_fieldName) {

        return new First();
    }

    private class First implements IAggregate<Object> {

        private Object m_last;

        @Override
        public void aggregate(final TemplateModel p_model) throws TemplateModelException {

            Object value = getValueOf(p_model);

            if (value != null) { m_last = value; }
        }

        @Override
        public Object getValue() {

            return m_last;
        }
    }
}
