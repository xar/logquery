/*
 * Copyright 2014 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.interfaces;

/**
 * Minimum interface requirements for an aliasable object
 */
public interface IAliasable {

    /**
     * Get the alias for the object
     *
     * @return the alias for the object
     */
    public String getAlias();

    /**
     * Sets the alias for the object.
     *
     * @param p_alias the alias
     */
    public void setAlias(String p_alias);

}
