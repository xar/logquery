/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.exceptions;

import com.xar.logquery.runtime.parse.ParseData;

/**
 * Exception thrown by a datasource if some error occurs during processing
 */
public class DataSourceException extends Exception {

    private final String m_message;

    private ParseData m_parseData;

    /**
     * Constructs a new exception with the specified detail message.  The
     * cause is not initialized, and may subsequently be initialized by
     * a call to {@link #initCause}.
     *
     * @param p_message the detail message. The detail message is saved for
     *                  later retrieval by the {@link #getMessage()} method.
     */
    public DataSourceException(final String p_message) {

        this(p_message, null);
    }

    /**
     * Constructs a new exception with the specified detail message.  The
     * cause is not initialized, and may subsequently be initialized by
     * a call to {@link #initCause}.
     *
     * @param p_message the detail message. The detail message is saved for
     *                  later retrieval by the {@link #getMessage()} method.
     * @param p_cause   the underlying exception
     */
    public DataSourceException(final String p_message, final Throwable p_cause) {

        this(null, p_message, p_cause);
    }

    /**
     * Constructs a new exception with the specified detail message.  The
     * cause is not initialized, and may subsequently be initialized by
     * a call to {@link #initCause}.
     *
     * @param p_message the detail message. The detail message is saved for
     *                  later retrieval by the {@link #getMessage()} method.
     */
    public DataSourceException(final ParseData p_parseData, final String p_message) {

        this(p_parseData, p_message, null);
    }

    /**
     * Constructs a new exception with the specified detail message.  The
     * cause is not initialized, and may subsequently be initialized by
     * a call to {@link #initCause}.
     *
     * @param p_message the detail message. The detail message is saved for
     *                  later retrieval by the {@link #getMessage()} method.
     * @param p_cause   the underlying exception
     */
    public DataSourceException(final ParseData p_parseData, final String p_message, final Throwable p_cause) {

        super(buildMessage(p_parseData, p_message), p_cause);
        m_message = p_message;
        m_parseData = p_parseData;
    }

    /**
     * Get the parse data associated with the exception
     *
     * @return the parse data associated with the exception
     */
    public ParseData getParseData() {

        return m_parseData;
    }

    /**
     * Get the message without location info
     * @return
     */
    public String getBasicMessage() {

        return m_message;
    }

    /**
     * Build a message from the message and parsedata
     *
     * @param p_parseData the parse data
     * @param p_message   the message
     *
     * @return a message built from the message and parsedata
     */
    private static String buildMessage(final ParseData p_parseData, final String p_message) {

        if (p_parseData == null) { return p_message;}
        return "[" + p_parseData.getRowNumber() + ", " + p_parseData.getColumnNumber() + "] " + p_message;
    }
}
