/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime;


import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;

import java.util.Enumeration;

/**
 * Created by akoekemo on 2/21/15.
 */
public class ResultSet implements IResultSet {

    private IColumnInfo[] m_columnInfo;

    private Enumeration<IDataRow> m_enumeration;

    public ResultSet(final IColumnInfo[] p_columnInfo, final Enumeration<IDataRow> p_enumeration) {

        m_columnInfo = p_columnInfo;
        m_enumeration = p_enumeration;
    }

    @Override
    public IColumnInfo[] getColumnInfo() {

        return m_columnInfo;
    }

    @Override
    public Enumeration<IDataRow> getRows() throws DataSourceException {

        return m_enumeration;
    }
}
