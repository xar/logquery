/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.output.text;

import com.xar.cli.annotations.CommandLineArgument;
import com.xar.cli.util.CommandlineFunctions;
import com.xar.logquery.runtime.ExecutionContext;
import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.datasource.interfaces.IDataRow;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.datasource.interfaces.IResultSet;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.output.interfaces.IOutputFormatter;
import com.xar.logquery.runtime.util.FormatFunctions;

import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 * Formats the datasource as text
 */
public class TextOutputFormatter implements IOutputFormatter {

    @CommandLineArgument(name = "ps", description = "page size")
    private int pageSize = 10;

    /**
     * Instantiates a new Text output formatter.
     *
     * @param p_pageSize the p _ page size
     */
    public TextOutputFormatter(final int p_pageSize) {

        setPageSize(p_pageSize);
    }

    /**
     * Instantiates a new Text output formatter.
     */
    public TextOutputFormatter() {

        this(10);
    }

    @Override
    public void format(final IDataSource p_dataSource) throws DataSourceException {

        IResultSet resultSet = p_dataSource.getResultSet(new ExecutionContext());
        IColumnInfo[] info = resultSet.getColumnInfo();

        int[] widths = new int[info.length];

        for (int i = 0; i < widths.length; i++) {
            widths[i] = info[i].getName().length();
        }
        List<String[][]> columns = new ArrayList<>();

        Enumeration<IDataRow> rows = resultSet.getRows();

        while (rows.hasMoreElements()) {
            IDataRow row = rows.nextElement();

            String[][] rowData = new String[widths.length][];
            columns.add(rowData);

            for (int i = 0; i < widths.length; i++) {
                rowData[i] = FormatFunctions.asString(row.get(i), info[i].getDataType()).split("\n");
                for (int r = 0; r < rowData[i].length; r++) {
                    widths[i] = Math.max(rowData[i][r].length(), widths[i]);
                }
            }
            if (pageSize > 0 && columns.size() >= pageSize) {
                printValues(columns, info, widths, System.out);
                columns.clear();
                if (rows.hasMoreElements()) {
                    try {
                        System.out.println("\nEnter for more...\n");
                        System.in.read();
                    }
                    catch (IOException e) {}
                }
            }

        }

        if (columns.size() > 0) {
            printValues(columns, info, widths, System.out);
        }
    }

    /**
     * Gets page size.
     *
     * @return the page size
     */
    public int getPageSize() {

        return pageSize;
    }

    /**
     * Sets page size.
     *
     * @param p_pageSize the p _ page size
     */
    public void setPageSize(final int p_pageSize) {

        pageSize = p_pageSize;
    }

    @Override
    public String toString() {

        return "TextOutputFormatter{" +
                "pageSize=" + getPageSize() + '}';
    }

    /**
     * Print the values from the list
     *
     * @param p_lines    the lines to output
     * @param p_headings the headings
     * @param p_writer   the column widths
     */
    private static void printValues(final List<String[][]> p_lines, IColumnInfo[] p_headings, int[] p_widths, final PrintStream p_writer) {

        if (p_lines.size() > 0) {

            StringBuilder line = new StringBuilder();
            for (int i = 0; i < p_headings.length; i++) {
                line.append(CommandlineFunctions.padr(p_headings[i].getName(), p_widths[i])).append(' ');
            }
            line.delete(line.length() - 1, line.length());
            p_writer.println(line);
            line.setLength(0);
            for (int i = 0; i < p_headings.length; i++) {
                line.append(CommandlineFunctions.padr("", p_widths[i], '-')).append(' ');
            }
            line.delete(line.length() - 1, line.length());
            p_writer.println(line);
            line.setLength(0);


            for (String[][] textLine : p_lines) {
                int row = 0;
                boolean output = true;
                while (output) {
                    output = false;
                    line.setLength(0);
                    for (int column = 0; column < textLine.length; column++) {
                        String[] textColumn = textLine[column];
                        if (row < textColumn.length) {
                            line.append(CommandlineFunctions.padr(textColumn[row], p_widths[column])).append(' ');
                            output = true;
                        }
                        else {
                            line.append(CommandlineFunctions.padr("", p_widths[column])).append(' ');
                        }
                    }
                    if (output) {
                        if (line.length() > 0) {
                            line.delete(line.length() - 1, line.length());
                        }
                        p_writer.println(line);
                        row++;
                    }

                }
            }
        }

    }
}
