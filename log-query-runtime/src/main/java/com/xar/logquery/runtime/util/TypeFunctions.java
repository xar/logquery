/*
 * Copyright 2014 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime.util;

import com.xar.logquery.runtime.enums.DataType;

import java.math.BigDecimal;
import java.math.BigInteger;

import static com.xar.logquery.runtime.enums.DataType.*;

/**
 * Created by Anton Koekemoer on 2014/12/12.
 */
public class TypeFunctions {

    public static final Class<?>[] INTEGER_TYPES = new Class<?>[]{int.class, Integer.class, long.class, Long.class,
                                                                  BigInteger.class};

    public static final Class<?>[] REAL_TYPES = new Class<?>[]{float.class, Float.class, double.class, Double.class,
                                                               BigDecimal.class};

    /**
     * Compatible data types
     */
    public static DataType[][] COMPATIBLES = new DataType[][]{
            new DataType[]{BOOLEAN, BOOLEAN, BOOLEAN},
            new DataType[]{BOOLEAN, NULL, BOOLEAN},
            new DataType[]{INTEGER, INTEGER, INTEGER},
            new DataType[]{INTEGER, NULL, INTEGER},
            new DataType[]{INTEGER, REAL, REAL},
            new DataType[]{REAL, REAL, REAL},
            new DataType[]{REAL, NULL, REAL},
            new DataType[]{STRING, STRING, STRING},
            new DataType[]{STRING, NULL, STRING},
            new DataType[]{DATE, DATE, DATE},
            new DataType[]{DATE, TIMESTAMP, TIMESTAMP},
            new DataType[]{DATE, NULL, DATE},
            new DataType[]{TIMESTAMP, TIMESTAMP, TIMESTAMP},
            new DataType[]{TIMESTAMP, NULL, TIMESTAMP},

    };

    private static TypeFunctions _instance = new TypeFunctions();

    private TypeFunctions() {

    }

    /**
     * Get a type compatible with both supplied types
     *
     * @param p_type1 the first type
     * @param p_type2 the second type
     *
     * @return a type compatible with both supplied types
     */
    public static DataType compatibleType(final DataType p_type1, final DataType p_type2) {

        if (p_type1 == p_type2) { return p_type1; }

        for (DataType[] compat : COMPATIBLES) {
            if (p_type1 == compat[0] && p_type2 == compat[1] || p_type1 == compat[1] && p_type2 == compat[0]) {
                return compat[2];
            }
        }

        return NULL;
    }

    /**
     * Checks to see if the types are compatible
     *
     * @param p_type1 the first type
     * @param p_type2 the second type
     *
     * @return true if the types are compatible; false otherwise
     */
    public static boolean isTypeCompatible(final DataType p_type1, final DataType p_type2) {

        return compatibleType(p_type1, p_type2) != NULL;
    }

    public static boolean isValueInteger(Object p_value) {

        return isValueTypeOf(p_value, INTEGER_TYPES);
    }

    public static boolean isValueReal(Object p_value) {

        return isValueTypeOf(p_value, REAL_TYPES);
    }

    public static boolean isValueTypeOf(Object p_value, Class<?>... p_type) {

        if (p_value == null) { return false; }

        Class<?> valueType = p_value.getClass();

        for (Class<?> type : p_type) {
            if (type == valueType) {
                return true;
            }
        }
        return false;
    }

}
