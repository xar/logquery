/*
 * Copyright 2014 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.runtime;

import com.xar.logquery.runtime.datasource.interfaces.IColumnInfo;
import com.xar.logquery.runtime.exceptions.DuplicateColumnNameException;
import com.xar.logquery.runtime.util.RuntimeFunctions;
import com.xar.logquery.runtime.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by koekemoera on 2014/08/25.
 */
public class ColumnList {

    Map<String, List<IColumnInfo>> m_columns = new HashMap<>();

    List<IColumnInfo> m_columnInfo = new ArrayList<>();

    /**
     * Adds the specified column to the list
     *
     * @param p_info the column to add
     *
     * @throws DuplicateColumnNameException if the column already exists
     */
    public void addColumn(IColumnInfo p_info) throws DuplicateColumnNameException {

        if (!addColumnIfNotExist(p_info)) {
            throw new DuplicateColumnNameException("Column name '" + RuntimeFunctions.getFQN(p_info) + "' already exists");
        }

    }

    /**
     * Adds the specified colunmn, only if it does not exist already
     *
     * @param p_info the column to add
     *
     * @return true if the column was added, false if a column with that context and name already exists, and was not added
     */
    public boolean addColumnIfNotExist(IColumnInfo p_info) {

        List<IColumnInfo> columns = this.m_columns.get(p_info.getName());
        if (columns == null) {
            columns = new ArrayList<>();
            this.m_columns.put(p_info.getName(), columns);
        }
        for (IColumnInfo item : columns) {
            if (item.getName().equals(p_info.getName()) && item.getContext().equals(p_info.getContext())) {
                return false;
            }
        }
        columns.add(p_info);
        m_columnInfo.add(p_info);
        return true;
    }

    /**
     * Clear the column list
     */
    public void clear() {

        m_columns.clear();
        m_columnInfo.clear();
    }

    /**
     * Get the columnInfo for the column with the specified name and context
     *
     * @param p_context    the context to search
     * @param p_columnName the name to search
     *
     * @return the column if it is found, or null if not
     */
    public IColumnInfo getColumnInfo(String p_context, String p_columnName) {

        List<IColumnInfo> columns = this.m_columns.get(p_columnName);
        if (columns != null) {
            if (StringUtils.isEmpty(p_context)) {
                if (columns.size() == 1) {
                    IColumnInfo item = columns.get(0);
                    return item;
                }
            }
            else {
                for (IColumnInfo reference : columns) {
                    if (reference.getContext() != null) {
                        if (reference.getContext().equals(p_context)) {
                            return reference;
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * Get all the columnsin the list
     *
     * @return
     */
    public List<IColumnInfo> getColumns() {

        return getColumnsForContext(null);
    }

    /**
     * Get all the columns for a specified context
     *
     * @param p_context the context to get columns for. If this is set to null, all columns are returned
     *
     * @return all the columns for a specified context, or all the columns if the context is null
     */
    public List<IColumnInfo> getColumnsForContext(String p_context) {

        if (p_context != null) {
            List<IColumnInfo> list = new ArrayList<>();
            for (int i = 0; i < m_columnInfo.size(); i++) {
                IColumnInfo columnInfo = m_columnInfo.get(i);
                if (columnInfo.getContext().equals(p_context)) {
                    list.add(columnInfo);
                }
            }
            return list;
        }
        return m_columnInfo;
    }
}
