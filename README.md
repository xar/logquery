# Log-Query #

Log-Query is a cross platform Java based equivalent to [Microsoft Logparser](http://www.microsoft.com/en-za/download/details.aspx?id=24659), used to query text files with a Sql-like syntax.


## Dependencies ##

### Running ###

1. JRE 1.7 or better

### Building ###

1. JDK 1.7 or better
2. Maven 3

## Building from source ##

Log-Query uses Maven as build system, so building from source should be simple.

After checking out the code

    mvn clean package

## License

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

 [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


## Documentation

Documentation can be found on the Project [wiki](wiki/Home.md).
