SELECT *
FROM
    'table' AS "aaa" USING (
    10 AS int,
    20.0 AS double,
    'String value' AS string,
     0x10 AS hex,
     aaa.RowNumber AS aliased,
     Text AS column,
     int AS ref,
     10+100-20+200+6 AS add
     )

