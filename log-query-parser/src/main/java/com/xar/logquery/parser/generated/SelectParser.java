// Generated from Select.g4 by ANTLR 4.3
package com.xar.logquery.parser.generated;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class SelectParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.3", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__60=1, T__59=2, T__58=3, T__57=4, T__56=5, T__55=6, T__54=7, T__53=8, 
		T__52=9, T__51=10, T__50=11, T__49=12, T__48=13, T__47=14, T__46=15, T__45=16, 
		T__44=17, T__43=18, T__42=19, T__41=20, T__40=21, T__39=22, T__38=23, 
		T__37=24, T__36=25, T__35=26, T__34=27, T__33=28, T__32=29, T__31=30, 
		T__30=31, T__29=32, T__28=33, T__27=34, T__26=35, T__25=36, T__24=37, 
		T__23=38, T__22=39, T__21=40, T__20=41, T__19=42, T__18=43, T__17=44, 
		T__16=45, T__15=46, T__14=47, T__13=48, T__12=49, T__11=50, T__10=51, 
		T__9=52, T__8=53, T__7=54, T__6=55, T__5=56, T__4=57, T__3=58, T__2=59, 
		T__1=60, T__0=61, WS=62, COMMENT=63, LINE_COMMENT=64, QuotedIdentifier=65, 
		NonQuotedIdentifier=66, ASCIIStringLiteral=67, Exponent=68, Integer=69, 
		Real=70, HexLiteral=71;
	public static final String[] tokenNames = {
		"<INVALID>", "'GROUP'", "'BEFORE'", "'DISTINCT'", "'ALL'", "'RIGHT'", 
		"'*'", "'WHERE'", "'<'", "'!='", "'END'", "'<='", "'AS'", "'ELSE'", "'BETWEEN'", 
		"'IN'", "'TRUE'", "'CASE'", "'ORDER'", "'TRANSFORM'", "'%'", "'AND'", 
		"'INNER'", "')'", "'DESC'", "'='", "'IS'", "'OUTER'", "'SKIP'", "'LIKE'", 
		"'FROM'", "'ESCAPE'", "'NOT'", "'SELECT'", "'THEN'", "'BY'", "','", "'ASC'", 
		"'HAVING'", "'-'", "'('", "'CROSS'", "'FULL'", "'OR'", "'JOIN'", "'FALSE'", 
		"'USING'", "'NULL'", "'.'", "'LEFT'", "'+'", "'<>'", "';'", "'ON'", "'>'", 
		"'AFTER'", "'WHEN'", "'LIMIT'", "'/'", "'PIVOT'", "'>='", "'UNION'", "WS", 
		"COMMENT", "LINE_COMMENT", "QuotedIdentifier", "NonQuotedIdentifier", 
		"ASCIIStringLiteral", "Exponent", "Integer", "Real", "HexLiteral"
	};
	public static final int
		RULE_statement = 0, RULE_selectStatement = 1, RULE_simpleQueryStatement = 2, 
		RULE_simpleSelectStatement = 3, RULE_unionStatement = 4, RULE_queryStatement = 5, 
		RULE_limitClause = 6, RULE_fromClause = 7, RULE_tableSource = 8, RULE_subTableSource = 9, 
		RULE_usingClause = 10, RULE_transformClause = 11, RULE_joinClause = 12, 
		RULE_dataSource = 13, RULE_fileDatasource = 14, RULE_customDatasource = 15, 
		RULE_propertyList = 16, RULE_propertyAssign = 17, RULE_whereClause = 18, 
		RULE_selectList = 19, RULE_usingList = 20, RULE_selectItem = 21, RULE_selectExpression = 22, 
		RULE_namedExpression = 23, RULE_wildcardItem = 24, RULE_expression = 25, 
		RULE_searchCondition = 26, RULE_subSearchCondition = 27, RULE_negateCondition = 28, 
		RULE_predicateCompare = 29, RULE_predicateNULL = 30, RULE_predicateLIKE = 31, 
		RULE_predicateBETWEEN = 32, RULE_predicateIN = 33, RULE_expressionList = 34, 
		RULE_orderByClause = 35, RULE_orderExpression = 36, RULE_groupByClause = 37, 
		RULE_pivotClause = 38, RULE_groupExpression = 39, RULE_havingClause = 40, 
		RULE_simpleExpression = 41, RULE_arithmeticExpression = 42, RULE_unaryOperator = 43, 
		RULE_subExpression = 44, RULE_primaryExpression = 45, RULE_columnName = 46, 
		RULE_function = 47, RULE_wildCardParameter = 48, RULE_functionParameterList = 49, 
		RULE_functionParameter = 50, RULE_caseFunction = 51, RULE_whenExpression = 52, 
		RULE_elseExpression = 53, RULE_identifier = 54, RULE_constant = 55, RULE_reservedConstant = 56, 
		RULE_integerLiteral = 57, RULE_realLiteral = 58, RULE_stringLiteral = 59, 
		RULE_hexLiteral = 60, RULE_number = 61;
	public static final String[] ruleNames = {
		"statement", "selectStatement", "simpleQueryStatement", "simpleSelectStatement", 
		"unionStatement", "queryStatement", "limitClause", "fromClause", "tableSource", 
		"subTableSource", "usingClause", "transformClause", "joinClause", "dataSource", 
		"fileDatasource", "customDatasource", "propertyList", "propertyAssign", 
		"whereClause", "selectList", "usingList", "selectItem", "selectExpression", 
		"namedExpression", "wildcardItem", "expression", "searchCondition", "subSearchCondition", 
		"negateCondition", "predicateCompare", "predicateNULL", "predicateLIKE", 
		"predicateBETWEEN", "predicateIN", "expressionList", "orderByClause", 
		"orderExpression", "groupByClause", "pivotClause", "groupExpression", 
		"havingClause", "simpleExpression", "arithmeticExpression", "unaryOperator", 
		"subExpression", "primaryExpression", "columnName", "function", "wildCardParameter", 
		"functionParameterList", "functionParameter", "caseFunction", "whenExpression", 
		"elseExpression", "identifier", "constant", "reservedConstant", "integerLiteral", 
		"realLiteral", "stringLiteral", "hexLiteral", "number"
	};

	@Override
	public String getGrammarFileName() { return "Select.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public SelectParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class StatementContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(SelectParser.EOF, 0); }
		public SelectStatementContext selectStatement() {
			return getRuleContext(SelectStatementContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_statement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(124); selectStatement();
			setState(126);
			_la = _input.LA(1);
			if (_la==T__9) {
				{
				setState(125); match(T__9);
				}
			}

			setState(128); match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SelectStatementContext extends ParserRuleContext {
		public OrderByClauseContext orderByClause() {
			return getRuleContext(OrderByClauseContext.class,0);
		}
		public LimitClauseContext limitClause() {
			return getRuleContext(LimitClauseContext.class,0);
		}
		public SimpleQueryStatementContext simpleQueryStatement() {
			return getRuleContext(SimpleQueryStatementContext.class,0);
		}
		public SelectStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_selectStatement; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitSelectStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SelectStatementContext selectStatement() throws RecognitionException {
		SelectStatementContext _localctx = new SelectStatementContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_selectStatement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(130); simpleQueryStatement();
			setState(132);
			_la = _input.LA(1);
			if (_la==T__43) {
				{
				setState(131); orderByClause();
				}
			}

			setState(135);
			_la = _input.LA(1);
			if (_la==T__4) {
				{
				setState(134); limitClause();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SimpleQueryStatementContext extends ParserRuleContext {
		public SimpleSelectStatementContext simpleSelectStatement() {
			return getRuleContext(SimpleSelectStatementContext.class,0);
		}
		public UnionStatementContext unionStatement() {
			return getRuleContext(UnionStatementContext.class,0);
		}
		public SimpleQueryStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_simpleQueryStatement; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitSimpleQueryStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SimpleQueryStatementContext simpleQueryStatement() throws RecognitionException {
		SimpleQueryStatementContext _localctx = new SimpleQueryStatementContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_simpleQueryStatement);
		try {
			setState(139);
			switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(137); simpleSelectStatement();
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(138); unionStatement();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SimpleSelectStatementContext extends ParserRuleContext {
		public PivotClauseContext pivotClause() {
			return getRuleContext(PivotClauseContext.class,0);
		}
		public WhereClauseContext whereClause() {
			return getRuleContext(WhereClauseContext.class,0);
		}
		public GroupByClauseContext groupByClause() {
			return getRuleContext(GroupByClauseContext.class,0);
		}
		public SelectListContext selectList() {
			return getRuleContext(SelectListContext.class,0);
		}
		public FromClauseContext fromClause() {
			return getRuleContext(FromClauseContext.class,0);
		}
		public SimpleSelectStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_simpleSelectStatement; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitSimpleSelectStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SimpleSelectStatementContext simpleSelectStatement() throws RecognitionException {
		SimpleSelectStatementContext _localctx = new SimpleSelectStatementContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_simpleSelectStatement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(141); match(T__28);
			setState(143);
			_la = _input.LA(1);
			if (_la==T__58 || _la==T__57) {
				{
				setState(142);
				_la = _input.LA(1);
				if ( !(_la==T__58 || _la==T__57) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				}
			}

			setState(145); selectList();
			setState(146); fromClause();
			setState(148);
			_la = _input.LA(1);
			if (_la==T__54) {
				{
				setState(147); whereClause();
				}
			}

			setState(151);
			_la = _input.LA(1);
			if (_la==T__60) {
				{
				setState(150); groupByClause();
				}
			}

			setState(154);
			_la = _input.LA(1);
			if (_la==T__2) {
				{
				setState(153); pivotClause();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UnionStatementContext extends ParserRuleContext {
		public List<QueryStatementContext> queryStatement() {
			return getRuleContexts(QueryStatementContext.class);
		}
		public QueryStatementContext queryStatement(int i) {
			return getRuleContext(QueryStatementContext.class,i);
		}
		public UnionStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unionStatement; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitUnionStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UnionStatementContext unionStatement() throws RecognitionException {
		UnionStatementContext _localctx = new UnionStatementContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_unionStatement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(156); queryStatement();
			setState(161);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__0) {
				{
				{
				setState(157); match(T__0);
				setState(158); queryStatement();
				}
				}
				setState(163);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class QueryStatementContext extends ParserRuleContext {
		public SimpleSelectStatementContext simpleSelectStatement() {
			return getRuleContext(SimpleSelectStatementContext.class,0);
		}
		public SelectStatementContext selectStatement() {
			return getRuleContext(SelectStatementContext.class,0);
		}
		public QueryStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_queryStatement; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitQueryStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final QueryStatementContext queryStatement() throws RecognitionException {
		QueryStatementContext _localctx = new QueryStatementContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_queryStatement);
		try {
			setState(169);
			switch (_input.LA(1)) {
			case T__21:
				enterOuterAlt(_localctx, 1);
				{
				setState(164); match(T__21);
				setState(165); selectStatement();
				setState(166); match(T__38);
				}
				break;
			case T__28:
				enterOuterAlt(_localctx, 2);
				{
				setState(168); simpleSelectStatement();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LimitClauseContext extends ParserRuleContext {
		public IntegerLiteralContext integerLiteral(int i) {
			return getRuleContext(IntegerLiteralContext.class,i);
		}
		public List<IntegerLiteralContext> integerLiteral() {
			return getRuleContexts(IntegerLiteralContext.class);
		}
		public LimitClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_limitClause; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitLimitClause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LimitClauseContext limitClause() throws RecognitionException {
		LimitClauseContext _localctx = new LimitClauseContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_limitClause);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(171); match(T__4);
			setState(172); integerLiteral();
			setState(175);
			_la = _input.LA(1);
			if (_la==T__33 || _la==T__25) {
				{
				setState(173);
				_la = _input.LA(1);
				if ( !(_la==T__33 || _la==T__25) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(174); integerLiteral();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FromClauseContext extends ParserRuleContext {
		public TableSourceContext tableSource() {
			return getRuleContext(TableSourceContext.class,0);
		}
		public FromClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fromClause; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitFromClause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FromClauseContext fromClause() throws RecognitionException {
		FromClauseContext _localctx = new FromClauseContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_fromClause);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(177); match(T__31);
			setState(178); tableSource();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TableSourceContext extends ParserRuleContext {
		public JoinClauseContext joinClause(int i) {
			return getRuleContext(JoinClauseContext.class,i);
		}
		public List<JoinClauseContext> joinClause() {
			return getRuleContexts(JoinClauseContext.class);
		}
		public SubTableSourceContext subTableSource() {
			return getRuleContext(SubTableSourceContext.class,0);
		}
		public TableSourceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tableSource; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitTableSource(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TableSourceContext tableSource() throws RecognitionException {
		TableSourceContext _localctx = new TableSourceContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_tableSource);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(180); subTableSource();
			setState(184);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__56) | (1L << T__39) | (1L << T__20) | (1L << T__19) | (1L << T__17) | (1L << T__12))) != 0)) {
				{
				{
				setState(181); joinClause();
				}
				}
				setState(186);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SubTableSourceContext extends ParserRuleContext {
		public TransformClauseContext transformClause() {
			return getRuleContext(TransformClauseContext.class,0);
		}
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public DataSourceContext dataSource() {
			return getRuleContext(DataSourceContext.class,0);
		}
		public SubTableSourceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_subTableSource; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitSubTableSource(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SubTableSourceContext subTableSource() throws RecognitionException {
		SubTableSourceContext _localctx = new SubTableSourceContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_subTableSource);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(187); dataSource();
			setState(189);
			_la = _input.LA(1);
			if (_la==T__42) {
				{
				setState(188); transformClause();
				}
			}

			setState(193);
			_la = _input.LA(1);
			if (_la==T__49) {
				{
				setState(191); match(T__49);
				setState(192); identifier();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UsingClauseContext extends ParserRuleContext {
		public PropertyListContext propertyList() {
			return getRuleContext(PropertyListContext.class,0);
		}
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public UsingClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_usingClause; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitUsingClause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UsingClauseContext usingClause() throws RecognitionException {
		UsingClauseContext _localctx = new UsingClauseContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_usingClause);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(195); match(T__15);
			setState(196); identifier();
			setState(201);
			_la = _input.LA(1);
			if (_la==T__21) {
				{
				setState(197); match(T__21);
				setState(198); propertyList();
				setState(199); match(T__38);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TransformClauseContext extends ParserRuleContext {
		public UsingListContext usingList() {
			return getRuleContext(UsingListContext.class,0);
		}
		public TransformClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_transformClause; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitTransformClause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TransformClauseContext transformClause() throws RecognitionException {
		TransformClauseContext _localctx = new TransformClauseContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_transformClause);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(203); match(T__42);
			setState(204); match(T__21);
			setState(205); usingList();
			setState(206); match(T__38);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class JoinClauseContext extends ParserRuleContext {
		public SearchConditionContext searchCondition() {
			return getRuleContext(SearchConditionContext.class,0);
		}
		public SubTableSourceContext subTableSource() {
			return getRuleContext(SubTableSourceContext.class,0);
		}
		public TableSourceContext tableSource() {
			return getRuleContext(TableSourceContext.class,0);
		}
		public JoinClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_joinClause; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitJoinClause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final JoinClauseContext joinClause() throws RecognitionException {
		JoinClauseContext _localctx = new JoinClauseContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_joinClause);
		int _la;
		try {
			setState(225);
			switch (_input.LA(1)) {
			case T__20:
				enterOuterAlt(_localctx, 1);
				{
				setState(208); match(T__20);
				setState(209); match(T__17);
				setState(210); subTableSource();
				}
				break;
			case T__56:
			case T__39:
			case T__19:
			case T__17:
			case T__12:
				enterOuterAlt(_localctx, 2);
				{
				setState(218);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__56) | (1L << T__39) | (1L << T__19) | (1L << T__12))) != 0)) {
					{
					setState(216);
					switch (_input.LA(1)) {
					case T__39:
						{
						setState(211); match(T__39);
						}
						break;
					case T__56:
					case T__19:
					case T__12:
						{
						setState(212);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__56) | (1L << T__19) | (1L << T__12))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						consume();
						setState(214);
						_la = _input.LA(1);
						if (_la==T__34) {
							{
							setState(213); match(T__34);
							}
						}

						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					}
				}

				setState(220); match(T__17);
				setState(221); tableSource();
				setState(222); match(T__8);
				setState(223); searchCondition(0);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DataSourceContext extends ParserRuleContext {
		public FileDatasourceContext fileDatasource() {
			return getRuleContext(FileDatasourceContext.class,0);
		}
		public CustomDatasourceContext customDatasource() {
			return getRuleContext(CustomDatasourceContext.class,0);
		}
		public SelectStatementContext selectStatement() {
			return getRuleContext(SelectStatementContext.class,0);
		}
		public DataSourceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dataSource; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitDataSource(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DataSourceContext dataSource() throws RecognitionException {
		DataSourceContext _localctx = new DataSourceContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_dataSource);
		try {
			setState(233);
			switch (_input.LA(1)) {
			case ASCIIStringLiteral:
				enterOuterAlt(_localctx, 1);
				{
				setState(227); fileDatasource();
				}
				break;
			case QuotedIdentifier:
			case NonQuotedIdentifier:
				enterOuterAlt(_localctx, 2);
				{
				setState(228); customDatasource();
				}
				break;
			case T__21:
				enterOuterAlt(_localctx, 3);
				{
				setState(229); match(T__21);
				setState(230); selectStatement();
				setState(231); match(T__38);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FileDatasourceContext extends ParserRuleContext {
		public UsingClauseContext usingClause() {
			return getRuleContext(UsingClauseContext.class,0);
		}
		public TerminalNode ASCIIStringLiteral() { return getToken(SelectParser.ASCIIStringLiteral, 0); }
		public FileDatasourceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fileDatasource; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitFileDatasource(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FileDatasourceContext fileDatasource() throws RecognitionException {
		FileDatasourceContext _localctx = new FileDatasourceContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_fileDatasource);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(235); match(ASCIIStringLiteral);
			setState(237);
			_la = _input.LA(1);
			if (_la==T__15) {
				{
				setState(236); usingClause();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CustomDatasourceContext extends ParserRuleContext {
		public UsingClauseContext usingClause() {
			return getRuleContext(UsingClauseContext.class,0);
		}
		public PropertyListContext propertyList() {
			return getRuleContext(PropertyListContext.class,0);
		}
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public CustomDatasourceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_customDatasource; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitCustomDatasource(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CustomDatasourceContext customDatasource() throws RecognitionException {
		CustomDatasourceContext _localctx = new CustomDatasourceContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_customDatasource);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(239); identifier();
			setState(244);
			_la = _input.LA(1);
			if (_la==T__21) {
				{
				setState(240); match(T__21);
				setState(241); propertyList();
				setState(242); match(T__38);
				}
			}

			setState(247);
			_la = _input.LA(1);
			if (_la==T__15) {
				{
				setState(246); usingClause();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyListContext extends ParserRuleContext {
		public List<PropertyAssignContext> propertyAssign() {
			return getRuleContexts(PropertyAssignContext.class);
		}
		public PropertyAssignContext propertyAssign(int i) {
			return getRuleContext(PropertyAssignContext.class,i);
		}
		public PropertyListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_propertyList; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitPropertyList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PropertyListContext propertyList() throws RecognitionException {
		PropertyListContext _localctx = new PropertyListContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_propertyList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(249); propertyAssign();
			setState(254);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__25) {
				{
				{
				setState(250); match(T__25);
				setState(251); propertyAssign();
				}
				}
				setState(256);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyAssignContext extends ParserRuleContext {
		public SimpleExpressionContext simpleExpression() {
			return getRuleContext(SimpleExpressionContext.class,0);
		}
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public PropertyAssignContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_propertyAssign; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitPropertyAssign(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PropertyAssignContext propertyAssign() throws RecognitionException {
		PropertyAssignContext _localctx = new PropertyAssignContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_propertyAssign);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(257); identifier();
			setState(258); match(T__36);
			setState(259); simpleExpression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WhereClauseContext extends ParserRuleContext {
		public SearchConditionContext searchCondition() {
			return getRuleContext(SearchConditionContext.class,0);
		}
		public WhereClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_whereClause; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitWhereClause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WhereClauseContext whereClause() throws RecognitionException {
		WhereClauseContext _localctx = new WhereClauseContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_whereClause);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(261); match(T__54);
			setState(262); searchCondition(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SelectListContext extends ParserRuleContext {
		public List<SelectItemContext> selectItem() {
			return getRuleContexts(SelectItemContext.class);
		}
		public SelectItemContext selectItem(int i) {
			return getRuleContext(SelectItemContext.class,i);
		}
		public SelectListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_selectList; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitSelectList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SelectListContext selectList() throws RecognitionException {
		SelectListContext _localctx = new SelectListContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_selectList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(264); selectItem();
			setState(269);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__25) {
				{
				{
				setState(265); match(T__25);
				setState(266); selectItem();
				}
				}
				setState(271);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UsingListContext extends ParserRuleContext {
		public NamedExpressionContext namedExpression(int i) {
			return getRuleContext(NamedExpressionContext.class,i);
		}
		public List<NamedExpressionContext> namedExpression() {
			return getRuleContexts(NamedExpressionContext.class);
		}
		public UsingListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_usingList; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitUsingList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UsingListContext usingList() throws RecognitionException {
		UsingListContext _localctx = new UsingListContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_usingList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(272); namedExpression();
			setState(277);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__25) {
				{
				{
				setState(273); match(T__25);
				setState(274); namedExpression();
				}
				}
				setState(279);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SelectItemContext extends ParserRuleContext {
		public WildcardItemContext wildcardItem() {
			return getRuleContext(WildcardItemContext.class,0);
		}
		public SelectExpressionContext selectExpression() {
			return getRuleContext(SelectExpressionContext.class,0);
		}
		public SelectItemContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_selectItem; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitSelectItem(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SelectItemContext selectItem() throws RecognitionException {
		SelectItemContext _localctx = new SelectItemContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_selectItem);
		try {
			setState(282);
			switch ( getInterpreter().adaptivePredict(_input,26,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(280); wildcardItem();
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(281); selectExpression();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SelectExpressionContext extends ParserRuleContext {
		public SearchConditionContext searchCondition() {
			return getRuleContext(SearchConditionContext.class,0);
		}
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public SelectExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_selectExpression; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitSelectExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SelectExpressionContext selectExpression() throws RecognitionException {
		SelectExpressionContext _localctx = new SelectExpressionContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_selectExpression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(284); searchCondition(0);
			setState(287);
			_la = _input.LA(1);
			if (_la==T__49) {
				{
				setState(285); match(T__49);
				setState(286); identifier();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NamedExpressionContext extends ParserRuleContext {
		public SimpleExpressionContext simpleExpression() {
			return getRuleContext(SimpleExpressionContext.class,0);
		}
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public NamedExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_namedExpression; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitNamedExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NamedExpressionContext namedExpression() throws RecognitionException {
		NamedExpressionContext _localctx = new NamedExpressionContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_namedExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(289); simpleExpression();
			setState(290); match(T__49);
			setState(291); identifier();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WildcardItemContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public WildcardItemContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_wildcardItem; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitWildcardItem(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WildcardItemContext wildcardItem() throws RecognitionException {
		WildcardItemContext _localctx = new WildcardItemContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_wildcardItem);
		try {
			setState(298);
			switch (_input.LA(1)) {
			case T__55:
				enterOuterAlt(_localctx, 1);
				{
				setState(293); match(T__55);
				}
				break;
			case QuotedIdentifier:
			case NonQuotedIdentifier:
				enterOuterAlt(_localctx, 2);
				{
				setState(294); identifier();
				setState(295); match(T__13);
				setState(296); match(T__55);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public SearchConditionContext searchCondition() {
			return getRuleContext(SearchConditionContext.class,0);
		}
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_expression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(300); searchCondition(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SearchConditionContext extends ParserRuleContext {
		public SearchConditionContext searchCondition(int i) {
			return getRuleContext(SearchConditionContext.class,i);
		}
		public SubSearchConditionContext subSearchCondition() {
			return getRuleContext(SubSearchConditionContext.class,0);
		}
		public List<SearchConditionContext> searchCondition() {
			return getRuleContexts(SearchConditionContext.class);
		}
		public SearchConditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_searchCondition; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitSearchCondition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SearchConditionContext searchCondition() throws RecognitionException {
		return searchCondition(0);
	}

	private SearchConditionContext searchCondition(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		SearchConditionContext _localctx = new SearchConditionContext(_ctx, _parentState);
		SearchConditionContext _prevctx = _localctx;
		int _startState = 52;
		enterRecursionRule(_localctx, 52, RULE_searchCondition, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(303); subSearchCondition();
			}
			_ctx.stop = _input.LT(-1);
			setState(313);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,30,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(311);
					switch ( getInterpreter().adaptivePredict(_input,29,_ctx) ) {
					case 1:
						{
						_localctx = new SearchConditionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_searchCondition);
						setState(305);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(306); match(T__18);
						setState(307); searchCondition(4);
						}
						break;

					case 2:
						{
						_localctx = new SearchConditionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_searchCondition);
						setState(308);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(309); match(T__40);
						setState(310); searchCondition(3);
						}
						break;
					}
					} 
				}
				setState(315);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,30,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class SubSearchConditionContext extends ParserRuleContext {
		public PredicateBETWEENContext predicateBETWEEN() {
			return getRuleContext(PredicateBETWEENContext.class,0);
		}
		public PredicateINContext predicateIN() {
			return getRuleContext(PredicateINContext.class,0);
		}
		public SearchConditionContext searchCondition() {
			return getRuleContext(SearchConditionContext.class,0);
		}
		public SimpleExpressionContext simpleExpression() {
			return getRuleContext(SimpleExpressionContext.class,0);
		}
		public PredicateLIKEContext predicateLIKE() {
			return getRuleContext(PredicateLIKEContext.class,0);
		}
		public PredicateNULLContext predicateNULL() {
			return getRuleContext(PredicateNULLContext.class,0);
		}
		public PredicateCompareContext predicateCompare() {
			return getRuleContext(PredicateCompareContext.class,0);
		}
		public NegateConditionContext negateCondition() {
			return getRuleContext(NegateConditionContext.class,0);
		}
		public SubSearchConditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_subSearchCondition; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitSubSearchCondition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SubSearchConditionContext subSearchCondition() throws RecognitionException {
		SubSearchConditionContext _localctx = new SubSearchConditionContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_subSearchCondition);
		try {
			setState(327);
			switch ( getInterpreter().adaptivePredict(_input,31,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(316); negateCondition();
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(317); match(T__21);
				setState(318); searchCondition(0);
				setState(319); match(T__38);
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(321); predicateCompare();
				}
				break;

			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(322); predicateNULL();
				}
				break;

			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(323); predicateLIKE();
				}
				break;

			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(324); predicateBETWEEN();
				}
				break;

			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(325); predicateIN();
				}
				break;

			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(326); simpleExpression();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NegateConditionContext extends ParserRuleContext {
		public SearchConditionContext searchCondition() {
			return getRuleContext(SearchConditionContext.class,0);
		}
		public NegateConditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_negateCondition; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitNegateCondition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NegateConditionContext negateCondition() throws RecognitionException {
		NegateConditionContext _localctx = new NegateConditionContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_negateCondition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(329); match(T__29);
			setState(330); searchCondition(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PredicateCompareContext extends ParserRuleContext {
		public SimpleExpressionContext simpleExpression(int i) {
			return getRuleContext(SimpleExpressionContext.class,i);
		}
		public List<SimpleExpressionContext> simpleExpression() {
			return getRuleContexts(SimpleExpressionContext.class);
		}
		public PredicateCompareContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_predicateCompare; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitPredicateCompare(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PredicateCompareContext predicateCompare() throws RecognitionException {
		PredicateCompareContext _localctx = new PredicateCompareContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_predicateCompare);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(332); simpleExpression();
			setState(333);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__53) | (1L << T__52) | (1L << T__50) | (1L << T__36) | (1L << T__10) | (1L << T__7) | (1L << T__1))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			setState(334); simpleExpression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PredicateNULLContext extends ParserRuleContext {
		public SimpleExpressionContext simpleExpression() {
			return getRuleContext(SimpleExpressionContext.class,0);
		}
		public PredicateNULLContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_predicateNULL; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitPredicateNULL(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PredicateNULLContext predicateNULL() throws RecognitionException {
		PredicateNULLContext _localctx = new PredicateNULLContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_predicateNULL);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(336); simpleExpression();
			setState(337); match(T__35);
			setState(339);
			_la = _input.LA(1);
			if (_la==T__29) {
				{
				setState(338); match(T__29);
				}
			}

			setState(341); match(T__14);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PredicateLIKEContext extends ParserRuleContext {
		public SimpleExpressionContext simpleExpression(int i) {
			return getRuleContext(SimpleExpressionContext.class,i);
		}
		public List<SimpleExpressionContext> simpleExpression() {
			return getRuleContexts(SimpleExpressionContext.class);
		}
		public PredicateLIKEContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_predicateLIKE; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitPredicateLIKE(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PredicateLIKEContext predicateLIKE() throws RecognitionException {
		PredicateLIKEContext _localctx = new PredicateLIKEContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_predicateLIKE);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(343); simpleExpression();
			setState(344); match(T__32);
			setState(345); simpleExpression();
			setState(348);
			switch ( getInterpreter().adaptivePredict(_input,33,_ctx) ) {
			case 1:
				{
				setState(346); match(T__30);
				setState(347); simpleExpression();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PredicateBETWEENContext extends ParserRuleContext {
		public SimpleExpressionContext simpleExpression(int i) {
			return getRuleContext(SimpleExpressionContext.class,i);
		}
		public List<SimpleExpressionContext> simpleExpression() {
			return getRuleContexts(SimpleExpressionContext.class);
		}
		public PredicateBETWEENContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_predicateBETWEEN; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitPredicateBETWEEN(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PredicateBETWEENContext predicateBETWEEN() throws RecognitionException {
		PredicateBETWEENContext _localctx = new PredicateBETWEENContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_predicateBETWEEN);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(350); simpleExpression();
			setState(351); match(T__47);
			setState(352); simpleExpression();
			setState(353); match(T__40);
			setState(354); simpleExpression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PredicateINContext extends ParserRuleContext {
		public ExpressionListContext expressionList() {
			return getRuleContext(ExpressionListContext.class,0);
		}
		public SelectStatementContext selectStatement() {
			return getRuleContext(SelectStatementContext.class,0);
		}
		public SimpleExpressionContext simpleExpression() {
			return getRuleContext(SimpleExpressionContext.class,0);
		}
		public PredicateINContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_predicateIN; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitPredicateIN(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PredicateINContext predicateIN() throws RecognitionException {
		PredicateINContext _localctx = new PredicateINContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_predicateIN);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(356); simpleExpression();
			setState(357); match(T__46);
			setState(358); match(T__21);
			setState(361);
			switch ( getInterpreter().adaptivePredict(_input,34,_ctx) ) {
			case 1:
				{
				setState(359); expressionList();
				}
				break;

			case 2:
				{
				setState(360); selectStatement();
				}
				break;
			}
			setState(363); match(T__38);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionListContext extends ParserRuleContext {
		public SimpleExpressionContext simpleExpression(int i) {
			return getRuleContext(SimpleExpressionContext.class,i);
		}
		public List<SimpleExpressionContext> simpleExpression() {
			return getRuleContexts(SimpleExpressionContext.class);
		}
		public ExpressionListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expressionList; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitExpressionList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionListContext expressionList() throws RecognitionException {
		ExpressionListContext _localctx = new ExpressionListContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_expressionList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(365); simpleExpression();
			setState(370);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__25) {
				{
				{
				setState(366); match(T__25);
				setState(367); simpleExpression();
				}
				}
				setState(372);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OrderByClauseContext extends ParserRuleContext {
		public OrderExpressionContext orderExpression(int i) {
			return getRuleContext(OrderExpressionContext.class,i);
		}
		public List<OrderExpressionContext> orderExpression() {
			return getRuleContexts(OrderExpressionContext.class);
		}
		public OrderByClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_orderByClause; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitOrderByClause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OrderByClauseContext orderByClause() throws RecognitionException {
		OrderByClauseContext _localctx = new OrderByClauseContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_orderByClause);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(373); match(T__43);
			setState(374); match(T__26);
			setState(375); orderExpression();
			setState(380);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__25) {
				{
				{
				setState(376); match(T__25);
				setState(377); orderExpression();
				}
				}
				setState(382);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OrderExpressionContext extends ParserRuleContext {
		public SimpleExpressionContext simpleExpression() {
			return getRuleContext(SimpleExpressionContext.class,0);
		}
		public OrderExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_orderExpression; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitOrderExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OrderExpressionContext orderExpression() throws RecognitionException {
		OrderExpressionContext _localctx = new OrderExpressionContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_orderExpression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(383); simpleExpression();
			setState(385);
			_la = _input.LA(1);
			if (_la==T__37 || _la==T__24) {
				{
				setState(384);
				_la = _input.LA(1);
				if ( !(_la==T__37 || _la==T__24) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GroupByClauseContext extends ParserRuleContext {
		public List<GroupExpressionContext> groupExpression() {
			return getRuleContexts(GroupExpressionContext.class);
		}
		public HavingClauseContext havingClause() {
			return getRuleContext(HavingClauseContext.class,0);
		}
		public GroupExpressionContext groupExpression(int i) {
			return getRuleContext(GroupExpressionContext.class,i);
		}
		public GroupByClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_groupByClause; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitGroupByClause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final GroupByClauseContext groupByClause() throws RecognitionException {
		GroupByClauseContext _localctx = new GroupByClauseContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_groupByClause);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(387); match(T__60);
			setState(388); match(T__26);
			setState(389); groupExpression();
			setState(394);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__25) {
				{
				{
				setState(390); match(T__25);
				setState(391); groupExpression();
				}
				}
				setState(396);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(398);
			_la = _input.LA(1);
			if (_la==T__23) {
				{
				setState(397); havingClause();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PivotClauseContext extends ParserRuleContext {
		public IntegerLiteralContext integerLiteral() {
			return getRuleContext(IntegerLiteralContext.class,0);
		}
		public SearchConditionContext searchCondition(int i) {
			return getRuleContext(SearchConditionContext.class,i);
		}
		public List<SearchConditionContext> searchCondition() {
			return getRuleContexts(SearchConditionContext.class);
		}
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public PivotClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pivotClause; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitPivotClause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PivotClauseContext pivotClause() throws RecognitionException {
		PivotClauseContext _localctx = new PivotClauseContext(_ctx, getState());
		enterRule(_localctx, 76, RULE_pivotClause);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(400); match(T__2);
			setState(401); searchCondition(0);
			setState(402); match(T__8);
			setState(403); searchCondition(0);
			setState(405);
			_la = _input.LA(1);
			if (_la==T__37 || _la==T__24) {
				{
				setState(404);
				_la = _input.LA(1);
				if ( !(_la==T__37 || _la==T__24) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				}
			}

			setState(412);
			_la = _input.LA(1);
			if (_la==T__59 || _la==T__6) {
				{
				setState(407);
				_la = _input.LA(1);
				if ( !(_la==T__59 || _la==T__6) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(410);
				switch (_input.LA(1)) {
				case Integer:
					{
					setState(408); integerLiteral();
					}
					break;
				case QuotedIdentifier:
				case NonQuotedIdentifier:
					{
					setState(409); identifier();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GroupExpressionContext extends ParserRuleContext {
		public SimpleExpressionContext simpleExpression() {
			return getRuleContext(SimpleExpressionContext.class,0);
		}
		public GroupExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_groupExpression; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitGroupExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final GroupExpressionContext groupExpression() throws RecognitionException {
		GroupExpressionContext _localctx = new GroupExpressionContext(_ctx, getState());
		enterRule(_localctx, 78, RULE_groupExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(414); simpleExpression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class HavingClauseContext extends ParserRuleContext {
		public SearchConditionContext searchCondition() {
			return getRuleContext(SearchConditionContext.class,0);
		}
		public HavingClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_havingClause; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitHavingClause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final HavingClauseContext havingClause() throws RecognitionException {
		HavingClauseContext _localctx = new HavingClauseContext(_ctx, getState());
		enterRule(_localctx, 80, RULE_havingClause);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(416); match(T__23);
			setState(417); searchCondition(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SimpleExpressionContext extends ParserRuleContext {
		public ArithmeticExpressionContext arithmeticExpression() {
			return getRuleContext(ArithmeticExpressionContext.class,0);
		}
		public SimpleExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_simpleExpression; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitSimpleExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SimpleExpressionContext simpleExpression() throws RecognitionException {
		SimpleExpressionContext _localctx = new SimpleExpressionContext(_ctx, getState());
		enterRule(_localctx, 82, RULE_simpleExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(419); arithmeticExpression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArithmeticExpressionContext extends ParserRuleContext {
		public List<ArithmeticExpressionContext> arithmeticExpression() {
			return getRuleContexts(ArithmeticExpressionContext.class);
		}
		public SubExpressionContext subExpression() {
			return getRuleContext(SubExpressionContext.class,0);
		}
		public ArithmeticExpressionContext arithmeticExpression(int i) {
			return getRuleContext(ArithmeticExpressionContext.class,i);
		}
		public ArithmeticExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arithmeticExpression; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitArithmeticExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArithmeticExpressionContext arithmeticExpression() throws RecognitionException {
		return arithmeticExpression(0);
	}

	private ArithmeticExpressionContext arithmeticExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ArithmeticExpressionContext _localctx = new ArithmeticExpressionContext(_ctx, _parentState);
		ArithmeticExpressionContext _prevctx = _localctx;
		int _startState = 84;
		enterRecursionRule(_localctx, 84, RULE_arithmeticExpression, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(422); subExpression();
			}
			_ctx.stop = _input.LT(-1);
			setState(432);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,44,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(430);
					switch ( getInterpreter().adaptivePredict(_input,43,_ctx) ) {
					case 1:
						{
						_localctx = new ArithmeticExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_arithmeticExpression);
						setState(424);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(425);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__55) | (1L << T__41) | (1L << T__3))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						consume();
						setState(426); arithmeticExpression(4);
						}
						break;

					case 2:
						{
						_localctx = new ArithmeticExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_arithmeticExpression);
						setState(427);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(428);
						_la = _input.LA(1);
						if ( !(_la==T__22 || _la==T__11) ) {
						_errHandler.recoverInline(this);
						}
						consume();
						setState(429); arithmeticExpression(3);
						}
						break;
					}
					} 
				}
				setState(434);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,44,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class UnaryOperatorContext extends ParserRuleContext {
		public UnaryOperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unaryOperator; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitUnaryOperator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UnaryOperatorContext unaryOperator() throws RecognitionException {
		UnaryOperatorContext _localctx = new UnaryOperatorContext(_ctx, getState());
		enterRule(_localctx, 86, RULE_unaryOperator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(435);
			_la = _input.LA(1);
			if ( !(_la==T__22 || _la==T__11) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SubExpressionContext extends ParserRuleContext {
		public UnaryOperatorContext unaryOperator() {
			return getRuleContext(UnaryOperatorContext.class,0);
		}
		public PrimaryExpressionContext primaryExpression() {
			return getRuleContext(PrimaryExpressionContext.class,0);
		}
		public SubExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_subExpression; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitSubExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SubExpressionContext subExpression() throws RecognitionException {
		SubExpressionContext _localctx = new SubExpressionContext(_ctx, getState());
		enterRule(_localctx, 88, RULE_subExpression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(438);
			_la = _input.LA(1);
			if (_la==T__22 || _la==T__11) {
				{
				setState(437); unaryOperator();
				}
			}

			setState(440); primaryExpression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrimaryExpressionContext extends ParserRuleContext {
		public FunctionContext function() {
			return getRuleContext(FunctionContext.class,0);
		}
		public ColumnNameContext columnName() {
			return getRuleContext(ColumnNameContext.class,0);
		}
		public CaseFunctionContext caseFunction() {
			return getRuleContext(CaseFunctionContext.class,0);
		}
		public SimpleExpressionContext simpleExpression() {
			return getRuleContext(SimpleExpressionContext.class,0);
		}
		public ConstantContext constant() {
			return getRuleContext(ConstantContext.class,0);
		}
		public PrimaryExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_primaryExpression; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitPrimaryExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PrimaryExpressionContext primaryExpression() throws RecognitionException {
		PrimaryExpressionContext _localctx = new PrimaryExpressionContext(_ctx, getState());
		enterRule(_localctx, 90, RULE_primaryExpression);
		try {
			setState(450);
			switch ( getInterpreter().adaptivePredict(_input,46,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(442); constant();
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(443); function();
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(444); match(T__21);
				{
				setState(445); simpleExpression();
				}
				setState(446); match(T__38);
				}
				break;

			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(448); columnName();
				}
				break;

			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(449); caseFunction();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ColumnNameContext extends ParserRuleContext {
		public IdentifierContext identifier(int i) {
			return getRuleContext(IdentifierContext.class,i);
		}
		public List<IdentifierContext> identifier() {
			return getRuleContexts(IdentifierContext.class);
		}
		public ColumnNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_columnName; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitColumnName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ColumnNameContext columnName() throws RecognitionException {
		ColumnNameContext _localctx = new ColumnNameContext(_ctx, getState());
		enterRule(_localctx, 92, RULE_columnName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(452); identifier();
			setState(455);
			switch ( getInterpreter().adaptivePredict(_input,47,_ctx) ) {
			case 1:
				{
				setState(453); match(T__13);
				setState(454); identifier();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionContext extends ParserRuleContext {
		public FunctionParameterListContext functionParameterList() {
			return getRuleContext(FunctionParameterListContext.class,0);
		}
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public FunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionContext function() throws RecognitionException {
		FunctionContext _localctx = new FunctionContext(_ctx, getState());
		enterRule(_localctx, 94, RULE_function);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(457); identifier();
			setState(458); match(T__21);
			{
			setState(460);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__58) | (1L << T__57) | (1L << T__55) | (1L << T__45) | (1L << T__44) | (1L << T__29) | (1L << T__22) | (1L << T__21) | (1L << T__16) | (1L << T__14) | (1L << T__11))) != 0) || ((((_la - 65)) & ~0x3f) == 0 && ((1L << (_la - 65)) & ((1L << (QuotedIdentifier - 65)) | (1L << (NonQuotedIdentifier - 65)) | (1L << (ASCIIStringLiteral - 65)) | (1L << (Integer - 65)) | (1L << (Real - 65)) | (1L << (HexLiteral - 65)))) != 0)) {
				{
				setState(459); functionParameterList();
				}
			}

			}
			setState(462); match(T__38);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WildCardParameterContext extends ParserRuleContext {
		public WildCardParameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_wildCardParameter; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitWildCardParameter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WildCardParameterContext wildCardParameter() throws RecognitionException {
		WildCardParameterContext _localctx = new WildCardParameterContext(_ctx, getState());
		enterRule(_localctx, 96, RULE_wildCardParameter);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(464); match(T__55);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionParameterListContext extends ParserRuleContext {
		public FunctionParameterContext functionParameter(int i) {
			return getRuleContext(FunctionParameterContext.class,i);
		}
		public List<FunctionParameterContext> functionParameter() {
			return getRuleContexts(FunctionParameterContext.class);
		}
		public FunctionParameterListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionParameterList; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitFunctionParameterList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionParameterListContext functionParameterList() throws RecognitionException {
		FunctionParameterListContext _localctx = new FunctionParameterListContext(_ctx, getState());
		enterRule(_localctx, 98, RULE_functionParameterList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(466); functionParameter();
			setState(471);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__25) {
				{
				{
				setState(467); match(T__25);
				setState(468); functionParameter();
				}
				}
				setState(473);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionParameterContext extends ParserRuleContext {
		public WildcardItemContext wildcardItem() {
			return getRuleContext(WildcardItemContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public FunctionParameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionParameter; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitFunctionParameter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionParameterContext functionParameter() throws RecognitionException {
		FunctionParameterContext _localctx = new FunctionParameterContext(_ctx, getState());
		enterRule(_localctx, 100, RULE_functionParameter);
		int _la;
		try {
			setState(479);
			switch ( getInterpreter().adaptivePredict(_input,51,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(475);
				_la = _input.LA(1);
				if (_la==T__58 || _la==T__57) {
					{
					setState(474);
					_la = _input.LA(1);
					if ( !(_la==T__58 || _la==T__57) ) {
					_errHandler.recoverInline(this);
					}
					consume();
					}
				}

				setState(477); expression();
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(478); wildcardItem();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CaseFunctionContext extends ParserRuleContext {
		public WhenExpressionContext whenExpression(int i) {
			return getRuleContext(WhenExpressionContext.class,i);
		}
		public ElseExpressionContext elseExpression() {
			return getRuleContext(ElseExpressionContext.class,0);
		}
		public List<WhenExpressionContext> whenExpression() {
			return getRuleContexts(WhenExpressionContext.class);
		}
		public SimpleExpressionContext simpleExpression() {
			return getRuleContext(SimpleExpressionContext.class,0);
		}
		public CaseFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_caseFunction; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitCaseFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CaseFunctionContext caseFunction() throws RecognitionException {
		CaseFunctionContext _localctx = new CaseFunctionContext(_ctx, getState());
		enterRule(_localctx, 102, RULE_caseFunction);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(481); match(T__44);
			setState(483);
			_la = _input.LA(1);
			if (((((_la - 16)) & ~0x3f) == 0 && ((1L << (_la - 16)) & ((1L << (T__45 - 16)) | (1L << (T__44 - 16)) | (1L << (T__22 - 16)) | (1L << (T__21 - 16)) | (1L << (T__16 - 16)) | (1L << (T__14 - 16)) | (1L << (T__11 - 16)) | (1L << (QuotedIdentifier - 16)) | (1L << (NonQuotedIdentifier - 16)) | (1L << (ASCIIStringLiteral - 16)) | (1L << (Integer - 16)) | (1L << (Real - 16)) | (1L << (HexLiteral - 16)))) != 0)) {
				{
				setState(482); simpleExpression();
				}
			}

			setState(486); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(485); whenExpression();
				}
				}
				setState(488); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==T__5 );
			setState(491);
			_la = _input.LA(1);
			if (_la==T__48) {
				{
				setState(490); elseExpression();
				}
			}

			setState(493); match(T__51);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WhenExpressionContext extends ParserRuleContext {
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public WhenExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_whenExpression; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitWhenExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WhenExpressionContext whenExpression() throws RecognitionException {
		WhenExpressionContext _localctx = new WhenExpressionContext(_ctx, getState());
		enterRule(_localctx, 104, RULE_whenExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(495); match(T__5);
			setState(496); expression();
			setState(497); match(T__27);
			setState(498); expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ElseExpressionContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ElseExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_elseExpression; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitElseExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ElseExpressionContext elseExpression() throws RecognitionException {
		ElseExpressionContext _localctx = new ElseExpressionContext(_ctx, getState());
		enterRule(_localctx, 106, RULE_elseExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(500); match(T__48);
			setState(501); expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IdentifierContext extends ParserRuleContext {
		public TerminalNode QuotedIdentifier() { return getToken(SelectParser.QuotedIdentifier, 0); }
		public TerminalNode NonQuotedIdentifier() { return getToken(SelectParser.NonQuotedIdentifier, 0); }
		public IdentifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_identifier; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitIdentifier(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IdentifierContext identifier() throws RecognitionException {
		IdentifierContext _localctx = new IdentifierContext(_ctx, getState());
		enterRule(_localctx, 108, RULE_identifier);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(503);
			_la = _input.LA(1);
			if ( !(_la==QuotedIdentifier || _la==NonQuotedIdentifier) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstantContext extends ParserRuleContext {
		public ReservedConstantContext reservedConstant() {
			return getRuleContext(ReservedConstantContext.class,0);
		}
		public NumberContext number() {
			return getRuleContext(NumberContext.class,0);
		}
		public StringLiteralContext stringLiteral() {
			return getRuleContext(StringLiteralContext.class,0);
		}
		public HexLiteralContext hexLiteral() {
			return getRuleContext(HexLiteralContext.class,0);
		}
		public ConstantContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constant; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitConstant(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConstantContext constant() throws RecognitionException {
		ConstantContext _localctx = new ConstantContext(_ctx, getState());
		enterRule(_localctx, 110, RULE_constant);
		try {
			setState(509);
			switch ( getInterpreter().adaptivePredict(_input,55,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(505); number();
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(506); stringLiteral();
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(507); hexLiteral();
				}
				break;

			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(508); reservedConstant();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ReservedConstantContext extends ParserRuleContext {
		public ReservedConstantContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_reservedConstant; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitReservedConstant(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ReservedConstantContext reservedConstant() throws RecognitionException {
		ReservedConstantContext _localctx = new ReservedConstantContext(_ctx, getState());
		enterRule(_localctx, 112, RULE_reservedConstant);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(511);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__45) | (1L << T__16) | (1L << T__14))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntegerLiteralContext extends ParserRuleContext {
		public TerminalNode Integer() { return getToken(SelectParser.Integer, 0); }
		public IntegerLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_integerLiteral; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitIntegerLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IntegerLiteralContext integerLiteral() throws RecognitionException {
		IntegerLiteralContext _localctx = new IntegerLiteralContext(_ctx, getState());
		enterRule(_localctx, 114, RULE_integerLiteral);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(513); match(Integer);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RealLiteralContext extends ParserRuleContext {
		public TerminalNode Real() { return getToken(SelectParser.Real, 0); }
		public RealLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_realLiteral; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitRealLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RealLiteralContext realLiteral() throws RecognitionException {
		RealLiteralContext _localctx = new RealLiteralContext(_ctx, getState());
		enterRule(_localctx, 116, RULE_realLiteral);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(515); match(Real);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StringLiteralContext extends ParserRuleContext {
		public TerminalNode ASCIIStringLiteral() { return getToken(SelectParser.ASCIIStringLiteral, 0); }
		public StringLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stringLiteral; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitStringLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StringLiteralContext stringLiteral() throws RecognitionException {
		StringLiteralContext _localctx = new StringLiteralContext(_ctx, getState());
		enterRule(_localctx, 118, RULE_stringLiteral);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(517); match(ASCIIStringLiteral);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class HexLiteralContext extends ParserRuleContext {
		public TerminalNode HexLiteral() { return getToken(SelectParser.HexLiteral, 0); }
		public HexLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_hexLiteral; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitHexLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final HexLiteralContext hexLiteral() throws RecognitionException {
		HexLiteralContext _localctx = new HexLiteralContext(_ctx, getState());
		enterRule(_localctx, 120, RULE_hexLiteral);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(519); match(HexLiteral);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NumberContext extends ParserRuleContext {
		public RealLiteralContext realLiteral() {
			return getRuleContext(RealLiteralContext.class,0);
		}
		public IntegerLiteralContext integerLiteral() {
			return getRuleContext(IntegerLiteralContext.class,0);
		}
		public HexLiteralContext hexLiteral() {
			return getRuleContext(HexLiteralContext.class,0);
		}
		public NumberContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_number; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SelectVisitor ) return ((SelectVisitor<? extends T>)visitor).visitNumber(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NumberContext number() throws RecognitionException {
		NumberContext _localctx = new NumberContext(_ctx, getState());
		enterRule(_localctx, 122, RULE_number);
		try {
			setState(524);
			switch (_input.LA(1)) {
			case Integer:
				enterOuterAlt(_localctx, 1);
				{
				setState(521); integerLiteral();
				}
				break;
			case Real:
				enterOuterAlt(_localctx, 2);
				{
				setState(522); realLiteral();
				}
				break;
			case HexLiteral:
				enterOuterAlt(_localctx, 3);
				{
				setState(523); hexLiteral();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 26: return searchCondition_sempred((SearchConditionContext)_localctx, predIndex);

		case 42: return arithmeticExpression_sempred((ArithmeticExpressionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean arithmeticExpression_sempred(ArithmeticExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 2: return precpred(_ctx, 3);

		case 3: return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean searchCondition_sempred(SearchConditionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0: return precpred(_ctx, 3);

		case 1: return precpred(_ctx, 2);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3I\u0211\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t="+
		"\4>\t>\4?\t?\3\2\3\2\5\2\u0081\n\2\3\2\3\2\3\3\3\3\5\3\u0087\n\3\3\3\5"+
		"\3\u008a\n\3\3\4\3\4\5\4\u008e\n\4\3\5\3\5\5\5\u0092\n\5\3\5\3\5\3\5\5"+
		"\5\u0097\n\5\3\5\5\5\u009a\n\5\3\5\5\5\u009d\n\5\3\6\3\6\3\6\7\6\u00a2"+
		"\n\6\f\6\16\6\u00a5\13\6\3\7\3\7\3\7\3\7\3\7\5\7\u00ac\n\7\3\b\3\b\3\b"+
		"\3\b\5\b\u00b2\n\b\3\t\3\t\3\t\3\n\3\n\7\n\u00b9\n\n\f\n\16\n\u00bc\13"+
		"\n\3\13\3\13\5\13\u00c0\n\13\3\13\3\13\5\13\u00c4\n\13\3\f\3\f\3\f\3\f"+
		"\3\f\3\f\5\f\u00cc\n\f\3\r\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\16\3"+
		"\16\5\16\u00d9\n\16\5\16\u00db\n\16\5\16\u00dd\n\16\3\16\3\16\3\16\3\16"+
		"\3\16\5\16\u00e4\n\16\3\17\3\17\3\17\3\17\3\17\3\17\5\17\u00ec\n\17\3"+
		"\20\3\20\5\20\u00f0\n\20\3\21\3\21\3\21\3\21\3\21\5\21\u00f7\n\21\3\21"+
		"\5\21\u00fa\n\21\3\22\3\22\3\22\7\22\u00ff\n\22\f\22\16\22\u0102\13\22"+
		"\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\25\3\25\3\25\7\25\u010e\n\25\f\25"+
		"\16\25\u0111\13\25\3\26\3\26\3\26\7\26\u0116\n\26\f\26\16\26\u0119\13"+
		"\26\3\27\3\27\5\27\u011d\n\27\3\30\3\30\3\30\5\30\u0122\n\30\3\31\3\31"+
		"\3\31\3\31\3\32\3\32\3\32\3\32\3\32\5\32\u012d\n\32\3\33\3\33\3\34\3\34"+
		"\3\34\3\34\3\34\3\34\3\34\3\34\3\34\7\34\u013a\n\34\f\34\16\34\u013d\13"+
		"\34\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\5\35\u014a"+
		"\n\35\3\36\3\36\3\36\3\37\3\37\3\37\3\37\3 \3 \3 \5 \u0156\n \3 \3 \3"+
		"!\3!\3!\3!\3!\5!\u015f\n!\3\"\3\"\3\"\3\"\3\"\3\"\3#\3#\3#\3#\3#\5#\u016c"+
		"\n#\3#\3#\3$\3$\3$\7$\u0173\n$\f$\16$\u0176\13$\3%\3%\3%\3%\3%\7%\u017d"+
		"\n%\f%\16%\u0180\13%\3&\3&\5&\u0184\n&\3\'\3\'\3\'\3\'\3\'\7\'\u018b\n"+
		"\'\f\'\16\'\u018e\13\'\3\'\5\'\u0191\n\'\3(\3(\3(\3(\3(\5(\u0198\n(\3"+
		"(\3(\3(\5(\u019d\n(\5(\u019f\n(\3)\3)\3*\3*\3*\3+\3+\3,\3,\3,\3,\3,\3"+
		",\3,\3,\3,\7,\u01b1\n,\f,\16,\u01b4\13,\3-\3-\3.\5.\u01b9\n.\3.\3.\3/"+
		"\3/\3/\3/\3/\3/\3/\3/\5/\u01c5\n/\3\60\3\60\3\60\5\60\u01ca\n\60\3\61"+
		"\3\61\3\61\5\61\u01cf\n\61\3\61\3\61\3\62\3\62\3\63\3\63\3\63\7\63\u01d8"+
		"\n\63\f\63\16\63\u01db\13\63\3\64\5\64\u01de\n\64\3\64\3\64\5\64\u01e2"+
		"\n\64\3\65\3\65\5\65\u01e6\n\65\3\65\6\65\u01e9\n\65\r\65\16\65\u01ea"+
		"\3\65\5\65\u01ee\n\65\3\65\3\65\3\66\3\66\3\66\3\66\3\66\3\67\3\67\3\67"+
		"\38\38\39\39\39\39\59\u0200\n9\3:\3:\3;\3;\3<\3<\3=\3=\3>\3>\3?\3?\3?"+
		"\5?\u020f\n?\3?\2\4\66V@\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&("+
		"*,.\60\62\64\668:<>@BDFHJLNPRTVXZ\\^`bdfhjlnprtvxz|\2\f\3\2\5\6\4\2\36"+
		"\36&&\5\2\7\7,,\63\63\b\2\n\13\r\r\33\33\65\6588>>\4\2\32\32\'\'\4\2\4"+
		"\499\5\2\b\b\26\26<<\4\2))\64\64\3\2CD\5\2\22\22//\61\61\u0218\2~\3\2"+
		"\2\2\4\u0084\3\2\2\2\6\u008d\3\2\2\2\b\u008f\3\2\2\2\n\u009e\3\2\2\2\f"+
		"\u00ab\3\2\2\2\16\u00ad\3\2\2\2\20\u00b3\3\2\2\2\22\u00b6\3\2\2\2\24\u00bd"+
		"\3\2\2\2\26\u00c5\3\2\2\2\30\u00cd\3\2\2\2\32\u00e3\3\2\2\2\34\u00eb\3"+
		"\2\2\2\36\u00ed\3\2\2\2 \u00f1\3\2\2\2\"\u00fb\3\2\2\2$\u0103\3\2\2\2"+
		"&\u0107\3\2\2\2(\u010a\3\2\2\2*\u0112\3\2\2\2,\u011c\3\2\2\2.\u011e\3"+
		"\2\2\2\60\u0123\3\2\2\2\62\u012c\3\2\2\2\64\u012e\3\2\2\2\66\u0130\3\2"+
		"\2\28\u0149\3\2\2\2:\u014b\3\2\2\2<\u014e\3\2\2\2>\u0152\3\2\2\2@\u0159"+
		"\3\2\2\2B\u0160\3\2\2\2D\u0166\3\2\2\2F\u016f\3\2\2\2H\u0177\3\2\2\2J"+
		"\u0181\3\2\2\2L\u0185\3\2\2\2N\u0192\3\2\2\2P\u01a0\3\2\2\2R\u01a2\3\2"+
		"\2\2T\u01a5\3\2\2\2V\u01a7\3\2\2\2X\u01b5\3\2\2\2Z\u01b8\3\2\2\2\\\u01c4"+
		"\3\2\2\2^\u01c6\3\2\2\2`\u01cb\3\2\2\2b\u01d2\3\2\2\2d\u01d4\3\2\2\2f"+
		"\u01e1\3\2\2\2h\u01e3\3\2\2\2j\u01f1\3\2\2\2l\u01f6\3\2\2\2n\u01f9\3\2"+
		"\2\2p\u01ff\3\2\2\2r\u0201\3\2\2\2t\u0203\3\2\2\2v\u0205\3\2\2\2x\u0207"+
		"\3\2\2\2z\u0209\3\2\2\2|\u020e\3\2\2\2~\u0080\5\4\3\2\177\u0081\7\66\2"+
		"\2\u0080\177\3\2\2\2\u0080\u0081\3\2\2\2\u0081\u0082\3\2\2\2\u0082\u0083"+
		"\7\2\2\3\u0083\3\3\2\2\2\u0084\u0086\5\6\4\2\u0085\u0087\5H%\2\u0086\u0085"+
		"\3\2\2\2\u0086\u0087\3\2\2\2\u0087\u0089\3\2\2\2\u0088\u008a\5\16\b\2"+
		"\u0089\u0088\3\2\2\2\u0089\u008a\3\2\2\2\u008a\5\3\2\2\2\u008b\u008e\5"+
		"\b\5\2\u008c\u008e\5\n\6\2\u008d\u008b\3\2\2\2\u008d\u008c\3\2\2\2\u008e"+
		"\7\3\2\2\2\u008f\u0091\7#\2\2\u0090\u0092\t\2\2\2\u0091\u0090\3\2\2\2"+
		"\u0091\u0092\3\2\2\2\u0092\u0093\3\2\2\2\u0093\u0094\5(\25\2\u0094\u0096"+
		"\5\20\t\2\u0095\u0097\5&\24\2\u0096\u0095\3\2\2\2\u0096\u0097\3\2\2\2"+
		"\u0097\u0099\3\2\2\2\u0098\u009a\5L\'\2\u0099\u0098\3\2\2\2\u0099\u009a"+
		"\3\2\2\2\u009a\u009c\3\2\2\2\u009b\u009d\5N(\2\u009c\u009b\3\2\2\2\u009c"+
		"\u009d\3\2\2\2\u009d\t\3\2\2\2\u009e\u00a3\5\f\7\2\u009f\u00a0\7?\2\2"+
		"\u00a0\u00a2\5\f\7\2\u00a1\u009f\3\2\2\2\u00a2\u00a5\3\2\2\2\u00a3\u00a1"+
		"\3\2\2\2\u00a3\u00a4\3\2\2\2\u00a4\13\3\2\2\2\u00a5\u00a3\3\2\2\2\u00a6"+
		"\u00a7\7*\2\2\u00a7\u00a8\5\4\3\2\u00a8\u00a9\7\31\2\2\u00a9\u00ac\3\2"+
		"\2\2\u00aa\u00ac\5\b\5\2\u00ab\u00a6\3\2\2\2\u00ab\u00aa\3\2\2\2\u00ac"+
		"\r\3\2\2\2\u00ad\u00ae\7;\2\2\u00ae\u00b1\5t;\2\u00af\u00b0\t\3\2\2\u00b0"+
		"\u00b2\5t;\2\u00b1\u00af\3\2\2\2\u00b1\u00b2\3\2\2\2\u00b2\17\3\2\2\2"+
		"\u00b3\u00b4\7 \2\2\u00b4\u00b5\5\22\n\2\u00b5\21\3\2\2\2\u00b6\u00ba"+
		"\5\24\13\2\u00b7\u00b9\5\32\16\2\u00b8\u00b7\3\2\2\2\u00b9\u00bc\3\2\2"+
		"\2\u00ba\u00b8\3\2\2\2\u00ba\u00bb\3\2\2\2\u00bb\23\3\2\2\2\u00bc\u00ba"+
		"\3\2\2\2\u00bd\u00bf\5\34\17\2\u00be\u00c0\5\30\r\2\u00bf\u00be\3\2\2"+
		"\2\u00bf\u00c0\3\2\2\2\u00c0\u00c3\3\2\2\2\u00c1\u00c2\7\16\2\2\u00c2"+
		"\u00c4\5n8\2\u00c3\u00c1\3\2\2\2\u00c3\u00c4\3\2\2\2\u00c4\25\3\2\2\2"+
		"\u00c5\u00c6\7\60\2\2\u00c6\u00cb\5n8\2\u00c7\u00c8\7*\2\2\u00c8\u00c9"+
		"\5\"\22\2\u00c9\u00ca\7\31\2\2\u00ca\u00cc\3\2\2\2\u00cb\u00c7\3\2\2\2"+
		"\u00cb\u00cc\3\2\2\2\u00cc\27\3\2\2\2\u00cd\u00ce\7\25\2\2\u00ce\u00cf"+
		"\7*\2\2\u00cf\u00d0\5*\26\2\u00d0\u00d1\7\31\2\2\u00d1\31\3\2\2\2\u00d2"+
		"\u00d3\7+\2\2\u00d3\u00d4\7.\2\2\u00d4\u00e4\5\24\13\2\u00d5\u00db\7\30"+
		"\2\2\u00d6\u00d8\t\4\2\2\u00d7\u00d9\7\35\2\2\u00d8\u00d7\3\2\2\2\u00d8"+
		"\u00d9\3\2\2\2\u00d9\u00db\3\2\2\2\u00da\u00d5\3\2\2\2\u00da\u00d6\3\2"+
		"\2\2\u00db\u00dd\3\2\2\2\u00dc\u00da\3\2\2\2\u00dc\u00dd\3\2\2\2\u00dd"+
		"\u00de\3\2\2\2\u00de\u00df\7.\2\2\u00df\u00e0\5\22\n\2\u00e0\u00e1\7\67"+
		"\2\2\u00e1\u00e2\5\66\34\2\u00e2\u00e4\3\2\2\2\u00e3\u00d2\3\2\2\2\u00e3"+
		"\u00dc\3\2\2\2\u00e4\33\3\2\2\2\u00e5\u00ec\5\36\20\2\u00e6\u00ec\5 \21"+
		"\2\u00e7\u00e8\7*\2\2\u00e8\u00e9\5\4\3\2\u00e9\u00ea\7\31\2\2\u00ea\u00ec"+
		"\3\2\2\2\u00eb\u00e5\3\2\2\2\u00eb\u00e6\3\2\2\2\u00eb\u00e7\3\2\2\2\u00ec"+
		"\35\3\2\2\2\u00ed\u00ef\7E\2\2\u00ee\u00f0\5\26\f\2\u00ef\u00ee\3\2\2"+
		"\2\u00ef\u00f0\3\2\2\2\u00f0\37\3\2\2\2\u00f1\u00f6\5n8\2\u00f2\u00f3"+
		"\7*\2\2\u00f3\u00f4\5\"\22\2\u00f4\u00f5\7\31\2\2\u00f5\u00f7\3\2\2\2"+
		"\u00f6\u00f2\3\2\2\2\u00f6\u00f7\3\2\2\2\u00f7\u00f9\3\2\2\2\u00f8\u00fa"+
		"\5\26\f\2\u00f9\u00f8\3\2\2\2\u00f9\u00fa\3\2\2\2\u00fa!\3\2\2\2\u00fb"+
		"\u0100\5$\23\2\u00fc\u00fd\7&\2\2\u00fd\u00ff\5$\23\2\u00fe\u00fc\3\2"+
		"\2\2\u00ff\u0102\3\2\2\2\u0100\u00fe\3\2\2\2\u0100\u0101\3\2\2\2\u0101"+
		"#\3\2\2\2\u0102\u0100\3\2\2\2\u0103\u0104\5n8\2\u0104\u0105\7\33\2\2\u0105"+
		"\u0106\5T+\2\u0106%\3\2\2\2\u0107\u0108\7\t\2\2\u0108\u0109\5\66\34\2"+
		"\u0109\'\3\2\2\2\u010a\u010f\5,\27\2\u010b\u010c\7&\2\2\u010c\u010e\5"+
		",\27\2\u010d\u010b\3\2\2\2\u010e\u0111\3\2\2\2\u010f\u010d\3\2\2\2\u010f"+
		"\u0110\3\2\2\2\u0110)\3\2\2\2\u0111\u010f\3\2\2\2\u0112\u0117\5\60\31"+
		"\2\u0113\u0114\7&\2\2\u0114\u0116\5\60\31\2\u0115\u0113\3\2\2\2\u0116"+
		"\u0119\3\2\2\2\u0117\u0115\3\2\2\2\u0117\u0118\3\2\2\2\u0118+\3\2\2\2"+
		"\u0119\u0117\3\2\2\2\u011a\u011d\5\62\32\2\u011b\u011d\5.\30\2\u011c\u011a"+
		"\3\2\2\2\u011c\u011b\3\2\2\2\u011d-\3\2\2\2\u011e\u0121\5\66\34\2\u011f"+
		"\u0120\7\16\2\2\u0120\u0122\5n8\2\u0121\u011f\3\2\2\2\u0121\u0122\3\2"+
		"\2\2\u0122/\3\2\2\2\u0123\u0124\5T+\2\u0124\u0125\7\16\2\2\u0125\u0126"+
		"\5n8\2\u0126\61\3\2\2\2\u0127\u012d\7\b\2\2\u0128\u0129\5n8\2\u0129\u012a"+
		"\7\62\2\2\u012a\u012b\7\b\2\2\u012b\u012d\3\2\2\2\u012c\u0127\3\2\2\2"+
		"\u012c\u0128\3\2\2\2\u012d\63\3\2\2\2\u012e\u012f\5\66\34\2\u012f\65\3"+
		"\2\2\2\u0130\u0131\b\34\1\2\u0131\u0132\58\35\2\u0132\u013b\3\2\2\2\u0133"+
		"\u0134\f\5\2\2\u0134\u0135\7-\2\2\u0135\u013a\5\66\34\6\u0136\u0137\f"+
		"\4\2\2\u0137\u0138\7\27\2\2\u0138\u013a\5\66\34\5\u0139\u0133\3\2\2\2"+
		"\u0139\u0136\3\2\2\2\u013a\u013d\3\2\2\2\u013b\u0139\3\2\2\2\u013b\u013c"+
		"\3\2\2\2\u013c\67\3\2\2\2\u013d\u013b\3\2\2\2\u013e\u014a\5:\36\2\u013f"+
		"\u0140\7*\2\2\u0140\u0141\5\66\34\2\u0141\u0142\7\31\2\2\u0142\u014a\3"+
		"\2\2\2\u0143\u014a\5<\37\2\u0144\u014a\5> \2\u0145\u014a\5@!\2\u0146\u014a"+
		"\5B\"\2\u0147\u014a\5D#\2\u0148\u014a\5T+\2\u0149\u013e\3\2\2\2\u0149"+
		"\u013f\3\2\2\2\u0149\u0143\3\2\2\2\u0149\u0144\3\2\2\2\u0149\u0145\3\2"+
		"\2\2\u0149\u0146\3\2\2\2\u0149\u0147\3\2\2\2\u0149\u0148\3\2\2\2\u014a"+
		"9\3\2\2\2\u014b\u014c\7\"\2\2\u014c\u014d\5\66\34\2\u014d;\3\2\2\2\u014e"+
		"\u014f\5T+\2\u014f\u0150\t\5\2\2\u0150\u0151\5T+\2\u0151=\3\2\2\2\u0152"+
		"\u0153\5T+\2\u0153\u0155\7\34\2\2\u0154\u0156\7\"\2\2\u0155\u0154\3\2"+
		"\2\2\u0155\u0156\3\2\2\2\u0156\u0157\3\2\2\2\u0157\u0158\7\61\2\2\u0158"+
		"?\3\2\2\2\u0159\u015a\5T+\2\u015a\u015b\7\37\2\2\u015b\u015e\5T+\2\u015c"+
		"\u015d\7!\2\2\u015d\u015f\5T+\2\u015e\u015c\3\2\2\2\u015e\u015f\3\2\2"+
		"\2\u015fA\3\2\2\2\u0160\u0161\5T+\2\u0161\u0162\7\20\2\2\u0162\u0163\5"+
		"T+\2\u0163\u0164\7\27\2\2\u0164\u0165\5T+\2\u0165C\3\2\2\2\u0166\u0167"+
		"\5T+\2\u0167\u0168\7\21\2\2\u0168\u016b\7*\2\2\u0169\u016c\5F$\2\u016a"+
		"\u016c\5\4\3\2\u016b\u0169\3\2\2\2\u016b\u016a\3\2\2\2\u016c\u016d\3\2"+
		"\2\2\u016d\u016e\7\31\2\2\u016eE\3\2\2\2\u016f\u0174\5T+\2\u0170\u0171"+
		"\7&\2\2\u0171\u0173\5T+\2\u0172\u0170\3\2\2\2\u0173\u0176\3\2\2\2\u0174"+
		"\u0172\3\2\2\2\u0174\u0175\3\2\2\2\u0175G\3\2\2\2\u0176\u0174\3\2\2\2"+
		"\u0177\u0178\7\24\2\2\u0178\u0179\7%\2\2\u0179\u017e\5J&\2\u017a\u017b"+
		"\7&\2\2\u017b\u017d\5J&\2\u017c\u017a\3\2\2\2\u017d\u0180\3\2\2\2\u017e"+
		"\u017c\3\2\2\2\u017e\u017f\3\2\2\2\u017fI\3\2\2\2\u0180\u017e\3\2\2\2"+
		"\u0181\u0183\5T+\2\u0182\u0184\t\6\2\2\u0183\u0182\3\2\2\2\u0183\u0184"+
		"\3\2\2\2\u0184K\3\2\2\2\u0185\u0186\7\3\2\2\u0186\u0187\7%\2\2\u0187\u018c"+
		"\5P)\2\u0188\u0189\7&\2\2\u0189\u018b\5P)\2\u018a\u0188\3\2\2\2\u018b"+
		"\u018e\3\2\2\2\u018c\u018a\3\2\2\2\u018c\u018d\3\2\2\2\u018d\u0190\3\2"+
		"\2\2\u018e\u018c\3\2\2\2\u018f\u0191\5R*\2\u0190\u018f\3\2\2\2\u0190\u0191"+
		"\3\2\2\2\u0191M\3\2\2\2\u0192\u0193\7=\2\2\u0193\u0194\5\66\34\2\u0194"+
		"\u0195\7\67\2\2\u0195\u0197\5\66\34\2\u0196\u0198\t\6\2\2\u0197\u0196"+
		"\3\2\2\2\u0197\u0198\3\2\2\2\u0198\u019e\3\2\2\2\u0199\u019c\t\7\2\2\u019a"+
		"\u019d\5t;\2\u019b\u019d\5n8\2\u019c\u019a\3\2\2\2\u019c\u019b\3\2\2\2"+
		"\u019d\u019f\3\2\2\2\u019e\u0199\3\2\2\2\u019e\u019f\3\2\2\2\u019fO\3"+
		"\2\2\2\u01a0\u01a1\5T+\2\u01a1Q\3\2\2\2\u01a2\u01a3\7(\2\2\u01a3\u01a4"+
		"\5\66\34\2\u01a4S\3\2\2\2\u01a5\u01a6\5V,\2\u01a6U\3\2\2\2\u01a7\u01a8"+
		"\b,\1\2\u01a8\u01a9\5Z.\2\u01a9\u01b2\3\2\2\2\u01aa\u01ab\f\5\2\2\u01ab"+
		"\u01ac\t\b\2\2\u01ac\u01b1\5V,\6\u01ad\u01ae\f\4\2\2\u01ae\u01af\t\t\2"+
		"\2\u01af\u01b1\5V,\5\u01b0\u01aa\3\2\2\2\u01b0\u01ad\3\2\2\2\u01b1\u01b4"+
		"\3\2\2\2\u01b2\u01b0\3\2\2\2\u01b2\u01b3\3\2\2\2\u01b3W\3\2\2\2\u01b4"+
		"\u01b2\3\2\2\2\u01b5\u01b6\t\t\2\2\u01b6Y\3\2\2\2\u01b7\u01b9\5X-\2\u01b8"+
		"\u01b7\3\2\2\2\u01b8\u01b9\3\2\2\2\u01b9\u01ba\3\2\2\2\u01ba\u01bb\5\\"+
		"/\2\u01bb[\3\2\2\2\u01bc\u01c5\5p9\2\u01bd\u01c5\5`\61\2\u01be\u01bf\7"+
		"*\2\2\u01bf\u01c0\5T+\2\u01c0\u01c1\7\31\2\2\u01c1\u01c5\3\2\2\2\u01c2"+
		"\u01c5\5^\60\2\u01c3\u01c5\5h\65\2\u01c4\u01bc\3\2\2\2\u01c4\u01bd\3\2"+
		"\2\2\u01c4\u01be\3\2\2\2\u01c4\u01c2\3\2\2\2\u01c4\u01c3\3\2\2\2\u01c5"+
		"]\3\2\2\2\u01c6\u01c9\5n8\2\u01c7\u01c8\7\62\2\2\u01c8\u01ca\5n8\2\u01c9"+
		"\u01c7\3\2\2\2\u01c9\u01ca\3\2\2\2\u01ca_\3\2\2\2\u01cb\u01cc\5n8\2\u01cc"+
		"\u01ce\7*\2\2\u01cd\u01cf\5d\63\2\u01ce\u01cd\3\2\2\2\u01ce\u01cf\3\2"+
		"\2\2\u01cf\u01d0\3\2\2\2\u01d0\u01d1\7\31\2\2\u01d1a\3\2\2\2\u01d2\u01d3"+
		"\7\b\2\2\u01d3c\3\2\2\2\u01d4\u01d9\5f\64\2\u01d5\u01d6\7&\2\2\u01d6\u01d8"+
		"\5f\64\2\u01d7\u01d5\3\2\2\2\u01d8\u01db\3\2\2\2\u01d9\u01d7\3\2\2\2\u01d9"+
		"\u01da\3\2\2\2\u01dae\3\2\2\2\u01db\u01d9\3\2\2\2\u01dc\u01de\t\2\2\2"+
		"\u01dd\u01dc\3\2\2\2\u01dd\u01de\3\2\2\2\u01de\u01df\3\2\2\2\u01df\u01e2"+
		"\5\64\33\2\u01e0\u01e2\5\62\32\2\u01e1\u01dd\3\2\2\2\u01e1\u01e0\3\2\2"+
		"\2\u01e2g\3\2\2\2\u01e3\u01e5\7\23\2\2\u01e4\u01e6\5T+\2\u01e5\u01e4\3"+
		"\2\2\2\u01e5\u01e6\3\2\2\2\u01e6\u01e8\3\2\2\2\u01e7\u01e9\5j\66\2\u01e8"+
		"\u01e7\3\2\2\2\u01e9\u01ea\3\2\2\2\u01ea\u01e8\3\2\2\2\u01ea\u01eb\3\2"+
		"\2\2\u01eb\u01ed\3\2\2\2\u01ec\u01ee\5l\67\2\u01ed\u01ec\3\2\2\2\u01ed"+
		"\u01ee\3\2\2\2\u01ee\u01ef\3\2\2\2\u01ef\u01f0\7\f\2\2\u01f0i\3\2\2\2"+
		"\u01f1\u01f2\7:\2\2\u01f2\u01f3\5\64\33\2\u01f3\u01f4\7$\2\2\u01f4\u01f5"+
		"\5\64\33\2\u01f5k\3\2\2\2\u01f6\u01f7\7\17\2\2\u01f7\u01f8\5\64\33\2\u01f8"+
		"m\3\2\2\2\u01f9\u01fa\t\n\2\2\u01fao\3\2\2\2\u01fb\u0200\5|?\2\u01fc\u0200"+
		"\5x=\2\u01fd\u0200\5z>\2\u01fe\u0200\5r:\2\u01ff\u01fb\3\2\2\2\u01ff\u01fc"+
		"\3\2\2\2\u01ff\u01fd\3\2\2\2\u01ff\u01fe\3\2\2\2\u0200q\3\2\2\2\u0201"+
		"\u0202\t\13\2\2\u0202s\3\2\2\2\u0203\u0204\7G\2\2\u0204u\3\2\2\2\u0205"+
		"\u0206\7H\2\2\u0206w\3\2\2\2\u0207\u0208\7E\2\2\u0208y\3\2\2\2\u0209\u020a"+
		"\7I\2\2\u020a{\3\2\2\2\u020b\u020f\5t;\2\u020c\u020f\5v<\2\u020d\u020f"+
		"\5z>\2\u020e\u020b\3\2\2\2\u020e\u020c\3\2\2\2\u020e\u020d\3\2\2\2\u020f"+
		"}\3\2\2\2;\u0080\u0086\u0089\u008d\u0091\u0096\u0099\u009c\u00a3\u00ab"+
		"\u00b1\u00ba\u00bf\u00c3\u00cb\u00d8\u00da\u00dc\u00e3\u00eb\u00ef\u00f6"+
		"\u00f9\u0100\u010f\u0117\u011c\u0121\u012c\u0139\u013b\u0149\u0155\u015e"+
		"\u016b\u0174\u017e\u0183\u018c\u0190\u0197\u019c\u019e\u01b0\u01b2\u01b8"+
		"\u01c4\u01c9\u01ce\u01d9\u01dd\u01e1\u01e5\u01ea\u01ed\u01ff\u020e";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}