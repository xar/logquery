// Generated from Select.g4 by ANTLR 4.3
package com.xar.logquery.parser.generated;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link SelectParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface SelectVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link SelectParser#realLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRealLiteral(@NotNull SelectParser.RealLiteralContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#selectItem}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelectItem(@NotNull SelectParser.SelectItemContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression(@NotNull SelectParser.ExpressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#whereClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhereClause(@NotNull SelectParser.WhereClauseContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#limitClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLimitClause(@NotNull SelectParser.LimitClauseContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#propertyAssign}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPropertyAssign(@NotNull SelectParser.PropertyAssignContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#expressionList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpressionList(@NotNull SelectParser.ExpressionListContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#stringLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringLiteral(@NotNull SelectParser.StringLiteralContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#propertyList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPropertyList(@NotNull SelectParser.PropertyListContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#primaryExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimaryExpression(@NotNull SelectParser.PrimaryExpressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#unionStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnionStatement(@NotNull SelectParser.UnionStatementContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#predicateNULL}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPredicateNULL(@NotNull SelectParser.PredicateNULLContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#function}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction(@NotNull SelectParser.FunctionContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#subSearchCondition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubSearchCondition(@NotNull SelectParser.SubSearchConditionContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#reservedConstant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReservedConstant(@NotNull SelectParser.ReservedConstantContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#whenExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhenExpression(@NotNull SelectParser.WhenExpressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#wildCardParameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWildCardParameter(@NotNull SelectParser.WildCardParameterContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#pivotClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPivotClause(@NotNull SelectParser.PivotClauseContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#transformClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTransformClause(@NotNull SelectParser.TransformClauseContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#simpleQueryStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimpleQueryStatement(@NotNull SelectParser.SimpleQueryStatementContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#usingList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUsingList(@NotNull SelectParser.UsingListContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#wildcardItem}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWildcardItem(@NotNull SelectParser.WildcardItemContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#number}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumber(@NotNull SelectParser.NumberContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#caseFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCaseFunction(@NotNull SelectParser.CaseFunctionContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#joinClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJoinClause(@NotNull SelectParser.JoinClauseContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#predicateIN}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPredicateIN(@NotNull SelectParser.PredicateINContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#negateCondition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNegateCondition(@NotNull SelectParser.NegateConditionContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(@NotNull SelectParser.StatementContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#unaryOperator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnaryOperator(@NotNull SelectParser.UnaryOperatorContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#namedExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNamedExpression(@NotNull SelectParser.NamedExpressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#subTableSource}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubTableSource(@NotNull SelectParser.SubTableSourceContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#simpleExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimpleExpression(@NotNull SelectParser.SimpleExpressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#identifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdentifier(@NotNull SelectParser.IdentifierContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#fileDatasource}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFileDatasource(@NotNull SelectParser.FileDatasourceContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#fromClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFromClause(@NotNull SelectParser.FromClauseContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#orderExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrderExpression(@NotNull SelectParser.OrderExpressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#dataSource}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDataSource(@NotNull SelectParser.DataSourceContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#tableSource}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTableSource(@NotNull SelectParser.TableSourceContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#predicateBETWEEN}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPredicateBETWEEN(@NotNull SelectParser.PredicateBETWEENContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#selectList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelectList(@NotNull SelectParser.SelectListContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#hexLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHexLiteral(@NotNull SelectParser.HexLiteralContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#havingClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHavingClause(@NotNull SelectParser.HavingClauseContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#integerLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntegerLiteral(@NotNull SelectParser.IntegerLiteralContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#customDatasource}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCustomDatasource(@NotNull SelectParser.CustomDatasourceContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#predicateLIKE}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPredicateLIKE(@NotNull SelectParser.PredicateLIKEContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#searchCondition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSearchCondition(@NotNull SelectParser.SearchConditionContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#groupExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGroupExpression(@NotNull SelectParser.GroupExpressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#columnName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumnName(@NotNull SelectParser.ColumnNameContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#usingClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUsingClause(@NotNull SelectParser.UsingClauseContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#predicateCompare}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPredicateCompare(@NotNull SelectParser.PredicateCompareContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#elseExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElseExpression(@NotNull SelectParser.ElseExpressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#simpleSelectStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimpleSelectStatement(@NotNull SelectParser.SimpleSelectStatementContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#constant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstant(@NotNull SelectParser.ConstantContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#subExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubExpression(@NotNull SelectParser.SubExpressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#queryStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitQueryStatement(@NotNull SelectParser.QueryStatementContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#orderByClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrderByClause(@NotNull SelectParser.OrderByClauseContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#functionParameterList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionParameterList(@NotNull SelectParser.FunctionParameterListContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#selectStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelectStatement(@NotNull SelectParser.SelectStatementContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#arithmeticExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArithmeticExpression(@NotNull SelectParser.ArithmeticExpressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#selectExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelectExpression(@NotNull SelectParser.SelectExpressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#groupByClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGroupByClause(@NotNull SelectParser.GroupByClauseContext ctx);

	/**
	 * Visit a parse tree produced by {@link SelectParser#functionParameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionParameter(@NotNull SelectParser.FunctionParameterContext ctx);
}