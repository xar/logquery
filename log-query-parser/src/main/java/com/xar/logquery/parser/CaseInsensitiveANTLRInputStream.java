/*
 * Copyright 2014 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.parser;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;

import java.io.IOException;
import java.io.Reader;

/**
 * Created by Anton Koekemoer on 2014/11/21.
 */
public class CaseInsensitiveANTLRInputStream extends ANTLRInputStream {

    /**
     * Copy data in string to a local char array
     *
     * @param input
     */
    public CaseInsensitiveANTLRInputStream(final String input) {

        super(input);
    }

    public CaseInsensitiveANTLRInputStream(final Reader r) throws IOException {

        super(r);
    }

    @Override
    public int LA(final int i) {
        int returnChar = super.LA(i);
        if (returnChar == CharStream.EOF) {
            return returnChar;
        } else if (returnChar == 0) {
            return returnChar;
        }
        return Character.toUpperCase((char) returnChar);
    }

}
