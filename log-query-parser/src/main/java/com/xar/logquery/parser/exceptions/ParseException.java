/*
 * Copyright 2014 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.parser.exceptions;

/**
 * Created by koekemoera on 2014/08/07.
 */
public class ParseException extends RuntimeException {

    private int m_row;

    private int m_column;

    /**
     * Constructs a new runtime exception with {@code null} as its
     * detail message.  The cause is not initialized, and may subsequently be
     * initialized by a call to {@link #initCause}.
     *
     * @param p_message the message describing the parse error
     * @param p_row     the row at which the parse error has occurred.
     * @param p_column  the column at which the parse error has occurred.
     */
    public ParseException(String p_message, int p_row, int p_column) {

        this(p_message, p_row, p_column, null);
    }

    /**
     * Constructs a new runtime exception with {@code null} as its
     * detail message.  The cause is not initialized, and may subsequently be
     * initialized by a call to {@link #initCause}.
     *
     * @param p_message the message describing the parse error
     * @param p_row     the row at which the parse error has occurred.
     * @param p_column  the column at which the parse error has occurred.
     */
    public ParseException(String p_message, int p_row, int p_column, Throwable p_cause) {

        super("[" + p_row + ", " + p_column + "] " + p_message, p_cause);


        m_row = p_row;
        m_column = p_column;
    }

    /**
     * Gets row at which the parse error has occurred.
     *
     * @return the row at which the parse error has occurred.
     */
    public int getRow() {

        return m_row;
    }

    /**
     * Gets column at which the parse error has occurred.
     *
     * @return the column at which the parse error has occurred.
     */
    public int getColumn() {

        return m_column;
    }


}
