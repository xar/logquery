grammar Select;

options {
}

statement:
    selectStatement (';')? EOF ;

selectStatement
     :   simpleQueryStatement
        (orderByClause)?
        (limitClause)?
    ;

simpleQueryStatement
    :   simpleSelectStatement
    |   unionStatement
    ;

simpleSelectStatement
    :   'SELECT' ('ALL' | 'DISTINCT')?
                    selectList
                    fromClause
                    (whereClause)?
                    (groupByClause)?
                    (pivotClause)?
    ;

unionStatement
    :   queryStatement ('UNION' queryStatement)*
    ;

queryStatement
    :   '(' selectStatement ')'
    |   simpleSelectStatement
    ;

limitClause
    :   'LIMIT' integerLiteral ((',' | 'SKIP') integerLiteral)?
    ;

fromClause:
    'FROM'
        tableSource
    ;

tableSource
    :   subTableSource (joinClause)*
    ;

subTableSource
    :   dataSource
        (transformClause)?
        ('AS' identifier)?
    ;


usingClause
    :   'USING' identifier ('(' propertyList ')')?
    ;

transformClause
    :   'TRANSFORM' '(' usingList ')'
    ;

joinClause
    :   'CROSS' 'JOIN' subTableSource
    |   ( ('INNER' | ('LEFT' | 'RIGHT' | 'FULL') ('OUTER')? ) )? 'JOIN' tableSource 'ON' searchCondition
    ;

dataSource
    :   fileDatasource
    |   customDatasource
    |   '(' selectStatement ')'
    ;

fileDatasource
    :   ASCIIStringLiteral
        (usingClause)?
    ;

customDatasource
    :   identifier ('(' propertyList ')')?
        (usingClause)?
    ;

propertyList
    :   propertyAssign (',' propertyAssign)*
    ;

propertyAssign
    :   identifier '=' simpleExpression
    ;

whereClause
    :   'WHERE' searchCondition
    ;

selectList
    :   selectItem (',' selectItem)*
;

usingList
    :   namedExpression (',' namedExpression)*
    ;

selectItem
    :   wildcardItem
    |   selectExpression
    ;

selectExpression
    :   searchCondition ('AS' identifier)? ;

namedExpression
    :   simpleExpression 'AS' identifier    ;

wildcardItem
    :   '*'
    |   identifier '.' '*'
    ;

expression
    :   searchCondition
    ;

searchCondition
    :   searchCondition 'OR' searchCondition
    |   searchCondition 'AND' searchCondition
    |   subSearchCondition
    ;


subSearchCondition
    :   negateCondition
    |   '(' searchCondition ')'
    |   predicateCompare
    |   predicateNULL
    |   predicateLIKE
    |   predicateBETWEEN
    |   predicateIN
    |   simpleExpression
    ;

negateCondition
    :   'NOT'   searchCondition
//    |   'NOT'   '(' searchCondition ')'
    ;

predicateCompare
    :   simpleExpression ('=' | '!=' | '<>' | '>' | '<' | '>=' | '<=') simpleExpression
    ;

predicateNULL
    :   simpleExpression 'IS' ('NOT')? 'NULL'
    ;

predicateLIKE
    :   simpleExpression 'LIKE' simpleExpression ('ESCAPE' simpleExpression)?
    ;

predicateBETWEEN
    :   simpleExpression 'BETWEEN' simpleExpression 'AND' simpleExpression
    ;

predicateIN
    :   simpleExpression 'IN' '(' (expressionList | selectStatement ) ')'
    ;

expressionList
    :  simpleExpression (',' simpleExpression)*
    ;

orderByClause
    :   'ORDER' 'BY'    orderExpression (',' orderExpression)*
    ;

orderExpression
    :   simpleExpression ('ASC' | 'DESC')?
    ;

groupByClause
    :   'GROUP' 'BY'    groupExpression (',' groupExpression)*
        (havingClause)?
    ;

pivotClause
    :   'PIVOT' searchCondition 'ON' searchCondition ('ASC' | 'DESC')? (('BEFORE' | 'AFTER') (integerLiteral | identifier))?
    ;

groupExpression
    :   simpleExpression
    ;

havingClause
    :   'HAVING' searchCondition
    ;

simpleExpression
    :   arithmeticExpression
    ;

arithmeticExpression
    :   arithmeticExpression ('*' | '/' | '%') arithmeticExpression
    |   arithmeticExpression ('+'|'-') arithmeticExpression
    |   subExpression
    ;

unaryOperator
    :   '+'
    |   '-'
    ;


subExpression
    :
    (unaryOperator)? primaryExpression
    ;

primaryExpression
    :
      constant
    | function
    | '(' (simpleExpression) ')'
    | columnName
    | caseFunction
    ;

columnName
    :   identifier ('.' identifier)?
    ;

function
    :
        identifier '('
        (
            (functionParameterList)?
        )

        ')'
    ;

 wildCardParameter
    :   '*'
    ;

functionParameterList
    :   functionParameter (',' functionParameter)*
    ;

functionParameter
    :   ('ALL' | 'DISTINCT')?  expression
    |   wildcardItem
    ;

caseFunction
    : 'CASE' (simpleExpression)?
      (whenExpression)+
      (elseExpression)?
      'END'
    ;

whenExpression
    : 'WHEN' expression 'THEN' expression
    ;

elseExpression
    : 'ELSE' expression
    ;

identifier
    :   NonQuotedIdentifier
    |   QuotedIdentifier
    ;

constant
    :   number
    |   stringLiteral
    |   hexLiteral
    |   reservedConstant
    ;

reservedConstant
    :   'TRUE'
    |   'FALSE'
    |   'NULL'
    ;

integerLiteral
    :   Integer
    ;

realLiteral
    :   Real
    ;

stringLiteral
    :   ASCIIStringLiteral
    ;

hexLiteral
    :   HexLiteral
    ;

number
    :   integerLiteral
    |   realLiteral
    |   hexLiteral
    ;




WS : [ \t\r\n]+ -> skip ; // Define whitespace rule, toss it out

COMMENT
    : '/*' .*? '*/' -> skip
    ;

LINE_COMMENT
    : '//' ~[\r\n]* -> skip
    ;

fragment Letter
    : 'a'..'z' | 'A' .. 'Z' | '\u0080'..'\ufffe'
    ;

fragment Digit
    : '0'..'9'
    ;


QuotedIdentifier
    :
    (
      '[' (~']')* ']' (']' (~']')* ']')*
    | '"' (~'"')* '"' ('"' (~'"')* '"')*
    )
    ;

NonQuotedIdentifier
    :
    (Letter | '_' | '#') (Letter | Digit | '_')* // first char other than '@'
    ;

ASCIIStringLiteral
    :
    '\'' (~'\'')* '\'' ( '\'' (~'\'')* '\'' )*
    ;


Exponent
    : ('e' | 'E') ( '+' | '-' )? (Digit)+
    ;

Integer:
        (Digit)+
    ;

Real:
        Integer ( '.' (Digit)* (Exponent)? | Exponent)
    |   '.' ( (Digit)+ (Exponent)? )
    ;

HexLiteral:
        '0X' ('A'..'F' | 'a'..'f' |Digit)*
    ;

