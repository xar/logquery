/*
 * Copyright 2014 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.cli;

import com.xar.cli.annotations.CommandLineArgument;
import com.xar.cli.entities.ArgumentDescriptor;
import com.xar.cli.exceptions.ArgumentException;
import com.xar.cli.exceptions.MissingArgumentException;
import com.xar.cli.exceptions.RequiredArgumentMissingException;
import com.xar.cli.interfaces.ICommandlineListener;
import com.xar.cli.interfaces.ICommandlineParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.xar.cli.util.CommandlineFunctions.*;

/**
 * Command line parser
 */
public class CommandLineParser implements ICommandlineParser {

    /**
     * The shared logger instance.
     */
    private static final Logger logger = LoggerFactory.getLogger(CommandLineParser.class);

    /**
     * Command line argument definitions and targets
     */
    List<ArgumentTarget> m_argumentTargets = new ArrayList<>();

    /**
     * Listeners for command line actions
     */
    List<ICommandlineListener> m_listeners = new ArrayList<>();

    /**
     * Is the help option specified
     */
    private boolean m_isHelpSelected;

    /**
     * Instantiates a new Command line parser with the default help option
     */
    public CommandLineParser() {

        this(new ArgumentDescriptor("?", "help", "", "Display this help and exit"));
    }

    /**
     * Create a commandline parser with the specified help option
     *
     * @param p_help the help option
     */
    public CommandLineParser(ArgumentDescriptor p_help) {

        ArgumentTarget target = new ArgumentTarget(p_help);
        target.target = this;
        try {
            target.field = getClass().getDeclaredField("m_isHelpSelected");
        }
        catch (NoSuchFieldException e) {
            logger.error("Error setting up help option", e);
        }
        target.annotated = true;
        this.m_argumentTargets.add(target);
    }

    /**
     * Adds the arguments defined in the class to the parser
     *
     * @param p_arguments the arguments object
     */
    public void addArguments(Object... p_arguments) {

        for (final Object p_argument : p_arguments) {

            Class<?> argumentClass = p_argument.getClass();

            while (argumentClass != Object.class) {
                Field[] fields = argumentClass.getDeclaredFields();

                for (Field field : fields) {
                    CommandLineArgument annotation = field.getAnnotation(CommandLineArgument.class);
                    ArgumentTarget target = new ArgumentTarget();
                    if (annotation != null) {
                        target.argumentDescriptor = new ArgumentDescriptor(annotation, field);
                        target.required = annotation.required();
                        target.annotated = true;
                    }
                    else {
                        target.argumentDescriptor = new ArgumentDescriptor("", field.getName(),
                                hasArgument(field.getType()) ? "<" + field.getType() + ">" : "", "");
                    }

                    target.field = field;
                    target.target = p_argument;

                    this.m_argumentTargets.add(target);

                }
                argumentClass = argumentClass.getSuperclass();
            }

        }
    }

    public void addListener(ICommandlineListener p_listener) {

        m_listeners.add(p_listener);
    }

    /**
     * Determines if the help option has been selected
     *
     * @return true if the help option was selected, false otherwise
     */
    public boolean isHelpSelected() {

        return this.m_isHelpSelected;
    }

    /**
     * Parses the arguments in the list
     *
     * @param p_arguments the arguments
     *
     * @return unused arguments
     *
     * @throws ArgumentException
     */
    public List<String> parse(final List<String> p_arguments) throws ArgumentException {

        List<String> arguments = new ArrayList<>();
        List<Object> values = new ArrayList<>();

        addAll(arguments, p_arguments);

        for (int i = 0; i < this.m_argumentTargets.size(); i++) {
            values.add(null);
            ArgumentTarget argumentTarget = this.m_argumentTargets.get(i);
            logger.debug("Process argument: " + argumentTarget);
            while (true) {
                int index = arguments.indexOf("-" + argumentTarget.argumentDescriptor.getName());
                if (index < 0) {
                    index = arguments.indexOf("--" + argumentTarget.argumentDescriptor.getLongForm());
                }

                if (index >= 0) {
                    if (hasArgument(argumentTarget.field.getType())) {
                        if (index < arguments.size() - 1) {
                            Class<?> fieldType = argumentTarget.field.getType();
                            String argument = arguments.remove(index + 1);

                            if (fieldType.isArray()) {
                                List<Object> fieldValues = (List<Object>) values.get(i);
                                if (fieldValues == null) {
                                    fieldValues = new ArrayList<>();
                                    values.set(i, fieldValues);
                                }
                                fieldValues.add(convert(argument, fieldType.getComponentType()));
                            }
                            else {
                                values.set(i, convert(argument, fieldType));
                            }
                        }
                        else {
                            throw new MissingArgumentException("Argument '" + argumentTarget.argumentDescriptor.getName() + "' has no value");
                        }
                    }
                    else {
                        values.set(i, true);
                    }
                    arguments.remove(index);
                }
                else if (argumentTarget.required && values.get(i) == null) {
                    throw new RequiredArgumentMissingException("No value specified for required argument '" + argumentTarget.argumentDescriptor.getName() + "'");
                }
                else {
                    break;
                }
            }

            try {
                Object value = values.get(i);
                if (value != null) {
                    argumentTarget.field.setAccessible(true);
                    argumentTarget.field.set(argumentTarget.target, value);
                    for (ICommandlineListener listener : this.m_listeners) {
                        listener.processedArgument(this, argumentTarget.argumentDescriptor);
                    }
                }
            }
            catch (IllegalAccessException e) {
                throw new ArgumentException("Error assigning value for argument '" + argumentTarget.argumentDescriptor.getName() + "'", e);
            }
        }

        return arguments;
    }

    public void removeListener(ICommandlineListener p_listener) {

        m_listeners.remove(p_listener);
    }

    /**
     * Display the help text for the command line
     *
     * @param p_stream the stream to output the help text to
     */
    public void showHelp(PrintStream p_stream) {

        this.showHelp(p_stream, 80);
    }

    /**
     * Display the help text for the command line
     *
     * @param p_stream the stream to output the help text to
     */
    public void showHelp(PrintStream p_stream, int p_screenWidth) {

        List<String[]> options = new ArrayList<>();

        int[] lengths = new int[4];
        for (ArgumentTarget target : this.m_argumentTargets) {
            ArgumentDescriptor descriptor = target.argumentDescriptor;

            if (target.annotated) {
                String[] optionHelp = new String[]{isNull(descriptor.getName(), ""),
                                                   isNull(descriptor.getLongForm(), ""),
                                                   isNull(descriptor.getArgumentName(), ""),
                                                   isNull(descriptor.getHelpText(), "")};
                options.add(optionHelp);

                lengths[0] = Math.max(optionHelp[0].length(), lengths[0]);
                lengths[1] = Math.max(optionHelp[1].length(), lengths[1]);
                lengths[2] = Math.max(optionHelp[2].length(), lengths[2]);
            }
        }

        lengths[0] += (lengths[0] > 0 ? 3 : 0);
        lengths[1] += (lengths[1] > 0 ? 3 : 0);
        lengths[2] += (lengths[2] > 0 ? 3 : 0);

        for (String[] strings : options) {
            StringBuilder builder = new StringBuilder();

            if (strings[0].length() > 0) {
                builder.append("-").append(strings[0]);
                if (strings[1].length() > 0) {
                    builder.append(",");
                }
                else if (!isNullOrEmpty(strings[2])) {
                    builder.append(" <").append(strings[2]).append(">");
                }
            }
            padr(builder, lengths[0]);

            if (strings[1].length() > 0) {
                builder.append("--").append(strings[1]);
                if (!isNullOrEmpty(strings[2])) {
                    builder.append(" <").append(strings[2]).append(">");
                }
            }


            for (String line : strings[3].split("\n")) {
                p_stream.print(padr(builder.toString(), lengths[0] + lengths[1] + lengths[2]));

                p_stream.print(line);

                builder.setLength(0);
                p_stream.println();
            }
        }


    }

    /**
     * Adds the arguments from the source list to the target list
     *
     * @param p_target the target collection
     * @param p_source the source collection
     */
    private void addAll(final Collection<String> p_target, final Collection<String> p_source) {

        for (String arg : p_source) {
            if (arg.startsWith("#")) { continue; }
            if (arg.startsWith("@")) {

                List<String> fileArgs = new ArrayList<>();
                try (BufferedReader reader = new BufferedReader(new FileReader(arg.substring(1)))) {
                    String line;
                    while ((line = reader.readLine()) != null) {
                        if (line.length() > 0) { fileArgs.add(line); }
                    }
                }
                catch (IOException e) {
                    logger.error("Error loading arguments from file", e);
                }
                addAll(p_target, fileArgs);
            }
            else {
                p_target.add(arg);
            }
        }

    }

    private class ArgumentTarget {

        ArgumentDescriptor argumentDescriptor;

        Object target;

        Field field;

        boolean required;

        boolean annotated;

        public ArgumentTarget() {

        }

        public ArgumentTarget(final ArgumentDescriptor p_argumentDescriptor) {

            argumentDescriptor = p_argumentDescriptor;
        }

        @Override
        public String toString() {

            return "ArgumentTarget{" +
                    "shortName='" + this.argumentDescriptor.getName() + '\'' +
                    ", longName='" + this.argumentDescriptor.getLongForm() + '\'' +
                    ", helpText='" + this.argumentDescriptor.getHelpText() + '\'' +
                    ", target=" + this.target +
                    ", field=" + this.field +
                    ", annotated=" + this.annotated +
                    '}';
        }
    }

}

