/*
 * Copyright 2014 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.cli.interfaces;

import com.xar.cli.entities.ArgumentDescriptor;
import com.xar.cli.exceptions.ArgumentException;

/**
 * Commandline listener
 */
public interface ICommandlineListener {

    /**
     * Called after an argument has been processed
     *  @param p_parser   the parser processing the argument
     * @param p_argument the short name of the processed argument
     */
    public void processedArgument(ICommandlineParser p_parser, ArgumentDescriptor p_argument) throws ArgumentException;

}
