/*
 * Copyright 2014 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.cli.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Annotation for a commandline argument.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface CommandLineArgument {

    /**
     * The argument name
     *
     * @return the argument name
     */
    String name();

    /**
     * The argument long name.
     *
     * @return the argument long name.
     */
    String longName() default "";


    /**
     * The description for the argument
     *
     * @return the description for the argument
     */
    String description();

    /**
     * Indicates whether the argument is the default argument
     *
     * @return true, if the argument is the default argument, false otherwise
     */
    boolean defaultArgument() default false;

    /**
     * Indicates whether the argument is required
     *
     * @return true if the argument is required, false otherwise
     */
    boolean required() default false;


    String argumentName() default "";
}
