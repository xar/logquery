/*
 * Copyright 2014 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.cli.entities;

import com.xar.cli.annotations.CommandLineArgument;

import java.lang.reflect.Field;

import static com.xar.cli.util.CommandlineFunctions.hasArgument;
import static com.xar.cli.util.CommandlineFunctions.isNullOrEmpty;

/**
 * Argument descriptor entity
 */
public class ArgumentDescriptor {

    String name;

    String longForm;

    String helpText;

    String argumentName;


    public ArgumentDescriptor(final String p_name, final String p_longForm, String argumentName, final String p_helpText) {

        this.name = p_name;
        this.longForm = p_longForm;
        this.argumentName = argumentName;
        this.helpText = p_helpText;

    }

    public ArgumentDescriptor(final CommandLineArgument p_annotation, final Field p_field) {

        this.name = p_annotation.name();
        this.longForm = p_annotation.longName();
        this.helpText = p_annotation.description();
        if (hasArgument(p_field.getType())) { this.argumentName = isNullOrEmpty(p_annotation.argumentName()) ? p_field.getName() : p_annotation.argumentName(); }
    }

    public String getName() {

        return this.name;
    }

    public String getLongForm() {

        return this.longForm;
    }

    public String getHelpText() {

        return this.helpText;
    }

    public String getArgumentName() {

        return this.argumentName;
    }
}
