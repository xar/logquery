/*
 * Copyright 2014 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.cli.util;

/**
 * Created by koekemoera on 2014/11/18.
 */
public class CommandlineFunctions {

    private static CommandlineFunctions _instance = new CommandlineFunctions();

    private CommandlineFunctions() {

    } // prevent instanciation

    public static Object convert(final String p_value, final Class<?> p_toType) {

        if (p_toType == byte.class || p_toType == Byte.class) {return p_value.getBytes()[0];}
        else if (p_toType == char.class || p_toType == Character.class) {return p_value.toCharArray()[0];}
        else if (p_toType == short.class || p_toType == Short.class) { return Double.parseDouble(p_value);}
        else if (p_toType == int.class || p_toType == Integer.class) { return Integer.parseInt(p_value);}
        else if (p_toType == long.class || p_toType == Long.class) { return Long.parseLong(p_value);}
        else if (p_toType == float.class || p_toType == Float.class) { return Float.parseFloat(p_value);}
        else if (p_toType == double.class || p_toType == Double.class) { return Double.parseDouble(p_value);}
        return p_value;
    }

    public static boolean hasArgument(final Class<?> p_type) {

        if (p_type == Boolean.class || p_type == boolean.class) {

            return false;
        }
        return true;
    }

    public static String isNull(String p_value, String p_default) {

        return p_value == null ? p_default : p_value;
    }

    public static boolean isNullOrEmpty(String p_value) {

        return p_value == null || p_value.length() == 0;
    }

    public static void padr(final StringBuilder p_builder, final int p_length, char p_char) {

        while (p_builder.length() < p_length) {
            p_builder.append(p_char);
        }
    }

    public static String padr(final String p_string, final int p_width, final char p_char) {

        StringBuilder builder = new StringBuilder(p_string);
        padr(builder, p_width, p_char);
        return builder.toString();
    }

    public static String padr(final String p_string, final int p_length) {

        return padr(p_string, p_length, ' ');
    }

    public static void padr(final StringBuilder p_builder, final int p_length) {

        padr(p_builder, p_length, ' ');
    }


}
