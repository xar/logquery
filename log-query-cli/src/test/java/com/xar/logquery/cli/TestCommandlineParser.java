/*
 * Copyright 2014 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.cli;

import com.xar.cli.CommandLineParser;
import com.xar.cli.annotations.CommandLineArgument;
import com.xar.cli.exceptions.ArgumentException;
import com.xar.cli.util.CommandlineFunctions;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Arrays;

/**
 * Created by koekemoera on 2014/11/18.
 */
public class TestCommandlineParser {


    @Test
    public void shouldPadRight() {

        Assert.assertEquals("a   ", CommandlineFunctions.padr("a", 4, ' '));
        Assert.assertEquals("a   ", CommandlineFunctions.padr("a", 4));
        StringBuilder bulder = new StringBuilder("a");
        CommandlineFunctions.padr(bulder, 4);
        Assert.assertEquals("a   ", bulder.toString());
    }

    @Test
    public void shouldPopulateAnnotatedProperties() throws ArgumentException {

        String[] args = new String[]{"-option1", "1", "-option2", "2"};
        CommandLineParser parser = new CommandLineParser();
        Options options = new Options();
        parser.addArguments(options);
        parser.parse(Arrays.asList(args));

        Assert.assertEquals(1, options.option1);
        Assert.assertEquals(2, options.option2);
    }

    @Test
    public void shouldPopulateFromFile() throws IOException, ArgumentException {

        try (PrintWriter writer = new PrintWriter(new FileWriter("arguments.txt"))) {
            writer.println("-option2");
            writer.println("2");
        }
        String[] args = new String[]{"-option1", "1", "@arguments.txt"};
        CommandLineParser parser = new CommandLineParser();
        Options options = new Options();
        parser.addArguments(options);
        parser.parse(Arrays.asList(args));

        Assert.assertEquals(1, options.option1);
        Assert.assertEquals(2, options.option2);
    }

    @Test
    public void shouldShowHelp() throws ArgumentException {


        String[] args = new String[]{"-option1", "1", "-option2", "2", "-?"};
        CommandLineParser parser = new CommandLineParser();
        Options options = new Options();
        parser.addArguments(options);
        parser.parse(Arrays.asList(args));

        ByteArrayOutputStream ms = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(ms);
        if (parser.isHelpSelected()) {
            parser.showHelp(ps);
        }
        Assert.assertEquals("-?,       --help           Display this help and exit\n" +
                "-option1 <option1>         description1\n" +
                "-option2 <option2>         description2", ms.toString().trim());

    }

    class Options {

        @CommandLineArgument(name = "option1", description = "description1")
        int option1;

        @CommandLineArgument(name = "option2", description = "description2")
        int option2;
    }


}
