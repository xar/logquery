SELECT
    Date, SUM(Total) as Total
FROM
    'data/sales_jan.txt'
TRANSFORM(
    STRTOK(Text, ' ', 0) AS Date,
    STRTOK(Text, ' ', 1) AS ItemCode,
    TO_INT(STRTOK(Text, ' ', 2)) AS Quantity,
    TO_DOUBLE(STRTOK(Text, ' ', 3)) AS ItemPrice,
    (Quantity * ItemPrice) AS Total
)
PIVOT ISNULL(SUM(Total), 0) ON ItemCode ASC

