    SELECT
        Date, 
        SUM(Total) as Total,
        SUM(DECODE(ItemCode, 'ITM00001', Total, 0)) AS "ITM00001",
        SUM(DECODE(ItemCode, 'ITM00002', Total, 0)) AS "ITM00002",
        SUM(DECODE(ItemCode, 'ITM00003', Total, 0)) AS "ITM00003",
        SUM(DECODE(ItemCode, 'ITM00004', Total, 0)) AS "ITM00004",
        SUM(DECODE(ItemCode, 'ITM00005', Total, 0)) AS "ITM00005",
        SUM(DECODE(ItemCode, 'ITM00006', Total, 0)) AS "ITM00006",
        SUM(DECODE(ItemCode, 'ITM00007', Total, 0)) AS "ITM00007",
        SUM(DECODE(ItemCode, 'ITM00008', Total, 0)) AS "ITM00008",
        SUM(DECODE(ItemCode, 'ITM00009', Total, 0)) AS "ITM00009",
        SUM(DECODE(ItemCode, 'ITM00010', Total, 0)) AS "ITM00010",
        SUM(DECODE(ItemCode, 'ITM00011', Total, 0)) AS "ITM00011",
        SUM(DECODE(ItemCode, 'ITM00012', Total, 0)) AS "ITM00012",
        SUM(DECODE(ItemCode, 'ITM00013', Total, 0)) AS "ITM00013",
        SUM(DECODE(ItemCode, 'ITM00014', Total, 0)) AS "ITM00014",
        SUM(DECODE(ItemCode, 'ITM00015', Total, 0)) AS "ITM00015",
        SUM(DECODE(ItemCode, 'ITM00016', Total, 0)) AS "ITM00016",
        SUM(DECODE(ItemCode, 'ITM00017', Total, 0)) AS "ITM00017",
        SUM(DECODE(ItemCode, 'ITM00018', Total, 0)) AS "ITM00018",
        SUM(DECODE(ItemCode, 'ITM00019', Total, 0)) AS "ITM00019",
        SUM(DECODE(ItemCode, 'ITM00020', Total, 0)) AS "ITM00020",
        SUM(DECODE(ItemCode, 'ITM00021', Total, 0)) AS "ITM00021",
        SUM(DECODE(ItemCode, 'ITM00022', Total, 0)) AS "ITM00022",
        SUM(DECODE(ItemCode, 'ITM00023', Total, 0)) AS "ITM00023",
        SUM(DECODE(ItemCode, 'ITM00024', Total, 0)) AS "ITM00024",
        SUM(DECODE(ItemCode, 'ITM00025', Total, 0)) AS "ITM00025",
        SUM(DECODE(ItemCode, 'ITM00026', Total, 0)) AS "ITM00026",
        SUM(DECODE(ItemCode, 'ITM00027', Total, 0)) AS "ITM00027",
        SUM(DECODE(ItemCode, 'ITM00028', Total, 0)) AS "ITM00028",
        SUM(DECODE(ItemCode, 'ITM00029', Total, 0)) AS "ITM00029"
    FROM
        'data/sales_jan.txt'
    TRANSFORM(
        STRTOK(Text, ' ', 0) AS Date,
        STRTOK(Text, ' ', 1) AS ItemCode,
        TO_INT(STRTOK(Text, ' ', 2)) AS Quantity,
        TO_DOUBLE(STRTOK(Text, ' ', 3)) AS ItemPrice,
        (Quantity * ItemPrice) AS Total
    )
    GROUP BY Date
    ORDER BY 1
