<#list dataSource.getColumnInfo() as column>${column.getName()}<#if column_has_next>,</#if></#list>
<@body; row>
<@sum field="Total" /><@max field="Total" /><@min field="Total" /><@avg field="Total" />
<#list dataSource.getColumnInfo() as column>${row.get(column.getName()).toString()}<#if column_has_next>,</#if></#list>
</@body>
Total Sales: ${SumOf_Total}
Maximum Sales: ${MaxOf_Total}
Minimum Sales: ${MinOf_Total}
Average Sales: ${AvgOf_Total}
