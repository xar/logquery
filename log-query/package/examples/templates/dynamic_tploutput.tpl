<TABLE border=1>
    <tr>
        <#list dataSource.getColumnInfo() as column><th<#if column_index gt 0> align="right"</#if>>${column.getName()}</th></#list>
    </tr>

<@body; row>
    <tr>
        <#list dataSource.getColumnInfo() as column><#if column_index gt 0><@sum field="${column.getName()}" /></#if><td<#if column_index gt 0> align="right"</#if> nowrap>${row.get(column_index)}</td></#list>
    </tr>
</@body>
    <tr>
        <#list dataSource.getColumnInfo() as column><#if column_index == 0><td></td><#else><td align="right">${.vars["SumOf_" + column.getName()]}</td></#if></#list>
    </tr>
</TABLE>
