<#list dataSource.getColumnInfo() as column>${column.getName()}<#if column_has_next>,</#if></#list>
<@body; row>
<#list dataSource.getColumnInfo() as column><#if column_index gt 0><@sum field="${column.getName()}" /></#if></#list>
<#list dataSource.getColumnInfo() as column>"${row.get(column.getName()).toString()}"<#if column_has_next>,</#if></#list>
</@body>
<#list dataSource.getColumnInfo() as column><#if column_index == 0><#else>,"${.vars["SumOf_" + column.getName()]}"</#if></#list>
