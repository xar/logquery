#!/bin/bash

# First we need to know in which directory the script file is located
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  TARGET="$(readlink "$SOURCE")"
  if [[ $SOURCE == /* ]]; then
    SOURCE="$TARGET"
  else
    DIR="$( dirname "$SOURCE" )"
    SOURCE="$DIR/$TARGET" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
  fi
done

RDIR="$( dirname "$SOURCE" )"
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
DIR="$(dirname "$DIR")"

filename=$(ls "$DIR/lib/"log-query_*.jar)

if [ -f "$DIR/config/log4j.properties" ]; 
then
   java -Dlog4j.configuration=file:"$DIR/config/log4j.properties" -jar $filename "$@"
else
   java -jar $filename "$@"
fi
