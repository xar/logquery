///*
// * Copyright 2014 Anton Koekemoer
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *     http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
// */
//
//package com.xar.logquery.util;
//
//import com.xar.logquery.runtime.annotations.Function;
//import org.reflections.Reflections;
//
//import java.util.Set;
//
///**
// * Created by koekemoera on 2014/11/14.
// */
//public class ApplicationUtils {
//
//    static Reflections reflections;
//    private static Set<Class<?>> functions;
//
//
//    private static Reflections getReflections(){
//        if (reflections == null){
//            reflections = new Reflections();
//        }
//        return reflections;
//    }
//
//    public Set<Class<?>> loadFunctions(){
//        if (functions == null) {
//            functions = reflections.getTypesAnnotatedWith(Function.class);
//        }
//        return functions;
//    }
//
//}
