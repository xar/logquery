/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.xar.logquery.util;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

/**
 * Created by akoekemo on 4/15/15.
 */
public class Log4JUtilities {

    private static final Log4JUtilities _instance = new Log4JUtilities();

    // prevent construction
    private Log4JUtilities() {

    } 
    
    @SuppressWarnings("unchecked")
    public static void configure(Level p_logLevel) {

        List<Logger> loggers = Collections.<Logger>list(LogManager.getCurrentLoggers());
        loggers.add(LogManager.getRootLogger());
        for (Logger logger : loggers) {
            logger.setLevel(p_logLevel);
        }
    }

    /**
     * Returns true if it appears that log4j have been previously configured. This code
     * checks to see if there are any appenders defined for log4j which is the
     * definitive way to tell if log4j is already initialized
     */
    public static boolean isConfigured() {

        Enumeration appenders = Logger.getRootLogger().getAllAppenders();

        if (appenders.hasMoreElements()) {
            return true;
        }

        Enumeration loggers = LogManager.getCurrentLoggers();

        while (loggers.hasMoreElements()) {
            Logger c = (Logger) loggers.nextElement();
            if (c.getAllAppenders().hasMoreElements()) { return true; }
        }
        return false;
    }
}
