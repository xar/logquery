///*
// * Copyright 2014 Anton Koekemoer
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *     http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
// */
//
//package com.xar.logquery.util;
//
//import com.xar.logquery.runtime.util.DateTimeFunctions;
//import com.xar.logquery.runtime.util.StringUtils;
//import org.joda.time.LocalDate;
//
//import java.io.FileWriter;
//import java.io.IOException;
//import java.io.PrintWriter;
//import java.text.DecimalFormat;
//import java.text.NumberFormat;
//
///**
// * Created by Anton Koekemoer on 2014/12/06.
// */
//public class ExampleSales {
//
//    public static void main(String[] args) throws IOException {
//
//        String[] monthNames = new String[]{"jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov",
//                                           "dec"};
//        int[] monthDays = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
//        NumberFormat fmt = new DecimalFormat("###.00");
//        for (int month = 0; month < monthDays.length; month++) {
//            try (PrintWriter out = new PrintWriter(new FileWriter("sales_" + monthNames[month] + ".txt"))) {
//                LocalDate date = new LocalDate(2000, month + 1, 1);
//                for (int i = 0; i < monthDays[month]; i++) {
//                    int rows = (int) (Math.random() * 100.0);
//                    for (int row = 0; row < rows; row++) {
//                        String item = "ITM" + StringUtils.padl(String.valueOf((int) (Math.random() * 30)), 5, '0');
//                        out.println(DateTimeFunctions.toString(date.toDate(), "yyyy-MM-dd") + " " + item + " " + String.valueOf((int) (Math.random() * 10) + 1) + " " + String.valueOf(fmt.format(Math.random() * 30)));
//                    }
//                    date = date.plusDays(1);
//                }
//            }
//        }
//    }
//}
