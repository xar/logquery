/*
* Copyright 2014 Anton Koekemoer
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.xar.logquery;

import com.google.gson.Gson;
import com.xar.cli.CommandLineParser;
import com.xar.cli.annotations.CommandLineArgument;
import com.xar.cli.entities.ArgumentDescriptor;
import com.xar.cli.exceptions.ArgumentException;
import com.xar.cli.interfaces.ICommandlineListener;
import com.xar.cli.interfaces.ICommandlineParser;
import com.xar.cli.util.CommandlineFunctions;
import com.xar.logquery.runtime.DataSource;
import com.xar.logquery.runtime.config.RuntimeConfiguration;
import com.xar.logquery.runtime.datasource.interfaces.IDataSource;
import com.xar.logquery.runtime.exceptions.DataSourceException;
import com.xar.logquery.runtime.output.csv.CSVOutputFormatter;
import com.xar.logquery.runtime.output.interfaces.IOutputFormatter;
import com.xar.logquery.runtime.output.text.TextOutputFormatter;
import com.xar.logquery.runtime.output.tpl.TemplateOutputFormatter;
import com.xar.logquery.runtime.util.FileFunctions;
import com.xar.logquery.runtime.util.StringUtils;
import com.xar.logquery.util.Log4JUtilities;
import org.apache.log4j.Level;
import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Log Query entry point
 */
public class Main {

    public static final String CONFIG_JSON = "config.json";

    static{
        
    }
    /**
     * The shared logger instance.
     */
    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    /**
     * Registered output formats
     */
    private static Map<String, Class<?>> outputFormats = new HashMap<>();

    /**
     * Inoput file name
     */
    @CommandLineArgument(name = "f", description = "Input file name", required = false, argumentName = "fileName")
    private String file;

    /**
     * Output format
     */
    @CommandLineArgument(name = "o", description = "Output format name", required = false, argumentName = "formatName")
    private String outputFormat;

    /**
     * The configuration file.
     */
    @CommandLineArgument(name = "c", longName = "config", description = "Configuration file to use", required = false)
    private String configurationFile = CONFIG_JSON;

    /**
     * Should output be quiet
     */
    @CommandLineArgument(name = "q", description = "Quiet", required = false)
    private boolean quiet;


    /**
     * Output formatter instance
     */
    private IOutputFormatter m_outputFormatter;

    /**
     * Query parameter
     */
    private String m_query;

    /**
     * Loaded configuration
     */
    private Configuration m_configuration;


    /**
     * Sets the query.
     *
     * @param p_query the query
     */
    public void setQuery(final String p_query) {

        logger.debug("setQuery(\"" + p_query.replace("\n", "\\n") + "\")");
        this.m_query = p_query;
    }

    private IDataSource getDatasource() throws IOException, DataSourceException {

        Reader reader = null;
        if (this.file != null) {
            File f = new File(file);
            if (f.isAbsolute()) { reader = new FileReader(this.file); }
            else { reader = new FileReader(new File(System.getProperty("user.dir"), this.file)); }
        }
        else if (!CommandlineFunctions.isNullOrEmpty(this.m_query)) {
            reader = new StringReader(this.m_query);
        }
        return DataSource.parse(reader);
    }

    /**
     * Start execution
     *
     * @throws IOException
     */
    private void go() throws IOException {

        // We now need to load the configuration.
        m_configuration = loadConfiguration(configurationFile);


        try {
            IDataSource dataSource = getDatasource();
            this.m_outputFormatter.format(dataSource);
        }
        catch (DataSourceException e) {
            e.printStackTrace();
        }

    }

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     *
     * @throws IOException the iO exception
     */
    public static void main(String[] args) throws IOException {

        if (!Log4JUtilities.isConfigured()){
            Log4JUtilities.configure(Level.OFF);
        }

        registerOutputFormatters();


        final Main program = new Main();

        final CommandLineParser parser = new CommandLineParser();
        parser.addListener(new OutputFormatListener(program, parser));
        parser.addArguments(program);

        try {
            StringBuilder query = new StringBuilder();
            List<String> argList = new ArrayList<>(Arrays.asList(args));
            if (!(argList.contains("-o") || argList.contains("--outputFormat"))) {
                argList.add(0, "-o");
                argList.add(1, "text");
            }
            List<String> unknown = parser.parse(argList);

            for (String arg : unknown) {
                if (arg.startsWith("-")) {
                    throw new ArgumentException("Unrecognised argument '" + arg + "'");
                }
                query.append(arg).append(' ');
            }
            if (query.length() > 0) { program.setQuery(query.toString().trim()); }

            if (parser.isHelpSelected()) {
                parser.showHelp(System.out, 80);
                System.exit(0);
            }
            if (StringUtils.isEmpty(program.file) && StringUtils.isEmpty(program.m_query)) {
                System.err.println("No Query specified!");
                parser.showHelp(System.out, 80);
                System.exit(0);
            }
        }
        catch (ArgumentException e) {
            if (!parser.isHelpSelected()) { System.err.println(e.getMessage()); }

            parser.showHelp(System.out, 80);
        }
        long _start = System.currentTimeMillis();
        program.go();
//        System.out.println(System.currentTimeMillis() - _start);

    }

    /**
     * Get the path in which the executing jar/class lies
     *
     * @return the path in which the executing jar/class lies
     */
    public static File getExecutionPath() {

        String path = System.getProperty("log-query.executionPath");

        if (StringUtils.isEmpty(path)) {
            File executionPath = new File(Main.class.getProtectionDomain().getCodeSource().getLocation().getPath());
            if (executionPath.isFile()) {
                executionPath = executionPath.getParentFile();
            }
            return executionPath;
        }
        return new File(path);
    }
    
    private static Configuration loadConfiguration(String configurationFile) throws IOException {

        // Check the config property
        if (StringUtils.isEmpty(configurationFile)) {
            configurationFile = CONFIG_JSON;
        }

        File config = new File(configurationFile);
        if (!config.exists()) {
            config = new File(new File(getExecutionPath().getParentFile(), "config"), configurationFile);
        }
        Configuration configuration;
        if (config.exists()) {
            try (Reader reader = new FileReader(config)) {
                Gson gson = new Gson();
                configuration = gson.fromJson(reader, Configuration.class);
            }
        }
        else {
            logger.debug("Create config: " + config);
            config.getParentFile().mkdirs();
            configuration = new Configuration();
            try (Writer writer = new FileWriter(config)) {
                new Gson().toJson(configuration, writer);
            }
        }

        // Now apply the configuration to the runtime config
        RuntimeConfiguration.setDateFormat(configuration.dateFormat);
        RuntimeConfiguration.setTimestampFormat(configuration.timeStampFormat);
        RuntimeConfiguration.setDateFormats(configuration.dateFormats);


        return configuration;
    }

    /**
     * Loads an instance of the specified formatter
     *
     * @param p_formatterName the formatter name
     *
     * @return an instance of the specified formatter
     *
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    private static IOutputFormatter loadFormatter(final String p_formatterName) throws IllegalAccessException, InstantiationException {

        return (IOutputFormatter) outputFormats.get(p_formatterName.toUpperCase()).newInstance();
    }

    /**
     * Registers the output formatters
     */
    private static void registerOutputFormatters() {

        outputFormats.put("CSV", CSVOutputFormatter.class);
        outputFormats.put("TEXT", TextOutputFormatter.class);
        outputFormats.put("TPL", TemplateOutputFormatter.class);

    }

    /**
     * Listener to respond to the output formatter parameter being processed
     */
    private static class OutputFormatListener implements ICommandlineListener {

        private final Main m_main;

        private final CommandLineParser m_parser;

        public OutputFormatListener(final Main p_main, final CommandLineParser p_parser) {

            m_main = p_main;
            m_parser = p_parser;
        }

        @Override
        public void processedArgument(final ICommandlineParser p_parser, final ArgumentDescriptor p_argument) throws ArgumentException {

            if (p_argument.getName().equals("o")) {
                try {
                    m_main.m_outputFormatter = loadFormatter(m_main.outputFormat);
                }
                catch (Exception e) {
                    throw new ArgumentException("Error loading formatter '" + m_main.outputFormat + "'", e);
                }
                m_parser.addArguments(m_main.m_outputFormatter);
            }
        }
    }
}
