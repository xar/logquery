/*
* Copyright 2014 Anton Koekemoer
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.xar.logquery.main.test;

import com.xar.logquery.Main;
import com.xar.logquery.runtime.util.FileFunctions;
import com.xar.logquery.runtime.util.StringUtils;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

/**
* Created by Anton Koekemoer on 2014/12/08.
*/
public class TestExamples {


    private static File templatePath;
    private static File examplesPath;

    private static String userDir;

    public void compareFiles(File expected, File actual) throws IOException {


        BufferedReader reader1 = new BufferedReader(new FileReader(expected));
        BufferedReader reader2 = new BufferedReader(new FileReader(actual));

        int flag = 1;
        while (true) {
            String line1 = reader1.readLine();
            String line2 = reader2.readLine();

            if (line1 == null && line2 == null) { break; }

            // We also trim the excess spaces from the EOL for consistency
            if (line1 != null) line1 = StringUtils.rtrim(line1);
            if (line2 != null) line2 = StringUtils.rtrim(line2);
            Assert.assertEquals(line1, line1, line2);
        }
    }

    @Test
    public void examples() throws IOException, TemplateException {

        System.setProperty("user.dir", examplesPath.getCanonicalPath());
        File[] files = examplesPath.listFiles();

        for (File file : files) {
            if (file.getName().endsWith(".args")) {
                executeExample(file.getCanonicalPath());
            }
        }
    }

    public void executeExample(String p_example) throws IOException, TemplateException {

        System.out.println(p_example);

        File path = new File(p_example);
        String exampleName = FileFunctions.getNameWithoutExtension(path.getName());



        File actual = new File(exampleName + ".actual");
        File expected = new File(exampleName + ".expected");

        // We redirect system.out to a text file
        PrintStream out = System.out;
        PrintStream printStream = new PrintStream(new FileOutputStream(actual));
        System.setOut(printStream);

        // If we have a text template for the example, we will do a text execute and compare the two
        try {

            Main.main(new String[]{"@" + p_example});
            expandTemplate(exampleName + ".tpl", expected);

            System.setOut(out);

            printStream.flush();
            printStream.close();

            compareFiles(expected, actual);

            expected.delete();
            actual.delete();


        }
        finally {
            System.setOut(out);
        }

    }

    public void expandTemplate(String p_templateName, final File p_outputName) throws IOException, TemplateException {
        // Create your Configuration instance, and specify if up to what FreeMarker
        // version (here 2.3.21) do you want to apply the fixes that are not 100%
        // backward-compatible. See the Configuration JavaDoc for details.
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_21);

        // Specify the source where the template files come from. Here I set a
        // plain directory for it, but non-file-system sources are possible too:
        cfg.setDirectoryForTemplateLoading(templatePath);

        // Set the preferred charset template files are stored in. UTF-8 is
        // a good choice in most applications:
        cfg.setDefaultEncoding("UTF-8");

        // Sets how errors will appear.
        // During development TemplateExceptionHandler.HTML_DEBUG_HANDLER is better.
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);

        // Create the root hash
        Map root = new HashMap();
        root.put("exampleFolder", examplesPath.getCanonicalPath());

        Template tmp = cfg.getTemplate(p_templateName);
        tmp.process(root, new FileWriter(p_outputName));
    }

    public static File findTemplates() throws IOException {

        File userPath = new File(".").getCanonicalFile();
        File examples = new File(userPath, "log-query/src/test/examples").getCanonicalFile();
        while (!examples.exists()) {
            userPath = userPath.getParentFile();
            examples = new File(userPath, "log-query/src/test/examples").getCanonicalFile();
        }

        if (examples.exists()) {
            examples = examples.getCanonicalFile();
        }
        else {
            examples = null;
        }
        return examples;
    }

    public static File findExamples() throws IOException {

        File examples = new File("package/examples");

        if (!examples.exists()) {
            examples = new File("log-query/package/examples");
        }

        if (examples.exists()) {
            examples = examples.getCanonicalFile();
        }
        else {
            examples = null;
        }
        return examples;
    }

    @BeforeClass
    public static void testFixtureSetup() throws IOException {

        File logFile = new File("log4j-test.properties");
        if (logFile.exists()) {
            System.setProperty("log4j.properties", "log4j-test.properties");
        }
        else {
            System.setProperty("log4j.properties", "../log4j-test.properties");
        }
        templatePath = findTemplates();
        examplesPath = findExamples();

        userDir = System.getProperty("user.dir");
        System.setProperty("user.dir", examplesPath.getAbsolutePath());
        System.out.println(System.getProperty("user.dir"));

    }

    @AfterClass
    public static void testFixtureTeardown(){
        System.setProperty("user.dir", userDir);
    }

}
