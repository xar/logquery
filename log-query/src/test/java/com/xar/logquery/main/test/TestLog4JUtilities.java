/*
 * Copyright 2015 Anton Koekemoer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xar.logquery.main.test;

import com.xar.logquery.util.Log4JUtilities;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.varia.NullAppender;
import org.apache.log4j.xml.Log4jEntityResolver;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by akoekemo on 4/15/15.
 */
public class TestLog4JUtilities {

    @Test
    public void shouldReturnTrueIfRootLoggerHasAppenders() {

        try {
            Logger.getRootLogger().addAppender(new NullAppender());
            Assert.assertTrue(Log4JUtilities.isConfigured());
        }
        finally {
            LogManager.resetConfiguration();
        }

    }

    @Test
    public void shouldReturnTrueIfLogManagerContainsLoggersWithAppenders() {

        Assert.assertFalse(Log4JUtilities.isConfigured());
        try{
            BasicConfigurator.configure();
            Assert.assertTrue(Log4JUtilities.isConfigured());
        }
        finally{
            LogManager.resetConfiguration();
            Assert.assertFalse(Log4JUtilities.isConfigured());
        }
    }


}
